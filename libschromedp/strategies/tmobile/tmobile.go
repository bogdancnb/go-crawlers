package tmobile

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"math/rand"
	"strings"
	"time"
)

const (
	logowanie = "https://www.t-mobile.pl/logowanie"
	mojeKonto = "https://www.t-mobile.pl/moje-konto"
	payments  = `https://miboa.t-mobile.pl/scnew/index.html#/payments`

	loginErrMsg = `#my-acc-login > div > div > div.path-form__error`
	passErrMsg1 = "Trzykrotnie wpisano błędne hasło lub numer telefonu" //An incorrect password or telephone number was entered three times
	passErrMsg2 = "Niepoprawne hasło"

	maxPaymentsRetries = 3

	siteFormat   = "02.01.2006"
	downloadPath = "/var/log/pago/pago-tmobile-crawler-go/downloads"

	pdfErrMsg = `Przepraszamy, przygotowanie pliku do pobrania trwa dłużej niż zwykle. Prosimy spróbować ponownie.`
)

var (
	BankName        = "mBank SA"
	BankBic         = " BREXPLPW"
	LegalEntityName = "T-MOBILE POLSKA S.A."
)

func Init() {
	factory := new(TMobileFactory)
	crawl.RegisterFactory("tmobile.crawler", factory)
}

type TMobileFactory struct{}

func (p *TMobileFactory) NewStrategy(log *logrus.Entry) crawl.Strategy {
	l := log.WithField("type", "TMobileStrategy")
	return &TMobileStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(log.WithField("type", "ChromeDpCrawler"), true),
	}
}

type TMobileStrategy struct {
	*logrus.Entry
	*browser.ChromeDpCrawler
	mainPage, paymentsPage string
}

func (s *TMobileStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}
func (s *TMobileStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) Login(a *model.Account) (*string, crawl.Exception) {
	err := s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(logowanie),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(logowanie + ": " + err.Error())
	}

	err = s.Run(chromedp.WaitVisible(`field-1`, chromedp.ByID))
	if err != nil {
		return nil, crawl.ParseErr("username input: " + err.Error())
	}
	err = s.Run(chromedp.SendKeys(`field-1`, a.Username, chromedp.ByID))
	if err != nil {
		return nil, crawl.ParseErr("set username input: " + err.Error())
	}
	err = s.Run(
		chromedp.Sleep(time.Duration(1000+rand.Intn(2000))*time.Millisecond),
		chromedp.Click(`login`, chromedp.ByID))
	if err != nil {
		return nil, crawl.ParseErr("click username confirm: " + err.Error())
	}

	errMsg := ""
	err = s.RunWithTimeout(5*time.Second,
		chromedp.WaitVisible(loginErrMsg),
		chromedp.Text(loginErrMsg, &errMsg),
	)
	if err != browser.ErrTimeout {
		if err == nil && errMsg != "" {
			return nil, crawl.LoginErr(errMsg)
		} else {
			return nil, crawl.ConnectionErr("waiting for login error msg")
		}
	}

	iframeUrl := ""
	iframeOk := false
	err = s.Run(chromedp.AttributeValue(".sso-login-box", "src", &iframeUrl, &iframeOk))
	if err != nil {
		return nil, crawl.ParseErr("iframe url: " + err.Error())
	}
	if !iframeOk || iframeUrl == "" {
		return nil, crawl.ParseErr("iframe url: " + err.Error())
	}
	err = s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(iframeUrl),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(iframeUrl + ": " + err.Error())
	}

	err = s.Run(chromedp.WaitVisible(`#field-password`, chromedp.ByID))
	if err != nil {
		return nil, crawl.ParseErr("wait password input: " + err.Error())
	}
	err = s.Run(chromedp.SetValue(`#field-password`, *a.Password, chromedp.ByID))
	if err != nil {
		return nil, crawl.ParseErr("set password input: " + err.Error())
	}
	err = s.Run(
		chromedp.Click(`#submit-number`, chromedp.ByID),
		chromedp.Sleep(3*time.Second),
	)
	if err != nil {
		return nil, crawl.ParseErr("click submit-number: " + err.Error())
	}

	passErrMsg := ""
	err = s.Run(chromedp.OuterHTML("html", &passErrMsg))
	if err != nil {
		return nil, crawl.ParseErr("parsing pass err msg: " + err.Error())
	}
	if strings.Contains(passErrMsg, passErrMsg1) {
		return nil, crawl.LoginErr(passErrMsg1)
	}
	if strings.Contains(passErrMsg, passErrMsg2) {
		return nil, crawl.LoginErr(passErrMsg2)
	}
	return nil, nil
}

func (s *TMobileStrategy) CheckLogin(response *string) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) LoadLocations(account *model.Account) crawl.Exception {
	defer func() {
		s.ChromeDpCrawler.CancelFuncs()
	}()
	if ex := s.parsePages(); ex != nil {
		return ex
	}
	s.logout()

	mainDoc, err := goquery.NewDocumentFromReader(strings.NewReader(s.mainPage))
	if err != nil {
		return crawl.ParseErr("parse doc for main page:" + err.Error())
	}
	if mainDoc == nil {
		return crawl.ParseErr("parse doc for main page")
	}
	ident := mainDoc.Find(`div[id='selectedMsisdn']`)
	if ident.Length() == 0 {
		return crawl.ParseErr("div[id='selectedMsisdn'] not found")
	}
	payDoc, err := goquery.NewDocumentFromReader(strings.NewReader(s.paymentsPage))
	if err != nil {
		return crawl.ParseErr("parse payments html: " + err.Error())
	}
	if payDoc == nil {
		return crawl.ParseErr("parse doc for payments page")
	}
	ibanEl := payDoc.Find(`[copy-to-clipboard]:not([copy-to-clipboard=''])`)
	if ibanEl.Length() == 0 {
		return crawl.ParseErr("could not find iban element")
	}
	iban, ex := crawl.Attr(ibanEl, `copy-to-clipboard`)
	if ex != nil {
		return ex
	}
	mainLoc := &model.Location{
		Service:    strings.TrimSpace(ident.Text()),
		Identifier: strings.TrimSpace(ident.Text()),
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &BankName,
			BankBIC:         &BankBic,
			LegalEntityName: &LegalEntityName,
		},
	}
	account.AddLocation(mainLoc)
	locsEl := mainDoc.Find(`li[class^='item slick']`)
	for i := 0; i < locsEl.Length(); i++ {
		selection := locsEl.Eq(i)
		if selection == nil {
			continue
		}
		selection = selection.Find(`p[class='tel-num']`)
		if selection == nil {
			continue
		}
		loc := strings.TrimSpace(selection.Text())
		if loc != mainLoc.Identifier {
			l := &model.Location{
				Service:    strings.TrimSpace(loc),
				Identifier: strings.TrimSpace(loc),
			}
			account.AddLocation(l)
		}
	}

	if ex := s.parseInvoicesInternal(account, mainLoc, payDoc); ex != nil {
		return ex
	}
	return nil
}

func (s *TMobileStrategy) parsePages() crawl.Exception {
	btnManageOrSubmitFormSel := `a[class='button manage'],a[onclick="submitForm();"]`
	btnManageSel := `a[class='button manage']`
	submitFormSel := `a[onclick="submitForm();"]`
	loginOrPaymentsHrefSel := `a[href='http://www.miboa.t-mobile.pl'],a[href='#/payments']`
	loginSel := `a[href='http://www.miboa.t-mobile.pl']`
	paymentsHrefSel := `a[href='#/payments']`

	s.Debug("waiting: ", btnManageOrSubmitFormSel)
	err := s.Run(
		chromedp.WaitVisible(btnManageOrSubmitFormSel),
		chromedp.OuterHTML("html", &s.mainPage))
	if err != nil {
		return crawl.ParseErr("parsing html for main page: " + err.Error())
	}
	s.Debug("found: ", btnManageOrSubmitFormSel)

	err = s.RunWithTimeout(time.Second*5, chromedp.WaitVisible(btnManageSel))
	if err == browser.ErrTimeout {
		s.Debug("not found: ", btnManageSel)
		s.Debug("waiting: ", submitFormSel)
		err = s.RunWithTimeout(time.Second*5, chromedp.WaitVisible(submitFormSel))
		if err != nil {
			s.Warn("not found: ", submitFormSel)
			s.LogHTML()
			return crawl.ConnectionErr("not found: " + submitFormSel + " : " + err.Error())
		}
		s.Debug("found: ", submitFormSel)

		err = s.RunWithTimeout(time.Minute,
			chromedp.Click(submitFormSel),
			chromedp.WaitVisible(loginOrPaymentsHrefSel),
		)
		if err != nil {
			s.Warn("not found: ", loginOrPaymentsHrefSel)
			s.LogHTML()
			return crawl.ConnectionErr("not found: " + loginOrPaymentsHrefSel + " : " + err.Error())
		}
	} else {
		s.Debug("found: ", btnManageSel)
		err = s.RunWithTimeout(time.Minute,
			chromedp.WaitVisible(btnManageSel),
			chromedp.Click(btnManageSel),
			chromedp.WaitVisible(loginOrPaymentsHrefSel),
		)
		if err != nil {
			s.Warn("not found: ", loginOrPaymentsHrefSel)
			s.LogHTML()
			return crawl.ConnectionErr("not found: " + loginOrPaymentsHrefSel + " : " + err.Error())
		}
	}

	err = s.RunWithTimeout(time.Second, chromedp.WaitVisible(loginSel))
	if err == nil {
		i := 0
		for i < maxPaymentsRetries {
			err = s.Run(
				chromedp.Click(loginSel),
				chromedp.WaitVisible(loginOrPaymentsHrefSel),
			)
			if err != nil {
				return crawl.ConnectionErr("payments page click: " + err.Error())
			}
			err = s.RunWithTimeout(time.Second, chromedp.WaitVisible(paymentsHrefSel))
			if err == nil {
				break
			}
			i++
		}
		if i >= maxPaymentsRetries {
			return crawl.ConnectionErr(fmt.Sprintf(`payments page not reached after %d retries`, maxPaymentsRetries))
		}
	}

	err = s.Run(
		chromedp.WaitVisible(paymentsHrefSel),
		chromedp.Click(paymentsHrefSel),
		chromedp.WaitVisible(`[copy-to-clipboard]:not([copy-to-clipboard=''])`),
	)
	if err != nil {
		return crawl.ConnectionErr("payments page click: " + err.Error())
	}

	err = s.Run(chromedp.OuterHTML("html", &s.paymentsPage))
	if err != nil {
		return crawl.ParseErr("parsing html for payments page: " + err.Error())
	}

	return nil
}

func (s *TMobileStrategy) logout() {
	err := s.RunWithTimeout(2*time.Second,
		chromedp.Click(`a[ng-click='logout()']`),
		chromedp.WaitVisible(`a[href='http://www.miboa.t-mobile.pl']`))
	if err != nil {
		s.Warn("click logout: " + err.Error())
	} else {
		s.Debug("logged out")
	}
}

func (s *TMobileStrategy) parseInvoicesInternal(account *model.Account, l *model.Location, payDoc *goquery.Document) crawl.Exception {
	unpaidEs := payDoc.Find(`h2:containsOwn('Nieopłacone faktury miesięczne')`)
	if unpaidEs.Length() > 0 && unpaidEs.Next().Length() > 0 {
		rows := unpaidEs.Next().Find(`table tbody tr`)
		for i := 0; i < rows.Length(); i++ {
			if ex := s.parseInvoice(l, rows.Eq(i), false); ex != nil {
				return ex
			}
		}
	}
	paidEls := payDoc.Find(`h2:containsOwn('Opłacone faktury miesięczne')`)
	if paidEls.Length() > 0 && paidEls.Next().Length() > 0 {
		rows := paidEls.Next().Find(`table tbody tr`)
		for i := 0; i < rows.Length(); i++ {
			if ex := s.parseInvoice(l, rows.Eq(i), true); ex != nil {
				return ex
			}
		}
	}
	return nil
}

func (s *TMobileStrategy) parseInvoice(l *model.Location, row *goquery.Selection, paid bool) crawl.Exception {
	cols := row.Find(`td`)
	ref := strings.TrimSpace(cols.Eq(2).Text())
	refP := strings.Split(ref, " ")
	ref = refP[len(refP)-1]
	for _, invoice := range l.Invoices {
		if ref == invoice.Ref {
			return nil
		}
	}
	issueS := strings.TrimSpace(cols.Eq(0).Text())
	issueP := strings.Split(issueS, " ")
	issueS = strings.TrimSpace(issueP[len(issueP)-1])
	issueDate, err := time.Parse(siteFormat, issueS)
	if err != nil {
		return crawl.ParseErr(fmt.Sprintf(`parse issue date %s: %s`, issueS, err.Error()))
	}
	dueS := strings.TrimSpace(cols.Eq(1).Text())
	dueP := strings.Split(dueS, " ")
	dueS = strings.TrimSpace(dueP[len(dueP)-1])
	dueDate, err := time.Parse(siteFormat, dueS)
	if err != nil {
		return crawl.ParseErr(fmt.Sprintf(`parse issue date %s: %s`, dueS, err.Error()))
	}
	amtAEl := cols.Eq(3).Find("a")
	amtS := strings.TrimSpace(amtAEl.Text())
	amtP := strings.Split(amtS, "\n")
	amtS = amtP[0]
	amt, ex := crawl.AmountFromString(amtS)
	if ex != nil {
		return ex
	}
	amtDue := 0.0
	if !paid {
		amtDue = amt
	}
	i := &model.Invoice{
		Ref:       ref,
		PdfUri:    &ref,
		Amount:    amt,
		AmountDue: amtDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(i)
	return nil
}

func (s *TMobileStrategy) LoadAmount(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) LoadInvoices(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) LoadPayments(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *TMobileStrategy) Pdf(a *model.Account) *model.PdfResponse {
	defer func() { s.ChromeDpCrawler.CancelFuncs() }()

	ref := a.Locations[0].Invoices[0].Ref

	pdf, err := s.CheckDownloads(downloadPath, ref, s.Entry)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	if len(pdf) > 0 {
		return &model.PdfResponse{
			Content:   pdf,
			PdfStatus: model.OK.Name,
		}
	}

	if ex := s.parsePages(); ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	err = s.Run(page.SetDownloadBehavior(page.SetDownloadBehaviorBehaviorAllow).WithDownloadPath(downloadPath))
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}

	linkSel := fmt.Sprintf(`a[reference-id='%s']`, ref)
	err = s.Run(
		chromedp.WaitVisible(linkSel),
		chromedp.Sleep(1*time.Second),
		chromedp.Click(linkSel),
		chromedp.Sleep(5*time.Second),
	)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}

	s.logout()

	pdf, err = s.CheckDownloads(downloadPath, ref, s.Entry)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	if len(pdf) > 0 {
		return &model.PdfResponse{
			Content:   pdf,
			PdfStatus: model.OK.Name,
		}
	} else {
		return model.PdfErrorResponse("could not download file", model.OTHER_EXCEPTION)
	}
}

func (s *TMobileStrategy) LoadsInternal() bool {
	return true
}

func (s *TMobileStrategy) Close(a *model.Account) {
	s.ChromeDpCrawler.CancelFuncs()
}

func (s *TMobileStrategy) SendConnectionError() {
}

func (s *TMobileStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *TMobileStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
