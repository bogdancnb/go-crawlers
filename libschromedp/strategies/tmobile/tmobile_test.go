package tmobile

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "tmobile.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"sadorobert@gmail.com", "Siarra33"},
		//{"lukascpu@gmail.com", "29@hU3YisLWh"},
		//{"norbert.koczara@gmail.com", "wkikAuT2f4"},
		//{"zuba.daniel@gmail.com", "lQ26f#RI%MgX"},

		//ParseException : parsing html for main page
		{"piasqnik@gmail.com", "NOtojou600"},
		//{"tomek.dudo@gmail.com", "Tomek123"},
		//{"karolka65@interia.pl", "Kochanie17"},
		//{"wojtek.krol@gmail.com", "Robot97:)"},
		//{"sebastian.szmyjda@gmail.com", "Balonek123"},
		//{"jakub.szopa@hotmail.com", "Roki0577"},
		//{"gumis635@gmail.com", "Kyokushin1"},
		//{"krzys1101@gmail.com", "Bombaj2001"},
		//{"komancza997@o2.pl", "Komancza211"},
		//{"mrmarekrogmr6@wp.pl", "IrAmidaMg6"},
	}
	for _, test := range tests {
		if err := strategies.AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*strategies.PdfRequest{
		{
			Username: "sadorobert@gmail.com", Password: "Siarra33", Uri: "tmobile.crawler",
			Identifier: "506124679",
			Ref:        "503102611220", PdfUri: "503102611220",
		},
	}
	for _, test := range tests {
		pdf := strategies.PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
