package play24

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"regexp"
	"time"
)

const (
	ACCESS_URL         = "https://24.play.pl/Play24/Welcome"
	NUMBER_LIST_URL    = "https://konto.play.pl/user-gui/myAccountPage/numberList.json?_="
	LOGIN_URL          = "https://logowanie.play.pl/opensso/logowanie"
	LOCATION_URL       = "https://konto.play.pl/user-gui/myAccountPage"
	TOKEN_URL          = "https://24.play.pl/Play24/CsrfJavaScriptServlet"
	BILLS_URL          = "https://24.play.pl/Play24/dwr/call/plaincall/templateRemoteService.sortData.dwr"
	SWITCH_CONTEXT_URL = "https://24.play.pl/Play24/dwr/call/plaincall/accountContextChangeService.changeAccountContext.dwr"
	UNPAID             = "Wszystkie Twoje faktury są opłacone"
	NO_PAYMENTS_MSG    = "Brak płatności dotyczących Twojego konta"

	BANK_NAME         = "DNB Bank Polska SA"
	LEGAL_ENTITY_NAME = "P4 Sp. z o.o."
	BANK_BIC          = "BIGBPLPW"
	USER_MSG1         = "//*[contains(text(),'Moje konto')]"
	USER_MSG2         = "//*[contains(text(),'Mój numer')]"
	USER_MSG          = "//*[contains(text(),'Moje konto')] | //*[contains(text(),'Mój numer')]"

	LOGIN_ERROR_MSG     = "//*[contains(text(),'Błędne hasło lub nazwa użytkownika')]"
	ALL_LOGIN_MSG       = "//*[contains(text(),'Moje konto')] | //*[contains(text(),'Mój numer')] | //*[contains(text(),'Błędne hasło lub nazwa użytkownika')]"
	INVOICE_LIST        = "//*[contains(text(),'Wykaz faktur')]"
	NUMBER_LIST         = "//*[contains(text(),'numberManagementListDto')]"
	loginErrMsg         = "login error"
	loginRespTimeoutMsg = "login response timed out after 60s"
)

func init() {
	factory := new(Play24Factory)
	crawl.RegisterFactory("play.crawler", factory)
}

type Play24Factory struct{}

func (p *Play24Factory) NewStrategy(log *logrus.Entry) crawl.Strategy {
	l := log.WithField("type", "Play24BrowserStrategy")
	return &Play24BrowserStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(log.WithField("type", "ChromeDpCrawler"), true),
	}
}

type Play24BrowserStrategy struct {
	*logrus.Entry
	*browser.ChromeDpCrawler
	nrs numbersList
}

func (s *Play24BrowserStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *Play24BrowserStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) crawl.Exception {
	return nil
}

func (s *Play24BrowserStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) crawl.Exception {
	return nil
}

type numbersList struct {
	NumberManagementListDto []numberManagement `json:"numberManagementListDto"`
}

type numberManagement struct {
	Msisdn string `json:"msisdn"`
	Active bool   `json:"active"`
}

func (s *Play24BrowserStrategy) Login(acc *model.Account) (*string, crawl.Exception) {
	err := s.Run(
		chromedp.Navigate(ACCESS_URL),
		chromedp.WaitVisible(`//button[@name="Login.Submit"]`),
		chromedp.SetValue(`#IDToken1`, acc.Username, chromedp.ByID),
		chromedp.SetValue(`#IDToken2`, *acc.Password, chromedp.ByID),
		chromedp.Click(`//button[@name="Login.Submit"]`),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(err.Error())
	}
	return nil, nil
}

func (s *Play24BrowserStrategy) CheckLogin(response *string) crawl.Exception {
	var ex crawl.Exception
	err := s.Run(
		chromedp.WaitVisible(ALL_LOGIN_MSG, chromedp.BySearch),
		chromedp.ActionFunc(func(ctx context.Context) error {
			s.Debug("got login response")
			foundResp := make(chan struct{})
			go func() {
				if err := chromedp.Run(ctx, chromedp.WaitVisible(LOGIN_ERROR_MSG, chromedp.BySearch)); err == nil {
					s.Error(loginErrMsg)
					ex = crawl.LoginErr(loginErrMsg)
					close(foundResp)
				}
			}()
			go func() {
				if err := chromedp.Run(ctx, chromedp.WaitVisible(USER_MSG2, chromedp.BySearch)); err == nil {
					s.Debug("logged in, account type: ", USER_MSG2)
					close(foundResp)
				}
			}()
			go func() {
				if err := chromedp.Run(ctx, chromedp.WaitVisible(USER_MSG1, chromedp.BySearch)); err == nil {
					s.Debug("logged in, account type: ", USER_MSG1)
					close(foundResp)
				}
			}()
			select {
			case <-time.After(60 * time.Second):
				s.Error(loginRespTimeoutMsg)
				return crawl.ConnectionErr(loginRespTimeoutMsg)
			case <-foundResp:
				return nil
			}
		}),
	)
	if err != nil {
		return crawl.ConnectionErr(err.Error())
	}
	return ex
}

func (s *Play24BrowserStrategy) LoadLocations(account *model.Account) crawl.Exception {
	if ex := s.parseNumbers(); ex != nil {
		return ex
	}
	if len(s.nrs.NumberManagementListDto) == 0 {
		s.Warn("empty numbers list")
		return nil
	}
	seen := make(map[string]bool)
	if err := s.Run(chromedp.Navigate(ACCESS_URL), chromedp.WaitVisible(USER_MSG, chromedp.BySearch)); err != nil {
		return crawl.ConnectionErr(err.Error())
	}
	if ex := s.parseLocation(seen); ex != nil {
		return ex
	}
	if len(seen) == len(s.nrs.NumberManagementListDto) {
		s.Debug("all locations parsed")
		return nil
	}
	for {
		remaining, ex := s.nextNotSeen(seen)
		if ex != nil {
			return ex
		}
		if !remaining {
			return nil
		}
		if ex := s.parseLocation(seen); ex != nil {
			return ex
		}
		if len(seen) == len(s.nrs.NumberManagementListDto) {
			s.Debug("all locations parsed")
			return nil
		}
	}
}

func (s *Play24BrowserStrategy) parseLocation(seen map[string]bool) crawl.Exception {
	currLoc, ex := s.selectedLocation()
	if ex != nil {
		return ex
	}
	s.Debug("parsing location ", currLoc)
	//todo
	//seen[currLoc] = true
	return nil
}

func (s *Play24BrowserStrategy) nextNotSeen(seen map[string]bool) (bool, crawl.Exception) {
	var clickPath string
	err := s.Run(
		chromedp.Click(`//div[@class="header__user-section__current-number__formattedMsisdn"]`),
		chromedp.WaitVisible(`#changeNumberDropdownTable`, chromedp.ByID),
		chromedp.QueryAfter(`#changeNumberDropdownTable > li > a:not(.selected)`, func(ctx context.Context, nodes ...*cdp.Node) error {
			if len(nodes) == 0 {
				//todo check this for single location
				return errors.New("no location options found")
			}
			for i := range nodes {
				j := i
				s.Trace(nodes[j].FullXPath())
				path := nodes[j].FullXPath() + `/input[1]`
				var text string
				var ok bool
				if err := s.Run(chromedp.AttributeValue(path, "value", &text, &ok)); err != nil {
					return err
				}
				if !ok || text == "" {
					return errors.New("could not parse value for: " + path)
				}
				s.Trace(text)
				//if !seen[text] {
				//	seen[text] = true
				//	clickPath = nodes[j].FullXPath()
				//	identifier = text
				//	break
				//}
			}
			return nil
		}, chromedp.ByQueryAll),
	)
	if err != nil {
		return false, crawl.ConnectionErr(err.Error())
	}
	if clickPath == "" {
		s.Debug("all locations visited")
		return false, nil
	}
	err = s.Run(
		chromedp.Click(clickPath),
		//todo replace with text^
		chromedp.WaitVisible(USER_MSG, chromedp.BySearch),
	)
	if err != nil {
		return false, crawl.ConnectionErr(err.Error())
	}
	return true, nil
}

func (s *Play24BrowserStrategy) selectedLocation() (string, crawl.Exception) {
	var selected string
	err := s.Run(
		chromedp.Text(`//div[@class="header__user-section__current-number__formattedMsisdn"]`, &selected, chromedp.NodeVisible),
	)
	if err != nil {
		return "", crawl.ConnectionErr(err.Error())
	}
	if selected == "" {
		return "", crawl.ConnectionErr("could not extract selected location")
	}
	return regexp.MustCompile(`\s+`).ReplaceAllString(selected, ""), nil
}

func (s *Play24BrowserStrategy) parseNumbers() crawl.Exception {
	var txt string
	err := s.Run(
		chromedp.Navigate(fmt.Sprintf(`%s%d`, NUMBER_LIST_URL, utils.Millis(time.Now()))),
		chromedp.WaitVisible(NUMBER_LIST, chromedp.BySearch),
		chromedp.Text(`pre`, &txt, chromedp.ByQuery),
	)
	if err != nil {
		return crawl.ConnectionErr(err.Error())
	}
	if txt == "" {
		return crawl.ConnectionErr("account numbers not found")
	}
	err = json.Unmarshal([]byte(txt), &s.nrs)
	if err != nil {
		return crawl.ParseErr("could not parse nrs list")
	}
	return nil
}

func (s *Play24BrowserStrategy) LoadAmount(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *Play24BrowserStrategy) LoadInvoices(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *Play24BrowserStrategy) LoadPayments(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *Play24BrowserStrategy) Pdf(a *model.Account) *model.PdfResponse {
	return nil
}

func (s *Play24BrowserStrategy) LoadsInternal() bool {
	return true
}

func (s *Play24BrowserStrategy) Close(a *model.Account) {
	s.ChromeDpCrawler.CancelFuncs()
}

func (s *Play24BrowserStrategy) SendConnectionError() {
}

func (s *Play24BrowserStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *Play24BrowserStrategy) SetExistingLocations(existingLocations []*model.Location) {
}

//
