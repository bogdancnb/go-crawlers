package polaris

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "polaris.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"silviu@enova-group.biz", "Rov371696@"}, //login err:  Email și/sau parola incorectă!
		//{"cristi@familiar.ro", "null1System!"}, //ok
		//{"danutcalin@gmail.com", "cody*1984"}, //ok
		//{"anadany7@gmail.com", "examplap14p"},
		{"Mirel.mihai@gmail.com", "kwjDMJ8XYs.cAWz"},
	}
	for _, test := range tests {
		if err := strategies.AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*strategies.PdfRequest{}
	for _, test := range tests {
		pdf := strategies.PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
