package polaris

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"math/rand"
	"strings"
	"time"
)

const (
	login = "https://my.polaris.ro/Login.aspx"

	siteFormat = `02.01.2006`
	locOptions = `select[id=cmbLocuriConsum]`
)

func Init() {
	factory := new(PolarisChromeFactory)
	crawl.RegisterFactory("polaris.crawler", factory)
}

type PolarisChromeFactory struct{}

func (p *PolarisChromeFactory) NewStrategy(log *logrus.Entry) crawl.Strategy {
	l := log.WithField("type", "PolarisChromeStrategy")
	return &PolarisChromeStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(log.WithField("type", "ChromeDpCrawler"), false),
	}
}

type PolarisChromeStrategy struct {
	*logrus.Entry
	*browser.ChromeDpCrawler
	billPage, paymentsPage string
}

func (s *PolarisChromeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PolarisChromeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) Login(account *model.Account) (*string, crawl.Exception) {
	rand.Seed(time.Now().Unix())
	err := s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(login),
		//chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
		//chromedp.WaitVisible(btnLogin),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(err.Error())
	}
	btnLogin := `input[id=btnLogin]`
	usernameInput := "input[id=txtEmail]"
	passInput := "input[id=txtParola]"
	err = s.Run(
		chromedp.WaitVisible(usernameInput),
		chromedp.SendKeys(usernameInput, account.Username),
		chromedp.WaitVisible(passInput),
		chromedp.SendKeys(passInput, *account.Password),
		chromedp.Click(btnLogin),
		//chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ParseErr("input credentials: " + err.Error())
	}

	userSel := `div[id=lblNumeUtilizator]`
	err = s.RunWithTimeout(7*time.Second, chromedp.WaitVisible(userSel))
	if err != nil {
		if err != browser.ErrTimeout {
			return nil, crawl.ConnectionErr(userSel + " : " + err.Error())
		}

		errorLoginSel := `div[class=errorMsg]`
		err = s.RunWithTimeout(2*time.Second, chromedp.WaitVisible(errorLoginSel))
		if err == nil {
			return nil, crawl.LoginErr("Email sau parola incorecte!")
		}

		return nil, crawl.ConnectionErr(errorLoginSel + " : " + err.Error())
	} else {
		s.Debug("logged in")
	}
	return nil, nil
}

func (s *PolarisChromeStrategy) CheckLogin(response *string) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) LoadLocations(account *model.Account) crawl.Exception {
	defer func() {
		s.ChromeDpCrawler.CancelFuncs()
	}()
	if ex := s.parsePages(); ex != nil {
		return ex
	}

	s.Debug("parse account ...")

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(s.billPage))
	if err != nil {
		return crawl.ParseErr("parse bills page: " + err.Error())
	}
	addrEl := doc.Find(locOptions)
	if addrEl.Length() == 0 {
		return crawl.ParseErr("not found :" + locOptions)
	}
	addr := strings.TrimSpace(addrEl.Text())
	loc := &model.Location{
		Service:    addr,
		Identifier: account.Username,
	}
	account.AddLocation(loc)

	invoices := doc.Find(`div[id=pnlSold] table tbody tr`)
	for i := 0; i < invoices.Length(); i++ {
		if ex := s.parseInvoice(loc, invoices.Eq(i)); ex != nil {
			return ex
		}
	}

	payments := doc.Find(`div[id=pnlPlati] table tbody tr`)
	for i := 0; i < payments.Length(); i++ {
		if ex := s.parsePayment(loc, payments.Eq(i)); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *PolarisChromeStrategy) parseInvoice(loc *model.Location, eq *goquery.Selection) crawl.Exception {
	tds := eq.Find("td")
	if tds.Length() <= 1 {
		return nil
	}
	ref := strings.TrimSpace(tds.Eq(1).Text())
	ref = utils.RemoveUnicodeSpaces([]byte(ref))
	issue := strings.TrimSpace(tds.Eq(2).Text())
	issueDate, err := time.Parse(siteFormat, issue)
	if err != nil {
		return crawl.ParseErr("parse issue date " + issue)
	}
	dueS := strings.TrimSpace(tds.Eq(3).Text())
	dueDate, err := time.Parse(siteFormat, dueS)
	if err != nil {
		return crawl.ParseErr("parse due date " + dueS)
	}
	amt, ex := parseAmount(tds.Eq(4))
	if ex != nil {
		return ex
	}
	amtDue, ex := parseAmount(tds.Eq(6))
	if ex != nil {
		return ex
	}
	inv := &model.Invoice{
		Ref:       ref,
		PdfUri:    nil,
		Amount:    amt,
		AmountDue: amtDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	loc.AddInvoice(inv)
	return nil
}

func parseAmount(selection *goquery.Selection) (float64, crawl.Exception) {
	ams := strings.TrimSpace(selection.Text())
	amsp := strings.Split(ams, " ")
	if len(amsp) != 2 {
		return 0.0, crawl.ParseErr("invalid amount format: " + ams)
	}
	amt, ex := crawl.AmountFromString(amsp[0])
	if ex != nil {
		return 0.0, ex
	}
	return amt, nil
}

func (s *PolarisChromeStrategy) parsePayment(loc *model.Location, eq *goquery.Selection) crawl.Exception {
	tds := eq.Find("td")
	if tds.Length() <= 1 {
		return nil
	}
	dateS := strings.TrimSpace(tds.Eq(2).Text())
	payDate, err := time.Parse(siteFormat, dateS)
	if err != nil {
		return crawl.ParseErr("parse pay date " + dateS)
	}
	amt, ex := parseAmount(tds.Eq(3))
	if ex != nil {
		return ex
	}
	ref := fmt.Sprintf(`%s_%s_%.2f`, strings.TrimSpace(tds.Eq(1).Text()), dateS, amt)
	p := &model.Payment{
		Ref:    ref,
		Amount: amt,
		Date:   payDate.Format(utils.DBDateFormat),
	}
	loc.AddPayment(p)
	return nil
}

func (s *PolarisChromeStrategy) parsePages() crawl.Exception {
	invBtn := `div[onclick="window.location.href='InvoicesAndPayments.aspx'"]`
	err := s.Run(
		chromedp.WaitVisible(invBtn),
		chromedp.Click(invBtn),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return crawl.ConnectionErr(invBtn + " : " + err.Error())
	}

	err = s.Run(chromedp.WaitVisible(locOptions))
	if err != nil {
		return crawl.ConnectionErr(locOptions + " : " + err.Error())
	}

	err = s.Run(
		chromedp.Sleep(time.Duration(3000+rand.Intn(1000))*time.Millisecond),
		chromedp.OuterHTML("html", &s.billPage))
	if err != nil {
		return crawl.ParseErr("parse html for bills page:" + err.Error())
	}

	//paymentsBtn := `a[id=ui-id-3]`
	//err = s.Run(
	//	chromedp.WaitVisible(paymentsBtn),
	//	chromedp.Click(paymentsBtn),
	//	chromedp.WaitVisible(`div[id=pnlPlati]`),
	//)
	//if err != nil {
	//	return crawl.ParseErr("click payments page: " + err.Error())
	//}
	//
	//err = s.Run(chromedp.OuterHTML("html", &s.paymentsPage))
	//if err != nil {
	//	return crawl.ParseErr("parse html for paymentsPage:" + err.Error())
	//}

	//logoutBtn := `a[href="Logout.aspx""]`
	//err = s.Run(
	//	chromedp.Click(`div[id=lblNumeUtilizator]`),
	//	chromedp.WaitVisible(logoutBtn),
	//	chromedp.Click(logoutBtn),
	//)
	//if err != nil {
	//	s.Error("logout: " + err.Error())
	//}
	return nil
}

func (s *PolarisChromeStrategy) LoadAmount(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) LoadInvoices(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) LoadPayments(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *PolarisChromeStrategy) Pdf(a *model.Account) *model.PdfResponse {
	return model.PdfErrorResponse("not available", model.OTHER_EXCEPTION)
}

func (s *PolarisChromeStrategy) LoadsInternal() bool {
	return true
}

func (s *PolarisChromeStrategy) Close(a *model.Account) {
	s.ChromeDpCrawler.CancelFuncs()
}

func (s *PolarisChromeStrategy) SendConnectionError() {
}

func (s *PolarisChromeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *PolarisChromeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
