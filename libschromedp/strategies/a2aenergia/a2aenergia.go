package a2aenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"math/rand"
	"time"
)

const (
	areaClienti = "https://www.a2aenergia.eu/area_clienti/"
	siteFormat  = `2006-01-02`
)

func Init() {
	factory := new(A2aEnergiaChromeFactory)
	crawl.RegisterFactory("a2aenergy.crawler", factory)
}

type A2aEnergiaChromeFactory struct{}

func (p *A2aEnergiaChromeFactory) NewStrategy(log *logrus.Entry) crawl.Strategy {
	l := log.WithField("type", "A2aEnergiaChromeStrategy")
	return &A2aEnergiaChromeStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(log.WithField("type", "ChromeDpCrawler"), true),
	}
}

type A2aEnergiaChromeStrategy struct {
	*logrus.Entry
	*browser.ChromeDpCrawler
	locPage, billPage string
}

func (s *A2aEnergiaChromeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *A2aEnergiaChromeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) Login(account *model.Account) (*string, crawl.Exception) {
	rand.Seed(time.Now().Unix())
	btnLoginMobile := `button[id=loginMobile]`
	err := s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(areaClienti),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
		chromedp.WaitVisible(btnLoginMobile),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(err.Error())
	}
	err = s.Run(
		chromedp.Click(btnLoginMobile),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(btnLoginMobile + ":" + err.Error())
	}
	loginBtnSel := `button[id=onLogSfdc]`
	err = s.Run(
		chromedp.WaitVisible(loginBtnSel),
		chromedp.SendKeys(`input[id=LoginForm_username]`, account.Username),
		chromedp.SendKeys(`input[id=LoginForm_password]`, *account.Password),
		chromedp.Click(loginBtnSel),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(btnLoginMobile + ":" + err.Error())
	}
	errorLoginSel := `div[id=errorLogin]`
	errMsg := ""
	err = s.RunWithTimeout(5*time.Second,
		chromedp.WaitVisible(errorLoginSel),
		chromedp.Text(errorLoginSel, &errMsg),
	)
	if err != browser.ErrTimeout {
		if err == nil && errMsg != "" {
			return nil, crawl.LoginErr(errMsg)
		} else {
			return nil, crawl.ConnectionErr("waiting for login error msg")
		}
	}

	userSel := `span[class=a2a-username_field]`
	err = s.Run(
		chromedp.Sleep(time.Duration(4000+rand.Intn(1000))*time.Millisecond),
		chromedp.WaitVisible(userSel),
		chromedp.Sleep(time.Duration(1000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(userSel + " : " + err.Error())
	}
	return nil, nil
}

func (s *A2aEnergiaChromeStrategy) CheckLogin(response *string) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) LoadLocations(account *model.Account) crawl.Exception {
	defer func() {
		s.ChromeDpCrawler.CancelFuncs()
	}()
	if ex := s.parsePages(); ex != nil {
		return ex
	}

	return nil
}

func (s *A2aEnergiaChromeStrategy) parseHtml(account *model.Account, html *string) crawl.Exception {
	if ex := s.parseLocations(account, html); ex != nil {
		return ex
	}
	if ex := s.parseInvoices(account, html); ex != nil {
		return ex
	}
	return nil
}

func (s *A2aEnergiaChromeStrategy) parseLocations(account *model.Account, html *string) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) parseInvoices(account *model.Account, html *string) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) getLocation(locations []*model.Location, contr string) *model.Location {
	for _, location := range locations {
		if contr == location.Identifier {
			return location
		}
	}
	return nil
}

func (s *A2aEnergiaChromeStrategy) parsePages() crawl.Exception {
	html := ""
	err := s.Run(chromedp.OuterHTML("html", &html))

	//locsSel := `div:containsOwn('Bollette'),div:containsOwn('Bollette')`//`a[class=icon-Bollette slds-navigation-list--vertical__action slds-text-link--reset menuItemLink]`
	locsSel := `a[class=icon-Bollette]`
	err = s.Run(chromedp.WaitVisible(locsSel),
		chromedp.Click(locsSel),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return crawl.ConnectionErr(locsSel + " : " + err.Error())
	}
	return nil
}

func (s *A2aEnergiaChromeStrategy) LoadAmount(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) LoadInvoices(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) LoadPayments(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *A2aEnergiaChromeStrategy) Pdf(a *model.Account) *model.PdfResponse {
	return model.PdfErrorResponse("not available", model.OTHER_EXCEPTION)
}

func (s *A2aEnergiaChromeStrategy) LoadsInternal() bool {
	return false
}

func (s *A2aEnergiaChromeStrategy) Close(a *model.Account) {
	s.ChromeDpCrawler.CancelFuncs()
}

func (s *A2aEnergiaChromeStrategy) SendConnectionError() {
}

func (s *A2aEnergiaChromeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *A2aEnergiaChromeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
