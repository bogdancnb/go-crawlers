package a2aenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "a2aenergy.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"simogrido@gmail.com", "Ottaviano_07"},
	}
	for _, test := range tests {
		if err := strategies.AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*strategies.PdfRequest{}
	for _, test := range tests {
		pdf := strategies.PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
