package engieitalia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"github.com/sirupsen/logrus"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "engie_italia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//*/{"event":{"descriptor":"markup://aura:clientOutOfSync","eventDef":{"descriptor":"markup://aura:clientOutOfSync","t":"APPLICATION","xs":"I"}},"exceptionEvent":true}/*ERROR*/
		//{"sonia.ottonello30@gmail.com", "Sonia1976"}, // disclameer not accepted
		//*/{"event":{"descriptor":"markup://aura:clientOutOfSync","eventDef":{"descriptor":"markup://aura:clientOutOfSync","t":"APPLICATION","xs":"I"}},"exceptionEvent":true}/*ERROR*/
		{"giuseppeamandonico", "600peppo"},
	}
	for _, test := range tests {
		if err := strategies.AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*strategies.PdfRequest{
		{
			Username: "giuseppeamandonico", Password: "600peppo", Uri: "engie_italia.crawler",
			Identifier: "0016210000687",
			Ref:        "100/MM/3425517", PdfUri: "https://media-platform.doxee.com/da-purl/document/713a617dc8894e5c81822258fbae5043",
		},
	}
	for _, test := range tests {
		pdf := strategies.PdfTest(test)
		t.Log(string(pdf.Content))
	}
}

func TestEngieItaliaChromeStrategy_parseHtml(t *testing.T) {
	pass := "600peppo"
	a := &model.Account{
		Username: "giuseppeamandonico",
		Password: &pass,
		Uri:      "engie_italia.crawler",
	}
	l := logrus.WithField("type", "EngieItaliaChromeStrategy")
	s := &EngieItaliaChromeStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(l.WithField("type", "ChromeDpCrawler")),
	}
	s.parseHtml(a, &htmlPage)
	a.PrettyPrint(l.Writer())
}

var htmlPage = `<html lang="it" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><head><script type="text/javascript" async="" src="https://www.google-analytics.com/plugins/ua/ec.js"></script><script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-5QQ9LW"></script><script src="/acma/static/111213/js/perf/stub.js" type="text/javascript"></script><script src="/acma/resource/1596784269000/acma/smartbanner/smartbanner.js?vs=1.1" type="text/javascript"></script><script src="https://web-gdfsuezenergieb2c.force.com/acma/lightning/lightning.out.js?v=2" type="text/javascript"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/lightning/lightning.out.delegate.js?v=dDIdorNC3N22LalQ5i3slQ"></script><script src="/acma/resource/1596784269000/acma/js/promise.min.js" type="text/javascript"></script><script src="/acma/resource/1596784269000/acma/js/fetch.js" type="text/javascript"></script><script src="/acma/resource/1588756276000/CookieManager" type="text/javascript"></script><link class="user" href="/acma/resource/1571900087000/material_icons/styles.css" rel="stylesheet" type="text/css"><link class="user" href="/acma/resource/1571900087000/lato/styles.css" rel="stylesheet" type="text/css"><link class="user" href="/acma/resource/1571900087000/roboto_mono/styles.css" rel="stylesheet" type="text/css"><link class="user" href="/acma/resource/1596784269000/acma/css/main.css" rel="stylesheet" type="text/css"><link class="user" href="/acma/resource/1596784269000/acma/smartbanner/smartbanner.min.css" rel="stylesheet" type="text/css"><script>(function(UITheme) {
    UITheme.getUITheme = function() { 
        return UserContext.uiTheme;
    };
}(window.UITheme = window.UITheme || {}));</script><script async="" src="https://static.hotjar.com/c/hotjar-925122.js?sv=6"></script><script async="" src="https://script.hotjar.com/modules.808c912e7ace5e8812a9.js" charset="utf-8"></script><style type="text/css">iframe#_hjRemoteVarsFrame {display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;}</style><script async="" src="https://static.hotjar.com/c/hotjar-1486967.js?sv=6"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22fwuid%22%3A%22dDIdorNC3N22LalQ5i3slQ%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22mlr%22%3A1%2C%22pathPrefix%22%3A%22%2Facma%22%2C%22dns%22%3A%22c%22%2C%22ls%22%3A1%7D/resources.js?pv=16074620490001413921634&amp;rv=1607007160000"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/auraFW/javascript/dDIdorNC3N22LalQ5i3slQ/aura_prod.js"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22fwuid%22%3A%22dDIdorNC3N22LalQ5i3slQ%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22mlr%22%3A1%2C%22pathPrefix%22%3A%22%2Facma%22%2C%22dns%22%3A%22c%22%2C%22ls%22%3A1%7D/inline.js?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..tgNn_jJYTXYw0U7QcNQoBPZNejLIdvc28wg8C_H5W0E&amp;ltngOut=true"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22serializationVersion%22%3A%221-1.7.10-228.5-b%22%2C%22split%22%3A%22t%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22dns%22%3A%22c%22%2C%22ls%22%3A1%7D/appcore.js?ltngOut=true"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22serializationVersion%22%3A%221-1.7.10-228.5-b%22%2C%22split%22%3A%22t%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22dns%22%3A%22c%22%2C%22ls%22%3A1%7D/app.js?ltngOut=true"></script><script type="text/javascript" src="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22fwuid%22%3A%22dDIdorNC3N22LalQ5i3slQ%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22mlr%22%3A1%2C%22pathPrefix%22%3A%22%2Facma%22%2C%22dns%22%3A%22c%22%2C%22ls%22%3A1%7D/bootstrap.js?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..tgNn_jJYTXYw0U7QcNQoBPZNejLIdvc28wg8C_H5W0E&amp;ltngOut=true"></script><link data-href="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22styleContext%22%3A%7B%22c%22%3A%22webkit%22%2C%22x%22%3A%5B%22isDesktop%22%5D%2C%22tokens%22%3A%5B%22markup%3A%2F%2Fforce%3AiconPaths%22%5D%2C%22tuid%22%3A%22zXl8kP8WSMms8IeHmRQexQ%22%2C%22cuid%22%3A1063257784%7D%2C%22pathPrefix%22%3A%22%2Facma%22%7D/app.css?1" type="text/css" rel="stylesheet" class="auraCss" href="https://web-gdfsuezenergieb2c.force.com/acma/l/%7B%22mode%22%3A%22PROD%22%2C%22app%22%3A%22c%3AACMAApp%22%2C%22loaded%22%3A%7B%22APPLICATION%40markup%3A%2F%2Fc%3AACMAApp%22%3A%22eGfeGOj-HT9Ci6q43gTAdw%22%7D%2C%22styleContext%22%3A%7B%22c%22%3A%22webkit%22%2C%22x%22%3A%5B%22isDesktop%22%5D%2C%22tokens%22%3A%5B%22markup%3A%2F%2Fforce%3AiconPaths%22%5D%2C%22tuid%22%3A%22zXl8kP8WSMms8IeHmRQexQ%22%2C%22cuid%22%3A1063257784%7D%2C%22pathPrefix%22%3A%22%2Facma%22%7D/app.css?1"><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1532615224000/redux" type="text/javascript"></script><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/js/utils.js" type="text/javascript"></script><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/js/bulma-toast.js" type="text/javascript"></script><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1532615224000/reselect" type="text/javascript"></script><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/js/ResizeObserver.global.js" type="text/javascript"></script><script data-locker-src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1532615224000/reduxThunk" type="text/javascript"></script></head><body><span id="ACMADashboard:j_id0">
    <script>
  if(!!''){
      //console.log('######redirectUrl: ');
            window.location = '';
  }
 </script></span>
        
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="Expires" content="Mon, 01 Jan 1990 12:00:00 GMT">

            <meta charset="utf-8">
            <meta content="ie=edge" http-equiv="x-ua-compatible">
            
            
            <meta content="ENGIE" name="smartbanner:title">
            <meta content="Engie Italia S.p.A." name="smartbanner:author">
            <meta content="GRATIS " name="smartbanner:price">
            <meta content="- Su App Store" name="smartbanner:price-suffix-apple">
            <meta content="- Su Google Play" name="smartbanner:price-suffix-google">
            <meta content="/acma/resource/1596784269000/acma/smartbanner/smartbanner-icon.png" name="smartbanner:icon-apple">
            <meta content="/acma/resource/1596784269000/acma/smartbanner/smartbanner-icon.png" name="smartbanner:icon-google">
            <meta content="APRI" name="smartbanner:button">
            <meta content="https://itunes.apple.com/it/app/engie-luce-gas-e-servizi/id1019118965" name="smartbanner:button-url-apple">
            <meta content="https://play.google.com/store/apps/details?id=it.engie.appengie&amp;hl=it" name="smartbanner:button-url-google">
            <meta content="android,ios" name="smartbanner:enabled-platforms">
            <meta content="1296000000" name="smartbanner:hide-ttl">
            <meta content="/" name="smartbanner:hide-path">
            <meta content="false" name="smartbanner:api">
            
            <title>Engie - Spazio Clienti</title>
            <meta content="width=device-width, initial-scale=1" name="viewport">
            <style>
                #auraErrorMessage {
                    position: absolute;
                    bottom: 0;
                    z-index: 30;
                    background-color: darkred;
                    color: white;
                }
            </style>

            
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer', 'GTM-5QQ9LW');</script>
            
            
            <link href="/acma/resource/1596784269000/acma/favicon.ico" rel="shortcut icon">
            

        <span id="ACMADashboard:j_id11">
            
            <noscript><iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-5QQ9LW" style="display:none;visibility:hidden" width="0"></iframe></noscript><span id="ACMADashboard:j_id11:ACMASupportedBrowsers:j_id17"></span>
            


            <script>
window.__data__ = {
    info: {"userId":"00520000003MB8OAAW","typeBIZ":"","social":{},"regStatus":"verified","registrationDate":"2014-07-01","profilePictureUrl":"","privacy":{"pMktThird":true,"pMkt":true,"pProf":false},"phone":"0997773635","oldestSupplyActivationDate":"2010-05-01","mobilePhone":"3208113115","loyalty":{"subscriptiondateloyalty":"2020-06-07","pointsC":10,"pointsP":0,"points":10,"clusterCode":"LMT001","cluster":"AUTENTICO","memberId":"a3P0J000003mVncUAE"},"lastName":"AMANDONICO","lastAccessDateWEB":"2020-12-09","lastAccessDateAPP":"2020-11-26","lastAccessDate":"2020-12-09T16:46:50","isProspect":false,"isPersonAccount":true,"gender":"M","fiscalCode":"MNDGPP60C19L049L","firstName":"GIUSEPPE","fax":"","email":"giuseppe.amandonico@libero.it","dmsCheckStato":"n","companyData":null,"cntCrmId":"0032000000WtrVsAAJ","birthDate":"1960-03-19","birthCity":"TARANTO","address":{"paese":"ITALIA","scala":"b","civicoEsteso":"SNC P","civico":"0","cap":"74122","provincia":"TA","comune":"TARANTO","localita":"TARANTO","indirizzo":"VIA GREGORIO VII"},"accWebId":"0012000001AP8Z9AAL","accCrmId":"0012000000VMQxcAAH"},
    /*contracts: {contracts!},*/
    cards: [],
    /*dashboard: {
        data: {dashboardData!},
        pending: {dashboardPending!},
    },*/
    timeline: {
        data: [],
        pending: false,
    },
    bills: {
        data: [{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/713a617dc8894e5c81822258fbae5043","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"n","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"87.22","consumoE":"687","perFat":"Dal 01/07/2020 Al 19/10/2020","idDocDett":"1141627556","dataPag":"","pagata":"y","importo":"215.92","stato":"S","dataScad":"2020-11-10","dataEmiss":"2020-10-19","numFisc":"100/MM/3425517","codeLine":"1205000056780600"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/79ef553a62824387bd0724da08279ab6","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"n","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"148.75","consumoE":"543","perFat":"Dal 01/04/2020 Al 10/08/2020","idDocDett":"1073905123","dataPag":"","pagata":"y","importo":"153.62","stato":"S","dataScad":"2020-09-08","dataEmiss":"2020-08-10","numFisc":"100/MM/2649755","codeLine":"1205000045077780"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/49f97dd79fef47099c2884808c411657","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"222.92","consumoE":"386","perFat":"Dal 01/02/2020 Al 08/06/2020","idDocDett":"1008336413","dataPag":"","pagata":"y","importo":"219.8","stato":"S","dataScad":"2020-06-30","dataEmiss":"2020-06-08","numFisc":"100/MM/2074555","codeLine":"1205000033804570"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/b104002a1cab4294bd4726dbef41771d","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"144.44","consumoE":"377","perFat":"Dal 01/01/2020 Al 14/04/2020","idDocDett":"954563947","dataPag":"","pagata":"y","importo":"166.68","stato":"S","dataScad":"2020-05-22","dataEmiss":"2020-04-14","numFisc":"100/MM/1250947","codeLine":"1205000020405240"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/31370fbf245c41f89156468c83f48f0f","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"108.71","consumoE":"386","perFat":"Dal 01/12/2019 Al 17/02/2020","idDocDett":"895860494","dataPag":"","pagata":"y","importo":"170.78","stato":"S","dataScad":"2020-03-10","dataEmiss":"2020-02-17","numFisc":"100/MM/714979","codeLine":"1205000010318490"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/8e334248ac43463c8d9f7f2705f333d1","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"133.95","consumoE":"390","perFat":"Dal 25/07/2019 Al 23/12/2019","idDocDett":"790920803","dataPag":"","pagata":"y","importo":"177.89","stato":"S","dataScad":"2020-01-21","dataEmiss":"2019-12-23","numFisc":"100/MM/4123231","codeLine":"1195000063960120"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/5fdb80c411ea40d3875ac2d5e7f4b015","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"77.69","consumoE":"662","perFat":"Dal 26/06/2019 Al 14/10/2019","idDocDett":"733706563","dataPag":"","pagata":"y","importo":"194.04","stato":"S","dataScad":"2019-11-05","dataEmiss":"2019-10-14","numFisc":"100/MM/3516112","codeLine":"1195000054343730"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/ddfeb57eca8b4101bea1e33052a0fee2","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"87.97","consumoE":"624","perFat":"Dal 26/04/2019 Al 05/08/2019","idDocDett":"667361993","dataPag":"","pagata":"y","importo":"144.44","stato":"S","dataScad":"2019-09-03","dataEmiss":"2019-08-05","numFisc":"100/MM/2692805","codeLine":"1195000043149140"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/006c6fa0bdfe4cd785aca1fa99f592b2","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"147.38","consumoE":"382","perFat":"Dal 26/02/2019 Al 10/06/2019","idDocDett":"635737147","dataPag":"","pagata":"y","importo":"189.89","stato":"S","dataScad":"2019-07-02","dataEmiss":"2019-06-10","numFisc":"100/MM/2046714","codeLine":"1195000033616600"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/8310fca2c8734606b3e70f02c1c428ef","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"152.59","consumoE":"390","perFat":"Dal 26/12/2018 Al 15/04/2019","idDocDett":"597522112","dataPag":"","pagata":"y","importo":"166.47","stato":"S","dataScad":"2019-05-07","dataEmiss":"2019-04-15","numFisc":"100/MM/1329570","codeLine":"1195000019861380"},{"urlpdfdownload":"https://media-platform.doxee.com/da-purl/document/2017e167990140a1ac7be4ee4e3a6ec7","address":"VIA GREGORIO VII 0, TARANTO","Pweb":"y","videoBill":"y","commodity":"Gas&Luce","codContr":"0016210000687","consumoG":"171.05","consumoE":"598","perFat":"Dal 13/10/2018 Al 18/02/2019","idDocDett":"559348883","dataPag":"","pagata":"y","importo":"210.83","stato":"S","dataScad":"2019-03-12","dataEmiss":"2019-02-18","numFisc":"100/MM/706134","codeLine":"1195000009318510"}],
        pending: false,
    },
    config: {"showModalWelcome":false,"isDisableSection":true,"whatsAppNumber":"393202041412","billCheck":{"list":[]},"socialProviderStartUrl":{"START_URL_GOOGLE":"https://web-gdfsuezenergieb2c.force.com/acma/ACMAAuthenticationSocial?provider=Google","START_URL_FACEBOOK":"https://web-gdfsuezenergieb2c.force.com/acma/ACMAAuthenticationSocial?provider=Facebook"},"maxLengthCompanyName":60,"maxLengthLastName":30,"maxLengthFirstName":29,"productCode":{"smartAndSave":["SESSP","SESPL"]},"geolabSMEndPoint":"https://italco.geolabsrl.com/suggest-engie/SuggestServer","geolabSMKey":"dJjSVqMdVU","tendrilLogin":"https://www.domuscheck.it/users/sign_in","EngieHomeBIZ":"https://imprese.engie.it/","EngieHomeRES":"https://casa.engie.it/","spazioClientiContent":{"BIZ":"https://imprese.engie.it/spazio-clienti?content=","RES":"https://casa.engie.it/spazio-clienti?content="}},
    contractChains: {"0016210000687":{"indirizzoSpedizioneObj":{"paese":"ITALIA","cortese_attenzione":"","presso":"","civico":"27","indirizzo":"VIA GREGORIO VII","cap":"74122","prov":"TA","comune":"TARANTO"},"forniture":[{"prodottoCodiceUnivoco":"OMBRA#00035","prodottoBolOnLine":false,"codUnivoco":"SwitchIn_P100415-000092800_T000118022","attivaDayAfter":3875,"attiva":"y","cessata":"n","annullata":"n","stepErr":"n","codContr":"0016210000687","dataStep":"2010-05-01","step":4,"pointCode":"IT001E74388899","fName":"F10-00081994","fId":"a082000000LygY8AAJ","praticaId":"a0T200000014ImvEAE","praticaName":"P100415-000092800","productCode":"SCO30","nRichForn":"F0079813","Luce":{"dataCess":null,"dataAtt":"2010-05-01"},"siglaTensione":"BT","addressObj":{"viario":"a0A2000000JkdWDEAZ","interno":null,"piano":null,"scala":"B","civicoExt":null,"civico":"0","indirizzo":"VIA GREGORIO VII","cap":"74100","prov":"TA","comune":"TARANTO"},"address":"VIA GREGORIO VII, 0 TARANTO (TA)","nomeOfferta":"Rinnovo prezzo fisso","punto":{"potenzaImpegnata":3.00,"potenzaConsumo":3.00,"pod":"IT001E74388899","apparati":[{"convertitore":{"matricola":""},"misuratore":{"matricola":""}}],"datiCatastali":{"tipoParticella":null,"segueParticella":null,"subalterno":null,"mappale":null,"foglio":null,"comuneCatastale":null},"id":"a062000000HyKOvAAN"},"dataCessazione":null,"statoCalc":"attiva","statoNumericoStr":"1","dataAttivazione":"2010-05-01","tipoContratto":"Domestico residente","mercato":"Libero","statoNumerico":1,"commodity":"Luce","id":"a082000000LygY8AAJ"},{"prodottoCodiceUnivoco":"OMBRA#00035","prodottoBolOnLine":false,"codUnivoco":"SwitchIn_P100415-000092800_T000337989","attivaDayAfter":3875,"attiva":"y","cessata":"n","annullata":"n","stepErr":"n","codContr":"0016210000687","dataStep":"2010-05-01","step":4,"pointCode":"01613870038900","fName":"F10-00273173","fId":"a082000000O64ssAAB","praticaId":"a0T200000014ImvEAE","praticaName":"P100415-000092800","productCode":"SCO30","nRichForn":"F0079813","Gas":{"dataCess":null,"dataAtt":"2010-05-01"},"autolettura":{"dataLettura":"","lettura":"","ultimaLetturaValidata":"1555.000","fineFinestra":"2020-12-14","inizioFinestra":"2020-12-10"},"tipoUso":"Uso cottura cibi e/o produzione di acqua calda sanitaria","addressObj":{"viario":"a0A2000000JkdWDEAZ","interno":null,"piano":null,"scala":"B","civicoExt":null,"civico":"0","indirizzo":"VIA GREGORIO VII","cap":"74100","prov":"TA","comune":"TARANTO"},"address":"VIA GREGORIO VII, 0 TARANTO (TA)","nomeOfferta":"Rinnovo prezzo fisso","punto":{"pdr":"01613870038900","apparati":[{"convertitore":{"matricola":""},"misuratore":{"matricola":"MTSB032800731803"}}],"datiCatastali":{"tipoParticella":null,"segueParticella":null,"subalterno":null,"mappale":null,"foglio":null,"comuneCatastale":null},"id":"a062000000Lvq85AAB"},"dataCessazione":null,"statoCalc":"attiva","statoNumericoStr":"1","dataAttivazione":"2010-05-01","tipoContratto":"A – Uso Domestico","mercato":"Libero","statoNumerico":1,"commodity":"Gas","id":"a082000000O64ssAAB"}],"type":"ACT","id":"800200000024ZymAAE","codContr":"0016210000687","stato":"Attivato","nomeOfferta":"Rinnovo prezzo fisso","label":null,"commodity":"Gas&Luce","indirizzoFornitura":"VIA GREGORIO VII 0, TARANTO","indirizzoFornituraObj":{"viario":"74122-541247","interno":null,"piano":null,"scala":"B","civicoExt":null,"civico":"0","indirizzo":"VIA GREGORIO VII","cap":"74100","prov":"TA","comune":"TARANTO"},"indirizzoSpedizione":"VIA GREGORIO VII 27, TARANTO","dataProxBol":["2020-12-21","2021-02-15","2021-04-12","2021-06-07","2021-08-09","2021-10-18","2021-12-20"],"sdd":{"parentId":"","attesaDoc":"n","stato":"y","titConto":"AMANDONICO GIUSEPPE","iban":"IT21Z0760115800000099204133","codiceUmr":"7071410000016210000687","cliNome":"GIUSEPPE","cliCognome":"AMANDONICO","cliCf":"MNDGPP60C19L049L","cliRagSoc":"","cliPIva":""},"bol":{"statoBOL":"y","email":"giuseppe.amandonico@libero.it"},"division":"GDFSE","segmento":"Residential"}},
    notifications: [],
};

            </script>



            <div class="container is-fluid is-dashboard slds-scope" id="root" data-ltngout-rendered-by="4:0"><!--render facet: 1421:0--><!--render facet: 1425:0--><!--render facet: 6:0--><div class="banner-app" id="banner-app-desktop" style="display:none;z-index:9999" data-aura-rendered-by="8:0"><img class="banner-app-icon" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/smartbanner/smartbanner-icon.png" data-aura-rendered-by="9:0"><span class="banner-app-text" data-aura-rendered-by="10:0">Una nuova app per vivere al meglio la tua esperienza in ENGIE. Disponibile per iOS e Android</span><a href="https://casa.engie.it/in-evidenza/app" data-aura-rendered-by="12:0" target="_blank" style="color:white!important"><span class="banner-app-cta" data-aura-rendered-by="13:0">SCOPRI DI PIÙ</span></a><a href="javascript:closeBannerApp();" style="color:white!important" data-aura-rendered-by="15:0"><span class="banner-app-close" id="close-banner-app" data-aura-rendered-by="16:0">X</span></a></div><!--render facet: 20:0--><!--render facet: 25:0--><!--render facet: 29:0--><!--render facet: 31:0--><!--render facet: 35:0--><!--render facet: 39:0--><!--render facet: 41:0--><!--render facet: 45:0--><!--render facet: 49:0--><!--render facet: 51:0--><!--render facet: 55:0--><!--render facet: 59:0--><!--render facet: 61:0--><!--render facet: 65:0--><!--render facet: 69:0--><!--render facet: 71:0--><!--render facet: 75:0--><!--render facet: 79:0--><!--render facet: 81:0--><!--render facet: 85:0--><!--render facet: 89:0--><!--render facet: 91:0--><!--render facet: 96:0--><!--render facet: 100:0--><!--render facet: 102:0--><!--render facet: 106:0--><!--render facet: 110:0--><!--render facet: 112:0--><!--render facet: 138:0--><!--render facet: 142:0--><div class="modal" style="z-index:41;" data-aura-rendered-by="118:0"><!--render facet: 119:0--><div class="modal-container" data-aura-rendered-by="120:0"><div class="modal-close" data-aura-rendered-by="122:0"><button class="is-large" data-aura-rendered-by="123:0"></button></div><div class="modal-content" data-aura-rendered-by="124:0"><!--render facet: 116:0--></div></div></div><div class="modal" data-aura-rendered-by="129:0"><!--render facet: 130:0--><div class="modal-container" data-aura-rendered-by="131:0"><div class="modal-close" data-aura-rendered-by="133:0"><button class="is-large" data-aura-rendered-by="134:0"></button></div><div class="modal-content" data-aura-rendered-by="135:0"><!--render facet: 127:0--></div></div></div><!--render facet: 148:0--><!--render facet: 152:0--><!--render facet: 154:0--><!--render facet: 158:0--><!--render facet: 162:0--><!--render facet: 164:0--><!--render facet: 170:0--><!--render facet: 174:0--><!--render facet: 167:0--><!--render facet: 182:0--><!--render facet: 186:0--><!--render facet: 179:0--><!--render facet: 192:0--><!--render facet: 196:0--><!--render facet: 198:0--><!--render facet: 202:0--><!--render facet: 206:0--><!--render facet: 208:0--><!--render facet: 1413:0--><!--render facet: 1417:0--><!--render facet: 1275:0--><!--render facet: 1369:0--><!--render facet: 1373:0--><div class="navbar is-expanded has-shadow has-home-selector cNavbar cACMALayout" role="navigation" aria-label="dropdown navigation" data-aura-rendered-by="1294:0" data-aura-class="cNavbar cACMALayout"><div class="navbar-brand" data-aura-rendered-by="1295:0"><div class="navbar-brand-menu" data-aura-rendered-by="1296:0"><span class="navbar-item is-hidden-desktop has-margin-left" data-aura-rendered-by="1297:0"><a data-aura-rendered-by="3983:0" href="javascript:void(0);"><span class="icon is-primary" style="" data-aura-rendered-by="3986:0"><i class=" material-icons" style="" data-aura-rendered-by="3988:0">dehaze</i><!--render facet: 3990:0--><!--render facet: 3991:0--></span></a></span><a class="navbar-item" data-aura-rendered-by="1308:0" href="javascript:void(0);"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/logo-blue.svg" data-aura-rendered-by="1309:0"></a><span class="navbar-item is-hidden-desktop has-margin-right" data-aura-rendered-by="1310:0"><!--render facet: 1344:0--><!--render facet: 1348:0--><!--render facet: 1313:0--><div class="dropdown is-right" data-aura-rendered-by="1336:0"><div class="dropdown-trigger" data-aura-rendered-by="1337:0"><a class="button is-text" data-aura-rendered-by="1316:0" href="javascript:void(0);"><span class="is-hidden-touch is-hidden-desktop-only has-padding-left has-padding-right-small" data-aura-rendered-by="1318:0">
                        Il tuo profilo
                    </span><span class="icon" style="" data-aura-rendered-by="1322:0"><svg viewBox="0 0 24 24" height="24" width="24" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <circle r="12" cy="12" cx="12" id="0-a"></circle>
    </defs>
    <g fill-rule="evenodd" fill="none">
        <mask fill="#fff" id="0-b">
            <use xlink:href="#0-a"></use>
        </mask>
        <use xlink:href="#0-a" fill-rule="nonzero" fill="#E5F6FF"></use>
        <circle mask="url(#0-b)" fill-rule="nonzero" fill="currentColor" r="4" cy="9" cx="12"></circle>
        <circle mask="url(#0-b)" fill-rule="nonzero" fill="currentColor" r="8" cy="23" cx="12"></circle>
    </g>
</svg>
</span><span class="icon is-hidden-touch is-hidden-desktop-only" style="" data-aura-rendered-by="1329:0"><i class=" material-icons" style="" data-aura-rendered-by="1331:0">keyboard_arrow_down</i><!--render facet: 1333:0--><!--render facet: 1334:0--></span></a></div><div class="dropdown-menu" role="menu" data-aura-rendered-by="1340:0"><div class="dropdown-content" data-aura-rendered-by="1341:0"><a class="dropdown-item has-text-slate" data-index="0" data-aura-rendered-by="1466:0" href="javascript:void(0);"><!--render facet: 1467:0-->Dati personali</a><a class="dropdown-item has-text-slate" data-index="1" data-aura-rendered-by="11913:0" href="javascript:void(0);"><!--render facet: 11914:0-->Porta un amico</a><a class="dropdown-item has-text-slate" data-index="2" data-aura-rendered-by="1470:0" href="javascript:void(0);"><!--render facet: 1471:0-->ENGIE Pulse</a><a class="dropdown-item has-text-slate" data-index="3" data-aura-rendered-by="1474:0" href="javascript:void(0);"><!--render facet: 1475:0-->Modalità di pagamento</a><a class="dropdown-item has-text-slate" data-index="4" data-aura-rendered-by="1478:0" href="javascript:void(0);"><!--render facet: 1479:0-->Gestione privacy</a><a class="dropdown-item has-text-slate" data-index="5" data-aura-rendered-by="1482:0" href="javascript:void(0);"><!--render facet: 1483:0-->Stato pratiche</a><hr class="dropdown-divider" data-aura-rendered-by="1486:0"><a class="dropdown-item has-text-slate" data-index="7" data-aura-rendered-by="1488:0" href="javascript:void(0);"><!--render facet: 1489:0-->Esci</a></div></div></div></span></div><div class="home-selector is-hidden-desktop" data-aura-rendered-by="1352:0"><!--render facet: 1355:0--><!--render facet: 1359:0--><div class="center-content-v has-padding-left has-padding-right" data-aura-rendered-by="6787:0"><span class="icon is-primary has-margin-right-small" style="" data-aura-rendered-by="6790:0"><svg viewBox="0 0 24 24" height="24" width="24" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M13 23v-6h4v6zM7 18v-3h3v3z"></path>
        <path d="M21 11v12H3V11M23 11H1l4-7h14zM1 23h22M7 4V1h3v3"></path>
    </g>
</svg>
</span><span class="ellipsis" data-aura-rendered-by="6795:0">Via Gregorio Vii 0, Taranto</span></div></div></div><div class="navbar-menu" data-aura-rendered-by="1363:0"><div class="navbar-start" data-aura-rendered-by="1364:0"><!--unrender facet: 1280:0--><div class="navbar-item home-selector" data-aura-rendered-by="1282:0"><!--render facet: 1285:0--><!--render facet: 1289:0--><div class="center-content-v has-padding-left has-padding-right is-modal" data-aura-rendered-by="6776:0"><span class="icon is-primary has-margin-right-small" style="" data-aura-rendered-by="6779:0"><svg viewBox="0 0 24 24" height="24" width="24" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M13 23v-6h4v6zM7 18v-3h3v3z"></path>
        <path d="M21 11v12H3V11M23 11H1l4-7h14zM1 23h22M7 4V1h3v3"></path>
    </g>
</svg>
</span><span class="ellipsis" data-aura-rendered-by="6784:0">Via Gregorio Vii 0, Taranto</span></div></div></div><div class="navbar-end" data-aura-rendered-by="1366:0"><span class="navbar-item is-hoverable" style="box-shadow: inset 1px 0 0 0 hsl(0, 0%, 96%);" data-aura-rendered-by="1278:0"><!--render facet: 246:0--><!--render facet: 250:0--><!--render facet: 215:0--><div class="dropdown is-right" data-aura-rendered-by="238:0"><div class="dropdown-trigger" data-aura-rendered-by="239:0"><a class="button is-text" data-aura-rendered-by="218:0" href="javascript:void(0);"><span class="is-hidden-touch is-hidden-desktop-only has-padding-left has-padding-right-small" data-aura-rendered-by="220:0">
                        Il tuo profilo
                    </span><span class="icon" style="" data-aura-rendered-by="224:0"><svg viewBox="0 0 24 24" height="24" width="24" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <circle r="12" cy="12" cx="12" id="1-a"></circle>
    </defs>
    <g fill-rule="evenodd" fill="none">
        <mask fill="#fff" id="1-b">
            <use xlink:href="#1-a"></use>
        </mask>
        <use xlink:href="#1-a" fill-rule="nonzero" fill="#E5F6FF"></use>
        <circle mask="url(#1-b)" fill-rule="nonzero" fill="currentColor" r="4" cy="9" cx="12"></circle>
        <circle mask="url(#1-b)" fill-rule="nonzero" fill="currentColor" r="8" cy="23" cx="12"></circle>
    </g>
</svg>
</span><span class="icon is-hidden-touch is-hidden-desktop-only" style="" data-aura-rendered-by="231:0"><i class=" material-icons" style="" data-aura-rendered-by="233:0">keyboard_arrow_down</i><!--render facet: 235:0--><!--render facet: 236:0--></span></a></div><div class="dropdown-menu" role="menu" data-aura-rendered-by="242:0"><div class="dropdown-content" data-aura-rendered-by="243:0"><a class="dropdown-item has-text-slate" data-index="0" data-aura-rendered-by="1440:0" href="javascript:void(0);"><!--render facet: 1441:0-->Dati personali</a><a class="dropdown-item has-text-slate" data-index="1" data-aura-rendered-by="11908:0" href="javascript:void(0);"><!--render facet: 11909:0-->Porta un amico</a><a class="dropdown-item has-text-slate" data-index="2" data-aura-rendered-by="1444:0" href="javascript:void(0);"><!--render facet: 1445:0-->ENGIE Pulse</a><a class="dropdown-item has-text-slate" data-index="3" data-aura-rendered-by="1448:0" href="javascript:void(0);"><!--render facet: 1449:0-->Modalità di pagamento</a><a class="dropdown-item has-text-slate" data-index="4" data-aura-rendered-by="1452:0" href="javascript:void(0);"><!--render facet: 1453:0-->Gestione privacy</a><a class="dropdown-item has-text-slate" data-index="5" data-aura-rendered-by="1456:0" href="javascript:void(0);"><!--render facet: 1457:0-->Stato pratiche</a><hr class="dropdown-divider" data-aura-rendered-by="1460:0"><a class="dropdown-item has-text-slate" data-index="7" data-aura-rendered-by="1462:0" href="javascript:void(0);"><!--render facet: 1463:0-->Esci</a></div></div></div></span></div></div></div><section class="content is-dashboard cACMALayout" data-aura-rendered-by="1376:0" data-aura-class="cACMALayout"><!--render facet: 4018:0--><!--render facet: 4022:0--><!--render facet: 3995:0--><div class="sidebar cSidebar" data-aura-rendered-by="3997:0" data-aura-class="cSidebar"><ul class="sidebar-menu" data-aura-rendered-by="3998:0"><!--render facet: 268:0--><!--render facet: 272:0--><!--render facet: 255:0--><li class="sidebar-menu-item is-active" data-aura-rendered-by="257:0"><a data-aura-rendered-by="258:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="261:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M19 20.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0zM19 19.005L27.506 13M26.199 14.286a11.442 11.442 0 0 1 1.947 6.406M5 20.5C5 14.148 10.148 9 16.5 9c2.861 0 5.479 1.045 7.49 2.773"></path>
    </g>
</svg>
</span>Attività</a></li><!--render facet: 290:0--><!--render facet: 294:0--><!--render facet: 277:0--><li class="sidebar-menu-item" data-aura-rendered-by="279:0"><a data-aura-rendered-by="280:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="283:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M11.5 14h9M11.5 18h9M11.5 22h9M11.765 7H8v21h16V7h-3.765"></path>
        <path d="M12 6v3h8V6z"></path>
    </g>
</svg>
</span>Forniture</a></li><!--render facet: 312:0--><!--render facet: 316:0--><!--render facet: 299:0--><li class="sidebar-menu-item" data-aura-rendered-by="301:0"><a data-aura-rendered-by="302:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="305:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M24 12.177V27H8V6h9.847z"></path>
        <path d="M18 6.5V12h5.5"></path>
    </g>
</svg>
</span>Bollette</a></li><!--render facet: 3975:0--><!--render facet: 3979:0--><!--render facet: 3962:0--><li class="sidebar-menu-item" data-aura-rendered-by="3964:0"><a data-aura-rendered-by="3965:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="3968:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
  <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none" transform="translate(-765.5 -521)" id="11-Puzzle_icon">
    <path fill="none" transform="translate(912.565 465.465)" d="M-130.72,79.206a4.317,4.317,0,0,0,.58-1.5.778.778,0,0,0,.014-.168,2.666,2.666,0,0,0-2.662-2.583,2.664,2.664,0,0,0-2.663,2.562.743.743,0,0,0,0,.1,3.456,3.456,0,0,0,.557,1.6.767.767,0,0,1-.675,1.132l-3.728,0a.767.767,0,0,1-.766-.767l.008-13.019a.767.767,0,0,1,.767-.766l3.7,0a.767.767,0,0,0,.675-1.132,3.336,3.336,0,0,1-.527-1.549.86.86,0,0,1,0-.1,2.663,2.663,0,0,1,2.663-2.561,2.664,2.664,0,0,1,2.661,2.582.768.768,0,0,1-.014.167,4.259,4.259,0,0,1-.548,1.443.767.767,0,0,0,.661,1.154l3.747,0a.767.767,0,0,1,.766.767l0,3.8a.767.767,0,0,0,1.132.675,3.753,3.753,0,0,1,1.725-.623.711.711,0,0,1,.1,0,2.664,2.664,0,0,1,2.562,2.663,2.664,2.664,0,0,1-2.582,2.661.779.779,0,0,1-.168-.014,4.432,4.432,0,0,1-1.607-.647.767.767,0,0,0-1.166.654l0,3.852a.767.767,0,0,1-.767.767l-3.78,0A.766.766,0,0,1-130.72,79.206Z" id="11-Puzzle"></path>
  </g>
</svg>
</span>ENGIE per Te</a></li><!--render facet: 335:0--><!--render facet: 339:0--><!--render facet: 322:0--><li class="sidebar-menu-item" data-aura-rendered-by="324:0"><a data-aura-rendered-by="325:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="328:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M6.24 23.456a1.745 1.745 0 1 1-3.49 0 1.745 1.745 0 0 1 3.49 0zM13.22 15.313a1.745 1.745 0 1 1-3.489 0 1.745 1.745 0 0 1 3.49 0zM22.528 18.803a1.745 1.745 0 1 1-3.49 0 1.745 1.745 0 0 1 3.49 0zM29.509 9.495a1.745 1.745 0 1 1-3.491 0 1.745 1.745 0 0 1 3.49 0zM10.177 16.476l-4.52 5.687M19.126 18.281l-6.028-2.33M21.84 17.42l4.868-6.544"></path>
    </g>
</svg>
</span>Domuscheck</a></li><!--render facet: 357:0--><!--render facet: 361:0--><!--render facet: 344:0--><li class="sidebar-menu-item" data-aura-rendered-by="346:0"><a data-aura-rendered-by="347:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="350:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <path d="M15.326 14H28l3 3-3 3-1.494-1.5-1.487 1.5-1.519-1.5L22 20l-1.5-1.5L19 20h-3.674a7 7 0 1 1 0-6zM7 18.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none"></path>
</svg>
</span>Gestione casa</a></li><!--render facet: 379:0--><!--render facet: 383:0--><!--render facet: 366:0--><li class="sidebar-menu-item" data-aura-rendered-by="368:0"><a data-aura-rendered-by="369:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="372:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" stroke="currentColor" fill-rule="evenodd" fill="none">
        <path d="M11 5h11v4l-4 7h-3l-4-7zM22 27H11v-4l4-7h3l4 7zM11 8.5h11M11 23.5h11"></path>
    </g>
</svg>
</span>Storia</a></li><li class="sidebar-menu-item" data-aura-rendered-by="4000:0"><a data-aura-rendered-by="4001:0" href="javascript:void(0);"><span class="icon" style="" data-aura-rendered-by="4004:0"><i class=" material-icons" style="" data-aura-rendered-by="4006:0">favorite_border</i><!--render facet: 4008:0--><!--render facet: 4009:0--></span>
                    Assistenza
                </a></li><li class="sidebar-menu-separator" data-aura-rendered-by="4011:0"></li><li class="sidebar-menu-item lastAccess is-size-9" style="margin-top:2rem" data-aura-rendered-by="4012:0"><span class="lastAccessTitle" data-aura-rendered-by="4013:0"> Ultimo accesso:</span><br data-aura-rendered-by="4015:0">9 Dicembre 2020 alle 16:46</li></ul></div><!--render facet: 1267:0--><!--render facet: 1271:0--><!--render facet: 1263:0--><!--render facet: 1876:0--><!--render facet: 1880:0--><section class="page-content is-cluster-lmt001 is-dashboard" data-aura-rendered-by="1801:0"><!--render facet: 1765:0--><!--render facet: 1588:0--><!--render facet: 1592:0--><!--render facet: 1520:0--><div class="dashboard-header is-grid-1-mobile is-grid-6 is-grid-2-widescreen cluster-lmt001 cACMADashboardHeader" style="grid-column-gap:3rem;" data-aura-rendered-by="1522:0" data-aura-class="cACMADashboardHeader"><div class="name-container is-1x1-mobile is-1x3 is-1x1-widescreen" data-aura-rendered-by="1523:0"><div class="avatar-container" data-aura-rendered-by="1524:0"><figure class="image avatar" data-aura-rendered-by="1525:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/loyalty/avatar-lmt001.svg" data-aura-rendered-by="1526:0"></figure><span class="avatar-label is-uppercase has-font-roboto" data-aura-rendered-by="4072:0">autentico</span></div><div data-aura-rendered-by="1528:0"><div class="has-text-charcoal-grey" data-aura-rendered-by="1529:0">Ciao</div><div class="has-margin-top-small is-size-4 has-text-weight-bold has-text-primary" data-aura-rendered-by="1531:0">Giuseppe</div><div class="has-text-primary is-size-8 has-padding-top-small" data-aura-rendered-by="4074:0"><a href="#" data-aura-rendered-by="4075:0">Vai al tuo profilo</a></div></div></div><!--render facet: 4106:0--><!--render facet: 4110:0--><div class="points-container has-text-charcoal-grey is-1x1-mobile is-1x3 is-1x1-widescreen" data-aura-rendered-by="4079:0"><div class="points-badge is-2x1" data-aura-rendered-by="4080:0"><figure class="image is-64x64" style="" data-aura-rendered-by="4081:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/circle_pink.svg" data-aura-rendered-by="4082:0"><span class="points" data-aura-rendered-by="4084:0">10</span></figure></div><span class="is-uppercase has-text-charcoal-grey has-text-weight-bold is-1x1 is-size-7" data-aura-rendered-by="4086:0">i tuoi punti led</span><div class="is-1x1" data-aura-rendered-by="4088:0">
                Ti mancano solo <strong class="has-text-darkish-pink" data-aura-rendered-by="9331:0">40 punti</strong> per avere uno sconto in bolletta!
                <div class="cta" data-aura-rendered-by="9335:0"><a class="has-text-primary is-size-7" href="javascript:void(0);" data-aura-rendered-by="9336:0">Storico azioni</a></div></div><div class="points-progressbar-container is-1x2" data-aura-rendered-by="9338:0"><progress class="progress is-darkish-pink" max="100" value="20" data-aura-rendered-by="9339:0">20%</progress><span class="tick" style="left:20%;" data-aura-rendered-by="9342:0"></span><span class="tick" style="left:40%;" data-aura-rendered-by="9343:0"></span><span class="tick" style="left:60%;" data-aura-rendered-by="9344:0"></span><span class="tick" style="left:80%;" data-aura-rendered-by="9345:0"></span></div></div><!--unrender facet: 1535:0--><div class="is-1x1-mobile is-1x6 is-1x2-widescreen" data-aura-rendered-by="11917:0"><!--render facet: 11951:0--><!--render facet: 11955:0--><!--render facet: 11945:0--><div class="widget cWidget cWidgetInvitaAmico" style="" data-aura-rendered-by="11947:0" data-aura-class="cWidget cWidgetInvitaAmico"><!--render facet: 11948:0--><div class="widget-content" data-aura-rendered-by="11920:0"><div class="d-flex f-row f-column-mobile f-align-center w-100" data-aura-rendered-by="11921:0"><div class="d-flex f-row" style="flex: 1;" data-aura-rendered-by="11922:0"><figure class="image is-96x96 is-64x64-mobile" data-aura-rendered-by="11923:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/mgm/gift-01-alt.svg" data-aura-rendered-by="11924:0"></figure><div class="px-4 px-1-mobile pb-1-mobile has-text-slate" style="color:#1d2d44" data-aura-rendered-by="11925:0"><p class="d-none-mobile is-size-4 has-text-weight-900 mb-1" style="color:#1d2d44!important" data-aura-rendered-by="11926:0">Porta un amico</p><p class="is-size-6" style="color:#1d2d44!important" data-aura-rendered-by="11928:0"><strong class="d-none-tablet" style="color:#1d2d44!important" data-aura-rendered-by="11929:0">Porta un amico:&nbsp;</strong>
                        Invita i tuoi amici in ENGIE e <strong data-aura-rendered-by="11932:0">guadagni fino a 300€</strong> di bonus.<br data-aura-rendered-by="11935:0"><a class="is-size-8" target="_self" style="color:#0aaaff" data-aura-rendered-by="11936:0" href="javascript:void(0);">Scopri di più</a></p></div></div><button class="button is-primary is-rounded is-uppercase is-standard w-100-mobile" data-aura-rendered-by="11940:0"><!--render facet: 11941:0-->Invita un amico</button><!--render facet: 11943:0--></div></div></div></div><div class="suggested-action is-1x1-mobile is-1x6 is-1x2-widescreen has-background-white" data-aura-rendered-by="9346:0"><a class="has-text-primary is-size-7" href="javascript:void(0);" data-aura-rendered-by="9347:0">Vedi tutte le azioni consigliate</a><div class="is-flex-row-tablet f-align-center-tablet" data-aura-rendered-by="9349:0"><div class="has-text-charcoal-grey is-size-4 has-text-weight-900 has-padding-bottom-mobile pr-4" style="flex:1;" data-aura-rendered-by="9350:0">Attiva il monitoraggio dei consumi Domuscheck, per te 20 Punti LED</div><button class="button is-primary is-rounded is-uppercase is-standard" data-aura-rendered-by="9354:0"><!--render facet: 9355:0-->attiva ora</button><!--render facet: 9357:0--></div><span class="action-flag" data-aura-rendered-by="9358:0">Ottieni <b data-aura-rendered-by="9360:0">20 Punti LED</b></span></div><!--render facet: 1555:0--><!--render facet: 1559:0--><div class="commodity-info is-1x1-mobile is-1x1-widescreen is-1x3" data-aura-rendered-by="4056:0"><div class="commodity-icon" data-aura-rendered-by="4058:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/light-and-fire.svg" data-aura-rendered-by="4059:0"></div><div class="commodity-details has-padding-left-small" data-aura-rendered-by="4060:0"><div class="commodity-name" data-aura-rendered-by="4061:0"><div class="commodity-name" data-aura-rendered-by="4062:0">Gas e Luce<span class="status" data-aura-rendered-by="4064:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/active.svg" data-aura-rendered-by="4065:0">attivi</span></div></div><span class="commodity-code" data-aura-rendered-by="4069:0">cod. cliente 0016210000687</span></div></div><!--render facet: 1579:0--><!--render facet: 1583:0--><!--render facet: 1564:0--><div class="is-1x1-mobile d-flex f-row f-align-center has-text-avocado-green is-1x3 is-1x1-widescreen" data-aura-rendered-by="1567:0"><span class="icon is-medium has-margin-left" style="flex: 0 0 auto;" data-aura-rendered-by="1570:0"><!--render facet: 1571:0--><!--render facet: 1572:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/check.svg" style="" data-aura-rendered-by="1574:0"></span><div class="state-description has-padding-left-small" data-aura-rendered-by="1575:0">
                    Tutto sotto controllo! Lo stato dei pagamenti è regolare.
                    </div></div><!--render facet: 1586:0--></div><div class="dashboard is-grid-1 is-grid-2-tablet is-grid-6-fullhd" data-aura-rendered-by="1762:0"><!--render facet: 4048:0--><!--render facet: 4052:0--><!--render facet: 4043:0--><!--render facet: 14401:0--><!--render facet: 14405:0--><div class="ticket ticket-bolletta bolletta-dual is-1x1 is-1x2-fullhd cACMATicket" data-aura-rendered-by="14395:0" data-aura-class="cACMATicket"><div class="ticket-header" data-aura-rendered-by="14396:0">Prossima bolletta</div><div class="ticket-content" data-aura-rendered-by="14398:0"><div class="commodity" data-aura-rendered-by="14388:0">Luce e Gas</div><div class="is-size-1 has-font-weight-500 has-font-roboto" data-aura-rendered-by="14390:0">21</div><div class="has-font-roboto is-uppercase" data-aura-rendered-by="14392:0">Dicembre</div></div></div><!--render facet: 14439:0--><!--render facet: 14443:0--><div class="ticket is-1x1 is-1x2-fullhd autolettura cACMATicket" data-aura-rendered-by="14433:0" data-aura-class="cACMATicket"><div class="ticket-header" data-aura-rendered-by="14434:0">Prossima autolettura</div><div class="ticket-content" data-aura-rendered-by="14436:0"><div class="commodity" data-aura-rendered-by="14410:0">Gas</div><div class="date-range" data-aura-rendered-by="14412:0"><div class="bill-date" data-aura-rendered-by="14413:0"><div data-aura-rendered-by="14414:0">10</div><div data-aura-rendered-by="14416:0">dic</div></div><div class="bill-type" data-aura-rendered-by="14418:0"><span class="icon is-medium" style="" data-aura-rendered-by="14421:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g fill-rule="evenodd" fill="none">
        <path d="M14 1c4.233 2.89 6.796 6.044 9.5 10.5 2.52 4.156 4.012 8.282 2 13-1.635 3.835-4.426 6.46-8.5 6.854 5.09-9.318 3.433-20.291-3.5-29.854L14 1z" fill="#A5FF9B"></path>
        <path d="M13.077 1c4.047 2.706 7.84 6.04 10.425 10.212 2.41 3.89 3.868 8.714 1.944 13.13-1.563 3.59-5.133 6.245-9.028 6.614-3.892.37-7.938-1.606-9.97-4.953-1.869-3.078-2.01-7.536.055-10.577 2.048-3.016 5.32-5.313 6.361-8.957.497-1.74.633-3.695.213-5.469z" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="#0D8100"></path>
        <path d="M10.333 25.376c0-3.105 5.868-10.443 5.868-10.443s5.866 7.338 5.866 10.443C22.067 28.482 19.44 31 16.2 31c-3.24 0-5.868-2.518-5.868-5.624z" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="#0D8100"></path>
    </g>
</svg>
</span><span class="connector" data-aura-rendered-by="14426:0"></span></div><div class="bill-date" data-aura-rendered-by="14427:0"><div data-aura-rendered-by="14428:0">14</div><div data-aura-rendered-by="14430:0">dic</div></div></div></div></div><!--render facet: 14475:0--><!--render facet: 14479:0--><!--render facet: 14469:0--><div class="widget is-bol is-active is-active is-1x1-mobile is-1x2 is-vertical-fullhd cWidget" style="" data-aura-rendered-by="14471:0" data-aura-class="cWidget"><!--render facet: 14472:0--><div class="widget-header" data-aura-rendered-by="14448:0"><span class="icon is-small" style="" data-aura-rendered-by="14452:0"><svg viewBox="0 0 32 32" height="32" width="32" xmlns="http://www.w3.org/2000/svg">
    <g fill-rule="evenodd" fill="none">
        <path d="M22.436 10.3c2.886 1.971 4.634 4.12 6.478 7.16 1.718 2.833 2.735 5.646 1.363 8.863-1.115 2.615-3.017 4.404-5.795 4.673 3.47-6.353 2.34-13.835-2.387-20.355l.341-.341z" fill="#B3E5FF"></path>
        <path d="M21.807 10.3c2.76 1.845 5.345 4.118 7.108 6.963 1.643 2.652 2.637 5.94 1.325 8.952-1.065 2.448-3.5 4.258-6.155 4.51-2.653.252-5.412-1.096-6.797-3.377-1.275-2.099-1.372-5.139.037-7.212 1.396-2.056 3.627-3.623 4.337-6.107.338-1.186.431-2.52.145-3.729z" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="#0069A7"></path>
        <path d="M19.936 26.92c0-2.117 4-7.12 4-7.12s4 5.003 4 7.12c0 2.118-1.79 3.835-4 3.835-2.208 0-4-1.717-4-3.835z" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="#0069A7"></path>
        <g>
            <path d="M11.53 14.61A7.03 7.03 0 0 0 8.892 1.06" fill="#B3E5FF"></path>
            <path d="M5.486 19.686H11.2M6.2 22.829h4.286M6 8.317h4.727M4.771 17v-2.556a7.143 7.143 0 1 1 7.143 0V17M8.343 8.971V17" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" stroke="#0069A7"></path>
        </g>
    </g>
</svg>
</span>gas e luce
        <!--render facet: 14458:0--><!--render facet: 14459:0--></div><div class="widget-icon" data-aura-rendered-by="14461:0"><figure class="image is-96x96" data-aura-rendered-by="14462:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/widget-sdd-bol.svg" data-aura-rendered-by="14463:0"></figure></div><div class="widget-content" data-aura-rendered-by="14464:0"><p data-aura-rendered-by="14466:0">Addebito diretto e Bolletta Online attivi</p></div></div></div><div class="widget-commerciali" style="min-width: 0;" data-aura-rendered-by="1770:0"><!--render facet: 1607:0--><!--render facet: 1611:0--><!--render facet: 1601:0--><div class="widget cWidget cWidgetTendrilHPQuestion" style="display:none;" data-aura-rendered-by="1603:0" data-aura-class="cWidget cWidgetTendrilHPQuestion"><!--render facet: 1604:0--><!--render facet: 1597:0--><!--render facet: 1599:0--></div><!--render facet: 1644:0--><!--render facet: 1648:0--><!--render facet: 1616:0--><!--render facet: 4226:0--><!--render facet: 4230:0--><!--render facet: 4220:0--><div class="widget cWidget cWidgetDomusCheckNotActive" style="" data-aura-rendered-by="4222:0" data-aura-class="cWidget cWidgetDomusCheckNotActive"><span class="loyalty" data-aura-rendered-by="11858:0"><span style="text-transform: capitalize;" data-aura-rendered-by="11859:0">Ottieni </span><b data-aura-rendered-by="11861:0">20 Punti LED</b></span><div class="widget-content" data-aura-rendered-by="4125:0"><!--render facet: 4202:0--><!--render facet: 4206:0--><div class="carousel carousel-animate-slide carousel-animated" data-aura-rendered-by="4197:0"><div class="carousel-container" data-aura-rendered-by="4198:0"><div class="carousel-item has-background has-background-cerulean has-border-radius" style="order:1;z-index:1;" data-aura-rendered-by="4148:0"><figure class="image" data-aura-rendered-by="4128:0"><img class="money" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/saving-blue.svg" data-aura-rendered-by="4129:0"></figure><div class="header is-transparent" data-aura-rendered-by="4130:0"><div class="columns is-marginless" data-aura-rendered-by="4131:0"><div class="column" data-aura-rendered-by="4132:0"><h1 class="has-text-white is-size-4 has-margin-bottom-small" style="font-weight:300;" data-aura-rendered-by="4133:0">
                                Domuscheck
                            </h1><a class="has-text-white is-size-6" data-aura-rendered-by="4135:0" href="javascript:void(0);">Scopri di più</a></div><div class="column" data-aura-rendered-by="4137:0"><p class="is-size-6 has-text-white" style="padding-top:0.5rem;" data-aura-rendered-by="4138:0">
                                Risparmia in bolletta con Domuscheck
                            </p></div></div></div><div class="title is-transparent" data-aura-rendered-by="4140:0"><button class="button is-white is-small is-rounded is-uppercase is-size-6 button-padding" data-aura-rendered-by="4143:0"><!--render facet: 4144:0-->Attiva ora</button><!--render facet: 4146:0--></div></div><div class="carousel-item has-background has-background-avocado-green has-border-radius" style="order:2;z-index:0;" data-aura-rendered-by="4171:0"><figure class="image" style="padding: 4rem;" data-aura-rendered-by="4151:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/domuscheck-graph-2.svg" data-aura-rendered-by="4152:0"></figure><div class="header is-transparent" data-aura-rendered-by="4153:0"><div class="columns is-marginless" data-aura-rendered-by="4154:0"><div class="column" data-aura-rendered-by="4155:0"><h1 class="has-text-white is-size-4 has-margin-bottom-small" style="font-weight:300;" data-aura-rendered-by="4156:0">
                                Domuscheck
                            </h1><a class="has-text-white is-size-6" data-aura-rendered-by="4158:0" href="javascript:void(0);">Scopri di più</a></div><div class="column" data-aura-rendered-by="4160:0"><p class="is-size-6 has-text-white" style="padding-top:0.5rem;" data-aura-rendered-by="4161:0">
                                Tieni sempre sotto controllo i tuoi consumi
                            </p></div></div></div><div class="title is-transparent" data-aura-rendered-by="4163:0"><button class="button is-white is-small is-rounded is-uppercase is-size-6 button-padding" data-aura-rendered-by="4166:0"><!--render facet: 4167:0-->Attiva ora</button><!--render facet: 4169:0--></div></div><div class="carousel-item has-background has-background-tealish has-border-radius" style="order:0;z-index:0;" data-aura-rendered-by="4194:0"><figure class="image" data-aura-rendered-by="4174:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/domuscheck-donut-icons.svg" data-aura-rendered-by="4175:0"></figure><div class="header is-transparent" data-aura-rendered-by="4176:0"><div class="columns is-marginless" data-aura-rendered-by="4177:0"><div class="column" data-aura-rendered-by="4178:0"><h1 class="has-text-white is-size-4 has-margin-bottom-small" style="font-weight:300;" data-aura-rendered-by="4179:0">
                                Domuscheck
                            </h1><a class="has-text-white is-size-6" data-aura-rendered-by="4181:0" href="javascript:void(0);">Scopri di più</a></div><div class="column" data-aura-rendered-by="4183:0"><p class="is-size-6 has-text-white" style="padding-top:0.5rem;" data-aura-rendered-by="4184:0">
                                Scopri dove consumi più energia
                            </p></div></div></div><div class="title is-transparent" data-aura-rendered-by="4186:0"><button class="button is-white is-small is-rounded is-uppercase is-size-6 button-padding" data-aura-rendered-by="4189:0"><!--render facet: 4190:0-->Attiva ora</button><!--render facet: 4192:0--></div></div></div><div class="carousel-navigation is-overlay" data-aura-rendered-by="4212:0"><div class="carousel-nav-left" data-aura-rendered-by="4213:0"><i class="material-icons" data-aura-rendered-by="4214:0">keyboard_arrow_left</i></div><div class="carousel-nav-right" data-aura-rendered-by="4216:0"><i class="material-icons" data-aura-rendered-by="4217:0">keyboard_arrow_right</i></div></div></div></div></div><!--render facet: 1692:0--><!--render facet: 1696:0--><!--render facet: 1686:0--><div class="widget cWidget cWidgetTendrilChallenges" style="display:none;" data-aura-rendered-by="1688:0" data-aura-class="cWidget cWidgetTendrilChallenges"><!--render facet: 1689:0--><div class="widget-header" data-aura-rendered-by="1653:0"><span class="icon is-medium" style="" data-aura-rendered-by="1656:0"><!--render facet: 1657:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/pillole.svg" style="" data-aura-rendered-by="1659:0"><!--render facet: 1660:0--></span><span class="has-text-slate is-size-7 is-uppercase is-family-monospace" data-aura-rendered-by="1661:0">le pillole di engie</span></div><div class="widget-content" data-aura-rendered-by="1663:0"><!--render facet: 1677:0--><!--render facet: 1681:0--><div class="carousel carousel-animate-slide carousel-animated is-fullwdith" data-aura-rendered-by="1672:0"><div class="carousel-container" data-aura-rendered-by="1673:0"><div class="carousel-item has-background has-text-slate" style="order:0;z-index:0;" data-aura-rendered-by="1669:0"><!--render facet: 1670:0--></div></div><!--render facet: 1675:0--></div></div></div><!--render facet: 1755:0--><!--render facet: 1759:0--><!--render facet: 1749:0--><div class="widget is-xselling cWidget" style="" data-aura-rendered-by="1751:0" data-aura-class="cWidget"><!--render facet: 1752:0--><!--render facet: 1732:0--><!--render facet: 1736:0--><div class="carousel carousel-animate-slide carousel-animated" data-aura-rendered-by="1727:0"><div class="carousel-container" data-aura-rendered-by="1728:0"><div class="carousel-item has-background" style="order:1;z-index:1;" data-aura-rendered-by="1712:0"><img class="is-background" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/xselling-02-Jillcover.png" data-aura-rendered-by="1703:0"><div class="title is-transparent" data-aura-rendered-by="1704:0"><button class="button is-primary is-rounded" data-aura-rendered-by="1707:0" data-name="fotovoltaico"><!--render facet: 1708:0-->Scopri l'offerta</button><!--render facet: 1710:0--></div></div><div class="carousel-item has-background" style="order:0;z-index:0;" data-aura-rendered-by="1724:0"><img class="is-background" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/xselling-03-supermanutenzione.png" data-aura-rendered-by="1715:0"><div class="title is-transparent" data-aura-rendered-by="1716:0"><button class="button is-primary is-rounded" data-aura-rendered-by="1719:0" data-name="manutenzione"><!--render facet: 1720:0-->Scopri l'offerta</button><!--render facet: 1722:0--></div></div></div><div class="carousel-navigation is-overlay" data-aura-rendered-by="1741:0"><div class="carousel-nav-left" data-aura-rendered-by="1742:0"><i class="material-icons" data-aura-rendered-by="1743:0">keyboard_arrow_left</i></div><div class="carousel-nav-right" data-aura-rendered-by="1745:0"><i class="material-icons" data-aura-rendered-by="1746:0">keyboard_arrow_right</i></div></div></div></div></div><div class="timeline-column has-background-pattern-lmt001-widescreen" data-aura-rendered-by="1772:0"><!--render facet: 1793:0--><!--render facet: 1797:0--><!--render facet: 1781:0--><div class="timeline is-slim has-padding-top-xlarge-widescreen has-padding-right-large-widescreen" style="display:block;" data-aura-rendered-by="1783:0"><header class="timeline-header" data-aura-rendered-by="1784:0"><span class="has-text-large" data-aura-rendered-by="1785:0">Oggi, 9 Dicembre</span></header><!--render facet: 12111:0--><!--render facet: 12115:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12068:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12069:0"><div class="timeline-label" data-aura-rendered-by="12070:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="12118:0">Bolletta Luce &amp; Gas €215,92 pagata</span><span data-aura-rendered-by="12120:0">10 novembre 2020</span></div><span class="icon" style="" data-aura-rendered-by="12138:0"><!--render facet: 12139:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="12141:0"><!--render facet: 12142:0--></span></div><!--render facet: 12073:0--></div><!--render facet: 12295:0--><!--render facet: 12299:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12252:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12253:0"><div class="timeline-label" data-aura-rendered-by="12254:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="12302:0">Bolletta Luce &amp; Gas €153,62 pagata</span><span data-aura-rendered-by="12304:0">08 settembre 2020</span></div><span class="icon" style="" data-aura-rendered-by="12322:0"><!--render facet: 12323:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="12325:0"><!--render facet: 12326:0--></span></div><!--render facet: 12257:0--></div><!--render facet: 12479:0--><!--render facet: 12483:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12436:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12437:0"><div class="timeline-label" data-aura-rendered-by="12438:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="12486:0">Bolletta Luce &amp; Gas €219,80 pagata</span><span data-aura-rendered-by="12488:0">30 giugno 2020</span></div><span class="icon" style="" data-aura-rendered-by="12506:0"><!--render facet: 12507:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="12509:0"><!--render facet: 12510:0--></span></div><!--render facet: 12441:0--></div><!--render facet: 12663:0--><!--render facet: 12667:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12620:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12621:0"><div class="timeline-label" data-aura-rendered-by="12622:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="12670:0">Bolletta Luce &amp; Gas €166,68 pagata</span><span data-aura-rendered-by="12672:0">22 maggio 2020</span></div><span class="icon" style="" data-aura-rendered-by="12690:0"><!--render facet: 12691:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="12693:0"><!--render facet: 12694:0--></span></div><!--render facet: 12625:0--></div><!--render facet: 12847:0--><!--render facet: 12851:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12804:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12805:0"><div class="timeline-label" data-aura-rendered-by="12806:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="12854:0">Bolletta Luce &amp; Gas €170,78 pagata</span><span data-aura-rendered-by="12856:0">10 marzo 2020</span></div><span class="icon" style="" data-aura-rendered-by="12874:0"><!--render facet: 12875:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="12877:0"><!--render facet: 12878:0--></span></div><!--render facet: 12809:0--></div><!--render facet: 13031:0--><!--render facet: 13035:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="12988:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="12989:0"><div class="timeline-label" data-aura-rendered-by="12990:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13038:0">Bolletta Luce &amp; Gas €177,89 pagata</span><span data-aura-rendered-by="13040:0">21 gennaio 2020</span></div><span class="icon" style="" data-aura-rendered-by="13058:0"><!--render facet: 13059:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13061:0"><!--render facet: 13062:0--></span></div><!--render facet: 12993:0--></div><!--render facet: 13215:0--><!--render facet: 13219:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="13172:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="13173:0"><div class="timeline-label" data-aura-rendered-by="13174:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13222:0">Bolletta Luce &amp; Gas €194,04 pagata</span><span data-aura-rendered-by="13224:0">05 novembre 2019</span></div><span class="icon" style="" data-aura-rendered-by="13242:0"><!--render facet: 13243:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13245:0"><!--render facet: 13246:0--></span></div><!--render facet: 13177:0--></div><!--render facet: 13399:0--><!--render facet: 13403:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="13356:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="13357:0"><div class="timeline-label" data-aura-rendered-by="13358:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13406:0">Bolletta Luce &amp; Gas €144,44 pagata</span><span data-aura-rendered-by="13408:0">03 settembre 2019</span></div><span class="icon" style="" data-aura-rendered-by="13426:0"><!--render facet: 13427:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13429:0"><!--render facet: 13430:0--></span></div><!--render facet: 13361:0--></div><!--render facet: 13583:0--><!--render facet: 13587:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="13540:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="13541:0"><div class="timeline-label" data-aura-rendered-by="13542:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13590:0">Bolletta Luce &amp; Gas €189,89 pagata</span><span data-aura-rendered-by="13592:0">02 luglio 2019</span></div><span class="icon" style="" data-aura-rendered-by="13610:0"><!--render facet: 13611:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13613:0"><!--render facet: 13614:0--></span></div><!--render facet: 13545:0--></div><!--render facet: 13767:0--><!--render facet: 13771:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="13724:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="13725:0"><div class="timeline-label" data-aura-rendered-by="13726:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13774:0">Bolletta Luce &amp; Gas €166,47 pagata</span><span data-aura-rendered-by="13776:0">07 maggio 2019</span></div><span class="icon" style="" data-aura-rendered-by="13794:0"><!--render facet: 13795:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13797:0"><!--render facet: 13798:0--></span></div><!--render facet: 13729:0--></div><!--render facet: 13951:0--><!--render facet: 13955:0--><div class="timeline-item is-closed is-bill cTimelineItem" data-aura-rendered-by="13908:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="13909:0"><div class="timeline-label" data-aura-rendered-by="13910:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="13958:0">Bolletta Luce &amp; Gas €210,83 pagata</span><span data-aura-rendered-by="13960:0">12 marzo 2019</span></div><span class="icon" style="" data-aura-rendered-by="13978:0"><!--render facet: 13979:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/bolletta-light.svg" style="" data-aura-rendered-by="13981:0"><!--render facet: 13982:0--></span></div><!--render facet: 13913:0--></div><!--render facet: 14026:0--><!--render facet: 14030:0--><div class="timeline-item is-closed is-acma-reg cTimelineItem" data-aura-rendered-by="14012:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="14013:0"><div class="timeline-label" data-aura-rendered-by="14014:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="14033:0">Iscrizione Spazio Clienti</span><span data-aura-rendered-by="14035:0">01 luglio 2014</span></div><span class="icon" style="" data-aura-rendered-by="14039:0"><!--render facet: 14040:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/welcome-light.svg" style="" data-aura-rendered-by="14042:0"><!--render facet: 14043:0--></span></div><!--render facet: 14017:0--></div><!--render facet: 14152:0--><!--render facet: 14156:0--><div class="timeline-item is-closed is-switchin cTimelineItem" data-aura-rendered-by="14114:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="14115:0"><div class="timeline-label" data-aura-rendered-by="14116:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="14159:0">Fornitura Luce attivata</span><span data-aura-rendered-by="14161:0">01 maggio 2010</span></div><span class="icon" style="" data-aura-rendered-by="14189:0"><!--render facet: 14190:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/nuova-fornitura-light.svg" style="" data-aura-rendered-by="14192:0"><!--render facet: 14193:0--></span></div><!--render facet: 14119:0--></div><!--render facet: 14302:0--><!--render facet: 14306:0--><div class="timeline-item is-closed is-last is-switchin cTimelineItem" data-aura-rendered-by="14264:0" data-aura-class="cTimelineItem"><div class="timeline-marker is-image" data-aura-rendered-by="14265:0"><div class="timeline-label" data-aura-rendered-by="14266:0"><span class="timeline-label-header is-uppercase has-text-charcoal-grey is-family-monospace is-size-7 has-text-weight-normal" data-aura-rendered-by="14309:0">Fornitura Gas attivata</span><span data-aura-rendered-by="14311:0">01 maggio 2010</span></div><span class="icon" style="" data-aura-rendered-by="14339:0"><!--render facet: 14340:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/nuova-fornitura-light.svg" style="" data-aura-rendered-by="14342:0"><!--render facet: 14343:0--></span></div><!--render facet: 14269:0--></div><!--render facet: 9519:0--><!--render facet: 9703:0--><!--render facet: 9887:0--><!--render facet: 10071:0--><!--render facet: 10255:0--><!--render facet: 10439:0--><!--render facet: 10623:0--><!--render facet: 10807:0--><!--render facet: 10991:0--><!--render facet: 11175:0--><!--render facet: 11359:0--><!--render facet: 11434:0--><!--render facet: 11560:0--><!--render facet: 11710:0--><!--render facet: 6954:0--><!--render facet: 7138:0--><!--render facet: 7322:0--><!--render facet: 7506:0--><!--render facet: 7690:0--><!--render facet: 7874:0--><!--render facet: 8058:0--><!--render facet: 8242:0--><!--render facet: 8426:0--><!--render facet: 8610:0--><!--render facet: 8794:0--><!--render facet: 8869:0--><!--render facet: 8995:0--><!--render facet: 9145:0--><!--render facet: 4389:0--><!--render facet: 4573:0--><!--render facet: 4757:0--><!--render facet: 4941:0--><!--render facet: 5125:0--><!--render facet: 5309:0--><!--render facet: 5493:0--><!--render facet: 5677:0--><!--render facet: 5861:0--><!--render facet: 6045:0--><!--render facet: 6229:0--><!--render facet: 6304:0--><!--render facet: 6430:0--><!--render facet: 6580:0--><!--render facet: 1788:0--><div class="timeline-cta" style="text-align: center;" data-aura-rendered-by="1790:0"><button class="button is-primary is-rounded is-outlined" data-aura-rendered-by="1776:0"><!--render facet: 1777:0-->Vedi la tua storia</button><!--render facet: 1779:0--></div></div></div><!--render facet: 1868:0--><!--render facet: 1872:0--><footer class="footer cACMAFooter" data-aura-rendered-by="1805:0" data-aura-class="cACMAFooter"><div class="columns" data-aura-rendered-by="1806:0"><div class="column" data-aura-rendered-by="1807:0"><ul class="menu-list is-size-9" data-aura-rendered-by="1808:0"><li data-aura-rendered-by="1809:0"><span class="icon is-large" style="" data-aura-rendered-by="1812:0"><!--render facet: 1813:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/heart-footer.svg" style="" data-aura-rendered-by="1815:0"><!--render facet: 1816:0--></span></li><li data-aura-rendered-by="1817:0"><a data-target="assistenza" data-aura-rendered-by="1818:0" href="javascript:void(0);">Assistenza</a></li><li data-aura-rendered-by="1820:0"><a data-target="contatti" data-aura-rendered-by="1821:0" href="javascript:void(0);">Contatti</a></li><li data-aura-rendered-by="1823:0"><a href="https://casa.engie.it/contatti#faq" target="_blank" data-aura-rendered-by="1824:0">Domande frequenti</a></li></ul></div><div class="column menu" data-aura-rendered-by="1826:0"><ul class="menu-list is-size-9" data-aura-rendered-by="1827:0"><li data-aura-rendered-by="1828:0"><span class="icon is-large" style="" data-aura-rendered-by="1831:0"><!--render facet: 1832:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/docs-footer.svg" style="" data-aura-rendered-by="1834:0"><!--render facet: 1835:0--></span></li><li data-aura-rendered-by="1836:0"><a href="https://casa.engie.it/privacy#subguidelines_11" target="_blank" data-aura-rendered-by="1837:0">Privacy</a></li><li data-aura-rendered-by="1839:0"><a href="https://web-gdfsuezenergieb2c.force.com/acma/resource/1571900087000/informativa/Condizioni_Generali_Registrazione_Spazio_Clienti_23112018.pdf" target="_blank" data-aura-rendered-by="1840:0">
                            Condizioni generali e informativa Spazio Clienti
                        </a></li><li data-aura-rendered-by="1842:0"><a href="https://casa.engie.it/privacy#subguidelines_18" target="_blank" data-aura-rendered-by="1843:0">Cookie policy</a></li></ul></div><div class="column" data-aura-rendered-by="1845:0"><ul class="menu-list is-size-9" data-aura-rendered-by="1846:0"><li data-aura-rendered-by="1847:0"><span class="icon is-large" style="" data-aura-rendered-by="1850:0"><!--render facet: 1851:0--><img class="" src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/icons/phone-footer.svg" style="" data-aura-rendered-by="1853:0"><!--render facet: 1854:0--></span></li><li class="has-text-slate" data-aura-rendered-by="1855:0">Contatto commerciale</li><li data-aura-rendered-by="1857:0"><a class="button is-primary is-rounded has-text-white is-small" href="tel:800901199" style="width:7rem;margin:1rem auto 0;" data-aura-rendered-by="1858:0">
                            800 90 11 99
                        </a></li></ul></div></div><hr data-aura-rendered-by="1860:0"><div class="columns" data-aura-rendered-by="1861:0"><div class="column is-centered has-text-centered" data-aura-rendered-by="1862:0"><figure class="image has-margin-top has-margin-bottom-small" data-aura-rendered-by="1863:0"><img src="https://web-gdfsuezenergieb2c.force.com/acma/resource/1596784269000/acma/img/logo-grey.svg" width="0" data-aura-rendered-by="1864:0"></figure><p class="has-text-slate is-size-10" style="opacity:0.5;" data-aura-rendered-by="1865:0">© ENGIE ITALIA S.p.A. - P. IVA 06289781004</p></div></div></footer></section><!--render facet: 2752:0--><!--render facet: 3277:0--><!--render facet: 3412:0--><!--render facet: 3521:0--><!--render facet: 3799:0--><!--render facet: 3959:0--><!--unrender facet: 388:0--></section></div>




            <script type="text/javascript">
                var cookieName = 'apex__rememberMe';
                //>>EGIMNT-1934
                // function setCookie(name, value, days) {
                //     var d = new Date();
                //     d.setTime(new Date().getTime() + (days*24*60*60*1000));
                //     var expires = days ? "; expires=" + d.toUTCString() : "";
                //     document.cookie = name + '=' + encodeURIComponent(value || '') + expires + '; path=/';
                // }
                //
                // /*function eraseCookie(name) {
                //     document.cookie = name + '=; path=/; domain=' + window.location.host + '; expires=Thu, 01 Jan 1970 00:00:01 GMT; Max-Age: -999999999';
                // }*/
                //
                // function getCookie(name) {
                //     var cookie = document.cookie.split(';')
                //         .map(function(c) {
                //             var t = c.split('=');
                //             return { name: t[0].trim(), value: t.slice(1).join('=')};
                //         }).filter(function(c) {
                //             return c.name === name;
                //         })[0];
                //     return cookie ? cookie.value : null;
                // }
                //<<EGIMNT-1934

                window.addEventListener('message', function(evt) {
                    if (evt.origin !== window.location.origin) return;
                    try {
                        var data = JSON.parse(evt.data) || {};
                        if (data.action === 'LOGOUT') {
                            setCookie(cookieName,'',-1);
                            window.location = '/acma/secur/logout.jsp';
                            if(!!data.url){
                                window.top.location = data.url;
                            }
                        }
                        if (data.action === 'GO_BACK_ECRM') {
                            window.top.location = 'https://imprese.engie.it/login-fox/-/ssoecrm/preferences?p_auth=ZDxKp5k7&_CustomerLogin_WAR_CustomerLoginportlet_surl=https%3A%2F%2Fweb-gdfsuezenergieb2c.force.com%2FeCRM%2FSiteAR';
                        }
                        if (data.action === 'OPEN_CHAT') {
                            window.top.postMessage({ evento: 'apri-chat-biz' }, '*');
                        }
                        if (data.action === 'ANALYTICS') {
                            dataLayer.push (
                                { 'event': data.event,
                                 'EventCategory': data.EventCategory,
                                 'EventAction': data.EventAction,
                                 'EventLabel':data.EventLabel,
                                 'Version': data.Version
                                }
                            );
                        }
                        if (data.action === 'OPEN_TOP') {
                            window.top.location = data.url;
                        }
                        if (data.action === 'OPEN_WINDOW') {
                            const win = window.open(data.url, '_blank');
                            if (win && win.focus) {
                              win.focus();
                            }
                        }
                    } catch(err) {
                        console.log('error parsing json', err);
                    }
                }, false);

                $Lightning.use('c:ACMAApp', function() {
                    $Lightning.createComponent(
                        'c:ACMARoot',
                        {

                            type: (__data__.info || { isPersonAccount: true }).isPersonAccount ? 'RES' : 'BIZ',
                            //type: ("" || 'RES').toUpperCase(),    // RES or BIZ
                            typeBIZ:(__data__.info || { typeBIZ:'' }).typeBIZ,
                            origin: window.location.origin, // serve per passare il post message
                            data: __data__,
                            notification: __data__.notifications,
                        },
                        'root',
                        function(cmp) {
                            window.cmp = cmp;
                        }
                    );
                });
//            >>EGIMNT-1948
            function notifyAnalytics(EventLabel){
                    let payloadPM = {
                        action: 'ANALYTICS',
                        event:'Spazio clienti',
                        EventCategory: 'Spazio clienti',
                        EventAction:'Banner App',
                        EventLabel: EventLabel,

                };
                console.log('notifyAnalytics -->', JSON.stringify(payloadPM));
                window.postMessage(JSON.stringify(payloadPM), window.location.origin);
                return true;
            }
//            >>EGIMNT-1948
            </script>

            
            <script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function()

{(h.hj.q=h.hj.q||[]).push(arguments)}
;
h._hjSettings={hjid:925122,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
            </script>

            
            <style>

/*BANNER APP DESKTOP*/
.banner-app{
	text-align: center;
	background-color: #425b67;
	color: white;
	position: relative;
	}
	.banner-app-text{
		margin: 0 30px;
		font-size: 14px;
	}
	.banner-app-icon{
		width: 31px;
		height: 31px;
		margin: 14px 0px 14px 0;
		border-radius: 5px;
		vertical-align:middle !important;
	}
	.banner-app-cta{
		border:1px solid white;
		border-radius:18px;
		font-size: 10px;
		padding:7px 14px;
		cursor: pointer;
	}

	.banner-app-cta a{
		color:white !important;
	}
	.banner-app-cta a:hover{
		color: #425b67 !important;
		text-decoration:none !important;
	}
	.banner-app-cta:hover{
		background-color: white;
		color: #425b67;
	}
	.banner-app-close{
		position: absolute;
	    right: 10px;
	    cursor:pointer;
	    line-height: 10px;
	    font-size:10px;
	    top: calc(50% - 5px);
	}


            </style>
            <script>
                function closeBannerApp(){
                     document.getElementById('banner-app-desktop').style.display='none';
                     return false;
                }
            </script></span>




        
    <script type="text/javascript" id="">var device,previousDevice,addClass,documentElement,find,handleOrientation,hasClass,orientationEvent,removeClass,userAgent;previousDevice=window.device;device={};window.device=device;documentElement=window.document.documentElement;userAgent=window.navigator.userAgent.toLowerCase();device.ios=function(){return device.iphone()||device.ipod()||device.ipad()};device.iphone=function(){return!device.windows()&&find("iphone")};device.ipod=function(){return find("ipod")};device.ipad=function(){return find("ipad")};
device.android=function(){return!device.windows()&&find("android")};device.androidPhone=function(){return device.android()&&find("mobile")};device.androidTablet=function(){return device.android()&&!find("mobile")};device.blackberry=function(){return find("blackberry")||find("bb10")||find("rim")};device.blackberryPhone=function(){return device.blackberry()&&!find("tablet")};device.blackberryTablet=function(){return device.blackberry()&&find("tablet")};device.windows=function(){return find("windows")};
device.windowsPhone=function(){return device.windows()&&find("phone")};device.windowsTablet=function(){return device.windows()&&find("touch")&&!device.windowsPhone()};device.fxos=function(){return(find("(mobile;")||find("(tablet;"))&&find("; rv:")};device.fxosPhone=function(){return device.fxos()&&find("mobile")};device.fxosTablet=function(){return device.fxos()&&find("tablet")};device.meego=function(){return find("meego")};device.cordova=function(){return window.cordova&&"file:"===location.protocol};
device.nodeWebkit=function(){return"object"===typeof window.process};device.mobile=function(){return device.androidPhone()||device.iphone()||device.ipod()||device.windowsPhone()||device.blackberryPhone()||device.fxosPhone()||device.meego()};device.tablet=function(){return device.ipad()||device.androidTablet()||device.blackberryTablet()||device.windowsTablet()||device.fxosTablet()};device.desktop=function(){return!device.tablet()&&!device.mobile()};
device.noConflict=function(){window.device=previousDevice;return this};find=function(a){return-1!==userAgent.indexOf(a)};hasClass=function(a){a=new RegExp(a,"i");return documentElement.className.match(a)};addClass=function(a){return a};removeClass=function(a){hasClass(a)&&(documentElement.className=documentElement.className.replace(" "+a,""))};</script> <script type="text/javascript" id="">(function(a,c,e,f,d,b){a.hj=a.hj||function(){(a.hj.q=a.hj.q||[]).push(arguments)};a._hjSettings={hjid:1486967,hjsv:6};d=c.getElementsByTagName("head")[0];b=c.createElement("script");b.async=1;b.src=e+a._hjSettings.hjid+f+a._hjSettings.hjsv;d.appendChild(b)})(window,document,"https://static.hotjar.com/c/hotjar-",".js?sv\x3d");</script><iframe name="_hjRemoteVarsFrame" title="_hjRemoteVarsFrame" id="_hjRemoteVarsFrame" src="https://vars.hotjar.com/box-469cf41adb11dc78be68c1ae7f9457a4.html" style="display: none !important; width: 1px !important; height: 1px !important; opacity: 0 !important; pointer-events: none !important;"></iframe><div id="auraErrorMessage"></div><!--render facet: 1:0--></body></html>`
