package engieitalia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/browser"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/input"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

const (
	dahsboard     = "https://casa.engie.it/spazio-clienti?content=ACMADashboard"
	inputUsername = `document.querySelector("#auth > div > div > div > div > div > section > form > div:nth-child(1) > div > input")` //`input[name="nome utente o Email"]`
	inputPass     = `//*[@id="auth"]/div/div/div/div/div/section/form/div[2]/div/input`
	btnLogin      = `/html/body/span/div/div/div/div/div/div/section/form/div[4]/div[1]/button`

	loginErrMsg = "todo"
	iframeRegex = `iframe id="resizable_iframe" src="(.+)"`
	siteFormat  = `2006-01-02`

	recapthaErr  = `Attenzione: è stato rilevato un tentativo di accesso anomalo per cui non è stato possibile completare l'operazione`
	downloadPath = "/var/log/pago/pago-engie-italia-crawler-go/downloads"
)

var (
	contractRegex = regexp.MustCompile(`contractChains:\s*(.+),\s*notifications:`)
	invoicesRegex = regexp.MustCompile(`bills:\s*{\s*data:\s*(.+),\s*pending`)
)

func Init() {
	factory := new(EngieItaliaFactory)
	crawl.RegisterFactory("engie_italia.crawler", factory)
}

type EngieItaliaFactory struct{}

func (p *EngieItaliaFactory) NewStrategy(log *logrus.Entry) crawl.Strategy {
	l := log.WithField("type", "EngieItaliaChromeStrategy")
	return &EngieItaliaChromeStrategy{
		Entry:           l,
		ChromeDpCrawler: browser.New(log.WithField("type", "ChromeDpCrawler"), true),
	}
}

type EngieItaliaChromeStrategy struct {
	*logrus.Entry
	*browser.ChromeDpCrawler
	mainPage, billPage string
}

func (s *EngieItaliaChromeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EngieItaliaChromeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) Login(acc *model.Account) (*string, crawl.Exception) {
	rand.Seed(time.Now().Unix())

	err := s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(dahsboard),
		chromedp.Sleep(time.Duration(4000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(err.Error())
	}
	err = s.Run(
		chromedp.WaitReady(`iframe[id='resizable_iframe']`),
	)
	if err != nil {
		return nil, crawl.ParseErr("username input: " + err.Error())
	}
	html := ""
	err = s.Run(chromedp.OuterHTML("html", &html))
	if err != nil {
		return nil, crawl.ParseErr("parsing html for login page: " + err.Error())
	}
	if html == "" {
		return nil, crawl.ParseErr("parsing html for login page")
	}
	loginDoc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if loginDoc == nil {
		return nil, crawl.ParseErr("parsing html for login page")
	}
	src, ex := crawl.Attr(loginDoc.Find(`iframe[id='resizable_iframe']`), "src")
	if ex != nil {
		return nil, ex
	}

	err = s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(src),
		chromedp.Sleep(time.Duration(3000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ConnectionErr(err.Error())
	}
	err = s.Run(
		chromedp.WaitVisible(`input[name='nome utente o Email']`),
	)
	if err != nil {
		return nil, crawl.ParseErr("username input: " + err.Error())
	}
	err = s.Run(
		chromedp.SendKeys(`input[name='nome utente o Email']`, acc.Username),
		chromedp.Sleep(time.Duration(rand.Intn(400))*time.Millisecond),
		chromedp.SendKeys(`input[type='password']`, *acc.Password),
		chromedp.Sleep(time.Duration(rand.Intn(400))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ParseErr("username/password input: " + err.Error())
	}

	var nodes []*cdp.Node
	err = s.Run(chromedp.Nodes(`button[class='button is-primary is-rounded is-uppercase is-fullwidth']`, &nodes, chromedp.ByQueryAll))
	if err != nil {
		return nil, crawl.ParseErr("username/password input: " + err.Error())
	}
	if len(nodes) == 0 {
		return nil, crawl.ParseErr("login btn not found")
	}
	box, err := s.GetBoxModel(nodes[0])
	if err != nil {
		return nil, crawl.ParseErr("login btn not found")
	}

	err = s.Run(
		chromedp.MouseEvent(input.MouseMoved,
			box.Border[0]+float64(box.Width)*(1.5+rand.Float64()/2),
			box.Border[1]+float64(box.Height)*(3.0+rand.Float64())),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
		chromedp.MouseEvent(input.MouseMoved,
			box.Border[0]+float64(box.Width)/(2.0+rand.Float64()),
			box.Border[1]+float64(box.Height)/(2.0+rand.Float64())),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
		chromedp.Click(`button[class='button is-primary is-rounded is-uppercase is-fullwidth']`),
		chromedp.Sleep(time.Duration(2000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return nil, crawl.ParseErr("login btn: " + err.Error())
	}

	errMsg := ""
	err = s.RunWithTimeout(2*time.Second,
		chromedp.WaitVisible(`p[class='help is-danger']`),
		chromedp.Text(`p[class='help is-danger']`, &errMsg))
	if err != browser.ErrTimeout {
		if err == nil && errMsg != "" {
			if strings.Contains(errMsg, recapthaErr) {
				return nil, crawl.ConnectionErr(errMsg)
			}
			return nil, crawl.LoginErr(errMsg)
		} else {
			return nil, crawl.ConnectionErr("waiting for login error msg")
		}
	}
	return nil, nil
}

func (s *EngieItaliaChromeStrategy) CheckLogin(response *string) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) LoadLocations(account *model.Account) crawl.Exception {
	defer func() {
		s.ChromeDpCrawler.CancelFuncs()
	}()
	if ex := s.parsePages(); ex != nil {
		return ex
	}
	if ex := s.parseHtml(account, &s.mainPage); ex != nil {
		return ex
	}
	return nil
}

func (s *EngieItaliaChromeStrategy) parseHtml(account *model.Account, html *string) crawl.Exception {
	if ex := s.parseLocations(account, html); ex != nil {
		return ex
	}
	if ex := s.parseInvoices(account, html); ex != nil {
		return ex
	}
	return nil
}

func (s *EngieItaliaChromeStrategy) parseLocations(account *model.Account, html *string) crawl.Exception {
	matches, err := utils.MatchRegex(*html, contractRegex)
	if err != nil {
		return crawl.ParseErr("parsing contracts regex: " + err.Error())
	}
	contracts := matches[0][1]
	s.Debug("contracts:", contracts)

	var contractsJson map[string]struct {
		IndirizzoFornitura string `json:"indirizzoFornitura"`
	}
	err = utils.FromJSON(&contractsJson, strings.NewReader(contracts))
	if err != nil {
		return crawl.ParseErr("parse contraicts json: " + err.Error())
	}
	for k, v := range contractsJson {
		l := &model.Location{
			Service:    v.IndirizzoFornitura,
			Identifier: k,
		}
		account.AddLocation(l)
	}
	return nil
}

func (s *EngieItaliaChromeStrategy) parseInvoices(account *model.Account, html *string) crawl.Exception {
	matches, err := utils.MatchRegex(*html, invoicesRegex)
	if err != nil {
		return crawl.ParseErr("parsing contracts regex: " + err.Error())
	}
	invoices := matches[0][1]
	s.Debug("invoices:", invoices)

	var invoicesJson []*struct {
		Urlpdfdownload string `json:"urlpdfdownload"`
		CodContr       string `json:"codContr"`
		NumFisc        string `json:"numFisc"`
		Importo        string `json:"importo"`
		DataEmiss      string `json:"dataEmiss"`
		DataScad       string `json:"dataScad"`
		Pagata         string `json:"pagata"`
		DataPag        string `json:"dataPag"`
	}
	err = utils.FromJSON(&invoicesJson, strings.NewReader(invoices))
	if err != nil {
		return crawl.ParseErr("parse invoices json: " + err.Error())
	}
	for _, i := range invoicesJson {
		issueDate, err := time.Parse(siteFormat, i.DataEmiss)
		if err != nil {
			return crawl.ParseErr("parse issue date: " + err.Error())
		}
		dueDate, err := time.Parse(siteFormat, i.DataScad)
		if err != nil {
			return crawl.ParseErr("parse due date: " + err.Error())
		}
		amt, ex := crawl.AmountFromString(i.Importo)
		if ex != nil {
			return ex
		}
		amtDue := 0.0
		if i.Pagata != "y" {
			amtDue = amt
		}
		inv := &model.Invoice{
			Ref:       i.NumFisc,
			PdfUri:    &i.Urlpdfdownload,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		l := s.getLocation(account.Locations, i.CodContr)
		if l != nil {
			l.AddInvoice(inv)
		} else {
			return crawl.ParseErr("no location previously parsed for code " + i.CodContr)
		}
		var payDate time.Time
		if i.Pagata == "y" && i.DataPag != "" {
			payDate, err = time.Parse(siteFormat, i.DataPag)
			if err != nil {
				return crawl.ParseErr("parse pay date: " + err.Error())
			}
			p := &model.Payment{
				Ref:    i.NumFisc,
				Amount: amt,
				Date:   payDate.Format(utils.DBDateFormat),
			}
			l.AddPayment(p)
		}
	}
	return nil
}

func (s *EngieItaliaChromeStrategy) getLocation(locations []*model.Location, contr string) *model.Location {
	for _, location := range locations {
		if contr == location.Identifier {
			return location
		}
	}
	return nil
}

func (s *EngieItaliaChromeStrategy) parsePages() crawl.Exception {
	err := s.Run(chromedp.WaitVisible(`span[class='commodity-code']`))
	if err != nil {
		return crawl.ConnectionErr("reaching main page: " + err.Error())
	}
	err = s.Run(chromedp.OuterHTML("html", &s.mainPage))
	if err != nil {
		return crawl.ParseErr("parsing main page: " + err.Error())
	}
	//err = s.Run(
	//	chromedp.WaitVisible(`span:containsOwn('Bollette')`),
	//	chromedp.Sleep(300*time.Millisecond),
	//	chromedp.Click(`span:containsOwn('Bollette')`),
	//)
	//if err != nil {
	//	return crawl.ParseErr("invoice page link: " + err.Error())
	//}
	return nil
}

func (s *EngieItaliaChromeStrategy) LoadAmount(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) LoadInvoices(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) LoadPayments(a *model.Account, l *model.Location) crawl.Exception {
	return nil
}

func (s *EngieItaliaChromeStrategy) Pdf(a *model.Account) *model.PdfResponse {
	defer func() {
		s.ChromeDpCrawler.CancelFuncs()
	}()

	ref := a.Locations[0].Invoices[0].Ref
	pdf, err := s.CheckDownloads(downloadPath, ref, s.Entry)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	if len(pdf) > 0 {
		return &model.PdfResponse{
			Content:   pdf,
			PdfStatus: model.OK.Name,
		}
	}

	a.Locations = make([]*model.Location, 0, 0)
	if ex := s.parsePages(); ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	if ex := s.parseHtml(a, &s.mainPage); ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	pdfUri := ""
	for _, location := range a.Locations {
		for _, invoice := range location.Invoices {
			if ref == invoice.Ref {
				pdfUri = *invoice.PdfUri
			}
		}
	}
	if pdfUri == "" {
		return model.PdfErrorResponse("invoice not found for ref="+ref, model.OTHER_EXCEPTION)
	}
	err = s.Run(page.SetDownloadBehavior(page.SetDownloadBehaviorBehaviorAllow).WithDownloadPath(downloadPath))
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	err = s.Run(
		s.SetUserAgentChrome(),
		chromedp.Navigate(pdfUri),
		chromedp.Sleep(time.Duration(4000+rand.Intn(1000))*time.Millisecond),
	)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	var html = ""
	err = s.Run(chromedp.OuterHTML("html", &html))

	err = s.RunWithTimeout(time.Second,
		chromedp.WaitVisible(`cr-icon-button[id='download']`),
		chromedp.Click(`cr-icon-button[id='download']`),
	)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}

	pdf, err = s.CheckDownloads(downloadPath, ref, s.Entry)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	if len(pdf) > 0 {
		return &model.PdfResponse{
			Content:   pdf,
			PdfStatus: model.OK.Name,
		}
	} else {
		return model.PdfErrorResponse("could not download file", model.OTHER_EXCEPTION)
	}

	return nil
}

func (s *EngieItaliaChromeStrategy) LoadsInternal() bool {
	return false
}

func (s *EngieItaliaChromeStrategy) Close(a *model.Account) {
	s.ChromeDpCrawler.CancelFuncs()
}

func (s *EngieItaliaChromeStrategy) SendConnectionError() {
}

func (s *EngieItaliaChromeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EngieItaliaChromeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
