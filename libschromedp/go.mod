module bitbucket.org/bogdancnb/go-crawlers/libschromedp

go 1.14

replace bitbucket.org/bogdancnb/go-crawlers/libs => ../libs

replace bitbucket.org/bogdancnb/go-crawlers/libscrawler => ../libscrawler

require (
	bitbucket.org/bogdancnb/go-crawlers/libs v0.0.0-00010101000000-000000000000
	bitbucket.org/bogdancnb/go-crawlers/libscrawler v0.0.0-00010101000000-000000000000
	github.com/PuerkitoBio/goquery v1.5.1
	//github.com/chromedp/cdproto v0.0.0-20200608134039-8a80cdaf865c
	github.com/chromedp/cdproto v0.0.0-20200209033844-7e00b02ea7d2
	github.com/chromedp/chromedp v0.5.3
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.3 // indirect
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
)
