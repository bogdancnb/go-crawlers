package browser

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"context"
	"errors"
	"fmt"
	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/dom"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"path/filepath"
	"strings"
	"time"
)

var ErrTimeout = errors.New("timeout")

func New(log *logrus.Entry, headless bool) *ChromeDpCrawler {
	log.Debug("starting chromedp instance")
	crawler := &ChromeDpCrawler{log: log}
	crawler.Init(headless)
	return crawler
}

type ChromeDpCrawler struct {
	log         *logrus.Entry
	ctx         context.Context
	CancelFuncs func() //must be called to release resources and close chrome instance
}

func (c *ChromeDpCrawler) Close() error {
	c.CancelFuncs()
	return nil
}

func (c *ChromeDpCrawler) Init(headless bool) {
	opts := append(chromedp.DefaultExecAllocatorOptions[:],
		chromedp.DisableGPU,
		chromedp.NoDefaultBrowserCheck,
		chromedp.Flag("ignore-certificate-errors", true),
		chromedp.Flag("blink-settings", "imagesEnabled=false"),
	)

	p, err := crawl.GetProxy()
	if err != nil {
		c.log.WithError(err).Error("obtaining proxy")
	} else {
		c.log.Debug("got proxy: ", p)
		opts = append(opts, chromedp.ProxyServer(fmt.Sprintf(crawl.ProxyFormat, p.Host, p.Port)))
	}

	// create chrome visible instance, default headless
	if appflags.Localhost.Active() {
		opts = append(opts, chromedp.Flag("headless", false))
	} else if !headless {
		opts = append(opts, chromedp.Flag("headless", false))
	}

	// create a global timeout
	ctx, cancelTimeout := context.WithTimeout(context.Background(), 120*time.Second)
	ctx, cancelParent := chromedp.NewExecAllocator(ctx, opts...)
	ctx, cancelChild := chromedp.NewContext(ctx /*, chromedp.WithLogf(c.Entry.Printf)*/)

	c.ctx = ctx
	c.CancelFuncs = func() {
		c.log.Debug("calling context cancel functions")
		cancelChild()
		cancelParent()
		cancelTimeout()
		c.log.Debug("context cancelled")
	}
	c.log.Debug("chromedp initialized")
}

func (c *ChromeDpCrawler) Run(actions ...chromedp.Action) error {
	return chromedp.Run(c.ctx, actions...)
}

func (c *ChromeDpCrawler) LogHTML() {
	html := ""
	err := c.Run(chromedp.OuterHTML("html", &html))
	if err != nil {
		c.log.Warn("could not parse page html: " + err.Error())
	} else {
		c.log.Debug("html: ", html)
	}
}

func (c *ChromeDpCrawler) GetBoxModel(node *cdp.Node) (*dom.BoxModel, error) {
	fromContext := chromedp.FromContext(c.ctx)
	box, err := dom.GetBoxModel().
		WithNodeID(node.NodeID).
		Do(cdp.WithExecutor(c.ctx, fromContext.Target))
	return box, err
}

func (c *ChromeDpCrawler) RunWithTimeout(timeout time.Duration, actions ...chromedp.Action) error {
	ch := make(chan error)
	go func() {
		ch <- chromedp.Run(c.ctx, actions...)
	}()
	timer := time.NewTimer(timeout)

	for {
		select {
		case <-timer.C:
			timer.Stop()
			return ErrTimeout
		case e := <-ch:
			return e
		}
	}
}

func (c *ChromeDpCrawler) SetHeaders(headers map[string]interface{}) chromedp.Tasks {
	return chromedp.Tasks{
		network.Enable(),
		network.SetExtraHTTPHeaders(network.Headers(headers)),
	}
}

func (c *ChromeDpCrawler) SetUserAgentChrome() chromedp.Tasks {
	return c.SetHeaders(map[string]interface{}{"user-agent": crawl.AgentChrome78})
}

func jsGetText(sel string) (js string) {
	const funcJS = `function getText(sel) {
				var text = [];
				var elements = document.body.querySelectorAll(sel);

				for(var i = 0; i < elements.length; i++) {
					var current = elements[i];
					if(current.children.length === 0 && current.textContent.replace(/ |\n/g,'') !== '') {
					// Check the element has no children && that it is not empty
						text.push(current.textContent + ',');
					}
				}
				return text
			 };`

	invokeFuncJS := `var a = getText('` + sel + `'); a;`
	return strings.Join([]string{funcJS, invokeFuncJS}, " ")
}

func (c *ChromeDpCrawler) Scrape(url, readySel, textSel string, text *[]string) chromedp.Tasks {
	jsText := jsGetText(textSel)

	return chromedp.Tasks{
		chromedp.Navigate(url),
		chromedp.WaitReady(readySel, chromedp.ByID),
		chromedp.Evaluate(jsText, &text),
	}
}

func (c *ChromeDpCrawler) ReadyText(readySel, textSel string, text *[]string) chromedp.Tasks {
	jsText := jsGetText(textSel)

	return chromedp.Tasks{
		chromedp.WaitReady(readySel, chromedp.ByID),
		chromedp.Evaluate(jsText, &text),
	}
}

func (s *ChromeDpCrawler) CheckDownloads(downloadPath string, ref string, l *logrus.Entry) ([]byte, error) {
	files, err := ioutil.ReadDir(downloadPath)
	if err != nil {
		return nil, fmt.Errorf("could not read downloads folder")
	}
	for _, file := range files {
		if strings.Contains(file.Name(), ref) {
			l.Debug("found downloaded file ", file.Name())
			bytes, err := ioutil.ReadFile(filepath.Join(downloadPath, file.Name()))
			return bytes, err
		}
	}
	return nil, nil
}
