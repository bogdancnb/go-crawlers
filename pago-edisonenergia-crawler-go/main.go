package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/edisonenergia"
)

func main() {
	crawlerapp.Run(edisonenergia.Init)
}
