package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/strategies/polaris"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
)

func main() {
	crawlerapp.Run(polaris.Init)
}
