package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/fastweb"
)

func main() {
	crawlerapp.Run(fastweb.Init)
}
