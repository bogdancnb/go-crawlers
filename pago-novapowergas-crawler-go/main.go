package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/novapowergas"
)

func main() {
	crawlerapp.Run(novapowergas.Init)
}
