package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/eonenergia"
)

func main() {
	crawlerapp.Run(eonenergia.Init)
}
