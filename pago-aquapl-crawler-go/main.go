package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/aquapl"
)

func main() {
	crawlerapp.Run(aquapl.Init)
}
