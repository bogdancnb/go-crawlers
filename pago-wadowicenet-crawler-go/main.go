package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/wadowicenet"
)

func main() {
	crawlerapp.Run(wadowicenet.Init)
}
