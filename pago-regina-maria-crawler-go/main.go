package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/reginamaria"
)

func main() {
	crawlerapp.Run(reginamaria.Init)
}
