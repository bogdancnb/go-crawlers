package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/digicomm"
)

func main() {
	crawlerapp.Run(digicomm.Init)
}
