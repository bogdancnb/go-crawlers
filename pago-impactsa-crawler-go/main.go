package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/impactsa"
)

func main() {
	crawlerapp.Run(impactsa.Init)
}
