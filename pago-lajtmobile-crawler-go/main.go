package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/lajtmobile"
)

func main() {
	crawlerapp.Run(lajtmobile.Init)
}
