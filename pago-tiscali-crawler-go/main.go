package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/tiscali"
)

func main() {
	crawlerapp.Run(tiscali.Init)
}
