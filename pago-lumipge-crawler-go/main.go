package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/lumipge"
)

func main() {
	crawlerapp.Run(lumipge.Init)
}
