package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/olimontel"
)

func main() {
	crawlerapp.Run(olimontel.Init)
}
