module bitbucket.org/bogdancnb/go-crawlers/pago-bio-florisal-crawler-go

go 1.14

replace bitbucket.org/bogdancnb/go-crawlers/libs => ../libs

replace bitbucket.org/bogdancnb/go-crawlers/libscrawler => ../libscrawler

require bitbucket.org/bogdancnb/go-crawlers/libscrawler v0.0.0-00010101000000-000000000000
