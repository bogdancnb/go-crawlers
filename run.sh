#!/bin/sh
echo "********************************************************"
echo "Starting ${SERVICE_NAME} Service"
echo "********************************************************"

echo GO111MODULE: ${GO111MODULE}
echo CGO_ENABLED: ${CGO_ENABLED}
echo GOOS: ${GOOS}
echo GOARCH: ${GOARCH}
echo STOP_TIMEOUT_SECONDS: ${STOP_TIMEOUT_SECONDS}

pwd
ls -l

LOG_FILE_PATH="${LOG_DIR}/${LOG_RELATIVE_DIR}/${SERVICE_NAME}-%s.log"

GO_CMD="./main \
  -profile=${SPRING_PROFILES_ACTIVE} \
  -configPath=/usr/local/app \
  -logLevel=debug \
  -stopTimeoutSeconds=${STOP_TIMEOUT_SECONDS}s \
  -certs=${CERTIFICATE_LIST} \
  -logFile=${LOG_FILE_PATH}"

echo profile: ${SPRING_PROFILES_ACTIVE}
echo Log : ${LOG_FILE_PATH}
echo Go command line:
echo ${GO_CMD}

mkdir -p ${LOG_DIR}

trap 'kill -TERM $PID' TERM INT
${GO_CMD} &
PID=$!
wait $PID
trap - TERM INT
wait $PID
EXIT_STATUS=$?