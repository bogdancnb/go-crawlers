package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/wodociagi"
)

func main() {
	crawlerapp.Run(wodociagi.Init)
}
