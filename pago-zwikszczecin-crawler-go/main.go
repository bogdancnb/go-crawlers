package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/zwikszczecin"
)

func main() {
	crawlerapp.Run(zwikszczecin.Init)
}
