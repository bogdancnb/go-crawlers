package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	telka "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/telka_wroklaw"
)

func main() {
	crawlerapp.Run(telka.Init)
}
