package digicomm

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	root       = "https://www.digicomm.ro"
	login      = "https://www.digicomm.ro/my-account/login"
	loginCheck = "https://www.digicomm.ro/my-account/login_check"
	myAccount  = "https://www.digicomm.ro/my-account/"
	invoices   = "https://www.digicomm.ro/my-account/invoices"

	siteFormat  = "2006-01-02"
	siteFormat2 = "01.2006"
)

var pdfRegex = regexp.MustCompile(`Cod Client\s+:\s+(\d+)[a-zA-Z]+`)

func Init() {
	RegisterFactory("digi_communication.crawler", new(DigiCommFactory))
}

type DigiCommFactory struct{}

func (p *DigiCommFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "DigiCommStrategy")
	return &DigiCommStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type DigiCommStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	phpSession  string
}

func (s *DigiCommStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	pdf, ex := s.pdfDownload(invoice)
	if ex != nil {
		return ConnectionErr("pdfDownload " + invoice.Ref + ": " + ex.Error())
	}
	text, err := PlainText(pdf)
	if err != nil {
		return ParseErr("parsing pdf " + *invoice.PdfUri + ": " + err.Error())
	}
	s.Trace(text)
	matches, ex := utils.MatchRegex(text, pdfRegex)
	if ex != nil {
		return ParseErr("parsing pdf regex" + *invoice.PdfUri + ": " + pdfRegex.String())
	}
	match := matches[0][1]
	barcode := match[len(match)-29:]
	invoice.BarCode = &barcode
	return nil
}

func (s *DigiCommStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *DigiCommStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *DigiCommStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Connection": "keep-alive",
		"Host":       "www.digicomm.ro",
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().Get(login)
	if err != nil {
		return nil, ConnectionErr("GET " + login + ": " + err.Error())
	}
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(res.Body()))
	if err != nil {
		return nil, ParseErr("parsing get " + invoices + ": " + err.Error())
	}
	_csrf_token, ex := Attr(doc.Find("input[name='_csrf_token']"), "value")
	if ex != nil {
		return nil, ex
	}
	s.phpSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	s.phpSession = s.phpSession[:strings.Index(s.phpSession, "; ")]

	s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	res, err = s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.phpSession + "; hl=ro",
		"Origin":       "https://www.digicomm.ro",
		"Referer":      login,
	}).SetFormData(map[string]string{
		"_csrf_token": _csrf_token,
		"_username":   account.Username,
		"_password":   *account.Password,
		"_submit":     "Autentificare",
	}).Post(loginCheck)
	if res == nil {
		return nil, ConnectionErr("POST " + loginCheck + "got no response")
	}
	if res.StatusCode() != 302 {
		return nil, ConnectionErr("POST " + loginCheck + "got status code: " + res.Status())
	}
	doc, err = goquery.NewDocumentFromReader(bytes.NewReader(res.Body()))
	if err != nil {
		return nil, ParseErr("parsing get " + invoices + ": " + err.Error())
	}
	s.phpSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	if s.phpSession == "" {
		return nil, LoginErr("no php session found")
	} else {
		s.phpSession = s.phpSession[:strings.Index(s.phpSession, "; ")]
	}

	s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie":  "hl=ro; " + s.phpSession,
		"Referer": login,
	}).Get(invoices))
	if ex != nil {
		return nil, ex
	}
	return nil, nil
}

func (s *DigiCommStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *DigiCommStrategy) LoadLocations(account *model.Account) Exception {
	l := &model.Location{}
	for _, exLoc := range s.exLocations {
		l.Identifier = exLoc.Identifier
		l.Details = exLoc.Details
		l.Service = exLoc.Service
		break
	}
	account.AddLocation(l)
	return nil
}

func (s *DigiCommStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *DigiCommStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	rows := s.doc.Find("fieldset table tbody tr")
	if rows.Length() == 0 {
		s.Error(s.doc.Html())
		return ParseErr("LoadInvoices: no invoice rows found")
	}
	for i := 0; i < rows.Length()-1; i++ {
		cols := rows.Eq(i).Find("td")
		if ex := s.parseInvoice(location, cols); ex != nil {
			return ParseErr("parseInvoice: " + ex.Error())
		}
	}
	return nil
}

func (s *DigiCommStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ref := utils.RemoveSpaces(cols.Eq(0).Text())
	issueDate, err := time.Parse(siteFormat2, utils.RemoveSpaces(cols.Eq(1).Text()))
	if err != nil {
		return ParseErr("parse issue date: " + err.Error())
	}
	dueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(2).Text()))
	if err != nil {
		return ParseErr("parse due date: " + err.Error())
	}
	text := cols.Eq(3).Text()
	parts := strings.Split(text, " ")
	if len(parts) == 0 {
		return ParseErr("parsing amount from " + text)
	}
	amount, ex := AmountFromString(parts[0])
	if ex != nil {
		return ex
	}
	amountDue := 0.0
	dueEl := cols.Eq(3).Find("span[class='label label-danger']")
	if dueEl.Length() > 0 {
		parts := strings.Split(dueEl.Text(), " ")
		amountDue, ex = AmountFromString(parts[0])
		if ex != nil {
			return ex
		}
	}
	var pdfUri *string
	hrefEl := cols.Eq(4).Find("a")
	if hrefEl.Length() == 0 {
		return ParseErr("no pdf uri link found for " + ref)
	}
	uri, ex := Attr(hrefEl.Eq(0), "href")
	if ex != nil {
		return ex
	}
	if uri == "" {
		return ParseErr("empty pdf uri link found for " + ref)
	}
	uri = root + uri
	pdfUri = &uri

	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)

	if location.Identifier == "" {
		if ex = s.parsePdf(location, invoice); ex != nil {
			return ParseErr("parsePdf " + ref + ": " + ex.Error())
		}
	}
	return nil
}

func (s *DigiCommStrategy) parsePdf(location *model.Location, invoice *model.Invoice) Exception {
	pdf, ex := s.pdfDownload(invoice)
	if ex != nil {
		return ConnectionErr("pdfDownload " + invoice.Ref + ": " + ex.Error())
	}
	text, err := PlainText(pdf)
	if err != nil {
		return ParseErr("parsing pdf " + *invoice.PdfUri + ": " + err.Error())
	}
	s.Trace(text)
	matches, ex := utils.MatchRegex(text, pdfRegex)
	if ex != nil {
		return ParseErr("parsing pdf regex" + *invoice.PdfUri + ": " + pdfRegex.String())
	}
	match := matches[0][1]
	codClient := match[:len(match)-29]
	barcode := match[len(match)-29:]
	invoice.BarCode = &barcode
	location.Identifier = codClient
	location.Service = "Cod client " + codClient
	return nil
}

func (s *DigiCommStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *DigiCommStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdf, ex := s.pdfDownload(invoice)
	if ex != nil {
		return model.PdfErrorResponse("ref "+invoice.Ref+": "+ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdf,
		PdfStatus: model.OK.Name,
	}
}

func (s *DigiCommStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Cookie":  "hl=ro; " + s.phpSession,
		"Referer": invoices,
	}).Get(*invoice.PdfUri))
	if ex != nil {
		return nil, ex
	}
	return pdfBytes, nil
}

func (s *DigiCommStrategy) LoadsInternal() bool {
	return false
}

func (s *DigiCommStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *DigiCommStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
