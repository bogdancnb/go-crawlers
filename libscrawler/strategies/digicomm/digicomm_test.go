package digicomm

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "digi_communication.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"BenkoMihaly", "Digicomm"},
		//{"erzsosz4", "erzso4SZ@"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		//{
		//	Username: "BenkoMihaly", Password: "Digicomm", Uri: "digi_communication.crawler",
		//	Identifier: "3047",
		//	Ref:        "PJ20707/2020-07-01", PdfUri: "https://www.digicomm.ro/my-account/invoices/43519/download",
		//},
		{
			Username: "erzsosz4", Password: "erzso4SZ@", Uri: "digi_communication.crawler",
			Identifier: "417",
			Ref:        "CTN1313505/2020-07-01", PdfUri: "https://www.digicomm.ro/my-account/invoices/42120/download",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
