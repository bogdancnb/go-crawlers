package estraenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"net/url"
	"strings"
)

const (
	portal       = "https://areaclienti.estra.it/portal/"
	auth         = "https://areaclienti.estra.it/portal/home?p_p_id=estraAreaClientiBusinessSecurity_WAR_estraAreaClientiBusinessGestioneUtenteportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=login&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=1"
	portalC      = "https://areaclienti.estra.it/portal/c"
	login        = "https://areaclienti.estra.it/portal/c/portal/login?p_l_id=20184&ticket=%s"
	contracts    = "https://areaclienti.estra.it/portal/group/guest/mieicontratti?p_p_id=estraAreaClientiBusinessIMieiContratti_WAR_estraAreaClientiBusinessIMieiContrattiportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=mieiContratti&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_count=1"
	invoices     = `https://areaclienti.estra.it/portal/group/guest/gas?p_p_id=estraAreaClientiBusinessGestioneGAS_WAR_estraAreaClientiBusinessGestioneContoContrattualeportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=lemiefatture&p_p_cacheability=cacheLevelPage&p_p_col_id=column-3&p_p_col_count=1`
	invoicesBase = `https://areaclienti.estra.it/portal/group/guest/gas?`
	invoicesDoc  = `https://areaclienti.estra.it/portal/group/guest/gas?tipoMenu=gas&fun=lemiefatture&stato=A&conto=1&cc=`
	download     = `https://areaclienti.estra.it/portal/delegate/downloadfile?downloadUrl=%s&contentType=pdf&downloadType=fattura&numeroFattura=%s`

	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("estra_Energia.crawler", new(EstraEnergiaFactory))
}

type EstraEnergiaFactory struct{}

func (p *EstraEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EstraEnergiaStrategy")
	return &EstraEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type EstraEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *EstraEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EstraEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EstraEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EstraEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	_, err := s.Client().R().SetHeaders(map[string]string{
		"accept": AcceptHeader,
	}).Get(portal)
	if err != nil {
		return nil, ConnectionErr("GET " + portal + " : " + err.Error())
	}

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/javascript, */*",
		"content-type":     "application/x-www-form-urlencoded; charset=UTF-8",
		"origin":           "https://areaclienti.estra.it",
		"referer":          "https://areaclienti.estra.it/portal/",
		"x-requested-with": "XMLHttpRequest",
	}).SetFormData(map[string]string{
		"_estraAreaClientiBusinessSecurity_WAR_estraAreaClientiBusinessGestioneUtenteportlet_username": account.Username,
		"_estraAreaClientiBusinessSecurity_WAR_estraAreaClientiBusinessGestioneUtenteportlet_password": *account.Password,
	}).Post(auth))
	s.Debug(res)
	if ex != nil {
		return nil, ex
	}
	res = strings.ReplaceAll(res, `"\`, "")
	res = strings.ReplaceAll(res, `\"`, "")
	var resJson struct {
		RedirectPage         string
		ResetPassword        bool
		CasServiceTicket     string
		RedirectPageResetPwd string
	}
	err = utils.FromJSON(&resJson, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("parse login response " + res + " : " + err.Error())
	}
	if resJson.CasServiceTicket == "" {
		return nil, LoginErr("login response " + res)
	}

	uri := fmt.Sprintf(login, resJson.CasServiceTicket)
	httpRes, err := s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/javascript, */*",
		"referer":          "https://areaclienti.estra.it/portal/",
		"x-requested-with": "XMLHttpRequest",
	}).Get(uri)
	if httpRes == nil {
		return nil, ConnectionErr("no response for " + uri)
	}
	if httpRes.StatusCode() != 200 {
		return nil, ConnectionErr(fmt.Sprintf("invalid status code response for %q : %d", uri, httpRes.StatusCode()))
	}

	//res, ex = OkString(s.Client().R().SetHeaders(map[string]string{
	//	"accept":           "application/json, text/javascript, */*",
	//	"referer":          "https://areaclienti.estra.it/portal/",
	//	"x-requested-with": "XMLHttpRequest",
	//}).Get(contracts))
	//s.Debug(res)
	//if ex != nil {
	//	return nil, ex
	//}
	return nil, nil
}

func (s *EstraEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

type contractsJson struct {
	Vkont        string
	TipoPag      string
	TipoDocAgg   string
	Bukrs        string
	CityNasc     string
	CsvFatture   string
	DesSocieta   string
	MobileUserId string
	NameFirst    string
	NameLast     string
	NumFatt      int
	NumjDocAgg   string
	StrStd       struct {
		Cap       string
		Civico    string
		Localita  string
		Provincia string
		Via       string
	}
	StrUbicazione struct {
		Item []*struct {
			City1     string
			HouseNum1 string
			PostCode1 string
			Pdr       string
			Region    string
			Street    string
		}
	}
}

func (s *EstraEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":       "application/json, text/javascript, */*",
		"content-type": "text/plain;charset=UTF-8",
		"origin":       "https://areaclienti.estra.it",
		//"referer":"https://areaclienti.estra.it/portal/group/guest/mieicontratti",
		"x-requested-with": "XMLHttpRequest",
	}).Post(contracts))
	s.Debug("contracts: ", res)
	if ex != nil {
		return ex
	}
	var resJson struct {
		LstContratti string
	}
	err := utils.FromJSON(&resJson, strings.NewReader(res))
	if err != nil {
		return ParseErr("contracts json: " + err.Error())
	}
	var contractList []*contractsJson
	err = utils.FromJSON(&contractList, strings.NewReader(resJson.LstContratti))
	if err != nil {
		return ParseErr("parsing contracts list: " + err.Error())
	}
	for _, c := range contractList {
		name := c.NameFirst + " " + c.NameLast
		for _, l := range c.StrUbicazione.Item {
			l := &model.Location{
				Service:        name + ", " + l.Street + " " + l.HouseNum1 + " -  " + l.PostCode1 + " " + l.City1,
				Details:        &c.Vkont,
				Identifier:     l.Pdr,
				AccountHelpers: nil,
			}
			account.AddLocation(l)
		}
	}
	if len(account.Locations) == 0 {
		return nil
	}
	if ex := s.loadInvoicesInternal(account, account.Locations[0]); ex != nil {
		return ex
	}
	return nil
}

type invoiceJson struct {
	CSVGENERATO    string
	DATAEMISS      string
	DATAPAGAMENTO  string
	DATASCAD       string
	FATTRATEIZZATA string
	FATTSTORNATA   string
	FPUNORARIO     string
	IMPORTOFATT    float64
	IMPORTOPAGATO  float64
	NUMEROFATTURA  string
	RATENONPAGATE  string
	STATOPAG       string
	TIPOPAG        string
	URLFILEPUN     string
	URLPDF         string
	URLPDFNC       string
}

func (s *EstraEnergiaStrategy) loadInvoicesInternal(account *model.Account, location *model.Location) Exception {
	clientCod := *location.Details
	clientCod = strings.TrimLeft(clientCod, "0")
	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(invoicesDoc + clientCod))
	if ex != nil {
		return ex
	}

	//	params := url.Values{
	//		"p_p_id":           []string{"estraAreaClientiBusinessGestioneGAS_WAR_estraAreaClientiBusinessGestioneContoContrattualeportlet"},
	//		"p_p_lifecycle":    []string{"2"},
	//		"p_p_state":        []string{"normal"},
	//		"p_p_mode":         []string{"view"},
	//		"p_p_resource_id":  []string{"lemiefatture"},
	//		"p_p_cacheability": []string{"cacheLevelPage"},
	//		"p_p_col_id":       []string{"column-3"},
	//		"p_p_col_count":    []string{"1"},
	//	}
	//	uri := invoicesBase + params.Encode()
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/javascript, */*",
		"accept-encoding":  "gzip, deflate, br",
		"content-type":     "text/plain;charset=UTF-8",
		"origin":           "https://areaclienti.estra.it",
		"x-requested-with": "XMLHttpRequest",
	}).
		//SetBody(nil).
		Post(invoices))
	s.Debug("invoices: ", res)
	if ex != nil {
		return ex
	}
	var json struct {
		LstFatture string
	}
	err := utils.FromJSON(&json, strings.NewReader(res))
	if err != nil {
		return ParseErr("invoice json: " + err.Error())
	}
	var invoices []*invoiceJson
	err = utils.FromJSON(&invoices, strings.NewReader(json.LstFatture))
	if err != nil {
		return ParseErr("invoice json: " + err.Error())
	}
	for _, invoice := range invoices {
		issueDate, ex := TimeFromString(invoiceFormat, invoice.DATAEMISS)
		if ex != nil {
			return ex
		}
		dueDate, ex := TimeFromString(invoiceFormat, invoice.DATASCAD)
		if ex != nil {
			return ex
		}
		amtDec := decimal.NewFromFloat(invoice.IMPORTOFATT)
		amtPayDec := decimal.NewFromFloat(invoice.IMPORTOPAGATO)
		dueDec, ok := amtDec.Sub(amtPayDec).Float64()
		if !ok {
			return ParseErr("parsing amount due for " + invoice.NUMEROFATTURA)
		}
		i := &model.Invoice{
			Ref:       invoice.NUMEROFATTURA,
			PdfUri:    &invoice.URLPDF,
			Amount:    invoice.IMPORTOFATT,
			AmountDue: dueDec,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
		if invoice.DATAPAGAMENTO != "" {
			payDate, ex := TimeFromString(invoiceFormat, invoice.DATAPAGAMENTO)
			if ex != nil {
				return ex
			}
			p := &model.Payment{
				Ref:    invoice.NUMEROFATTURA,
				Amount: invoice.IMPORTOPAGATO,
				Date:   payDate.Format(utils.DBDateFormat),
			}
			location.AddPayment(p)
		}
	}
	return nil
}

func (s *EstraEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EstraEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EstraEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EstraEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	//p := url.Values{
	//	"downloadUrl":   []string{pdfUri},
	//	"contentType":   []string{"pdf"},
	//	"downloadType":  []string{"fattura}"},
	//	"numeroFattura": []string{invoice.Ref},
	//}
	//uri := download + p.Encode()
	uri := fmt.Sprintf(download, url.QueryEscape(pdfUri), invoice.Ref)
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"accept": "*/*",
	}).Get(uri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EstraEnergiaStrategy) LoadsInternal() bool {
	return true
}

func (s *EstraEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *EstraEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
