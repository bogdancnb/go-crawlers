package blocexpert

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "bloc_expert.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"Andrei.cioclea", "cioclea94"}, //ok
		//{"146245", "f17ece9e44"},        //ok
		//{"172856", `Dn\T~9rz.U`},
		{"1407437", "f5e2e572c1"},
		//{"1407288", "979ae2aca4"}, //ok
		//{"1387789", "7faa36ce72"}, //ok
		//{"174309", "f049d288b2"}, //ok
		//{"1397881", "41bf776083"}, //login err
		//{"1407679", "1d9667c363"},
		//{"1395372", "089d822988"},
		//{"172899", "Armaauto20"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
