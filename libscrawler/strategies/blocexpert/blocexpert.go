package blocexpert

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	index             = "https://www.blocexpert.ro/app/files/index.php"
	detalii           = "https://www.blocexpert.ro/app/files/citeste_detalii_rest_plata.php"
	cote              = "https://www.blocexpert.ro/app/files/dashboardlocatarcote.php?"
	plati             = "https://www.blocexpert.ro/app/files/dashboardlocataristoricplati.php?"
	notaplatadownload = "https://www.blocexpert.ro/app/files/notaplatadownload.php"

	ymd = "02.01.2006"
	mF  = "01/2006"
)

var (
	codFiscalRegex = regexp.MustCompile(`COD FISCAL:\s*(\d+)\D+`)
	bankRegex      = regexp.MustCompile(`:\s*(RO\w+),\s*([\w\s]+),`)

	nameRegex = []*regexp.Regexp{
		regexp.MustCompile(`Stimata\s*doamna\s*\/\s*Stimate\s*domn\s*([\w\s-]+)\s*,`),
		regexp.MustCompile(`Stimate\/a\s*([\w\s-]+)\s*,`),
		regexp.MustCompile(`Catre\s*([\w\s-]+)\s*avand`),
	}
	addressRegex = []*regexp.Regexp{
		regexp.MustCompile(`cheltuielilor\s*de\s*intretinere\s*ale\s*(.+)\s*situat`),
		regexp.MustCompile(`proprietar\s*al\s*([\w\s\-\.]+)\s*,`),
		regexp.MustCompile(`calitatea\s*de\s*\w+\s*al\s*(.+)\s*situat`),
	}
	legalEntityName = `Bloc Expert`
)

func Init() {
	RegisterFactory("bloc_expert.crawler", new(BlocExpertFactory))
}

type BlocExpertFactory struct{}

func (p *BlocExpertFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "BlocExpertStrategy")
	return &BlocExpertStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type BlocExpertStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	sesiuneId   string
	PHPSESSID   string
}

func (s *BlocExpertStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *BlocExpertStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *BlocExpertStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *BlocExpertStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	s.EnableRedirects(false)
	response, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.blocexpert.ro",
	}).Get(index)
	doc, ex := OkDocument(response, err)
	if ex != nil {
		return nil, ex
	}
	s.PHPSESSID = HeaderByNameAndValStartsWith(response.Header(), "Set-Cookie", "PHPSESSID")
	s.PHPSESSID = s.PHPSESSID[:strings.Index(s.PHPSESSID, "; ")]

	postLogin, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"cookie":       s.PHPSESSID,
		"host":         "www.blocexpert.ro",
		"origin":       "https://www.blocexpert.ro",
		"referer":      "https://www.blocexpert.ro/app/files/index.php",
	}).SetFormData(map[string]string{
		"mainpage":    "verifica",
		"szLoginName": account.Username,
		"szPassword":  *account.Password,
	}).Post(index)
	doc, ex = OkDocument(postLogin, err)
	if ex != nil {
		return nil, ex
	}
	backForm := doc.Find("form[name='backForm']")
	if backForm.Length() > 0 {
		//bLoginFailed := backForm.Find("input[name='bLoginFailed']")
		//if bLoginFailed.Length() > 0 {
		//	return nil, LoginErr("login failed")
		//} else {
		//	return nil, ParseErr("bLoginFailed not found in backForm")
		//}
		mainpage, ex := Attr(backForm.Find("input[name='mainpage']"), "value")
		if ex != nil {
			s.Debug(doc.Html())
			return nil, ex
		}
		if mainpage == "" {
			s.Debug(doc.Html())
			return nil, ParseErr("mainpage input has no value")
		}
		if mainpage != "dashboardlocatar" {
			s.Debug(doc.Html())
			return nil, LoginErr("eroare de login: " + mainpage)
		}
	} else {
		//fredirerr:= doc.Find("form[name='fredirerr']")
		//if fredirerr.Length() == 0 {
		//	return nil, ParseErr("fredirerr not found")
		//}
		return nil, ParseErr("backForm not found")
	}

	postOk, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"cookie":       s.PHPSESSID,
		"host":         "www.blocexpert.ro",
		"origin":       "https://www.blocexpert.ro",
		"referer":      "https://www.blocexpert.ro/app/files/index.php",
	}).SetFormData(map[string]string{
		"mainpage":     "dashboardlocatar",
		"tab":          "",
		"subtab":       "",
		"bLoginFailed": "",
	}).Post(index)
	s.doc, ex = OkDocument(postOk, err)
	if ex != nil {
		return nil, ex
	}
	content := postOk.String()
	return &content, nil
}

func (s *BlocExpertStrategy) CheckLogin(response *string) Exception {
	searchString := `sesiune_id=`
	startIndex := strings.Index(*response, searchString)
	if startIndex == -1 {
		return LoginErr("sesiune_id not found")
	}
	s.sesiuneId = (*response)[startIndex:]
	s.sesiuneId = s.sesiuneId[:strings.Index(s.sesiuneId, `"`)]
	return nil
}

func (s *BlocExpertStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":     "*/*",
		"connection": "keep-alive",
		"cookie":     s.PHPSESSID,
		"host":       "www.blocexpert.ro",
		"referer":    index,
	}).Get(detalii))
	s.Debug(res)
	if ex != nil {
		return ex
	}
	due := 0.0
	if res != "Array" {
		var info []interface{}
		err := utils.FromJSON(&info, strings.NewReader(res))
		if err != nil {
			return ParseErr("parse details response " + res + " : " + err.Error())
		}
		due, ex = AmountFromString(info[0].(string))
		if ex != nil {
			return ex
		}
	}

	codClEl := s.doc.Find("div:containsOwn('Cod client :')")
	if codClEl.Length() == 0 {
		return ParseErr("client code element not found")
	}
	codClient := strings.TrimSpace(codClEl.Text())
	parts := strings.Split(codClient, ":")
	if len(parts) != 2 {
		return ParseErr("parse client code from " + codClient)
	}
	codClient = strings.TrimSpace(parts[1])
	l := &model.Location{
		Service:    account.Username,
		Identifier: codClient,
		Amount:     &due,
	}
	account.AddLocation(l)

	if ex := s.loadInternal(account, l); ex != nil {
		return ex
	}
	ok := l.SetInvoiceDueFromAmount()
	if !ok {
		return ParseErr("could not set invoices due from location amount")
	}

	if ex := s.downloadNotaPlata(l); ex != nil {
		s.WithError(ex).Error("download nota de plata")
	}

	return nil
}

func (s *BlocExpertStrategy) downloadNotaPlata(l *model.Location) Exception {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"connection": "keep-alive",
		"cookie":     s.PHPSESSID,
		"host":       "www.blocexpert.ro",
		"referer":    index,
	}).Get(notaplatadownload)
	if err != nil {
		return ConnectionErr(notaplatadownload + " : " + err.Error())
	}

	text, err := PlainText(res.Body())
	if err != nil {
		return ParseErr("parse nota plata pdf text: " + err.Error())
	}
	s.Debug(text)
	m, err := utils.MatchRegex(text, codFiscalRegex)
	if err != nil {
		return ParseErr("could not parse cod fiscal regex: " + err.Error())
	}
	codFiscal := m[0][1]
	m, err = utils.MatchRegex(text, bankRegex)
	if err != nil {
		return ParseErr("could not parse bank regex: " + err.Error())
	}
	iban := m[0][1]
	bankName := m[0][2]
	bankBIC := iban[4:8]
	l.ProviderBranch = &model.ProviderBranchDTO{
		Cui:             &codFiscal,
		Iban:            &iban,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalEntityName,
	}

	name := ""
	for _, r := range nameRegex {
		m, err = utils.MatchRegex(text, r)
		if err == nil {
			name = m[0][1]
			s.Debug("name matched with ", r, name)
			break
		}
	}
	if name == "" {
		return ParseErr("could not parse name regex: " + text)
	}

	addr := ""
	for _, r := range addressRegex {
		m, err = utils.MatchRegex(text, r)
		if err == nil {
			addr = m[0][1]
			s.Debug("address matched with ", r, addr)
			break
		}
	}
	if addr == "" {
		return ParseErr("could not parse address regex: " + text)
	}

	l.Service = name + " " + addr
	l.Details = &l.Service
	return nil
}

func (s *BlocExpertStrategy) loadInternal(account *model.Account, l *model.Location) Exception {
	res, ex := s.doRequest(cote)
	if ex != nil {
		return ex
	}
	html := WrapHtmlBody(*res)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ParseErr("parse cote response: " + err.Error())
	}
	facturi := doc.Find("div[class=globalcontainer]")
	for i := 0; i < facturi.Length(); i++ {
		if ex := s.parseInvoice(facturi.Eq(i), l); ex != nil {
			return ex
		}
	}

	res, ex = s.doRequest(plati)
	if ex != nil {
		return ex
	}
	html = WrapHtmlBody(*res)
	doc2, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ParseErr("parse plati response: " + err.Error())
	}
	plati := doc2.Find("div[class=globalcontainer]")
	for i := 0; i < plati.Length(); i++ {
		if ex := s.parsePayment(plati.Eq(i), l); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *BlocExpertStrategy) parsePayment(selection *goquery.Selection, l *model.Location) Exception {
	divs := selection.Find("div")
	ref := strings.ToLower(strings.TrimSpace(divs.Eq(0).Text()))
	ref = strings.ReplaceAll(ref, `&nbsp;`, ` `)
	ref = utils.RemoveNonASCII(ref)
	for k, v := range utils.ShortMonthsRO {
		ref = strings.ReplaceAll(ref, k, v)
	}

	amS := strings.TrimSpace(divs.Eq(1).Text())
	amS = strings.ReplaceAll(amS, `&nbsp;`, ` `)
	am, ex := AmountFromString(amS)
	if ex != nil {
		return ex
	}
	dateS := ref[strings.Index(ref, "("):strings.Index(ref, ")")]
	parts := strings.Split(dateS, "/")
	datepart := strings.TrimSpace(parts[len(parts)-1])
	date, err := time.Parse(ymd, datepart)
	if err != nil {
		return ParseErr("parse date " + datepart + ": " + err.Error())
	}
	ref = truncRef(ref)
	p := &model.Payment{
		Ref:    ref,
		Amount: am,
		Date:   date.Format(utils.DBDateFormat),
	}
	l.AddPayment(p)
	//s.checkPaidInvoices(ref, l.Invoices)
	return nil
}

func truncRef(ref string) string {
	if len(ref) >= 255 {
		ref = ref[:255]
	}
	return ref
}

//func (s *BlocExpertStrategy) checkPaidInvoices(ref string, invoices []*model.Invoice) {
//	for _, i := range invoices {
//		refPart := strings.Split(i.Ref, " ")
//		if strings.Contains(ref, refPart[len(refPart)-1]) && strings.Contains(ref, refPart[len(refPart)-2]) {
//			i.AmountDue = 0
//		}
//	}
//}

func (s *BlocExpertStrategy) parseInvoice(selection *goquery.Selection, l *model.Location) Exception {
	divs := selection.Find("div")
	ref := strings.ToLower(strings.TrimSpace(divs.Eq(0).Text()))
	ref = strings.ReplaceAll(ref, `&nbsp;`, ` `)
	ref = utils.RemoveNonASCII(ref)
	amS := strings.TrimSpace(divs.Eq(1).Text())
	amS = strings.ReplaceAll(amS, `&nbsp;`, ` `)
	am, ex := AmountFromString(amS)
	if ex != nil {
		return ex
	}
	dateS := ref[strings.LastIndex(ref, "("):strings.LastIndex(ref, ")")]
	parts := strings.Split(dateS, " ")
	//if len(parts) != 3 {
	//	return ParseErr("invalid ref format for " + ref)
	//}
	period, err := time.Parse(mF, utils.MonthsNrRO[parts[len(parts)-2]]+"/"+parts[len(parts)-1])
	if err != nil {
		s.Error("parsing date from " + ref + " : " + err.Error())
		return nil
	}
	issueDate := period.AddDate(0, 2, 0)
	dueDate := period.AddDate(0, 3, -1)
	ref = truncRef(ref)
	i := &model.Invoice{
		Ref:       ref,
		Amount:    am,
		AmountDue: 0,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(i)
	return nil
}

func (s *BlocExpertStrategy) doRequest(url string) (*string, Exception) {
	doc, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":     "*/*",
		"connection": "keep-alive",
		"cookie":     s.PHPSESSID,
		"host":       "www.blocexpert.ro",
		"referer":    index,
	}).Get(url + s.sesiuneId))
	s.Debug(doc)
	if ex != nil {
		return nil, ex
	}
	return &doc, nil
}

func (s *BlocExpertStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *BlocExpertStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *BlocExpertStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *BlocExpertStrategy) Pdf(account *model.Account) *model.PdfResponse {
	uri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		//SetHeaders(map[string]string{ "accept": "*/*"}).
		Get(uri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *BlocExpertStrategy) LoadsInternal() bool {
	return true
}

func (s *BlocExpertStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *BlocExpertStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
