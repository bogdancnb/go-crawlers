package citynet

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "city_net.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"kl005486", "elzbieta7"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "kl005486", Password: "elzbieta7", Uri: "city_net.crawler",
			Identifier: "kl005486",
			Ref:        "FV/000754/03/21/I", PdfUri: "https://ebok.city-net.pl/index.php?75TI=NQZmpGA1RGCxyzW5ZQZk0GMaSTpY0R",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
