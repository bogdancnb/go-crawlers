package citynet

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	root  = "https://ebok.city-net.pl/index.php"
	index = `https://ebok.city-net.pl/index.php?`
	ajax  = `https://ebok.city-net.pl/ajax.php`

	invoiceFormat = "02/01/2006"
	paymentFormat = "2006-01-0215:04:05"
)

var (
	loginErrs = []string{`Zły login lub hasło`, `Z�y login lub has�o`, `y login lub has`}
	bankName  = "Bank Pocztowy Spolka Akcyjna"
	bankBIC   = "POCZPLP4"
	legalName = "CITYNET SP. Z O.O."

	ajaxMenuRegex = regexp.MustCompile(`__AJAX_address_menu='(.+)'`)
)

func Init() {
	RegisterFactory("city_net.crawler", new(CityNetFactory))
}

type CityNetFactory struct{}

func (p *CityNetFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "CityNetStrategy")
	return &CityNetStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type CityNetStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	seCookie    string
}

func (s *CityNetStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *CityNetStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *CityNetStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *CityNetStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	get, err := s.Client().R().SetHeader("accept", AcceptHeader).Get(root)
	doc, ex := OkDocument(get, err)
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())
	przycLog, ex := Attr(doc.Find(`input[id="przyc_log"]`), "value")
	if ex != nil {
		return nil, ex
	}
	s.seCookie = HeaderByNameAndValStartsWith(get.Header(), "set-cookie", "SE")
	if s.seCookie == "" {
		return nil, ParseErr("could not find set-cookie on response")
	}
	s.seCookie = s.seCookie[:strings.Index(s.seCookie, `;`)]

	s.EnableRedirects(false)

	post, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://ebok.city-net.pl",
		"referer":      "https://ebok.city-net.pl/index.php",
		"cookie":       s.seCookie,
	}).SetFormData(map[string]string{
		"log":       account.Username,
		"pass":      *account.Password,
		"przyc_log": przycLog,
	}).Post(index)
	if post == nil {
		return nil, ConnectionErr("no response: " + index)
	}
	location := HeaderByNameAndValStartsWith(post.Header(), "location", "")

	get, err = s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"referer": "https://ebok.city-net.pl/index.php",
	}).Get(root + location)
	if get == nil {
		return nil, ConnectionErr("no response: " + root + location)
	}
	if get.StatusCode() == 200 {
		body := get.String()
		return &body, nil
	}
	location = HeaderByNameAndValStartsWith(get.Header(), "location", "")

	get, err = s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"referer": "https://ebok.city-net.pl/index.php",
	}).Get(root + location)
	if get == nil {
		return nil, ConnectionErr("no response: " + root + location)
	}
	if get.StatusCode() != 200 {
		return nil, ConnectionErr(root + location)
	}
	body := get.String()
	s.doc, err = goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		return nil, ParseErr("parse html :" + err.Error())
	}

	return &body, nil
}

func (s *CityNetStrategy) CheckLogin(response *string) Exception {
	for _, loginErr := range loginErrs {
		if strings.Contains(*response, loginErr) {
			return LoginErr(loginErrs[0])
		}
	}
	return nil
}

func (s *CityNetStrategy) LoadLocations(account *model.Account) Exception {
	//html, err := s.doc.Html()
	//if err != nil {
	//	return ParseErr("parse html: " + err.Error())
	//}
	//m, err:=utils.MatchRegex(html, ajaxMenuRegex)
	//if err != nil {
	//	return ParseErr("parse ajaxMenuRegex:" + err.Error())
	//}
	//ajaxMenu:= m[0][1]
	//ajaxcheck:=""
	//urlAjax:= ajax + ajaxMenu + `&ajxid=1012` + `&ajxid2=1` + `&ajaxcheck=` + ajaxcheck
	//res, err := s.Client().R().Get(urlAjax)
	//s.Trace(res.StatusCode(), res.String())
	//if err != nil {
	//	return ConnectionErr(urlAjax + " : " + err.Error())
	//}

	s.EnableRedirects(true)

	financeSel := `a:containsOwn('Moje finanse')`
	financeEl := s.doc.Find(financeSel)
	if financeEl.Length() != 4 {
		return ParseErr("not found : " + financeSel)
	}
	href, ex := Attr(financeEl.Eq(1), "href")
	if ex != nil {
		return ex
	}
	s.Trace(href)

	url := root + href
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(url))
	if ex != nil {
		return ConnectionErr("GET " + url + " : " + ex.Error())
	}

	ibanSel := `p:containsOwn('Numer konta')`
	ibanEl := s.doc.Find(ibanSel)
	if ibanEl.Length() == 0 {
		return ParseErr("not found " + ibanSel)
	}
	iban := strings.TrimSpace(ibanEl.Text())
	idx := strings.Index(iban, ":")
	if idx == -1 {
		return ParseErr("invalid iban: " + iban)
	}
	iban = strings.TrimSpace(iban[idx+1:])
	l := &model.Location{
		Service:    account.Username,
		Identifier: account.Username,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalName,
		},
	}
	account.AddLocation(l)

	serviceSel := `a:containsOwn('Us�ugi')`
	serviceEl := s.doc.Find(serviceSel)
	if serviceEl.Length() != 1 {
		return ParseErr("not found : " + serviceSel)
	}
	href, ex = Attr(serviceEl.Eq(0), "href")
	if ex != nil {
		return ex
	}
	url = root + href
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(url))
	if ex != nil {
		return ConnectionErr("GET " + url + " : " + ex.Error())
	}
	addrSel := `tr[class^=r] p`
	addrEl := doc.Find(addrSel)
	if addrEl.Length() == 0 {
		return ParseErr("not found :" + addrSel)
	}
	l.Service = strings.TrimSpace(addrEl.Eq(0).Text())
	l.Service = utils.RemoveNonASCII(l.Service)
	return nil
}

func (s *CityNetStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *CityNetStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	rowsSel := `form[id=form_payment] table tbody tr[class^=r]`
	rows := s.doc.Find(rowsSel)
	for i := 0; i < rows.Length(); i++ {
		if ex := s.parseInvoice(location, rows.Eq(i)); ex != nil {
			return ParseErr("parse invoice: " + ex.Error())
		}
	}
	return nil
}

func (s *CityNetStrategy) parseInvoice(location *model.Location, row *goquery.Selection) Exception {
	cols := row.Find(`td`)
	ref := strings.TrimSpace(cols.Eq(0).Text())
	issueString := utils.RemoveUnicodeSpaces([]byte(cols.Eq(1).Text()))
	issueDate, err := time.Parse(invoiceFormat, issueString)
	if err != nil {
		return ParseErr("parse date string: " + issueString)
	}
	am, ex := AmountFromString(utils.RemoveUnicodeSpaces([]byte(cols.Eq(2).Text())))
	if ex != nil {
		return ex
	}
	status := utils.RemoveUnicodeSpaces([]byte(cols.Eq(3).Text()))
	amDue := am
	if strings.ToLower(status) == "tak" {
		amDue = 0.0
	}
	uri, ex := Attr(cols.Eq(4).Find(`a`), "rel")
	if ex != nil {
		return ex
	}
	uri = root + uri
	i := &model.Invoice{
		Ref:       ref,
		PdfUri:    &uri,
		Amount:    am,
		AmountDue: amDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   issueDate.Add(14 * 24 * time.Hour).Format(utils.DBDateFormat),
	}
	location.AddInvoice(i)
	return nil
}

func (s *CityNetStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	paymentsSel := `a:containsOwn('Rozliczenia')`
	href, ex := Attr(s.doc.Find(paymentsSel), "href")
	if ex != nil {
		return ParseErr("payments link: " + ex.Error())
	}
	url := root + href
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(url))
	if ex != nil {
		return ConnectionErr("GET " + url + " : " + ex.Error())
	}
	rowsSel := `div[id=content] table tbody tr[class^=r]`
	rows := s.doc.Find(rowsSel)
	for i := 0; i < rows.Length(); i++ {
		if ex := s.parsePayment(location, rows.Eq(i)); ex != nil {
			return ParseErr("parse payment: " + ex.Error())
		}
	}
	return nil
}

func (s *CityNetStrategy) parsePayment(location *model.Location, row *goquery.Selection) Exception {
	cols := row.Find(`td`)
	ref := strings.TrimSpace(cols.Eq(0).Text())
	issueString := utils.RemoveUnicodeSpaces([]byte(cols.Eq(1).Text()))
	date, err := time.Parse(paymentFormat, issueString)
	if err != nil {
		return ParseErr("parse date string: " + issueString)
	}
	am, ex := AmountFromString(utils.RemoveUnicodeSpaces([]byte(cols.Eq(2).Text())))
	if ex != nil {
		return ex
	}
	p := &model.Payment{
		Ref:    ref,
		Amount: am,
		Date:   date.Format(utils.DBDateFormat),
	}
	location.AddPayment(p)
	return nil
}

func (s *CityNetStrategy) Pdf(account *model.Account) *model.PdfResponse {
	ref := account.Locations[0].Invoices[0].Ref

	account.Locations = account.Locations[:0]
	ex := s.LoadLocations(account)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	ex = s.LoadInvoices(account, account.Locations[0])
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	pdfUri := ""
	for _, invoice := range account.Locations[0].Invoices {
		if ref == invoice.Ref {
			pdfUri = *invoice.PdfUri
			break
		}
	}
	if pdfUri == "" {
		return model.PdfErrorResponse("could not find invoice with ref "+ref, model.OTHER_EXCEPTION)
	}

	pdf, err := s.pdfDownload(pdfUri)
	if err != nil {
		return model.PdfErrorResponse("ref "+ref+": "+ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdf,
		PdfStatus: model.OK.Name,
	}
}

func (s *CityNetStrategy) pdfDownload(uri string) ([]byte, error) {
	post, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"content-type": "application/x-www-form-urlencoded",
	}).SetFormData(map[string]string{
		"ff[print_original]":  "0",
		"ff[print_copy]":      "1",
		"ff[print_duplicate]": "1",
		"ff[set_printed]":     "1",
		"formbutton":          "1",
	}).Post(uri)
	if err != nil {
		return nil, err
	}
	return post.Body(), nil
}

func (s *CityNetStrategy) LoadsInternal() bool {
	return false
}

func (s *CityNetStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *CityNetStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
