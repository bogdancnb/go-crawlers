package egeaenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	home        = "https://netaweb.egea.it/public/egea"
	webServices = "https://netaweb.egea.it/public/prontoweb-web-services"

	loginErrMsg = "Username e Password inserite non corrispondono"
	siteFormat  = "2006-01-02"
)

func Init() {
	RegisterFactory("egea_Energia.crawler", new(EgeaEnergiaFactory))
}

type EgeaEnergiaFactory struct{}

func (p *EgeaEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EgeaEnergiaStrategy")
	return &EgeaEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type EgeaEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document
}

func (s *EgeaEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EgeaEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "netaweb.egea.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())
	identifier, ex := Attr(doc.Find("input[name='identifier']"), "value")
	if ex != nil {
		return nil, ex
	}
	loginSessione, ex := Attr(doc.Find("input[name='loginSessione']"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "netaweb.egea.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://netaweb.egea.it",
		"referer":      "https://netaweb.egea.it/public/egea",
	}).SetFormData(map[string]string{
		"operation":     "login",
		"identifier":    identifier,
		"multi-site":    "egea",
		"i18n-locale":   "",
		"loginSessione": loginSessione,
		"username":      account.Username,
		"password":      *account.Password,
	}).Post(webServices))
	if ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("parse login post html: " + err.Error())
	}
	return &html, nil
}

func (s *EgeaEnergiaStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return ParseErr("could not obtain information")

}

func (s *EgeaEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EgeaEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EgeaEnergiaStrategy) LoadsInternal() bool {
	return false
}

func (s *EgeaEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EgeaEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
