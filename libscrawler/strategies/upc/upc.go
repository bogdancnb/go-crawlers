package upc

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	home          = "https://www.upc.ro/sign-in?protocol=oidc"
	startProtocol = "https://www.upc.ro/rest/v40/session/start?protocol=oidc&rememberMe=false"
	profile       = "https://upc.ro/profile/"
	amountUri     = "https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=PayBillPage_v2"
	accountUri    = "https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=BillDefaultPage_v2"
	paymentUri    = "https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=BillDefaultPage_v2"
	switchUri     = "https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_windowLabel=User_Info_Portlet&_urlType=action"

	badCredentials = "Bad credentials"
	siteFormat     = "02.01.2006"
)

var loginErrMsg = []string{`La password inserita non è corretta`, `Attenzione! Non sei ancora registrato`}
var pdfAmountRegex = regexp.MustCompile(`\s+(\d+,\d{2})\d+,\d{2}Total facturã curentã`)

func isLoginErrMsg(res string) string {
	for _, m := range loginErrMsg {
		if strings.Contains(res, m) {
			return m
		}
	}
	return ""
}

func Init() {
	RegisterFactory("upc.crawler", new(UPCFactory))
}

type UPCFactory struct{}

func (p *UPCFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "UPCStrategy")
	return &UPCStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type UPCStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations        map[string]*model.Location
	exInvoices         map[string]*model.Invoice
	doc                *goquery.Document
	csrfToken          string
	ULMJSESSIONID      string
	JRUNTIMEID         string
	JSESSIONID         string
	BIGipServermyupcro string
	BIGipServermy      string
}

func (s *UPCStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	barcode := generateBarcode(invoice.Ref, invoice.Amount, location.Identifier)
	invoice.BarCode = &barcode
	return nil
}

func generateBarcode(ref string, invoiceAmount float64, locationIdentifier string) string {
	amountString := decimal.NewFromFloat(invoiceAmount).Mul(decimal.NewFromInt(100)).String()
	amountString = fmt.Sprintf("%07s", amountString)
	barcode := fmt.Sprintf(`%s0010000%s%s`, ref, locationIdentifier, amountString)
	return barcode
}

func (s *UPCStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *UPCStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *UPCStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":         "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":     "keep-alive",
		"host":           "www.upc.ro",
		"Sec-Fetch-Dest": "document",
		"Sec-Fetch-Mode": "navigate",
		"Sec-Fetch-Site": "none",
		"Sec-Fetch-User": "?1",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	s.Trace(res)

	payload := fmt.Sprintf(`{"username":"%s","credential":"%s"}`, account.Username, *account.Password)
	resp, err := s.Client().R().SetHeaders(map[string]string{
		"accept":         "*/*",
		"Authorization":  `Atmosphere atmosphere_app_id="AEM_RO"`,
		"Connection":     "keep-alive",
		"content-type":   "application/json; charset=UTF-8",
		"CSRF-Token":     "undefined",
		"host":           "www.upc.ro",
		"origin":         "https://www.upc.ro",
		"referer":        "https://www.upc.ro/sign-in?protocol=oidc",
		"Sec-Fetch-Dest": "empty",
		"Sec-Fetch-Mode": "cors",
		"Sec-Fetch-Site": "same-origin",
	}).SetBody(payload).Post(startProtocol)
	if err != nil {
		return nil, ConnectionErr(startProtocol + " : " + err.Error())
	}
	respString := resp.String()
	s.Debug("login response :", respString)

	if resp.StatusCode() == 401 /*&& strings.Contains(respString, "Bad Credentials") */ {
		return nil, LoginErr(badCredentials)
	}
	if resp.StatusCode() != 200 {
		return nil, ParseErr(startProtocol + " : " + respString)
	}
	s.ULMJSESSIONID = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "ULM-JSESSIONID")
	s.ULMJSESSIONID = s.ULMJSESSIONID[:strings.Index(s.ULMJSESSIONID, ";")]
	s.JRUNTIMEID = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "JRUNTIMEID")
	s.JRUNTIMEID = s.JRUNTIMEID[:strings.Index(s.JRUNTIMEID, ";")]

	s.EnableRedirects(false)

	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(profile)
	s.Trace(resp, err)
	if resp.StatusCode() != 301 {
		return nil, ConnectionErr(profile + " : status " + resp.Status())
	}
	location := HeaderByNameAndValStartsWith(resp.Header(), "Location", "")

	//https://www.upc.ro/profile/
	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "www.upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(location)
	s.Trace(resp, err)
	if resp.StatusCode() != 301 {
		return nil, ConnectionErr(location + " : status " + resp.Status())
	}
	location = HeaderByNameAndValStartsWith(resp.Header(), "Location", "")
	s.Trace(location)

	//https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=DashboardBook_v2
	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "my.upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(location)
	s.Trace(resp, err)
	if resp.StatusCode() != 302 {
		return nil, ConnectionErr(location + " : status " + resp.Status())
	}
	location = HeaderByNameAndValStartsWith(resp.Header(), "Location", "")
	s.Trace(location)
	s.JSESSIONID = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "JSESSIONID")
	s.JSESSIONID = s.JSESSIONID[:strings.Index(s.JSESSIONID, ";")]
	s.BIGipServermyupcro = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "BIGipServermyupcro")
	s.BIGipServermyupcro = s.BIGipServermyupcro[:strings.Index(s.BIGipServermyupcro, ";")]
	s.BIGipServermy = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "BIGipServermy.upc.ro")
	s.BIGipServermy = s.BIGipServermy[:strings.Index(s.BIGipServermy, ";")]

	//https://www.upc.ro/oidc/authorize?client_id=0a73b1b7-fbf3-40de-be0e-9877b6760abd&response_type=code&scope=accounts+openid+email+profile&state=79f82207-e859-4132-a643-8a518fdbd6a9&redirect_uri=https%3A%2F%2Fmy.upc.ro%2Fmyupc-web%2Foidccb
	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "www.upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(location)
	s.Trace(resp, err)
	if resp.StatusCode() != 302 {
		return nil, ConnectionErr(location + " : status " + resp.Status())
	}
	location = HeaderByNameAndValStartsWith(resp.Header(), "Location", "")
	s.Trace(location)

	//https://my.upc.ro/myupc-web/oidccb?code=4Y9sCv&state=79f82207-e859-4132-a643-8a518fdbd6a9
	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "my.upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(location)
	s.Trace(resp, err)
	if resp.StatusCode() != 302 {
		return nil, ConnectionErr(location + " : status " + resp.Status())
	}
	location = HeaderByNameAndValStartsWith(resp.Header(), "Location", "")
	s.Trace(location)
	if location == `https://www.upc.ro/sign-in/add-account` {
		return nil, ParseErr("Codul tău de client nu este asociat cu niciun cont")
	}

	s.EnableRedirects(true)

	//https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=DashboardBook_v2
	resp, err = s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection":                "keep-alive",
		"host":                      "my.upc.ro",
		"referer":                   "https://www.upc.ro/",
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(location)
	s.Trace(resp, err)
	if resp.StatusCode() != 200 {
		return nil, ConnectionErr(location + " : status " + resp.Status())
	}

	s.doc, err = goquery.NewDocumentFromReader(strings.NewReader(resp.String()))
	if err != nil {
		return nil, ParseErr("parse dashboard page: " + err.Error())
	}

	return nil, nil
}

func (s *UPCStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *UPCStrategy) parseCSRFToken() Exception {
	csrfTokenEl := s.doc.Find("input[name=csrfToken]")
	if csrfTokenEl.Length() == 0 {
		return ParseErr("input[name=csrfToken] not found")
	}
	var ex Exception
	s.csrfToken, ex = Attr(csrfTokenEl, "value")
	s.Debug("csrfToken=", s.csrfToken)
	if ex != nil {
		return ex
	}
	return nil
}

func (s *UPCStrategy) LoadLocations(account *model.Account) Exception {
	accountsEl := s.doc.Find(`.navbar-account__account-code`)
	if accountsEl.Length() > 0 {
		s.Debug("multiple client codes, need csrfToken")
		if ex := s.parseCSRFToken(); ex != nil {
			return ex
		}
	}

	for i := 0; i < accountsEl.Length(); i++ {
		trimSpace := strings.TrimSpace(accountsEl.Eq(i).Text())
		if trimSpace == "" {
			html, _ := accountsEl.Html()
			return ParseErr("invalid .navbar-account__account-code content: " + html)
		}
		codeP := strings.Split(trimSpace, " ")
		if len(codeP) < 2 {
			html, _ := accountsEl.Html()
			return ParseErr("invalid .navbar-account__account-code content: " + html)
		}
		code := codeP[1]

		l := &model.Location{
			Service:    code,
			Identifier: code,
		}
		account.AddLocation(l)

		if i > 0 {
			if _, ex := s.switchloc(code); ex != nil {
				return ex
			}
		}
		if ex := s.LoadAmount(account, l); ex != nil {
			return ex
		}
		if ex := s.LoadInvoices(account, l); ex != nil {
			return ex
		}
		if ex := s.LoadPayments(account, l); ex != nil {
			return ex
		}

		if account.ExtractBarcode != nil && *account.ExtractBarcode {
			for _, parsedInvoice := range l.Invoices {
				if parsedInvoice.AmountDue <= 0 {
					continue
				}
				existingInvoice, ok := s.exInvoices[parsedInvoice.Ref]
				if !ok || (ok && (existingInvoice.BarCode == nil || *existingInvoice.BarCode == "")) {
					s.Debug("get barcode for", parsedInvoice.Ref)
					if ex := s.GetBarcode(account, l, parsedInvoice); ex != nil {
						s.WithError(ex).Error("get barcode error for", parsedInvoice.Ref)
					}
				}
			}
		}
	}
	return nil
}

func (s *UPCStrategy) switchloc(clientCode string) (*goquery.Document, Exception) {
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "my.upc.ro",
		"origin":       "https://my.upc.ro",
		"referer":      "https://my.upc.ro/myupc-web/appmanager/portal/home?_nfpb=true&_st=&_pageLabel=DashboardPage_v2",
	}).SetFormData(map[string]string{
		"csrfToken":      s.csrfToken,
		"selectedUser":   clientCode,
		":cq_csrf_token": "undefined",
	}).Post(switchUri))
	if ex != nil {
		return doc, ex
	}
	return doc, nil
}

func (s *UPCStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	//doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
	//	"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
	//	"Connection":                "keep-alive",
	//	"host":                      "my.upc.ro",
	//	"Sec-Fetch-Dest":            "document",
	//	"Sec-Fetch-Mode":            "navigate",
	//	"Sec-Fetch-Site":            "same-site",
	//	"Sec-Fetch-User":            "?1",
	//	"Upgrade-Insecure-Requests": "1",
	//}).Get(amountUri))
	//if ex != nil {
	//	return ex
	//}
	//
	//amtEls := doc.Find("input[id='inputAmount']")
	//amt := 0.0
	//if amtEls.Length() > 0 {
	//	text, ex := Attr(amtEls, "value")
	//	if ex != nil {
	//		return ex
	//	}
	//	s.Debug("found amount element input[id='inputAmount'] value=" + text)
	//	amt, ex = AmountFromString(text)
	//	if ex != nil {
	//		return ex
	//	}
	//}
	//location.Amount = &amt
	return nil
}

func (s *UPCStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection": "keep-alive",
		"host":       "my.upc.ro",
		//"referer":                   amountUri,
		"Sec-Fetch-Dest":            "document",
		"Sec-Fetch-Mode":            "navigate",
		"Sec-Fetch-Site":            "same-site",
		"Sec-Fetch-User":            "?1",
		"Upgrade-Insecure-Requests": "1",
	}).Get(accountUri))
	if ex != nil {
		return ex
	}

	soldEl := doc.Find(".invoiceheader--number")
	am := 0.0
	if soldEl.Length() > 0 {
		am, ex = AmountFromString(strings.TrimSpace(soldEl.Text()))
		if ex != nil {
			return ex
		}
	}
	location.Amount = &am

	locAmtDec := decimal.NewFromFloat(*location.Amount)
	locAmount := &locAmtDec
	invoiceEls := doc.Find("table[id='invoiceslist'] tbody tr")
	for i := 0; i < invoiceEls.Length(); i++ {
		cols := invoiceEls.Eq(i).Find("td")
		if cols.Length() == 0 {
			continue
		}
		var ex Exception
		locAmount, ex = s.parseInvoice(cols, location, locAmount)
		if ex != nil {
			return ex
		}
	}
	paymentEls := doc.Find(`table[id='paymentslist'] tbody tr`)
	for i := 0; i < paymentEls.Length(); i++ {
		cols := paymentEls.Eq(i).Find("td")
		if cols.Length() != 6 {
			continue
		}
		if ex := s.parsePayment(cols, location); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *UPCStrategy) parsePayment(cols *goquery.Selection, location *model.Location) Exception {
	status := cols.Eq(4).Text()
	if !strings.Contains(status, "Efectuată") {
		return nil
	}

	ref := strings.TrimSpace(cols.Eq(0).Text())
	if ref == "0" {
		return nil
	}

	ams := strings.TrimSpace(cols.Eq(5).Text())
	amsP := strings.Split(ams, " ")
	if len(amsP) != 2 {
		return ParseErr("invalid amount string format for:" + ams)
	}
	amt, ex := AmountFromString(amsP[0])
	if ex != nil {
		return ex
	}
	if amt < 0 {
		return nil
	}

	dateS := strings.TrimSpace(cols.Eq(2).Text())
	date, err := time.Parse(siteFormat, dateS)
	if err != nil {
		return ParseErr("parse date string " + dateS + " : " + err.Error())
	}

	p := &model.Payment{
		Ref:    ref,
		Amount: amt,
		Date:   date.Format(utils.DBDateFormat),
	}
	location.AddPayment(p)
	return nil
}

func (s *UPCStrategy) parseInvoice(cols *goquery.Selection, location *model.Location, locAmount *decimal.Decimal) (*decimal.Decimal, Exception) {
	ref := strings.TrimSpace(cols.Eq(0).Text())

	issueS := strings.TrimSpace(cols.Eq(2).Text())
	issueDate, err := time.Parse(siteFormat, issueS)
	if err != nil {
		return locAmount, ParseErr("parse date string " + issueS)
	}

	dueS := strings.TrimSpace(cols.Eq(3).Text())
	dueDate, err := time.Parse(siteFormat, dueS)
	if err != nil {
		return locAmount, ParseErr("parse date string " + dueS)
	}

	ams := strings.TrimSpace(cols.Eq(5).Text())
	amsP := strings.Split(ams, " ")
	if len(amsP) != 2 {
		return locAmount, ParseErr("invalid amount string format for:" + ams)
	}
	amt, ex := AmountFromString(amsP[0])
	if ex != nil {
		return locAmount, ex
	}

	var pdfUri *string
	href, ex := Attr(cols.Eq(5).Find("a"), "href")
	if ex != nil {
		return locAmount, ex
	}
	if href == "" {
		s.Debug("could not find pdf link for ref=", ref)
	} else {
		href = "https://my.upc.ro" + href + "|" + location.Identifier
		pdfUri = &href
	}

	inv := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}

	status := cols.Eq(4).Text()
	if strings.Contains(status, "Efectuată") {
		inv.Amount = amt
		inv.AmountDue = 0
	} else {
		locAmtVal, exact := locAmount.Float64()
		if !exact {
			s.Warnf("Decimal's float64 value not exact: %f", locAmtVal)
		}

		amtParsed, err := s.amountFromPdf(inv)
		s.Debug("amount parsed from pdf: ", amtParsed, ", error: ", err)
		if err != nil {
			s.Debug("could not parse amount from pdf for ref", ref, " : ", err.Error())
			// PDF not available - https://pagojira.atlassian.net/browse/PAGO-5098
			inv.Amount = amt
			inv.AmountDue = locAmtVal
		} else {
			inv.Amount = amtParsed
			if amtParsed < locAmtVal {
				inv.AmountDue = amtParsed
				if amtParsed > 0 {
					locAmountRemain := locAmount.Sub(decimal.NewFromFloat(amtParsed))
					locAmount = &locAmountRemain
				}
			} else if locAmtVal > 0 {
				inv.AmountDue = locAmtVal
				locAmount = &decimal.Zero
			}
		}
	}
	location.AddInvoice(inv)
	return locAmount, nil
}

func (s *UPCStrategy) amountFromPdf(inv *model.Invoice) (float64, error) {
	pdfBytes, ex := s.downloadPdf(inv)
	if ex != nil {
		return 0, ex
	}
	pdfBytes = TrimPdf(pdfBytes)
	pdftext, err := PlainText(pdfBytes)
	if err != nil {
		return 0, fmt.Errorf(`parsing pdf text:` + err.Error())
	}
	matches, err := utils.MatchRegex(pdftext, pdfAmountRegex)
	if err != nil {
		return 0, fmt.Errorf(`matching amount regex for pdf text` + err.Error())
	}
	m := matches[0][1]
	amt, ex := AmountFromString(m)
	if ex != nil {
		return 0, ex
	}
	return amt, nil
}

func (s *UPCStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *UPCStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	uri := pdfUri

	if ex := s.parseCSRFToken(); ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}

	uriP := strings.Split(pdfUri, "|")
	if len(uriP) == 2 {
		if _, ex := s.switchloc(uriP[1]); ex != nil {
			return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
		}
		uri = uriP[0]
	}

	pdfBytes, ex := s.getBytes(uri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *UPCStrategy) downloadPdf(invoice *model.Invoice) ([]byte, error) {
	pdfUri := *invoice.PdfUri
	uri := pdfUri
	uriP := strings.Split(pdfUri, "|")
	if len(uriP) == 2 {
		uri = uriP[0]
	}
	pdfBytes, ex := s.getBytes(uri)
	return pdfBytes, ex
}

func (s *UPCStrategy) getBytes(uri string) ([]byte, Exception) {
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"Connection": "keep-alive",
		"host":       "my.upc.ro",
	}).Get(uri))
	return pdfBytes, ex
}

func (s *UPCStrategy) LoadsInternal() bool {
	return true
}

func (s *UPCStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *UPCStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
