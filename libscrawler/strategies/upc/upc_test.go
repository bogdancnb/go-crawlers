package upc

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "upc.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"grigorcelestin@yahoo.com", "@19800707Gc"}, //ok
		//{"a.george04@yahoo.com", "Callatis1"}, //ok 2 locs
		//{"bogdan@before.ro", "norton5012"}, //5 locs
		//{"fromipad2@yahoo.com", "giulesti424"}, //amount from pdf
		//{"carmen_barboi@yahoo.com", "182821kna"}, //login err
		//{"caliman_daniel@yahoo.com", "Baronul1962."},
		//{"sorin.meleca@gmail.com", "Srmlksrmlk79"},
		//{"allyy_jucan@yahoo.com", "Alinutza2909@"}, //no contract
		//{"elenapoleac6@gmail.com", "elenapoleac6@gmail.com"},
		//{"adrian@tutorialevideo.info", "teiubesc88"},

		//{"georgianbobocbz@gmail.com", "Asdfghjkl01Info"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		//{
		//	Username: "fromipad2@yahoo.com", Password: "giulesti424", Uri: "upc.crawler",
		//	Identifier: "2260241",
		//	Ref:        "532043528", PdfUri: "https://my.upc.ro/myupc-web/public/invoice/P1RO/202012/53204/532043528-00.PDF|2260241",
		//},

		{
			Username: "puscasuvlad21@gmail.com", Password: "3c0n0m1cus", Uri: "upc.crawler",
			Identifier: "2874263",
			Ref:        "533332831", PdfUri: "https://my.upc.ro/myupc-web/public/invoice/P1RO/202012/53333/533332831-00.PDF|2874263",
		},

		//{
		//	Username: "puscasuvlad21@gmail.com", Password: "3c0n0m1cus", Uri: "upc.crawler",
		//	Identifier: "3438455",
		//	Ref:        "532629730", PdfUri: "https://my.upc.ro/myupc-web/public/invoice/P1RO/202012/53262/532629730-00.PDF|3438455",
		//},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}

func Test_generateBarcode(t *testing.T) {
	want := "532658544001000030957790007990"
	got := generateBarcode("532658544", 79.90, "3095779")
	t.Logf("\n got=" + got + "\nwant=" + want)
	if got != want {
		t.Error()
	}
}
