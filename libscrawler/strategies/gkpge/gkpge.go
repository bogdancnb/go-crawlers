package gkpge

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	index     = "https://ebok.gkpge.pl/ebok/index.xhtml"
	logowanie = "https://ebok.gkpge.pl/ebok/profil/logowanie.xhtml"
	switchUrl = "https://ebok.gkpge.pl/ebok/konta/zarzadzanieKontami.xhtml"
	finances  = "https://ebok.gkpge.pl/ebok/finanse/finanse.xhtml"

	siteFormat   = "02.01.2006"
	siteFormat2  = "01/02/2006"
	tempLoginErr = `Podane dane uwierzytelniania są niewłaściwe lub zawierają spację. Jeżeli widzisz ten komunikat po raz kolejny, może to oznaczać że twoje konto zostało zablokowane. Po upływie 10 minut konto zostanie odblokowane`
)

var (
	bankName    = "Bank Polska Kasa Opieki SA"
	bankBIC     = "PKOPPLPW"
	legalEntity = "PGE Polska Grupa Energetyczna S.A."

	primeFacesRegex   = regexp.MustCompile(`.*PrimeFaces\.addSubmitParam\('(?P<p1>\w+)',\{'(?P<p2>[\w:]+)'\:'(?P<p3>[\w:]+)'\}\)\.submit\('(?P<p4>\w+)'\).*`)
	primeFacesAbRegex = regexp.MustCompile(`checkAndUpdateSessionTimeoutSettings = function\(\) \{PrimeFaces\.ab\({s:"(?P<p1>\w+)",f:"(?P<p2>\w+)",g`)
	primeFacesCwRegex = regexp.MustCompile(`PrimeFaces\.cw\("SelectBooleanCheckbox","widget_.+",{id:"(?P<p12>(?P<p1>\w+):(?P<p2>\w+))"}\).+PrimeFaces\.cw\("CommandButton","widget_.+",{id:"(?P<p34>(?P<p3>\w+):(?P<p4>\w+))"}\)`)
	cdataRegex        = regexp.MustCompile(`CDATA\[([\d:\-]+)\]`)
)

func Init() {
	factory := new(GkPgeFactory)
	RegisterFactory("pge.crawler", factory)
}

type GkPgeFactory struct{}

func (p *GkPgeFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "GkPgeStrategy")
	return &GkPgeStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type GkPgeStrategy struct {
	*logrus.Entry
	HttpStrategy
	accDoc              *goquery.Document
	switchJavaViewState string
	jSessionId          string
	javaxfacesViewState string
}

func (s *GkPgeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *GkPgeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *GkPgeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *GkPgeStrategy) Login(acc *model.Account) (*string, Exception) {
	resp, err := s.Client().R().Get(logowanie)
	if err != nil {
		return nil, ConnectionErr(logowanie + ": " + err.Error())
	}
	s.jSessionId = HeaderByNameAndValStartsWith(resp.Header(), "Set-Cookie", "JSESSIONID")
	s.jSessionId = s.jSessionId[11:strings.Index(s.jSessionId, "; path")]
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(resp.Body()))
	if err != nil {
		return nil, ParseErr("parsing " + logowanie + ": " + err.Error())
	}
	javaxfacesViewState, ok := doc.Find("input[name='javax.faces.ViewState']").Eq(0).Attr("value")
	if !ok {
		return nil, ParseErr("javaxfacesViewState not found")
	}

	url := "https://ebok.gkpge.pl/ebok/profil/logowanie.xhtml;jsessionid=" + s.jSessionId
	var ex Exception
	s.accDoc, ex = OkDocument(s.Client().R().SetFormData(map[string]string{
		"hiddenLoginForm":                "hiddenLoginForm",
		"hiddenLoginForm:hiddenLogin":    acc.Username,
		"hiddenLoginForm:hiddenPassword": *acc.Password,
		"hiddenLoginForm:loginButton":    "",
		"javax.faces.ViewState":          javaxfacesViewState,
	}).Post(url))
	if ex != nil {
		return nil, ConnectionErr(url + ": " + ex.Error())
	}
	html, err := s.accDoc.Html()
	if err != nil {
		return nil, ParseErr("parse login response html: " + err.Error())
	}
	if strings.Contains(html, tempLoginErr) {
		return nil, ParseErr(tempLoginErr)
	}

	popupSel := `div[id=regulaminPopup]`
	popupEl := s.accDoc.Find(popupSel)
	if popupEl.Length() > 0 {
		s.Debug("retry, popup found " + popupSel)
		javaxfacesViewState, ok = s.accDoc.Find("input[name='javax.faces.ViewState']").Eq(0).Attr("value")
		if !ok {
			return nil, ParseErr("javaxfacesViewState not found")
		}
		html, _ := s.accDoc.Html()
		params := utils.MatchRegexGroups(html, primeFacesAbRegex)

		responseString, ex := OkString(s.Client().R().SetHeaders(map[string]string{
			"accept":            "application/xml, text/xml, */*; q=0.01",
			"connection":        "keep-alive",
			"content-type":      "application/x-www-form-urlencoded; charset=UTF-8",
			"Faces-Request":     "partial/ajax",
			"referer":           url,
			"X-Requested-With":  "XMLHttpRequest",
			"X-TS-AJAX-Request": "true",
		}).SetFormData(map[string]string{
			"javax.faces.partial.ajax":    "true",
			"javax.faces.source":          params["p1"],
			"javax.faces.partial.execute": "@all",
			params["p1"]:                  params["p1"],
			params["p2"]:                  params["p2"],
			"javax.faces.ViewState":       javaxfacesViewState,
		}).Post(logowanie))
		s.Trace(responseString)
		if ex != nil {
			return nil, ex
		}
		m, err := utils.MatchRegex(responseString, cdataRegex)
		if err != nil {
			return nil, ParseErr("parse cdataRegex: " + err.Error())
		}
		javaxfacesViewState = m[0][1]

		params2 := utils.MatchRegexGroups(html, primeFacesCwRegex)
		responseString, ex = OkString(s.Client().R().SetHeaders(map[string]string{
			"accept":        "application/xml, text/xml, */*; q=0.01",
			"connection":    "keep-alive",
			"content-type":  "application/x-www-form-urlencoded; charset=UTF-8",
			"Faces-Request": "partial/ajax",
			//"referer":           url,
			"X-Requested-With":  "XMLHttpRequest",
			"X-TS-AJAX-Request": "true",
		}).SetFormData(map[string]string{
			"javax.faces.partial.ajax":    "true",
			"javax.faces.source":          params2["p34"], //javax.faces.source: j_idt675:j_idt682
			"javax.faces.partial.execute": "@all",
			"javax.faces.partial.render":  params2["p1"] + ":messages1", //javax.faces.partial.render: j_idt675:messages1
			params2["p34"]:                params2["p34"],               //j_idt675:j_idt682: j_idt675:j_idt682
			params2["p1"]:                 params2["p1"],                //j_idt675: j_idt675
			params2["p12"] + "_input":     "on",                         //j_idt675:j_idt680_input: on
			"javax.faces.ViewState":       javaxfacesViewState,
		}).Post("https://ebok.gkpge.pl/ebok/profil/logowanie.xhtml"))
		s.Trace(responseString)
		if ex != nil {
			return nil, ex
		}

		s.accDoc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
			"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
			//"referer":           url,
		}).Get(index))
		if ex != nil {
			return nil, ConnectionErr(url + ": " + ex.Error())
		}
	}
	text, _ := s.accDoc.Html()
	return &text, nil
}

func (s *GkPgeStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, "Podane dane uwierzytelniania są niewłaściwe") {
		return LoginErr("")
	}
	if strings.Contains(*response, tempLoginErr) {
		return ParseErr(tempLoginErr)
	}
	return nil
}

func (s *GkPgeStrategy) LoadLocations(account *model.Account) Exception {
	javaxfacesViewState, exception := s.parseJavaxfacesViewState()
	if exception != nil {
		return exception
	}

	res, ex := OkString(s.Client().R().SetFormData(map[string]string{
		"javax.faces.partial.ajax":              "true",
		"javax.faces.source":                    "userActionForm:kontoPanel",
		"javax.faces.partial.execute":           "userActionForm:kontoPanel",
		"javax.faces.partial.render":            "userActionForm:kontoPanel",
		"userActionForm:kontoPanel":             "userActionForm:kontoPanel",
		"userActionForm:kontoPanel_contentLoad": "true",
		"userActionForm":                        "userActionForm",
		"javax.faces.ViewState":                 javaxfacesViewState,
	}).Post(index))
	if ex != nil {
		return ConnectionErr(index + ": " + ex.Error())
	}
	s.switchJavaViewState = res[strings.LastIndex(res, "javax.faces.ViewState:0\"><![CDATA[")+34 : strings.LastIndex(res, "]]")]
	res = res[strings.Index(res, "[CDATA[")+7 : strings.Index(res, "]]")]
	doc, ex := DocFromString(WrapHtmlTableBody(res))
	if ex != nil {
		return ex
	}

	options := doc.Find("tbody[id='userActionForm:dostepyTableHeader_data'] tr")
	if options.Length() == 0 {
		return ParseErr("no account dropdown found")
	}

	for i := 0; i < options.Length(); i++ {
		spans := options.Eq(i).Find("span")
		if spans.Length() != 2 {
			return ParseErr("account option " + options.Eq(i).Text())
		}
		clientNr := spans.Eq(1).Text()
		name := spans.Eq(0).Text()
		dataRK, ok := options.Eq(i).Attr("data-rk")
		if !ok {
			return ParseErr("account option " + options.Eq(i).Text())
		}
		account.AddLocation(&model.Location{
			Service:        fmt.Sprintf(`%s: %s`, name, clientNr),
			Identifier:     clientNr,
			AccountHelpers: dataRK,
		})
	}
	return nil
}

func (s *GkPgeStrategy) parseJavaxfacesViewState() (string, Exception) {
	javaxfacesViewState, ok := s.accDoc.Find("input[name='javax.faces.ViewState']").Eq(0).Attr("value")
	if !ok {
		return "", ParseErr("javaxfacesViewState not found")
	}
	return javaxfacesViewState, nil
}

func (s *GkPgeStrategy) LoadAmount(a *model.Account, l *model.Location) Exception {
	currLoc := s.accDoc.Find("label[class='ui-outputlabel ui-widget  hello-name orange']")
	if currLoc.Length() == 0 {
		return ParseErr("not found current location")
	}
	currLocText := currLoc.Text()
	if currLocText != l.Identifier {
		s.Debugf("switching to location %s from current %s", l.Identifier, currLocText)
		if ex := s.switchToLocation(l); ex != nil {
			return ex
		}
	}

	return nil
}

func (s *GkPgeStrategy) switchToLocation(l *model.Location) Exception {
	resp, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":            "application/xml, text/xml, */*; q=0.01",
		"connection":        "keep-alive",
		"content-type":      "application/x-www-form-urlencoded; charset=UTF-8",
		"Faces-Request":     "partial/ajax",
		"referer":           "https://ebok.gkpge.pl/ebok/index.xhtml",
		"X-Requested-With":  "XMLHttpRequest",
		"X-TS-AJAX-Request": "true",
	}).SetFormData(map[string]string{
		"javax.faces.partial.ajax":                                "true",
		"javax.faces.source":                                      "userActionForm:dostepyTableHeader",
		"javax.faces.partial.execute":                             "userActionForm:dostepyTableHeader",
		"javax.faces.partial.render":                              "@all",
		"javax.faces.behavior.event":                              "rowSelect",
		"javax.faces.partial.event":                               "rowSelect",
		"userActionForm:dostepyTableHeader_instantSelectedRowKey": l.AccountHelpers.(string),
		"userActionForm":                                          "userActionForm",
		"userActionForm:dostepyTableHeader_selection":             l.AccountHelpers.(string),
		"javax.faces.ViewState":                                   s.switchJavaViewState,
	}).Post(index))
	if ex != nil {
		return ex
	}
	s.Trace(resp)

	s.accDoc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"referer": "https://ebok.gkpge.pl/ebok/index.xhtml",
		//"referer":           url,
	}).Get(index))
	if ex != nil {
		return ConnectionErr(index + ": " + ex.Error())
	}
	return nil
}

func (s *GkPgeStrategy) LoadInvoices(a *model.Account, l *model.Location) Exception {
	javaxfacesViewState, exception := s.parseJavaxfacesViewState()
	if exception != nil {
		return exception
	}
	invoiceLingSel := `ul[class="ui-menu-list ui-helper-reset"] li`
	invoiceLink := s.accDoc.Find(invoiceLingSel)
	if invoiceLink.Length() < 2 {
		return ParseErr("could not find " + invoiceLingSel)
	}
	invoiceLink = invoiceLink.Eq(1).Find("a")
	params, ex := primeFacesParams(invoiceLink)
	if ex != nil {
		return ex
	}
	doc, ex := OkDocument(s.Client().R().SetFormData(map[string]string{
		params["p1"]:            params["p4"],
		"javax.faces.ViewState": javaxfacesViewState,
		params["p2"]:            params["p3"],
	}).Post(index))
	if ex != nil {
		return ex
	}
	exception = s.parseAccNr(doc, l)
	if exception != nil {
		return exception
	}
	ok := false
	s.javaxfacesViewState, ok = doc.Find("input[name='javax.faces.ViewState']").Eq(0).Attr("value")
	if !ok {
		return ParseErr("javaxfacesViewState not found")
	}
	invoiceTable := doc.Find("tbody[id*='listaFaktur_data']")
	if invoiceTable.Length() == 0 {
		s.Error(doc.Html())
		return ParseErr("invoice table not found")
	}
	idParam, _ := invoiceTable.Attr("id")
	idParam = idParam[:strings.Index(idParam, "_data")]
	rows := invoiceTable.Find("tr")
	for i := 0; i < rows.Length(); i++ {
		if e := s.parseInvoice(rows, i, l, idParam, s.javaxfacesViewState, true); e != nil {
			return ParseErr("parsing invoice " + rows.Eq(i).Text() + ": " + e.Error())
		}
	}

	//doc, ex := OkDocument(s.Client().R().Get(finances))
	//if ex != nil {
	//	return ex
	//}
	//s.Trace(doc.Html())
	return nil
}

func primeFacesParams(elem *goquery.Selection) (map[string]string, Exception) {
	onclick, ex := Attr(elem, "onclick")
	if ex != nil {
		return nil, ex
	}
	res := utils.MatchRegexGroups(onclick, primeFacesRegex)
	return res, nil
}

func (s *GkPgeStrategy) parseInvoice(rows *goquery.Selection, i int, l *model.Location, idParam string, javaxfacesViewState string, parseDetails bool) Exception {
	cols := rows.Eq(i).Find("td")
	amountDue, ex := parseAmount(cols.Eq(2))
	if ex != nil {
		return ex
	}
	amount, ex := parseAmount(cols.Eq(1))
	if ex != nil {
		return ex
	}
	if amountDue > 0.0 || parseDetails {
		if details, ex := s.loadInvoiceDetails(l, i, idParam, javaxfacesViewState); ex != nil {
			return ParseErr("parsing invoice details " + rows.Eq(i).Text() + ": " + ex.Error())
		} else {
			s.Debug(details)
			issueDate, err := time.Parse(siteFormat, details.issueString)
			if err != nil {
				return ParseErr("could not parse issue date " + details.issueString)
			}
			if issueDate.After(time.Now()) {
				s.Debug("future issue date:", issueDate)
				return nil
			}
			dueString := utils.RemoveSpaces(cols.Eq(0).Text())
			dueDate, err := time.Parse(siteFormat, dueString)
			if err != nil {
				return ParseErr("could not parse due date " + cols.Eq(0).Text())
			}
			invoice := &model.Invoice{
				Ref:       details.ref,
				Amount:    amount,
				AmountDue: amountDue,
				IssueDate: issueDate.Format(utils.DBDateFormat),
				DueDate:   dueDate.Format(utils.DBDateFormat),
				PdfUri:    details.pdfUri,
			}
			l.AddInvoice(invoice)
		}
	}
	return nil
}

func (s *GkPgeStrategy) parseAccNr(doc *goquery.Document, l *model.Location) Exception {
	var accNrS string

	accNr := doc.Find("div[class*='accountPanel']")

	if accNr.Length() == 0 {
		accNr = doc.Find(".accountPanel")
		if accNr.Length() == 0 {
			return ParseErr("could not find .accountPanel element")
		}
		for i := 0; i < accNr.Length(); i++ {
			if strings.Contains(accNr.Text(), "Konto bankowe") {
				label := accNr.Find("label")
				if label.Length() == 0 {
					return ParseErr("could not find .accountPanel label element")
				}
				accNrS = label.Text()
				accNrS = utils.RemoveSpaces(accNrS)
			}
		}
		return ParseErr("could not find .accountPanel label element")
	}

	if accNr.Length() != 2 {
		return ParseErr("could not find acc nr element")
	}
	if subdiv := accNr.Eq(1).Find("div"); subdiv.Length() != 2 {
		return ParseErr("could not find acc nr element")
	} else {
		accNrS = accNr.Eq(1).Find("div").Eq(1).Text()
		accNrS = utils.RemoveSpaces(accNrS)
	}
	if accNrS == "" {
		return ParseErr("could not find acc nr element")
	}
	l.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNrS,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalEntity,
	}
	return nil
}

type invoiceDetails struct {
	ref         string
	issueString string
	pdfUri      *string
}

func (s *GkPgeStrategy) loadInvoiceDetails(l *model.Location, index int, param string, javaxfacesViewState string) (*invoiceDetails, Exception) {
	prefix := param[:strings.Index(param, ":listaFaktur")]
	res, ex := OkString(s.Client().R().SetFormData(map[string]string{
		"javax.faces.partial.ajax":               "true",
		"javax.faces.source":                     param,
		"javax.faces.partial.execute":            param,
		"javax.faces.partial.render":             param,
		"javax.faces.behavior.event":             "rowToggle",
		"javax.faces.partial.event":              "rowToggle",
		prefix + ":listaFaktur_rowExpansion":     "true",
		prefix + ":listaFaktur_expandedRowIndex": strconv.Itoa(index),
		prefix + ":listaFaktur_encodeFeature":    "true",
		prefix + ":listaFaktur_skipChildren":     "true",
		prefix:                                   prefix,
		"javax.faces.ViewState":                  javaxfacesViewState,
	}).Post(finances))
	if ex != nil {
		return nil, ex
	}
	res = res[strings.Index(res, "[CDATA[")+7 : strings.Index(res, "]]")]
	doc, ex := DocFromString(WrapHtmlTableBody(res))
	if ex != nil {
		return nil, ex
	}

	ref := doc.Find("label[id*='numerDokumentu']")
	if ref.Length() == 0 {
		return nil, ParseErr("could not parse ref " + ref.Text())
	}
	el := doc.Find("label:containsOwn('Data wystawienia')")
	if el.Length() == 0 {
		s.Error(doc.Html())
		return nil, ParseErr("issue date element not found ")
	}
	var pdfUri *string
	btn := doc.Find("button[id*=':listaFaktur:']")
	if btn.Length() == 0 {
		s.Debug("no pdf button found")
	} else {
		text := btn.Eq(0).Text()
		if strings.Contains(text, "Pobierz eFakturę") {
			if attr, ok := btn.Eq(0).Attr("disabled"); ok && attr == "disabled" {
				s.Debug("pdf button disabled for ", ref)
			} else {
				uri, _ := btn.Eq(0).Attr("id")
				pdfUri = &uri
			}
		} else {
			s.Debug("pdf button not available for ", ref)
		}
	}
	return &invoiceDetails{
		ref:         ref.Text(),
		issueString: el.Next().Next().Text(),
		pdfUri:      pdfUri,
	}, nil
}

func parseAmount(s *goquery.Selection) (float64, Exception) {
	parts := strings.Split(s.Text(), " ")
	part := strings.ReplaceAll(parts[0], ",", ".")
	part = utils.RemoveNonASCII(part)
	am, err := strconv.ParseFloat(part, 64)
	if err != nil {
		return 0.0, ParseErr("could not parse amount from " + s.Text())
	}
	return am, nil
}

func (s *GkPgeStrategy) LoadPayments(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *GkPgeStrategy) Pdf(a *model.Account) *model.PdfResponse {
	loc := a.Locations[0]
	invoice := a.Locations[0].Invoices[0]
	ref := invoice.Ref
	pdfUri := *invoice.PdfUri
	s.Debugf("downloading pdf for ref=%s, pdfUri=%s", pdfUri, ref)

	a.Locations = make([]*model.Location, 0, 0)
	if ex := s.LoadLocations(a); ex != nil {
		return model.PdfErrorResponse("pdf error for "+ref+": "+ex.Error(), model.OTHER_EXCEPTION)
	}
	for _, l := range a.Locations {
		if l.Identifier == loc.Identifier {
			if ex := s.switchToLocation(l); ex != nil {
				return model.PdfErrorResponse("pdf error for "+ref+": "+ex.Error(), model.OTHER_EXCEPTION)
			}
			if ex := s.LoadInvoices(a, l); ex != nil {
				return model.PdfErrorResponse("pdf error for "+ref+": "+ex.Error(), model.OTHER_EXCEPTION)
			}
			for _, i := range l.Invoices {
				if i.Ref == ref {
					if i.PdfUri == nil {
						return model.PdfErrorResponse("no pdf available for "+ref, model.OTHER_EXCEPTION)
					}
					pdfUri = *i.PdfUri
					break
				}
			}
			break
		}
	}

	parts := strings.Split(pdfUri, ":")
	pdfBytes, ex := GetBytes(s.Client().R().SetFormData(map[string]string{
		parts[0]:                     parts[0],
		parts[0] + ":kwotaOd_input":  "",
		parts[0] + ":kwotaOd_hinput": "",
		parts[0] + ":kwotaDo_input":  "",
		parts[0] + ":kwotaDo_hinput": "",
		parts[0] + ":j_idt675_input": "",
		parts[0] + ":j_idt677_input": "",
		parts[0] + ":adres_focus":    "",
		parts[0] + ":adres_input":    "",
		pdfUri:                       "",
		"javax.faces.ViewState":      s.javaxfacesViewState,
	}).SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "ebok.gkpge.pl",
		"origin":       "https://ebok.gkpge.pl",
		"referer":      "https://ebok.gkpge.pl/ebok/finanse/finanse.xhtml",
		"cookie":       "JSESSIONID=" + s.jSessionId,
	}).Post(finances))
	if ex != nil {
		return model.PdfErrorResponse("pdf error for "+ref+": "+ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *GkPgeStrategy) LoadsInternal() bool {
	return false
}

func (s *GkPgeStrategy) Close(a *model.Account) {
	s.HttpStrategy.Close(a)
}

func (s *GkPgeStrategy) SendConnectionError() {
}

func (s *GkPgeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *GkPgeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
