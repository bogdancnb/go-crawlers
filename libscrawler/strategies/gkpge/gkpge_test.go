package gkpge

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "pge.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"sylwek1211@interia.pl", "1234sl@SL"},
		//{"kamil.oleksiak@gmail.com", "kamil.oleksiak@gmail.com"},
		//{"p.jednacz@gmail.com", "1z2w3OQp&"}, //pdf
		//{"ppaul1989@gmail.com", "EHv28RPY!"},
		//{"vincent.duthel@gmail.com", "Dhju100l!"},

		//{"wasyl450", "Wasyl450"},
		//{"ryszardmyslicki@gmail.com", "Wasyl450"},
		{"litynski98@gmail.com", "13579Lukas@"},
		//{"wojm8@wp.pl", "14022012Ania!"}, //{"pagocrawlererror":"ParseException: could not find .accountPanel element","exitIP":"188.240.3.36"}

		//https://pagojira.atlassian.net/browse/PAGO-16865
		//{"redo@spoko.pl", "Rafik1968&maciek"},

		//https://pagojira.atlassian.net/browse/PAGO-17310
		//{"sebaloz@wp.pl", "Aa214214!"},

		//https://pagojira.atlassian.net/browse/PAGO-18315
		//{"maciej.muller@gmail.com", "Maciek88"},
		//
		//{"dark_raven@wp.pl", "J@ponia01gp"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		//{
		//	Username: "p.jednacz@gmail.com", Password: "1z2w3OQp&", Uri: "pge.crawler",
		//	Identifier: "11405201",
		//	Ref:        "O793554960", PdfUri: /*"j_idt668:listaFaktur:3:j_idt752",*/ "j_idt668:listaFaktur:0:j_idt751",
		//},
		{
			Username: "vincent.duthel@gmail.com", Password: "Dhju100l!", Uri: "pge.crawler",
			Identifier: "28115326",
			Ref:        "28115326/25R/2020", PdfUri: "xxx",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
