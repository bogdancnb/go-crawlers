package asirom

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "asirom.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"spiridonalina3@gmail.com", "Alina&Vali13"}, //nevalidat
		//{"ale_sw3ety@yahoo.com", "qfPu7vp:[%2C<UYp"}, //no due
		{"dana_c28@yahoo.com", "Isis0704@"}, //due
		//{"nicky.filipas@gmail.com", "Pamela&1993\n\n"}, //EUR
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
