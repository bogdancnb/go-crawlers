package asirom

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	root             = "https://www.myasirom.ro/myAsirom"
	loginErr         = `Adresa de mail sau parola, invalide`
	policyRegexpS    = `Wicket\.Ajax\.ajax\({"u":"\.\.\/(.+)","e":"click","c":"%s"`
	duePolicyRegexpS = `Wicket\.Ajax\.ajax\({"u":"\.\.\/\.\.\/(.+)","e":"click","c":"%s"}\);;`
	siteFormat       = "02/01/2006"
)

var (
	loginPostRegexp   = regexp.MustCompile(`Wicket\.Ajax\.ajax\({"f":"id2","u":"\.(.+)","e":"click","c":"id3"`)
	ajaxBaseUrlRegexp = regexp.MustCompile(`Wicket\.Ajax\.baseUrl="(.+)"`)
)

func Init() {
	RegisterFactory("asirom.crawler", new(AsiromFactory))
}

type AsiromFactory struct{}

func (p *AsiromFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AsiromStrategy")
	return &AsiromStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		locs:         make(map[string]*model.Location),
	}
}

type AsiromStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc  *goquery.Document
	locs map[string]*model.Location
}

func (s *AsiromStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AsiromStrategy) EInvoiceNeedsLoadLocations() bool { return false }

func (s *AsiromStrategy) ActivateEInvoice(a *model.Account, r *model.Location, e model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *AsiromStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Connection": "keep-alive",
		"Host":       "www.myasirom.ro",
		"User-Agent": AgentChrome78,
	})
	s.EnableRedirects(false)

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
	}).Get(root + "/")
	if err != nil {
		s.Warn("GET " + root + "/ : " + err.Error())
	}
	if res == nil {
		return nil, ParseErr("nil response: GET " + root + "/")
	}
	redirectUrlWicketCrypt := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	wicketCryptParts := strings.Split(redirectUrlWicketCrypt, "?")
	if len(wicketCryptParts) != 2 {
		return nil, ParseErr("invalid redirect url: " + redirectUrlWicketCrypt)
	}

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
	}).Get(redirectUrlWicketCrypt))
	if ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("parsing html doc: " + err.Error())
	}
	matches, err := utils.MatchRegex(html, loginPostRegexp)
	if err != nil {
		return nil, ParseErr("parsing url for login post: " + err.Error())
	}
	loginFormAction := matches[0][1]
	postUrl := root + loginFormAction

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":                  "application/xml, text/xml, */*; q=0.01",
		"Content-Type":            "application/x-www-form-urlencoded; charset=UTF-8",
		"Origin":                  "https://www.myasirom.ro",
		"Referer":                 redirectUrlWicketCrypt,
		"Wicket-Ajax":             "true",
		"Wicket-Ajax-BaseURL":     "?" + wicketCryptParts[1],
		"Wicket-FocusedElementId": "id3",
		"X-Requested-With":        "XMLHttpRequest",
	}).SetFormData(map[string]string{
		"id2_hf_0": "",
		"username": account.Username,
		"password": *account.Password,
		"closeOK":  "1",
	}).Post(postUrl))
	if ex != nil {
		return nil, ex
	}
	html, err = doc.Html()
	if err != nil {
		return nil, ParseErr("parsing html doc: " + err.Error())
	}
	if strings.Contains(html, loginErr) {
		return nil, LoginErr(loginErr)
	}

	res, err = s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": redirectUrlWicketCrypt,
	}).Get(root + "/")
	if res == nil {
		return nil, ParseErr("nil response: GET " + root + "/")
	}
	redirectUrl2 := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": redirectUrlWicketCrypt,
	}).Get(redirectUrl2))
	if ex != nil {
		return nil, ex
	}
	html, err = doc.Html()
	if err != nil {
		return nil, ParseErr("parsing html doc: " + err.Error())
	}
	s.EnableRedirects(true)
	return &html, nil
}

func (s *AsiromStrategy) CheckLogin(response *string) Exception { return nil }

func (s *AsiromStrategy) LoadLocations(account *model.Account) Exception {
	html, err := s.doc.Html()
	if err != nil {
		return ParseErr("parsing html doc: " + err.Error())
	}
	politeEl := s.doc.Find("a:containsOwn('Polite')")
	if politeEl.Length() == 0 {
		return ParseErr("could not find policy link")
	}
	idPolite, ex := Attr(politeEl.Eq(0), "id")
	if ex != nil {
		return ex
	}
	policyRegexStr := fmt.Sprintf(policyRegexpS, idPolite)
	policyRegexp := regexp.MustCompile(policyRegexStr)
	matches, err := utils.MatchRegex(html, policyRegexp)
	if err != nil {
		return ParseErr("parsing policies url: " + err.Error())
	}
	url := matches[0][1]
	url = root + "/" + url + "?_=" + utils.MillisString(time.Now())
	matches, err = utils.MatchRegex(html, ajaxBaseUrlRegexp)
	if err != nil {
		return ParseErr("parsing ajax base url: " + err.Error())
	}
	ajaxBaseUrl := matches[0][1]

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":                  "application/xml, text/xml, */*; q=0.01",
		"Referer":                 root + "/" + ajaxBaseUrl,
		"Wicket-Ajax":             "true",
		"Wicket-Ajax-BaseURL":     ajaxBaseUrl,
		"Wicket-FocusedElementId": idPolite,
		"X-Requested-With":        "XMLHttpRequest",
	}).Get(url)
	if res == nil {
		return ConnectionErr("GET " + url)
	}
	if err != nil {
		return ConnectionErr("GET " + url + " : " + err.Error())
	}
	ajaxLocation := HeaderByNameAndValStartsWith(res.Header(), "Ajax-Location", "..")
	ajaxLocation = ajaxLocation[3:]

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": root + "/" + ajaxBaseUrl,
	}).Get(root + "/" + ajaxLocation))
	if ex != nil {
		return ex
	}

	rows := s.doc.Find("div[class='row']")
	if rows.Length() <= 1 {
		return ParseErr("no policies found")
	}
	for i := 1; i < rows.Length(); i++ {
		ps := rows.Eq(i).Find("p")
		details := strings.TrimSpace(ps.Eq(1).Text())
		contractNr := strings.TrimSpace(ps.Eq(0).Text())
		expDate, err := time.Parse(siteFormat, details)
		if err != nil {
			return ParseErr("parsing expiry date: " + details)
		}
		if time.Now().After(expDate) {
			s.Info("expired policy: ", contractNr, details)
			continue
		}
		loc := &model.Location{
			Service:    strings.TrimSpace(ps.Eq(2).Text()),
			Details:    &details,
			Identifier: contractNr,
		}
		account.AddLocation(loc)
		s.locs[contractNr] = loc
	}

	if ex := s.loadInvoicesInternally(account); ex != nil {
		return ex
	}
	if ex := s.loadPaymentsInternally(account); ex != nil {
		return ex
	}
	return nil
}

func (s *AsiromStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AsiromStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AsiromStrategy) loadInvoicesInternally(account *model.Account) Exception {
	invoicesEl := s.doc.Find("a:containsOwn('Rate scadente')")
	if invoicesEl.Length() == 0 {
		return ParseErr("could not find due policies link")
	}
	idDuePolite, ex := Attr(invoicesEl.Eq(0), "id")
	if ex != nil {
		return ex
	}
	duePolicyRegexStr := fmt.Sprintf(duePolicyRegexpS, idDuePolite)
	policyRegexp := regexp.MustCompile(duePolicyRegexStr)
	html, err := s.doc.Html()
	if err != nil {
		return ParseErr("parsing html doc: " + err.Error())
	}
	matches, err := utils.MatchRegex(html, policyRegexp)
	if err != nil {
		return ParseErr("parsing policies url: " + err.Error())
	}
	url := matches[0][1]
	url = root + "/" + url + "?_=" + utils.MillisString(time.Now())
	matches, err = utils.MatchRegex(html, ajaxBaseUrlRegexp)
	if err != nil {
		return ParseErr("parsing ajax base url: " + err.Error())
	}
	ajaxBaseUrl := matches[0][1]

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":                  "application/xml, text/xml, */*; q=0.01",
		"Referer":                 root + "/" + ajaxBaseUrl,
		"Wicket-Ajax":             "true",
		"Wicket-Ajax-BaseURL":     ajaxBaseUrl,
		"Wicket-FocusedElementId": idDuePolite,
		"X-Requested-With":        "XMLHttpRequest",
	}).Get(url)
	if res == nil {
		return ConnectionErr("GET " + url)
	}
	if err != nil {
		return ConnectionErr("GET " + url + " : " + err.Error())
	}
	ajaxLocation := HeaderByNameAndValStartsWith(res.Header(), "Ajax-Location", "..")
	ajaxLocation = ajaxLocation[6:]

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": root + "/" + ajaxBaseUrl,
	}).Get(root + "/" + ajaxLocation))
	if ex != nil {
		return ex
	}
	html, err = s.doc.Html()
	if err != nil {
		return ParseErr("parsing html doc: " + err.Error())
	}

	rows := s.doc.Find("div[class='row']")
	if rows.Length() <= 1 {
		s.Warn("no due policies found")
	}
	for i := 1; i < rows.Length(); i++ {
		ps := rows.Eq(i).Find("p")
		contractNr := strings.TrimSpace(ps.Eq(0).Text())
		dueDateS := strings.TrimSpace(ps.Eq(2).Text())

		detailsId, ex := Attr(ps.Eq(2).Next(), "id")
		if ex != nil {
			return ex
		}
		detailsRegexStr := fmt.Sprintf(policyRegexpS, detailsId)
		detailsRegexp := regexp.MustCompile(detailsRegexStr)
		matches, err := utils.MatchRegex(html, detailsRegexp)
		if err != nil {
			return ParseErr("parsing policies url: " + err.Error())
		}
		url := matches[0][1]
		url = root + "/" + url + "?_=" + utils.MillisString(time.Now())

		resS, ex := OkString(s.Client().R().SetHeaders(map[string]string{
			"Accept":                  "application/xml, text/xml, */*; q=0.01",
			"Referer":                 root + "/" + ajaxLocation,
			"Wicket-Ajax":             "true",
			"Wicket-Ajax-BaseURL":     ajaxLocation,
			"Wicket-FocusedElementId": detailsId,
			"X-Requested-With":        "XMLHttpRequest",
		}).Get(url))
		if ex != nil {
			return ex
		}
		if ex := s.parseInvoice(contractNr, dueDateS, resS); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *AsiromStrategy) parseInvoice(contractNr string, dueDateS string, resS string) Exception {
	htmlS := resS[strings.Index(resS, "<![CDATA[")+9:]
	htmlS = htmlS[:strings.Index(htmlS, "]]")]
	doc, ex := DocFromString(WrapHtmlBody(htmlS))
	if ex != nil {
		return ex
	}

	dueDate, err := time.Parse(siteFormat, dueDateS)
	if err != nil {
		return ParseErr("parsing due date: " + dueDateS)
	}

	amountSEl := doc.Find("span:containsOwn('Suma rata')").Next()
	if amountSEl.Length() == 0 {
		return ParseErr("parsing amount element: " + resS)
	}
	amtP := strings.Split(strings.TrimSpace(amountSEl.Text()), " ")
	if len(amtP) != 2 {
		return ParseErr("parsing amount element: " + resS)
	}
	ref := dueDateS + "_" + amtP[0]

	if amtP[1] == "EUR" {
		s.Debug("invoice ", ref, " in EUR, skipping")
		return nil
	}
	amt, ex := AmountFromString(amtP[0])
	if ex != nil {
		return ex
	}

	dueSEl := doc.Find("span:containsOwn('Rest de plata')").Next()
	if dueSEl.Length() == 0 {
		return ParseErr("parsing due amount element: " + resS)
	}
	dueP := strings.Split(strings.TrimSpace(dueSEl.Text()), " ")
	if len(dueP) != 2 {
		return ParseErr("parsing due amount element: " + resS)
	}
	due, ex := AmountFromString(dueP[0])
	if ex != nil {
		return ex
	}

	issueEl := doc.Find("span:containsOwn('Data inceput valabilitate')").Next()
	if issueEl.Length() == 0 {
		return ParseErr("no issue dae element found: " + resS)
	}
	issueString := strings.TrimSpace(issueEl.Text())
	issueDate, err := time.Parse(siteFormat, issueString)
	if err != nil {
		return ParseErr("parisng issue date: " + issueString)
	}
	barCode := contractNr + "_" + ref
	invoice := &model.Invoice{
		Ref:       ref,
		Amount:    amt,
		AmountDue: due,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
		BarCode:   &barCode,
	}
	loc, ok := s.locs[contractNr]
	if !ok {
		return ParseErr("location not parsed for contract number " + contractNr)
	}
	loc.AddInvoice(invoice)
	return nil
}

func (s *AsiromStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AsiromStrategy) loadPaymentsInternally(account *model.Account) Exception {
	html, err := s.doc.Html()
	if err != nil {
		return ParseErr("parsing html doc: " + err.Error())
	}
	paymentsEl := s.doc.Find("a:containsOwn('Istoric plati')")
	if paymentsEl.Length() == 0 {
		return ParseErr("could not find payments link")
	}
	idPayments, ex := Attr(paymentsEl.Eq(0), "id")
	if ex != nil {
		return ex
	}
	payRegexStr := fmt.Sprintf(policyRegexpS, idPayments)
	payRegexp := regexp.MustCompile(payRegexStr)
	matches, err := utils.MatchRegex(html, payRegexp)
	if err != nil {
		return ParseErr("parsing policies url: " + err.Error())
	}
	url := matches[0][1]
	url = root + "/" + url + "?_=" + utils.MillisString(time.Now())
	matches, err = utils.MatchRegex(html, ajaxBaseUrlRegexp)
	if err != nil {
		return ParseErr("parsing ajax base url: " + err.Error())
	}
	ajaxBaseUrl := matches[0][1]

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":                  "application/xml, text/xml, */*; q=0.01",
		"Referer":                 root + "/" + ajaxBaseUrl,
		"Wicket-Ajax":             "true",
		"Wicket-Ajax-BaseURL":     ajaxBaseUrl,
		"Wicket-FocusedElementId": idPayments,
		"X-Requested-With":        "XMLHttpRequest",
	}).Get(url)
	if res == nil {
		return ConnectionErr("GET " + url)
	}
	if err != nil {
		return ConnectionErr("GET " + url + " : " + err.Error())
	}
	ajaxLocation := HeaderByNameAndValStartsWith(res.Header(), "Ajax-Location", "..")
	ajaxLocation = ajaxLocation[3:]

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": root + "/" + ajaxBaseUrl,
	}).Get(root + "/" + ajaxLocation))
	if ex != nil {
		return ex
	}
	rows := s.doc.Find("div[class='row']")
	if rows.Length() <= 1 {
		s.Warn("no payments history found")
	}
	for i := 1; i < rows.Length(); i++ {
		ps := rows.Eq(i).Find("p")
		if ps.Length() == 5 {
			contractNr := strings.TrimSpace(ps.Eq(0).Text())
			dateP := strings.Split(strings.TrimSpace(ps.Eq(2).Text()), " ")
			if len(dateP) != 2 {
				continue
			}
			date, err := time.Parse(siteFormat, dateP[0])
			if err != nil {
				return ParseErr("parsing payment date: " + dateP[0])
			}
			amtP := strings.Split(strings.TrimSpace(ps.Eq(3).Text()), " ")
			if len(amtP) != 2 {
				return ParseErr("parsing amount element: " + ps.Eq(3).Text())
			}
			amt, ex := AmountFromString(amtP[0])
			if ex != nil {
				return ex
			}
			p := &model.Payment{
				Ref:    dateP[0] + "_" + amtP[0],
				Amount: amt,
				Date:   date.Format(utils.DBDateFormat),
			}
			loc, ok := s.locs[contractNr]
			if !ok {
				return ParseErr("contract nr not parsed: " + contractNr)
			}
			loc.AddPayment(p)
		}
	}
	return nil
}

func (s *AsiromStrategy) Pdf(account *model.Account) *model.PdfResponse {
	return &model.PdfResponse{
		Content:   nil,
		PdfStatus: model.NOT_GENERATED.Name,
		ErrMsg:    nil,
	}
}

func (s *AsiromStrategy) LoadsInternal() bool { return true }

func (s *AsiromStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {}

func (s *AsiromStrategy) SetExistingLocations(existingLocations []*model.Location) {}
