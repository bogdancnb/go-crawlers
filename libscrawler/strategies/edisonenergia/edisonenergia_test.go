package edisonenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "edisonEnergia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"sammartino.m@alice.it", "Sasimico61@"}, //ok
		//{"tiziboncoraglio", "Accesso1973@"}, //ok
		//{"marianobarbara", "Atnsi200348!"}, //ok
		//{"Andrea.vinci-1984", "forzaparma"}, //ok
		{"trapani52", "samu130613"}, //ok
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "sammartino.m@alice.it", Password: "Sasimico61@", Uri: "edisonEnergia.crawler",
			Identifier: "02330000218182",
			Ref:        "0000006051900948", PdfUri: "https://edisonenergia.it/apimanager-pa-s-mc/downloads/v1/bolletta/6051900948?flagSintesiDettaglio=S",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
