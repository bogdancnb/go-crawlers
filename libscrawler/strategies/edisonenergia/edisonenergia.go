package edisonenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

const (
	home                 = "https://edisonenergia.it/edison/casa/"
	loginForm            = "https://edisonenergia.it/pkmslogin.form"
	loginSuccessRedirect = "https://edisonenergia.it/myportal/myedison?operation=login_success"
	contracts            = "https://edisonenergia.it/apimanager-pa-s-mc/contratti/v1/recuperoLista?request.preventCache="
	invoices             = `https://edisonenergia.it/apimanager-pa-s-mc/verifiche/v1/recuperoListaBollette/%s?numElemPagina=7&numPagina=0&inizioPeriodo=%s&finePeriodo=%s&request.preventCache=`
	pdfUri               = `https://edisonenergia.it/apimanager-pa-s-mc/downloads/v1/bolletta/%s?flagSintesiDettaglio=S`

	invReqFormat = "2006-01-02"
	qqFormat     = "02/01/2006"
	jsonFormat   = "02.01.2006"
)

func Init() {
	RegisterFactory("edisonEnergia.crawler", new(EdisonEnergiaFactory))
}

type EdisonEnergiaFactory struct{}

func (p *EdisonEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EdisonEnergiaStrategy")
	return &EdisonEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type EdisonEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document
}

func (s *EdisonEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EdisonEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EdisonEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EdisonEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":     AcceptHeader,
		"Connection": "keep-alive",
		"Host":       "edisonenergia.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}

	s.EnableRedirects(false)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":           "*/*",
		"Connection":       "keep-alive",
		"Host":             "edisonenergia.it",
		"Origin":           "https://edisonenergia.it",
		"Referer":          "https://edisonenergia.it/edison/casa/",
		"X-Requested-With": "XMLHttpRequest",
	}).SetFormData(map[string]string{
		"username":        account.Username,
		"password":        *account.Password,
		"login-form-type": "pwd",
	}).Post(loginForm)
	s.Trace(err)
	if res == nil {
		return nil, ConnectionErr("no response for POST " + loginForm + ": ")
	}
	locationHeader := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	if !strings.Contains(locationHeader, loginSuccessRedirect) {
		return nil, LoginErr("redirected to " + locationHeader)
	}
	s.EnableRedirects(true)

	_, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":           "*/*",
		"Connection":       "keep-alive",
		"Host":             "edisonenergia.it",
		"Referer":          "https://edisonenergia.it/edison/casa/",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(locationHeader))
	if ex != nil {
		return nil, ex
	}

	return nil, nil
}

func (s *EdisonEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

type contractsJson struct {
	Contratti []*contractJson `json:"contratti"`
}
type contractJson struct {
	CodiceCliente       string `json:"codiceCliente"`
	TipoContratto       string `json:"tipoContratto"`
	CodiceContratto     string `json:"codiceContratto"`
	ContractAccount     string `json:"contractAccount"`
	Via                 string `json:"via"`
	Cap                 string `json:"cap"`
	Citta               string `json:"citta"`
	Provincia           string `json:"provincia"`
	Nazione             string `json:"nazione"`
	Pdr                 string `json:"pdr"`
	Pod                 string `json:"pod"`
	Tutelato            string `json:"tutelato"`
	Nickname            string `json:"nickname"`
	Stato               string `json:"stato"`
	DataInizioFornitura string `json:"dataInizioFornitura"`
	DataFineFornitura   string `json:"dataFineFornitura"`
	FlagAutolettura     string `json:"flagAutolettura"`
	Quotation           bool   `json:"quotation"`
	DataFirma           string `json:"dataFirma"`
	CodiceFiscale       string `json:"codiceFiscale"`
}

func (s *EdisonEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":           "application/json",
		"Connection":       "keep-alive",
		"Content-Type":     "application/json; charset=utf-8",
		"Host":             "edisonenergia.it",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(contracts))
	if ex != nil {
		return ex
	}
	s.Debug(res)
	var contracts contractsJson
	err := utils.FromJSON(&contracts, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserializing contracts : " + err.Error())
	}
	for _, c := range contracts.Contratti {
		details := c.CodiceCliente + "," + c.CodiceContratto + "," + c.ContractAccount
		identifier := ""
		if c.Pdr != "" {
			identifier = c.Pdr
		} else if c.Pod != "" {
			identifier = c.Pod
		}
		if identifier == "" {
			return ParseErr("unknown identider: " + res)
		}
		l := &model.Location{
			Service:    c.Via + " " + c.Cap + " " + c.Citta + " " + c.Provincia + " " + c.Nickname,
			Details:    &details,
			Identifier: identifier,
		}
		account.AddLocation(l)
	}
	return nil
}

func (s *EdisonEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

type invoicesJson struct {
	Header struct {
		CodiceErrore interface{} `json:"codiceErrore"`
		Messaggio    interface{} `json:"messaggio"`
	} `json:"_header"`
	Bollette []*invoiceJson `json:"bollette"`
}
type invoiceJson struct {
	NumeroBolletta             string `json:"numeroBolletta"`
	StatoBolletta              string `json:"statoBolletta"`
	DataEmissione              string `json:"dataEmissione"`
	DataScadenza               string `json:"dataScadenza"`
	ImportoAperto              string `json:"importoAperto"`
	ImportoTotale              string `json:"importoTotale"`
	MetodoPagamento            string `json:"metodoPagamento"`
	PresentazioneSDDEffettuata string `json:"presentazioneSDDEffettuata"`
	EsitoSDI                   string `json:"esitoSDI"`
	Prescrizione               string `json:"prescrizione"`
}

func (s *EdisonEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	details := strings.Split(*location.Details, ",")
	codiceContratto := details[1]
	now := time.Now()
	end := now.Format(invReqFormat)
	start := now.AddDate(-1, 0, 0).Format(invReqFormat)
	uri := fmt.Sprintf(invoices, codiceContratto, start, end)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":           "application/json",
		"Connection":       "keep-alive",
		"Content-Type":     "application/json; charset=utf-8",
		"Host":             "edisonenergia.it",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(uri)
	if res == nil {
		return ConnectionErr("no response for GET " + uri)
	}
	s.Debug(res.Body())
	if err != nil {
		return ConnectionErr("GET " + uri + ": " + err.Error())
	}
	var invoiceRes invoicesJson
	err = utils.FromJSON(&invoiceRes, bytes.NewBuffer(res.Body()))
	if err != nil {
		return ParseErr("deserialize invoice response: " + err.Error())
	}

	if len(invoiceRes.Bollette) == 0 && invoiceRes.Header.CodiceErrore != nil {
		return ParseErr("invoice response error " + string(res.Body()))
	}
	for _, i := range invoiceRes.Bollette {
		amt, err := strconv.ParseFloat(i.ImportoTotale, 64)
		if err != nil {
			return ParseErr("amount " + i.ImportoTotale)
		}
		amtDue, err := strconv.ParseFloat(i.ImportoAperto, 64)
		if err != nil {
			return ParseErr("amount due " + i.ImportoAperto)
		}
		issueDate, err := time.Parse(invReqFormat, i.DataEmissione)
		if err != nil {
			return ParseErr("issue date " + i.DataEmissione)
		}
		dueDate, err := time.Parse(invReqFormat, i.DataScadenza)
		if err != nil {
			return ParseErr("due date " + i.DataScadenza)
		}
		uri := fmt.Sprintf(pdfUri, strings.TrimLeft(i.NumeroBolletta, "0"))
		i := &model.Invoice{
			Ref:       i.NumeroBolletta,
			PdfUri:    &uri,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
	}
	return nil
}

func (s *EdisonEnergiaStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EdisonEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EdisonEnergiaStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EdisonEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Accept":           "application/pdf",
		"Connection":       "keep-alive",
		"Content-Type":     "application/x-www-form-urlencoded",
		"Host":             "edisonenergia.it",
		"X-Requested-With": "XMLHttpRequest",
	}).Post(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	pdfBytes, err := base64.StdEncoding.DecodeString(string(res))
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EdisonEnergiaStrategy) LoadsInternal() bool {
	return false
}

func (s *EdisonEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EdisonEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
