package florisal

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccountDrusal(t *testing.T) {
	Init()
	uri := "drusal.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"dragomir_srn@yahoo.com", "aE0Tb"},
		{"duraberg@adslexpress.ro", "rVY5l"},
	}
	for _, test := range tests {
		if err := strategies.AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestAccountFlorisal(t *testing.T) {
	Init()
	uri := "florisal.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"rovas_alexandra@yahoo.com", "Rbr08"},
	}
	for _, test := range tests {
		if err := strategies.AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestAccountBioFlorisal(t *testing.T) {
	Init()
	uri := "bio_florisal.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"zolipazman@yahoo.com", "T50y0"},
	}
	for _, test := range tests {
		if err := strategies.AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*strategies.PdfRequest{
		//{
		//	Username: "102", Password: "445884", Uri: "luksus.crawler",
		//	Identifier: "90051514861",
		//	Ref:        "90051514861", PdfUri: "https://bok.luksus.net.pl/?m=finances&f=invoice&id=190406",
		//},
	}
	for _, test := range tests {
		pdf := strategies.PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
