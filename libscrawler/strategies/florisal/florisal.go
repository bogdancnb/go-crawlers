package florisal

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	m "bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	selectFlorisalUrl    = "http://florisal.real-host.eu:8080/portal_florisal/select_florisal"
	selectDrusalUrl      = "http://florisal.real-host.eu:8080/portal_florisal/select_drusal"
	selectBioFlorisalUrl = "http://florisal.real-host.eu:8080/portal_florisal/select_bioflorisal"
	accessUrl            = "http://florisal.real-host.eu:8080/portal_florisal/login.html"
	loginUrl             = "http://florisal.real-host.eu:8080/portal_florisal/validate_login"
	contentUrl           = "http://florisal.real-host.eu:8080/portal_florisal/lista_facturi/0"
	basicUrl             = "http://florisal.real-host.eu:8080/portal_florisal"

	siteFormat  = "02.01.2006"
	loginErrMsg = "Autentificare esuata"
)

type barcodeInfo struct {
	amountLength string
}

var (
	portals = map[string]string{
		"drusal.crawler":       selectDrusalUrl,
		"bio_florisal.crawler": selectBioFlorisalUrl,
		"florisal.crawler":     selectFlorisalUrl,
	}
	barcodes = map[string]barcodeInfo{
		"drusal.crawler":       {"%09s"},
		"bio_florisal.crawler": {"%08s"},
		"florisal.crawler":     {"%09s"},
	}

	invalidPortal = &ParseException{Msg: "invalid portal"}
	loginErr      = &LoginException{Msg: "login error"}
	noInvoiceErr  = &ParseException{Msg: "could not find invoice table"}
	noPdfUri      = "could not parse pdf uri"
	pdfErr        = "pdf error"
	pdfErrRes     = &m.PdfResponse{
		Content:   nil,
		PdfStatus: m.OTHER_EXCEPTION.Name,
		ErrMsg:    &pdfErr,
	}
)

func Init() {
	factory := new(DrusalFactory)
	RegisterFactory("drusal.crawler", factory)
	RegisterFactory("bio_florisal.crawler", factory)
	RegisterFactory("florisal.crawler", factory)
}

type DrusalFactory struct{}

func (e *DrusalFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "DrusalStrategy")
	return &DrusalStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type DrusalStrategy struct {
	*logrus.Entry
	HttpStrategy
}

func (s *DrusalStrategy) GetBarcode(account *m.Account, location *m.Location, invoice *m.Invoice) Exception {
	info := barcodes[account.Uri]
	amountString := decimal.NewFromFloat(invoice.Amount).String()
	amountString = fmt.Sprintf(info.amountLength, amountString)
	clientCodeString := fmt.Sprintf("%09s", location.Identifier)
	refParts := strings.Split(invoice.Ref, " ")
	refString := fmt.Sprintf("%08s%06s", refParts[0], refParts[1])
	barcode := fmt.Sprintf(`%s%s%s`, amountString, clientCodeString, refString)
	s.Debug("barcode=", barcode, " for ref", invoice.Ref)
	if barcode != "" {
		invoice.BarCode = &barcode
	}
	return nil
}

func (s *DrusalStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *DrusalStrategy) ActivateEInvoice(account *m.Account, received *m.Location, electronicInvoiceType m.ElectronicInvoiceType) Exception {
	return nil
}

func (s *DrusalStrategy) Login(account *m.Account) (*string, Exception) {
	crawlEx := s.switchPortal(account.Uri)
	if crawlEx != nil {
		return nil, crawlEx
	}
	resp, er := s.Client().R().
		SetFormData(map[string]string{
			"email":         account.Username,
			"user_password": *account.Password,
		}).
		Post(loginUrl)
	resp, connErr := Accepted(resp, er, 302)
	if connErr != nil {
		return nil, connErr
	}
	respS := resp.String()
	return &respS, nil
}

func (s *DrusalStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return loginErr
	}
	return nil
}

func (s *DrusalStrategy) switchPortal(uri string) Exception {
	uri, ok := portals[uri]
	if !ok {
		return invalidPortal
	}
	_, err := Ok(s.Client().R().Get(uri))
	if err != nil {
		return err
	}
	return nil
}

func (s *DrusalStrategy) LoadLocations(account *m.Account) Exception {
	doc, err := OkDocument(s.Client().R().Get(contentUrl))
	if err != nil {
		return err
	}

	clientsData := doc.Find(".fn_total_plata select option")
	if clientsData.Length() == 0 {
		return &ParseException{Msg: "LoadLocations: Content invalid for url: " + contentUrl}
	}
	clientsData.Each(func(i int, selection *goquery.Selection) {
		service := selection.Text()
		clientCode := strings.Split(service, " ")[0]
		switchAccountValue, ok := selection.Attr("value")
		if !ok {
			s.Error("no switchAccountValue found: ", selection.Text())
			return
		}
		loc := &m.Location{
			Service:        service,
			Identifier:     clientCode,
			AccountHelpers: switchAccountValue,
		}
		account.AddLocation(loc)
	})
	return nil
}

func (s *DrusalStrategy) LoadAmount(account *m.Account, location *m.Location) Exception {
	return nil
}

func (s *DrusalStrategy) LoadInvoices(account *m.Account, location *m.Location) Exception {
	doc, ex := invoicesDoc(location, s)
	if ex != nil {
		return ex
	}
	ex = parseAmount(doc, location)
	if ex != nil {
		return ex
	}

	invoices := doc.Find("table tbody tr")
	if invoices.Length() == 0 {
		s.Error(doc.Html())
		return noInvoiceErr
	}
	var err Exception
	invoices.EachWithBreak(func(i int, elem *goquery.Selection) bool {
		if err != nil || i == invoices.Length()-1 {
			return false
		}
		iErr := parseInvoice(s.Logger, location, elem)
		if iErr != nil {
			s.WithError(iErr).Error("parsing invoice: ", elem.Text())
			err = iErr
			return false
		}
		return true
	})
	return err
}

func invoicesDoc(location *m.Location, s *DrusalStrategy) (*goquery.Document, Exception) {
	switchAccountValue := location.AccountHelpers.(string)
	switchAccountUrl := fmt.Sprintf(`%s?cb_filter=%s`, contentUrl, url.QueryEscape(switchAccountValue))
	doc, ex := OkDocument(s.Client().R().Get(switchAccountUrl))
	return doc, ex
}

func parseInvoice(log *logrus.Logger, location *m.Location, node *goquery.Selection) *ParseException {
	td := node.Find("td")
	if td.Length() != 9 {
		log.Warn(fmt.Sprintf("invalid nr of td for row: %d", td.Length()))
		return nil
	}
	pdfUri, cells := invoiceInfo(log, td)

	ref := trimString(cells[1])
	issueDate, err := time.Parse(siteFormat, cells[2])
	if err != nil {
		return &ParseException{Msg: "parse date: " + cells[2]}
	}
	dueDate, err := time.Parse(siteFormat, cells[3])
	if err != nil {
		return &ParseException{Msg: "parse date: " + cells[3]}
	}
	amount, err := strconv.ParseFloat(cells[4], 64)
	if err != nil {
		return &ParseException{Msg: "parsing amount " + cells[4]}
	}
	amountDue, err := strconv.ParseFloat(cells[6], 64)
	if err != nil {
		return &ParseException{Msg: "parsing amount due" + cells[6]}
	}

	invoice := &m.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)

	if am, paid := cells[4], cells[5]; paid == am {
		_ = parsePayment(log, location, ref, paid, dueDate)
	}
	return nil
}

func invoiceInfo(log *logrus.Logger, td *goquery.Selection) (*string, []string) {
	var pdfUri *string
	cells := td.Map(func(i int, cell *goquery.Selection) string {
		if i == 1 {
			pdf, ok := cell.Find("a").Attr("href")
			if !ok {
				html, _ := cell.Html()
				log.Debug("no pdf uri: ", html)
			} else {
				pdfUri = &pdf
			}
		}
		return trimString(cell.Text())
	})
	return pdfUri, cells
}

func parsePayment(log *logrus.Logger, location *m.Location, ref string, paid string, date time.Time) *ParseException {
	amount, err := strconv.ParseFloat(paid, 64)
	if err != nil {
		return &ParseException{Msg: "parsing amount " + paid}
	}
	payDate := date
	if time.Now().Before(payDate) {
		payDate = time.Now()
	}
	payment := &m.Payment{
		Ref:    ref,
		Amount: amount,
		Date:   payDate.Format(utils.DBDateFormat),
	}
	location.AddPayment(payment)
	return nil
}

func parseAmount(doc *goquery.Document, location *m.Location) Exception {
	amountEl := doc.Find(".fn_total_plata_container")
	if amountEl.Length() == 0 {
		return &ParseException{Msg: "LoadInvoices: no amount element found"}
	}
	amountString := strings.Split(amountEl.Text(), ":")[1]
	amountString = trimString(amountString)
	amount, err := strconv.ParseFloat(amountString, 64)
	if err != nil {
		return &ParseException{Msg: "could not parse amount: " + amountString}
	}
	location.Amount = &amount
	return nil
}

func trimString(s string) string {
	return strings.TrimSpace(s)
	//strings.Trim(s, "\\u00a0")
}

func (s *DrusalStrategy) LoadPayments(account *m.Account, location *m.Location) Exception {
	return nil
}

func (s *DrusalStrategy) Pdf(account *m.Account) *m.PdfResponse {
	ref := account.Locations[0].Invoices[0].Ref
	account.Locations = account.Locations[:0]
	ex := s.LoadLocations(account)
	if ex != nil {
		return m.PdfErrorResponse(ex.Error(), m.OTHER_EXCEPTION)
	}

	var pdfRes *m.PdfResponse
	for _, location := range account.Locations {
		doc, ex := invoicesDoc(location, s)
		if ex != nil {
			msg := ex.Error()
			return m.PdfErrorResponse(msg, m.OTHER_EXCEPTION)
		}
		invoices := doc.Find("table tbody tr")
		if invoices.Length() == 0 {
			return m.PdfErrorResponse(noInvoiceErr.Msg, m.OTHER_EXCEPTION)
		}
		invoices.EachWithBreak(func(i int, elem *goquery.Selection) bool {
			if strings.Contains(elem.Text(), ref) {
				td := elem.Find("td")
				pdfUri, _ := invoiceInfo(s.Logger, td)
				if pdfUri == nil {
					pdfRes = m.PdfErrorResponse(noPdfUri, m.NO_PDF_URI)
					return false
				}
				xid := strings.Split(*pdfUri, "xid")[1][1:]
				uri := "http://florisal.real-host.eu:8080/portal_florisal/listare_pdf?xid=" + url.QueryEscape(xid)
				doc, ex := OkDocument(s.Client().R().Get(uri))
				if ex != nil {
					pdfRes = pdfErrRes
					return false
				}
				fileName, _ := doc.Find("input[name=fisier]").Attr("value")
				idfactura, _ := doc.Find("input[name=idfactura]").Attr("value")
				doc, ex = OkDocument(s.Client().R().
					SetFormData(map[string]string{
						"fisier":    fileName,
						"idfactura": idfactura,
					}).
					Post("http://florisal.real-host.eu:8080/portal_florisal/vizualizare_pdf"))
				if ex != nil {
					pdfRes = pdfErrRes
					return false
				}
				src, ok := doc.Find("frame").Attr("src")
				if !ok {
					pdfRes = pdfErrRes
					return false
				}
				uri = basicUrl + src[1:]
				pdfBytes, err := GetBytes(s.Client().R().Get(uri))
				if err != nil {
					pdfRes = pdfErrRes
					return false
				}
				pdfRes = &m.PdfResponse{
					Content:   pdfBytes,
					PdfStatus: m.OK.Name,
					ErrMsg:    nil,
				}
				return false
			}
			return true
		})
		if pdfRes != nil {
			break
		}
	}
	if pdfRes == nil {
		pdfRes = pdfErrRes
	}
	return pdfRes
}

func (s *DrusalStrategy) LoadsInternal() bool {
	return false
}

func (s *DrusalStrategy) Close(a *m.Account) {
	s.HttpStrategy.Close(a)
}

func (s *DrusalStrategy) SendConnectionError() {
	s.HttpStrategy.SendConnectionError()
}

func (s *DrusalStrategy) SetExistingInvoices(existingInvoices map[string]*m.Invoice) {
}

func (s *DrusalStrategy) SetExistingLocations(existingLocations []*m.Location) {
}
