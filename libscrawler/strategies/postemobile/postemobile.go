package postemobile

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	auth  = "https://securelogin.poste.it/jod-fcc/fcc-authentication.html"
	login = "https://securelogin.poste.it/jod-fcc/login"

	loginErrMsg   = `authentication-failed`
	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("posteMobile.crawler", new(PosteMobileFactory))
}

type PosteMobileFactory struct{}

func (p *PosteMobileFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "PosteMobileStrategy")
	return &PosteMobileStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type PosteMobileStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *PosteMobileStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *PosteMobileStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PosteMobileStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *PosteMobileStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "securelogin.poste.it",
	}).Get(auth))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())

	s.EnableRedirects(false)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "securelogin.poste.it",
		"origin":       "https://securelogin.poste.it",
		"referer":      auth,
	}).SetFormData(map[string]string{
		"dep":        `version%3D3%2E7%2E1%5F1%26pm%5Ffpua%3Dmozilla%2F5%2E0%20%28x11%3B%20linux%20x86%5F64%29%20applewebkit%2F537%2E36%20%28khtml%2C%20like%20gecko%29%20chrome%2F78%2E0%2E3904%2E108%20safari%2F537%2E36%7C5%2E0%20%28X11%3B%20Linux%20x86%5F64%29%20AppleWebKit%2F537%2E36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F78%2E0%2E3904%2E108%20Safari%2F537%2E36%7CLinux%20x86%5F64%26pm%5Ffpsc%3D24%7C2560%7C1440%7C1407%26pm%5Ffpsw%3Dpdf%7Cpdf%7Cnacl%26pm%5Ffptz%3D2%26pm%5Ffpln%3Dlang%3Den%2DUS%7Csyslang%3D%7Cuserlang%3D%26pm%5Ffpjv%3D0%26pm%5Ffpco%3D1%26pm%5Ffpasw%3Dinternal%2Dpdf%2Dviewer%7Cmhjfbmdgcfjbbpaeojofohoefgiehjai%7Cinternal%2Dnacl%2Dplugin%26pm%5Ffpan%3DNetscape%26pm%5Ffpacn%3DMozilla%26pm%5Ffpol%3Dtrue%26pm%5Ffposp%3D%26pm%5Ffpup%3D%26pm%5Ffpsaw%3D2560%26pm%5Ffpspd%3D24%26pm%5Ffpsbd%3D%26pm%5Ffpsdx%3D%26pm%5Ffpsdy%3D%26pm%5Ffpslx%3D%26pm%5Ffpsly%3D%26pm%5Ffpsfse%3D%26pm%5Ffpsui%3D%26pm%5Fos%3DLinux%26pm%5Fbrmjv%3D78%26pm%5Fbr%3DChrome%26pm%5Finpt%3D%26pm%5Fexpt%3D`,
		"dop":        `{"functions":{"names":["$","appmeasurement","appmeasurement_module_activitymap","blackberrylocationcollector","browserdetection","domdatacollection","fingerprint","html5locationcollector","hashtable","ie_fingerprint","interactionelement","mozilla_fingerprint","opera_fingerprint","rsauievent","timer","uielementlist","_dom_data_collection2","_truste_eu","activeonscroll","activexdetect","addeventlistener","add_deviceprint","aiutaci_a_migliorare","alert","anchorscrollingtofocus","apripannellomenu","atob","blur","botheader","btoa","buildsurvey","callbackdrop","cancelanimationframe","cancelidlecallback","captureevents","chatstatusreset","checkdataelement"],"excluded":{"size":0,"count":0},"truncated":true},"inputs":["no_name","checkbox-remember","dep","dep","dop","dop","evp","evp","mid","password","rememberme","securetoken","signature","username"],"iframes":[],"scripts":[0,0,0,0,0,0,0,0,0,0,0,0,593,0,787,1037,0,0,0,0,0,317,240,0,0,0,0,0,1519,0,0,0,0,0,189,6,63,522,1701,0,621,1579,2867],"collection_status":0}`,
		"evp":        "1,1,INPUT:text,25@1,3,0;1,1,0;1,5,0;1,5,0;1,1,0;1,4,0;1,3,0;1,4,0@0,967,0,1",
		"mid":        "",
		"rememberme": "",
		"username":   account.Username,
		"password":   *account.Password,
	}).Post(login)
	s.Trace(err)
	if res == nil {
		return nil, ConnectionErr("POST " + login + " : no response")
	}
	s.Trace(res.String())
	if res.StatusCode() != 302 {
		return nil, ConnectionErr(fmt.Sprintf("POST %q : status code : %d", login, res.StatusCode()))
	}
	locHeader := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	if locHeader == "" {
		return nil, ConnectionErr(fmt.Sprintf("POST %q : no redirect location header", login))
	}
	if strings.Contains(locHeader, loginErrMsg) {
		return nil, LoginErr(loginErrMsg)
	}
	s.EnableRedirects(true)

	return nil, nil
}

func (s *PosteMobileStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *PosteMobileStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *PosteMobileStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PosteMobileStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PosteMobileStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PosteMobileStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *PosteMobileStrategy) LoadsInternal() bool {
	return false
}

func (s *PosteMobileStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *PosteMobileStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
