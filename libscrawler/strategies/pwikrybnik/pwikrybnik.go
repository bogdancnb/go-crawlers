package pwikrybnik

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	base    = "https://ebok.pwik-rybnik.pl/"
	home    = "https://ebok.pwik-rybnik.pl/web/pwik-ebok/home"
	facturi = "https://ebok.pwik-rybnik.pl/web/pwik-ebok/faktury"

	loginErr   = "Uwierzytelnianie nie powiodło się. Proszę spróbować ponownie"
	siteFormat = "2006-01-02"
)

var (
	bankName  = "Powszechna Kasa Oszczednosci Bank Polski Spolka Akcyjna"
	bankBIC   = "BPKOPLPW"
	legalName = "Przedsiębiorstwo Wodociągów i Kanalizacji"

	userIdRegex = regexp.MustCompile(`getUserId: function\(\) {\s*return "(\d+)";`)
	pdfUriRegex = regexp.MustCompile(`var url = '(.+)'`)
	accNtRegex  = regexp.MustCompile(`CustomerNRB=(\w+)&Hash`)
)

func Init() {
	RegisterFactory("pwik_rybnik.crawler", new(PwikRybnikFactory))
}

type PwikRybnikFactory struct{}

func (p *PwikRybnikFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "PwikRybnikStrategy")
	return &PwikRybnikStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type PwikRybnikStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	//headers
	jSESSIONID, COMPANY_ID, ID, USER_UUID, userId string
}

func (s *PwikRybnikStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *PwikRybnikStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PwikRybnikStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *PwikRybnikStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Connection": "keep-alive",
		"Host":       "ebok.pwik-rybnik.pl",
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().Get(base)
	if err != nil {
		return nil, ConnectionErr("GET " + base + ": " + err.Error())
	}
	s.jSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "JSESSIONID")
	s.jSESSIONID = s.jSESSIONID[:strings.Index(s.jSESSIONID, `; `)]
	doc, ex := DocFromBytes(res.Body())
	if ex != nil {
		return nil, ex
	}
	formDate, ex := Attr(doc.Find("input[name='_58_formDate']"), "value")
	if ex != nil {
		return nil, ex
	}
	redirect, ex := Attr(doc.Find("input[id='_58_redirect']"), "value")
	if ex != nil {
		return nil, ex
	}
	uri, ex := Attr(doc.Find("form[id='_58_fm']"), "action")
	if ex != nil {
		return nil, ex
	}

	s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	res, err = s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.jSESSIONID + "; GUEST_LANGUAGE_ID=pl_PL; COOKIE_SUPPORT=true",
		"Referer":      "https://ebok.pwik-rybnik.pl/",
	}).SetFormData(map[string]string{
		"_58_formDate": formDate,
		"_58_redirect": redirect,
		"_58_login":    account.Username,
		"_58_password": *account.Password,
	}).Post(uri)
	if strings.Contains(res.String(), loginErr) {
		return nil, LoginErr(loginErr)
	}
	if res.StatusCode() != 302 {
		return nil, ConnectionErr("POst " + uri + ": " + res.Status())
	}
	s.jSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "JSESSIONID")
	s.jSESSIONID = s.jSESSIONID[:strings.Index(s.jSESSIONID, `; `)]
	s.COMPANY_ID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "COMPANY_ID")
	s.COMPANY_ID = s.COMPANY_ID[:strings.Index(s.COMPANY_ID, `; `)]
	s.ID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "ID")
	s.ID = s.ID[:strings.Index(s.ID, `; `)]
	s.USER_UUID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "USER_UUID")
	s.USER_UUID = s.USER_UUID[:strings.Index(s.USER_UUID, `; `)]

	s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie":  "GUEST_LANGUAGE_ID=pl_PL; COOKIE_SUPPORT=true; " + s.jSESSIONID + "; " + s.COMPANY_ID + "; " + s.ID + "; " + s.USER_UUID,
		"Referer": "https://ebok.pwik-rybnik.pl/",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	exception := s.parseUserId()
	if exception != nil {
		return nil, exception
	}
	html, _ := s.doc.Html()
	return &html, nil
}

func (s *PwikRybnikStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *PwikRybnikStrategy) LoadLocations(account *model.Account) Exception {
	codEl := s.doc.Find("strong:containsOwn('Kod klienta')")
	if codEl.Length() == 0 || codEl.Next().Length() == 0 {
		return ParseErr("no client code element found")
	}
	clientCode := strings.TrimSpace(codEl.Next().Text())
	adrEl := s.doc.Find("strong:containsOwn('Adres')")
	if adrEl.Length() == 0 || adrEl.Next().Length() == 0 {
		return ParseErr("no address element found")
	}
	address := utils.RemoveAdjacentUnicodeSpaces([]byte(adrEl.Next().Text()))
	location := &model.Location{
		Service:    strings.ReplaceAll(string(address), "\n", ""),
		Identifier: clientCode,
	}
	if exLoc, ok := s.exLocations[clientCode]; ok {
		if exLoc.ProviderBranch != nil {
			location.ProviderBranch = exLoc.ProviderBranch
		} else {
			s.Warn("!!! existing location, but no provider branch: ", exLoc)
		}
	}
	account.AddLocation(location)
	return nil
}

func (s *PwikRybnikStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PwikRybnikStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	cookieH := s.buildCookieHeader()
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie":  cookieH,
		"Referer": "https://ebok.pwik-rybnik.pl/web/pwik-ebok/home",
	}).Get(facturi))
	if ex != nil {
		return ex
	}

	rows := doc.Find(`div[id="_iBOKInvoices_WAR_iBOKInvoicesportlet_invoicesSearchContainerSearchContainer"] table tbody tr`)
	if rows.Length() < 2 {
		s.Error(doc.Html())
		return ParseErr("no invoice rows found")
	}
	for i := 2; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if ex := s.parseInvoice(location, cols); ex != nil {
			return ParseErr("parseInvoice " + cols.Text() + ": " + ex.Error())
		}
	}
	return nil
}

func (s *PwikRybnikStrategy) buildCookieHeader() string {
	now := time.Now()
	cookieH := "LFR_SESSION_STATE_" + s.userId + "=" + utils.MillisString(now) + "; GUEST_LANGUAGE_ID=pl_PL; COOKIE_SUPPORT=true; " + s.jSESSIONID + "; " + s.COMPANY_ID + "; " + s.ID + "; " + s.USER_UUID + "; LFR_SESSION_STATE" + s.userId + "=" + utils.MillisString(now.Add(2*time.Millisecond))
	return cookieH
}

func (s *PwikRybnikStrategy) parseUserId() Exception {
	html, err := s.doc.Html()
	if err != nil {
		return ParseErr("could not parse html from homepage")
	}
	matches, err := utils.MatchRegex(html, userIdRegex)
	if err != nil {
		return ParseErr(err.Error())
	}
	s.userId = matches[0][1]
	return nil
}

func (s *PwikRybnikStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ref := utils.RemoveSpaces(cols.Eq(0).Find("a").Text())
	pdfUri := cols.Eq(0).Find("script").Text()
	matches, err := utils.MatchRegex(pdfUri, pdfUriRegex)
	if err != nil {
		return ParseErr("parsing pdf uri: " + err.Error())
	}
	pdfUri = matches[0][1]
	issueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(1).Text()))
	if err != nil {
		return ParseErr("parse issue date: " + err.Error())
	}
	dueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(2).Text()))
	if err != nil {
		return ParseErr("parse due date: " + err.Error())
	}
	amount, ex := AmountFromString(strings.Split(cols.Eq(3).Text(), " ")[0])
	if ex != nil {
		return ex
	}
	status := utils.RemoveSpaces(cols.Eq(4).Text())
	amountDue := 0.0
	if status == "--" {
		amountDue = amount
	}
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    &pdfUri,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	if location.ProviderBranch == nil {
		if ex := s.parsePdf(location, invoice); ex != nil {
			return ParseErr("parsing pdf " + pdfUri + ": " + ex.Error())
		}
	}
	return nil
}

func (s *PwikRybnikStrategy) parsePdf(location *model.Location, invoice *model.Invoice) Exception {
	pdfBytes, err := s.pdfDownload(invoice)
	if err != nil {
		return ConnectionErr("pdf download for " + invoice.Ref)
	}
	matches, err := utils.MatchRegex(string(pdfBytes), accNtRegex)
	if err != nil {
		return ParseErr("parsing account nr from pdf: " + err.Error())
	}
	accNr := matches[0][1]
	location.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNr,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}
	return nil
}

func (s *PwikRybnikStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PwikRybnikStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	if err != nil {
		return model.PdfErrorResponse("error", model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *PwikRybnikStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	now := time.Now()
	uri := *invoice.PdfUri + "&_iBOKInvoices_WAR_iBOKInvoicesportlet_reportDate=" + utils.MillisString(now)
	cookieH := s.buildCookieHeader()

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Cookie":  cookieH,
		"Referer": "https://ebok.pwik-rybnik.pl/web/pwik-ebok/faktury",
	}).Get(uri)
	if err != nil {
		return nil, err
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *PwikRybnikStrategy) LoadsInternal() bool {
	return false
}

func (s *PwikRybnikStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *PwikRybnikStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
