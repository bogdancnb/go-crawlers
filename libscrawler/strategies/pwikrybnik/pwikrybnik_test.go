package pwikrybnik

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "pwik_rybnik.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"135222USER", "maxma272268200"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "135222USER", Password: "maxma272268200", Uri: "pwik_rybnik.crawler",
			Identifier: "135222",
			Ref:        "50/23763/2020", PdfUri: "https://ebok.pwik-rybnik.pl/web/pwik-ebok/faktury?p_p_id=iBOKInvoices_WAR_iBOKInvoicesportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&p_p_col_id=column-2&p_p_col_count=1&_iBOKInvoices_WAR_iBOKInvoicesportlet_invoice_id=23335410",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
