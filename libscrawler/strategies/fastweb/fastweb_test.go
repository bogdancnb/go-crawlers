package fastweb

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "fastweb.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"spizzirristefano16@gmail.com", "Giovanni@2010"}, //login err
		{"nazareno.viggiani", "Assunta%1955"}, //ok

	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "nazareno.viggiani", Password: "Assunta%1955", Uri: "fastweb.crawler",
			Identifier: "0994724525",
			Ref:        "M022744687", PdfUri: "https://fastweb.it/myfastweb/abbonamento/visualizza-conto-fastweb/conto-fastweb/?type=PDF_SUMMARY&invoiceIssueDate=2020-10-01&invoiceNumber=M022744687",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
