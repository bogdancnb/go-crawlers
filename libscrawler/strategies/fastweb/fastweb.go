package fastweb

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

const (
	login          = "https://fastweb.it/myfastweb/accesso/login/"
	authCredSubmit = "https://logon.fastweb.it/oam/server/auth_cred_submit"
	profile        = "https://fastweb.it/myfastweb/il-tuo-profilo/"
	invoiceList    = "https://fastweb.it/myfastweb/abbonamento/visualizza-conto-fastweb/ajax/index.php?action=loadInvoiceList"
	pdf            = `https://fastweb.it/myfastweb/abbonamento/visualizza-conto-fastweb/conto-fastweb/?type=PDF_SUMMARY&invoiceIssueDate=%s&invoiceNumber=%s`

	maxRedirects = 20
	loginErrMsg  = "Username / Password non corretta"
	ymd          = "2006-01-02"
)

func Init() {
	RegisterFactory("fastweb.crawler", new(FastwebFactory))
}

type FastwebFactory struct{}

func (p *FastwebFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "FastwebStrategy")
	return &FastwebStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type FastwebStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations   map[string]*model.Location
	exInvoices    map[string]*model.Invoice
	doc           *goquery.Document
	cookies       map[string]string
	securityToken string
}

func (s *FastwebStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *FastwebStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *FastwebStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *FastwebStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})
	s.EnableRedirects(false)

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
	}).Get(login)
	if res == nil {
		return nil, ConnectionErr("no reponse for " + login)
	}
	FW_VisitorID := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "FW_VisitorID")
	FW_VisitorID = FW_VisitorID[:strings.Index(FW_VisitorID, ";")]
	PHPSESSID := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	PHPSESSID = PHPSESSID[:strings.Index(PHPSESSID, ";")]
	TS01a22e7e := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS01a22e7e")
	TS01a22e7e = TS01a22e7e[:strings.Index(TS01a22e7e, ";")]

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
	}).Get("https://fastweb.it/myfastweb/accesso/")
	if res == nil {
		return nil, ConnectionErr("no reponse for " + "https://fastweb.it/myfastweb/accesso/")
	}
	TS01a22e7e = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS01a22e7e")
	TS01a22e7e = TS01a22e7e[:strings.Index(TS01a22e7e, ";")]
	OAMRequestContext_fastweb := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "OAMRequestContext_fastweb")
	OAMRequestContext_fastweb = OAMRequestContext_fastweb[:strings.Index(OAMRequestContext_fastweb, ";")]
	location := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	location = strings.ReplaceAll(location, ":443", "")
	s.Debug("location=", location)

	res, err = s.Client().R().
		//SetHeaders(map[string]string{
		//	"accept":     AcceptHeader,
		//	"connection": "keep-alive",
		//	"cookie":     "cookiego_s=ok; " + TS01a22e7e,
		//	"host":       "fastweb.it",
		//	"user-agent": AgentChrome78,
		//}).
		Get(location)
	if res == nil {
		return nil, ConnectionErr("no reponse for " + location)
	}
	doc, ex := OkDocument(res, err)
	if ex != nil {
		return nil, ex
	}
	form := doc.Find("form[name='myForm']")
	if form.Length() == 0 {
		return nil, ParseErr("login form not found")
	}
	username, ex := Attr(doc.Find("input[name='username']"), "value")
	if ex != nil {
		return nil, ex
	}
	contextType, ex := Attr(doc.Find("input[name='contextType']"), "value")
	if ex != nil {
		return nil, ex
	}
	overrideRetryLimit, ex := Attr(doc.Find("input[name='OverrideRetryLimit']"), "value")
	if ex != nil {
		return nil, ex
	}
	password, ex := Attr(doc.Find("input[name='password']"), "value")
	if ex != nil {
		return nil, ex
	}
	challengeUrl, ex := Attr(doc.Find("input[name='challenge_url']"), "value")
	if ex != nil {
		return nil, ex
	}
	requestId, ex := Attr(doc.Find("input[name='request_id']"), "value")
	if ex != nil {
		return nil, ex
	}
	authnTryCount, ex := Attr(doc.Find("input[name='authn_try_count']"), "value")
	if ex != nil {
		return nil, ex
	}
	oamReq, ex := Attr(doc.Find("input[name='OAM_REQ']"), "value")
	if ex != nil {
		return nil, ex
	}
	locale, ex := Attr(doc.Find("input[name='locale']"), "value")
	if ex != nil {
		return nil, ex
	}
	resourceUrl, ex := Attr(doc.Find("input[name='resource_url']"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "fastweb.it",
		"origin":       "https://logon.fastweb.it",
	}).SetFormData(map[string]string{
		"contextType":        contextType,
		"username":           username,
		"OverrideRetryLimit": overrideRetryLimit,
		"password":           password,
		"challenge_url":      challengeUrl,
		"request_id":         requestId,
		"authn_try_count":    authnTryCount,
		"OAM_REQ":            oamReq,
		"locale":             locale,
		"resource_url":       resourceUrl,
	}).Post(login))
	if ex != nil {
		return nil, ex
	}

	//https://logon.fastweb.it/oam/server/auth_cred_submit
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "logon.fastweb.it",
		"origin":       "https://fastweb.it",
		"referer":      "https://fastweb.it/myfastweb/accesso/login/",
		"cookie":       "cookiego_s=ok; " + TS01a22e7e,
	}).SetFormData(map[string]string{
		"request_id":      requestId,
		"PersistentLogin": "",
		"OAM_REQ":         oamReq,
		"username":        account.Username,
		"password":        *account.Password,
	}).Post(authCredSubmit)
	if res == nil {
		return nil, ConnectionErr("POST " + authCredSubmit + " no response")
	}
	if err != nil {
		s.WithError(err).Debug("POST " + authCredSubmit)
	}
	//login error
	if res.StatusCode() == 200 {
		doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
		if err != nil {
			return nil, ParseErr("parse html doc for POST " + authCredSubmit + " : " + err.Error())
		}
		errorCode := doc.Find("input[name='p_error_code']")
		if errorCode.Length() > 0 {
			v, _ := Attr(errorCode, "value")
			if v == "OAM-2" {
				return nil, LoginErr("invalid credentials")
			} else {
				return nil, ParseErr("POST " + authCredSubmit + ", got 200, but input[name=p_error_code] with value different than OAM-2: " + v)
			}
		} else {
			return nil, ParseErr("POST " + authCredSubmit + ", got 200, but input[name=p_error_code] with value=OAM-2 not found")
		}
	} else if res.StatusCode() != 302 {
		return nil, ConnectionErr(fmt.Sprintf("POST %q, got status code: %d", authCredSubmit, res.StatusCode()))
	}

	//login ok
	//https://fastweb.it/obrar.cgi?...
	location = HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	s.Debug("location=", location)
	if strings.HasPrefix(location, "/") {
		location = "https://fastweb.it" + location
	}
	OAM_ID := HeaderByNameAndValStartsWith(res.Header(), "Set-cookie", "OAM_ID")
	OAM_ID = OAM_ID[:strings.Index(OAM_ID, ";")]
	ORA_OTD_JROUTE := HeaderByNameAndValStartsWith(res.Header(), "Set-cookie", "ORA_OTD_JROUTE")
	ORA_OTD_JROUTE = ORA_OTD_JROUTE[:strings.Index(ORA_OTD_JROUTE, ";")]

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
		"referer":    "https://fastweb.it/myfastweb/accesso/login/",
		"cookie":     FW_VisitorID + "; " + PHPSESSID + "; cookiego_s=ok; " + OAMRequestContext_fastweb + "; " + TS01a22e7e,
	}).Get(location)
	if res == nil {
		return nil, ConnectionErr("GET " + location + " no response")
	}
	location = HeaderByNameAndValStartsWith(res.Header(), "Location", "")
	s.Debug("location=", location)
	if strings.HasPrefix(location, "/") {
		location = "https://fastweb.it" + location
	}
	OAMRequestContext_fastweb = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "OAMRequestContext_fastweb")
	OAMRequestContext_fastweb = OAMRequestContext_fastweb[:strings.Index(OAMRequestContext_fastweb, ";")]
	OAMAuthnCookie_fastweb := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "OAMAuthnCookie_fastweb")
	OAMAuthnCookie_fastweb = OAMAuthnCookie_fastweb[:strings.Index(OAMAuthnCookie_fastweb, ";")]
	TS01a22e7e = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS01a22e7e")
	TS01a22e7e = TS01a22e7e[:strings.Index(TS01a22e7e, ";")]

	//https://fastweb.it/myfastweb/accesso/
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
		"referer":    "https://fastweb.it/myfastweb/accesso/login/",
		"cookie":     FW_VisitorID + "; " + PHPSESSID + "; cookiego_s=ok; " + OAMAuthnCookie_fastweb + "; OAMAuthnHintCookie=X; " + TS01a22e7e,
	}).Get("https://fastweb.it/myfastweb/accesso/")
	if res == nil {
		return nil, ConnectionErr("GET " + location + " no response")
	}
	location = HeaderByNameAndValStartsWith(res.Header(), "Location", "") ///myfastweb/accesso/profile/?DirectLink=%2Fmyfastweb%2F ????  https://logon.fastweb.it:443/oam/server/obrareq.cgi?encquery%3D5%2BmZjKAhp%2BW0exd8DY6HYWJ4GD%2F8wo%2BkQpEFlsgxTAkn1LlYC4MKFWVDOKmjyQS5CI776X2DFwo1FtFD3PxRXlGvSk32d4%2BSvrQ83Xzx7EvJlOs5cHWBCuYFHXOcWLLqNM9iY%2BawXXxLgVwcSx1B0sHGY1NP%2FWlkzKZgqYkixZZGO7corNj8ZW6lyu1WpX3sGD50SZuJQCEvOvGCu2z5Sp%2FTA9L%2FRIvlzkZsubRYCmYQOs74%2F72Z%2Fz9QHWIlaz53MPUtH18bfi3jV03U6bdMrCDB1lx%2FCntGcJmFxV%2BHAa36gxS7XqTpm40yatdGsO%2Fx%20agentid%3Dportalemfp-pro-agent%20ver%3D1%20crmethod%3D2
	s.Debug("location=", location)
	if strings.HasPrefix(location, "/") {
		location = "https://fastweb.it" + location
	}
	//OAMAuthnHintCookie := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "OAMAuthnHintCookie")
	//OAMAuthnHintCookie = OAMAuthnHintCookie[:strings.Index(OAMAuthnHintCookie, "; ")]
	//Set-Cookie: OAMAuthnHintCookie=1
	TS01a22e7e = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS01a22e7e")
	TS01a22e7e = TS01a22e7e[:strings.Index(TS01a22e7e, "; ")]

	//https://fastweb.it/myfastweb/accesso/profile/?DirectLink=%2Fmyfastweb%2F
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
		"referer":    "https://fastweb.it/myfastweb/accesso/login/",
		"cookie":     FW_VisitorID + "; " + PHPSESSID + "; cookiego_s=ok; " + OAMAuthnCookie_fastweb + "; " + TS01a22e7e,
	}).Get("https://fastweb.it/myfastweb/accesso/profile/?DirectLink=%2Fmyfastweb%2F")
	if res == nil {
		return nil, ConnectionErr("GET " + location + " no response")
	}
	location = HeaderByNameAndValStartsWith(res.Header(), "Location", "") ///myfastweb/accesso/profile/?DirectLink=%2Fmyfastweb%2F
	s.Debug("location=", location)
	if strings.HasPrefix(location, "/") {
		location = "https://fastweb.it" + location
	}
	PHPSESSID2 := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID2")
	PHPSESSID2 = PHPSESSID2[:strings.Index(PHPSESSID2, ";")]
	cb_login_async := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "cb_login_async")
	cb_login_async = cb_login_async[:strings.Index(cb_login_async, ";")]
	wasin := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "wasin")
	wasin = wasin[:strings.Index(wasin, ";")]
	hasOffertaFissa := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "hasOffertaFissa")
	hasOffertaFissa = hasOffertaFissa[:strings.Index(hasOffertaFissa, ";")]
	hasOffertaMobile := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "hasOffertaMobile")
	hasOffertaMobile = hasOffertaMobile[:strings.Index(hasOffertaMobile, ";")]
	MyFPCPECity := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "MyFPCPECity")
	MyFPCPECity = MyFPCPECity[:strings.Index(MyFPCPECity, ";")]
	TS017ef010 := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS017ef010")
	TS017ef010 = TS017ef010[:strings.Index(TS017ef010, ";")]
	//Set-Cookie: OAMAuthnHintCookie=1; httponly; secure; path=/; domain=.it

	//https://fastweb.it/myfastweb/
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "fastweb.it",
		"referer":    "https://fastweb.it/myfastweb/accesso/login/",
		"cookie": PHPSESSID2 + "; " + TS017ef010 + "; " + FW_VisitorID + "; " + PHPSESSID + "; cookiego_s=ok; " + OAMAuthnCookie_fastweb +
			"; " + cb_login_async + "; " + wasin + "; " + hasOffertaFissa + "; " + hasOffertaMobile + "; " + MyFPCPECity + "; " + TS01a22e7e,
	}).Get("https://fastweb.it/myfastweb/")
	if res == nil {
		return nil, ConnectionErr("GET " + location + " no response")
	}
	if res.StatusCode() != 200 {
		//return nil, ConnectionErr(fmt.Sprintf(`after %d redirects got status %d; last redirect location=%q`, i, res.StatusCode(), location))
		return nil, ConnectionErr(fmt.Sprintf(`got status %d`, res.StatusCode()))
	}
	s.EnableRedirects(true)

	s.cookies = make(map[string]string)
	s.cookies["PHPSESSID2"] = PHPSESSID2
	s.cookies["TS017ef010"] = TS017ef010
	s.cookies["FW_VisitorID"] = FW_VisitorID
	s.cookies["PHPSESSID"] = PHPSESSID
	s.cookies["OAMAuthnCookie_fastweb"] = OAMAuthnCookie_fastweb
	s.cookies["wasin"] = wasin
	s.cookies["hasOffertaFissa"] = hasOffertaFissa
	s.cookies["hasOffertaMobile"] = hasOffertaMobile
	s.cookies["MyFPCPECity"] = MyFPCPECity
	s.cookies["TS01a22e7e"] = TS01a22e7e

	content := res.String()
	s.Debug(content)
	return nil, nil
}

func (s *FastwebStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *FastwebStrategy) LoadLocations(account *model.Account) Exception {
	//FW_TAN, FW_TRK_MYFP_CTYPE, VIVRdId, VIVRdId,
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie": s.cookies["PHPSESSID2"] + "; " + s.cookies["TS017ef010"] + "; " + s.cookies["FW_VisitorID"] +
			"; " + s.cookies["PHPSESSID"] + "; cookiego_s=ok; " + s.cookies["OAMAuthnCookie_fastweb"] + "; " +
			"; outbrain_cid_fetch=true; " + s.cookies["wasin"] + "; " + s.cookies["hasOffertaFissa"] + "; " +
			s.cookies["hasOffertaMobile"] + "; " + s.cookies["MyFPCPECity"] + "; " +
			s.cookies["TS01a22e7e"],
		"host": "fastweb.it",
	}).Get(profile)
	if err != nil {
		return ConnectionErr("GET " + profile + " : " + err.Error())
	}
	TS01a22e7e := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "TS01a22e7e")
	TS01a22e7e = TS01a22e7e[:strings.Index(TS01a22e7e, ";")]
	s.cookies["TS01a22e7e"] = TS01a22e7e
	doc, ex := OkDocument(res, err)
	if ex != nil {
		return ex
	}

	clientCodeEl := doc.Find("div:containsOwn('codice cliente')")
	if clientCodeEl.Length() == 0 {
		return ParseErr("no client code element found")
	}
	clientCodeEl = clientCodeEl.Next()
	if clientCodeEl.Length() == 0 {
		return ParseErr("no client code element found")
	}
	clientCode := strings.TrimSpace(clientCodeEl.Text())
	if clientCode == "" {
		s.Debug(doc.Html())
		return ParseErr("missing client code")
	}
	s.Trace(clientCode)
	addEl := doc.Find("div:containsOwn('indirizzo')")
	if addEl.Length() == 0 {
		return ParseErr("no address element found")
	}
	addEl = addEl.Next()
	if addEl.Length() == 0 {
		return ParseErr("no address element found")
	}
	address := strings.TrimSpace(addEl.Text())
	s.Trace(address)

	nrEls := doc.Find("div[class='fw-profile-phonenumber']")
	for i := 0; i < nrEls.Length(); i++ {
		selection := nrEls.Eq(i)
		nr := strings.TrimSpace(selection.Text())
		if nr == "" {
			s.Debug(doc.Html())
			return ParseErr("missing phone number")
		}
		typeSel := strings.TrimSpace(selection.Parent().Prev().Text())
		fix := false
		if strings.Contains(typeSel, "fiss") {
			fix = true
		}
		l := &model.Location{
			Service:        address,
			Details:        &clientCode,
			Identifier:     nr,
			AccountHelpers: fix,
		}
		account.AddLocation(l)
	}
	if len(account.Locations) == 0 {
		return nil
	}

	securityToken, ex := Attr(doc.Find("input[name='securityToken']"), "value")
	if ex != nil {
		return ex
	}
	if ex := s.loadInvoicesInternal(account, securityToken); ex != nil {
		return ex
	}
	return nil
}

type invoiceJson struct {
	ErrorCode           int
	ErrorMessage        string
	HasPaidInvoice      bool
	HasUnpaidInvoice    bool
	InvoiceDeliveryMode string
	PaymentMethod       string
	InvoiceList         []*struct {
		DocAmount             string
		DocAmountFormatted    string
		DocDateDMY            string
		DocDateYMD            string
		DocExpireDateDMY      string
		DocExpireDateYMD      string
		DocOnlinePaymentDate  string
		DocTypeId             string
		FiscalYear            int
		FrontendInvoiceStatus string
		InvoiceTechStatus     string
		NumDoc                string
		OnlinePaybilityStatus string
		OnlinePayment         string
		HasDetail             bool
		HasHTMLFormat         bool
		PayNow                bool
	}
}

func (s *FastwebStrategy) loadInvoicesInternal(account *model.Account, securityToken string) Exception {
	headers := map[string]string{
		"accept":       "application/json, text/javascript, */*; q=0.01",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded; charset=UTF-8",
		"cookie": s.cookies["PHPSESSID2"] + "; " + s.cookies["TS017ef010"] + "; " + s.cookies["FW_VisitorID"] + "; " +
			s.cookies["PHPSESSID"] + "; " +
			"; cookiego_s=ok; " + s.cookies["OAMAuthnCookie_fastweb"] + "; " +
			s.cookies["wasin"] + "; " + s.cookies["hasOffertaFissa"] + "; " +
			s.cookies["hasOffertaMobile"] + "; " + s.cookies["MyFPCPECity"] + "; " +
			"OAMAuthnHintCookie=1; " + s.cookies["TS01a22e7e"] + "; cb_login_async=1",
		//FW_TAN=MTM3szSztLQAAA%3D%3D; FW_TRK_MYFP_CTYPE=5; VIVRdId=1aef1da7-021b-4fc9-b6bd-0e978a47e21f; VIVRtId=M0239df2bed63e166;
		"host":             "fastweb.it",
		"origin":           "https://fastweb.it",
		"X-Requested-With": "XMLHttpRequest",
	}
	res, ex := OkString(s.Client().R().SetHeaders(headers).SetFormData(map[string]string{
		"action":        "loadInvoiceList",
		"securityToken": securityToken,
	}).Post(invoiceList))
	s.Debug(res)
	if ex != nil {
		return ex
	}
	var invoices invoiceJson
	err := utils.FromJSON(&invoices, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse invoice response: " + err.Error())
	}
	if invoices.ErrorMessage != "" {
		return ParseErr("invoices response: " + res)
	}
	for _, i := range invoices.InvoiceList {
		if i.InvoiceTechStatus == "Pagata" {
			continue
		}

		uri := fmt.Sprintf(pdf, i.DocDateYMD, i.NumDoc)
		am, ex := AmountFromString(i.DocAmountFormatted)
		if ex != nil {
			return ex
		}
		issueDate, err := time.Parse(ymd, i.DocDateYMD)
		if err != nil {
			return ParseErr("parse issue date " + i.DocDateYMD + " : " + err.Error())
		}
		dueDate, err := time.Parse(ymd, i.DocExpireDateYMD)
		if err != nil {
			return ParseErr("parse due date " + i.DocExpireDateYMD + " : " + err.Error())
		}
		inv := &model.Invoice{
			Ref:       i.NumDoc,
			PdfUri:    &uri,
			Amount:    am,
			AmountDue: am,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}

		location, ex := s.chooseFixLocation(account.Locations, inv)
		if ex != nil {
			s.Error("parse location from pdf for " + i.NumDoc + " : " + ex.Error())
		}
		if location == nil {
			location = account.Locations[0]
		}
		location.AddInvoice(inv)
	}
	return nil
}

func (s *FastwebStrategy) chooseFixLocation(locations []*model.Location, inv *model.Invoice) (*model.Location, Exception) {
	//resp := s.downloadPdf(inv)
	//if resp.ErrMsg != nil && *resp.ErrMsg != "" {
	//	return nil, ParseErr("download pdf:" + *resp.ErrMsg)
	//}
	//body:=resp.Content
	//eof := []byte(`%%EOF`)
	//body = body[:bytes.Index(body, eof)+len(eof)]
	//txt, err := PlainText(body)
	//if err != nil {
	//	return nil, ParseErr("parse pdf text: " + err.Error())
	//}
	//s.Trace(txt)
	for _, l := range locations {
		isFix, ok := l.AccountHelpers.(bool)
		if ok && isFix {
			return l, nil
		}
	}
	return nil, nil
}

func (s *FastwebStrategy) downloadPdf(invoice *model.Invoice) *model.PdfResponse {
	pdfUri := *invoice.PdfUri
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"connection": "keep-alive",
		"cookie": s.cookies["PHPSESSID2"] + "; " + s.cookies["TS017ef010"] + "; " + s.cookies["FW_VisitorID"] + "; " +
			s.cookies["PHPSESSID"] + "; cookiego_s=ok; " + s.cookies["OAMAuthnCookie_fastweb"] + "; " +
			s.cookies["wasin"] + "; " + s.cookies["hasOffertaFissa"] + "; " +
			s.cookies["hasOffertaMobile"] + "; " + s.cookies["MyFPCPECity"] + "; " +
			"OAMAuthnHintCookie=1; " + s.cookies["TS01a22e7e"] + "; cb_login_async=1",
		"host": "fastweb.it",
	}).Get(pdfUri))
	s.Trace(res)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	index := res[strings.Index(res, `viewInvoice('`)+13:]
	index = index[:strings.Index(index, `')`)]
	uri := "https://fastweb.it/myfastweb/abbonamento/visualizza-conto-fastweb/conto-fastweb/" + index
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie": s.cookies["PHPSESSID2"] + "; " + s.cookies["TS017ef010"] + "; " + s.cookies["FW_VisitorID"] + "; " +
			s.cookies["PHPSESSID"] + "; cookiego_s=ok; " + s.cookies["OAMAuthnCookie_fastweb"] + "; " +
			s.cookies["wasin"] + "; " + s.cookies["hasOffertaFissa"] + "; " +
			s.cookies["hasOffertaMobile"] + "; " + s.cookies["MyFPCPECity"] + "; " +
			"OAMAuthnHintCookie=1; " + s.cookies["TS01a22e7e"] + "; cb_login_async=1",
		"host": "fastweb.it",
	}).Get(uri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *FastwebStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *FastwebStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *FastwebStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *FastwebStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	return s.downloadPdf(invoice)
}

func (s *FastwebStrategy) LoadsInternal() bool {
	return true
}

func (s *FastwebStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *FastwebStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
