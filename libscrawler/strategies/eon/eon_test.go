package eon

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "eon.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"paaulina_01@yahoo.com", "Kitolino1234"},
		//{"ailenei_raluca@yahoo.com", "Raluka'24"},
		//{"gabriel.oneit@gmail.com", "Crocodil1"},
		//{"muresan.danconstantin@gmail.com", `Yellow12+"`},
		//{"ap@pilotti.it", ",ZV~B-WTm\\qa\\}d!M85>"},
		//{"grosucornel@yahoo.com", "MyLine123456"},
		//{"rodiprostean68@gmail.com", "Rodica68"},
		//{"viasumada90@gmail.com", "Madalin04"},

		//{"Adi20ibm@yahoo.com", "Adi20ibm"},
		//{"andreea_gandila@yahoo.com", ""},
		//{"claudiuveronica@yahoo.com", "Raul1610"},
		//{"cristian_corman@yahoo.com", "Dark123!!!"},
		//{"emifulop@yahoo.com", "erikF2006"},
		//{"ioncristescu60@yahoo.com", "Viorica60"},
		//{"samuelstefan306@gmail.com", "Brailei15"},
		//{"Dobrin.campean@gmail.com", "Dobrin120675"},
		//{"dragomir.cristi08@gmail.com", "Cristii77"},
		//{"coco_daria@yahoo.com", "Tm79daria"},//https://pagojira.atlassian.net/browse/PAGO-16489
		//{"marius_mary68@yahoo.com", "Milka2019"}, //https://pagojira.atlassian.net/browse/PAGO-16489

		//https://pagojira.atlassian.net/browse/PAGO-16899
		//{"anca.flore@yahoo.com", "Petralma2"},

		//{"mihaela@contver.ro", "Totolica.2008"},
		//{"vrinceanustefan80@gmail.com", "Bibicu2014"},

		//https://pagojira.atlassian.net/browse/PAGO-18060
		//{"alexa_dool@yahoo.com", "137784asdY"},
		//{"cristi_andreib@yahoo.com", "Vagava10"},
		//{"streteana@gmail.com", "1Cipri2Feli"},

		//https://pagojira.atlassian.net/browse/PAGO-18938
		{"bpmorosan@yahoo.com", "Saraelena2013!"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "dragomir.cristi08@gmail.com", Password: "Cristii77", Uri: "eon.crawler",
			Identifier: "5000990707",
			Ref:        "0000010920454522", PdfUri: "011269270785",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}

func TestGenerateEonBarcode(t *testing.T) {
	want := "210274663822000114061119740020000013442"

	details := "2102746638"
	got, ex := GenerateEonBarcode(&model.Location{
		Service: "",
		Details: &details,
	}, &model.Invoice{
		Ref:       "0000011406111974",
		Amount:    134.42,
		AmountDue: 134.42,
	})

	if ex != nil {
		t.Error(ex)
	}
	_ = "210274663822000114061119740020000013441"
	if got != want {
		t.Errorf("\n got=%q,\nwant=%q", got, want)
	} else {
		t.Logf("\n got=%q,\nwant=%q", got, want)
	}
}
