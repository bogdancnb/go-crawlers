package eon

type UserDetails struct {
	FirstName         string `json:"firstName"`
	LastName          string `json:"lastName"`
	LegacyAddress     string `json:"legacyAddress"`
	Address           string `json:"address"`
	FixPhoneNumber    string `json:"fixPhoneNumber"`
	MobilePhoneNumber string `json:"mobilePhoneNumber"`
	Email             string `json:"email"`
	UserType          string `json:"userType"`
	Migrated          bool   `json:"migrated"`
	//userSocialList interface{} `json:"userSocialList"`
}

type Partner struct {
	AccessLevel string `json:"accessLevel"`
	Name        string `json:"name"`
	PartnerCode string `json:"partnerCode"`
	Segment     string `json:"segment"`
}

type AccountContractJson struct {
	AccessLevel             string  `json:"accessLevel"`
	AccountContract         string  `json:"accountContract"`
	Active                  bool    `json:"active"`
	ConsumptionPointCode    string  `json:"consumptionPointCode"`
	ElectronicInvoice       bool    `json:"electronicInvoice"`
	Label                   string  `json:"label"`
	PartnerCode             string  `json:"partnerCode"`
	UtilityType             string  `json:"utilityType"`
	Visibility              string  `json:"visibility"`
	ConsumptionPointAddress Address `json:"consumptionPointAddress"`
}

type Address struct {
	Apartment    string   `json:"apartment"`
	Building     string   `json:"building"`
	CountyName   string   `json:"countyName"`
	PostalCode   string   `json:"postalCode"`
	StreetNumber string   `json:"streetNumber"`
	Locality     Locality `json:"locality"`
	Street       Street   `json:"street"`
	Floor        *string  `json:"floor"`
	Flat         *string  `json:"flat"`
}

func (a Address) String() string {
	return a.Locality.CountyName + ", " +
		a.Locality.LocalityName + ", " +
		a.Street.StreetType.Label + " " +
		a.Street.StreetName + " nr." +
		a.StreetNumber + ", ap." +
		a.Apartment
}

type Locality struct {
	CountyCode   string `json:"countyCode"`
	CountyName   string `json:"countyName"`
	LocalityCode string `json:"localityCode"`
	LocalityName string `json:"localityName"`
}

type Street struct {
	StreetCode string     `json:"streetCode"`
	StreetName string     `json:"streetName"`
	StreetType StreetType `json:"streetType"`
}

type StreetType struct {
	Label string `json:"label"`
	Value string `json:"value"`
}

type AccountContractDetails struct {
	AccessLevel                   string  `json:"accessLevel"`
	AccountContract               string  `json:"accountContract"`
	Active                        bool    `json:"active"`
	CollectiveContract            bool    `json:"collectiveContract"`
	ConsumptionPointAddress       Address `json:"consumptionPointAddress"`
	ConsumptionPointCode          string  `json:"consumptionPointCode"`
	Contract                      string  `json:"contract"`
	ElectronicInvoice             bool    `json:"electronicInvoice"`
	ElectronicInvoiceDays         int     `json:"electronicInvoiceDays"`
	ElectronicInvoiceModifiedDate string  `json:"electronicInvoiceModifiedDate"`
	MailingAddress                Address `json:"mailingAddress"`
	PartnerCode                   string  `json:"partnerCode"`
	ProductName                   string  `json:"productName"`
	Type                          string  `json:"type"`
	UtilityType                   string  `json:"utilityType"`
	Visibility                    string  `json:"visibility"`
}

type InvoiceJson struct {
	AccountContract string  `json:"accountContract"`
	BalanceValue    float64 `json:"balanceValue"`
	Cb              string  `json:"cb"`
	CompanyCode     string  `json:"companyCode"`
	Electronic      bool    `json:"electronic"`
	EmissionDate    string  `json:"emissionDate"`
	FiscalNumber    string  `json:"fiscalNumber"`
	InvoiceNumber   string  `json:"invoiceNumber"`
	IssuedValue     float64 `json:"issuedValue"`
	MaturityDate    string  `json:"maturityDate"`
	PrintDate       string  `json:"printDate"`
	Sector          string  `json:"sector"`
	State           string  `json:"state"`
	Type            string  `json:"type"`
}

type PaymentJson struct {
	AccountContract string  `json:"accountContract"`
	DocumentNumber  string  `json:"documentNumber"`
	PaymentChannel  string  `json:"paymentChannel"`
	PaymentDate     string  `json:"paymentDate"`
	PaymentType     string  `json:"paymentType"`
	Value           float64 `json:"value"`
}
