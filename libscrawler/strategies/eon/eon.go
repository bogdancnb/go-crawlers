package eon

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"regexp"
	"sort"
	"strings"
	"time"
)

const (
	LOGIN_AUTH                      = "https://api.eon.ro/users/v1/userauth/login"
	LOGIN                           = "https://www.eon.ro/myline/login"
	USER_DETAILS2                   = "https://api.eon.ro/users/v1/users/user-details"
	LIST_PARTNERS_INDIVIDUAL        = "https://api.eon.ro/partners/v2/partners/list?accountType=Individual&limit=-1"
	LIST_PARTNERS_COMPANY           = "https://api.eon.ro/partners/v2/partners/list?accountType=Company&limit=-1"
	PARTNER_ACCOUNT_CONTRACTS       = "https://api.eon.ro/partners/v2/account-contracts/list?partnerCode=%s&limit=-1"
	INVOICES_DAHSBOARD_ACC_CONTRACT = "https://api.eon.ro/invoices/v1/invoices/dashboard-data?accountContract=%s"
	ACC_CONTRACT_DETAILS            = "https://api.eon.ro/partners/v2/account-contracts/%s"
	E_INVOICE_DEACTIVATE            = "https://api.eon.ro/partners/v2/account-contracts/%s/electronic-invoice/deactivate"
	E_INVOICE_ACTIVATE              = "https://api.eon.ro/partners/v2/account-contracts/%s/electronic-invoice/activate"
	INVOICES                        = "https://api.eon.ro/invoices/v1/invoices/list?accountContract=%s&status="
	PAYMENTS                        = "https://api.eon.ro/invoices/v1/payments?accountContract=%s"
	PDF_MIGRATED                    = "https://api.eon.ro/invoices/v1/invoices/%s/pdf"

	siteFormat     = "02.01.2006"
	eInvoiceFormat = "2006-01-02T15:04:05"
	subKey         = "Ocp-Apim-Subscription-Key"
)

var barcodeRegex = regexp.MustCompile(`[\d]{39}`)

func Init() {
	RegisterFactory("eon.crawler", new(EonFactory))
}

type EonFactory struct{}

func (p *EonFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EonStrategy")
	return &EonStrategy{
		Entry:                 l,
		HttpStrategy:          NewHttpStrategy(l),
		exLocations:           make(map[string]*model.Location),
		exInvoices:            make(map[string]*model.Invoice),
		reziliatInvoices:      make(map[string][]*model.Invoice),
		accountContractsByRef: make(map[string]string),
	}
}

type EonStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations      map[string]*model.Location
	exInvoices       map[string]*model.Invoice
	reziliatInvoices map[string][]*model.Invoice
	doc              *goquery.Document

	subscriptionID string
	accessToken    string
	legacyId       string
	uuid           string

	userDetails           UserDetails
	individualPartners    []*Partner
	companyPartners       []*Partner
	accountContractsByRef map[string]string
}

func (s *EonStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	//pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	//if err != nil {
	//	s.WithError(err).Error("pdf download error, try generating")
	//	return s.generateBarcode(account, location, invoice)
	//}
	//
	//barcode, err := MatchBarcodePatterns(pdfBytes, []*regexp.Regexp{barcodeRegex})
	//if err != nil {
	//	s.WithError(err).Error("MatchBarcodePatterns, try generating")
	//	return s.generateBarcode(account, location, invoice)
	//}
	//invoice.BarCode = &barcode
	//return nil
	return s.generateBarcode(account, location, invoice)
}

func (s *EonStrategy) generateBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	//barcode, ex := GenerateEonBarcode(location, invoice)
	//if ex != nil {
	//	return ex
	//}
	//s.Debugf("ref %s got barcode=%s", invoice.Ref, barcode)
	//invoice.BarCode = &barcode
	return nil
}

func GenerateEonBarcode(location *model.Location, invoice *model.Invoice) (string, Exception) {
	incomeCode := ""
	if location.Details != nil && *location.Details != "" {
		incomeCode = *location.Details
	} else if len(location.Service) > 10 {
		incomeCode = location.Service[:10]
	}
	if incomeCode == "" {
		return "", ParseErr("could not read the incomeCode from location")
	}

	regionCode := "22"

	ref := strings.TrimLeft(invoice.Ref, "0")
	ref = fmt.Sprintf("%014s", ref)

	amountDigit := "0"
	if invoice.Amount <= 0 {
		amountDigit = "1"
	}

	amountToUse := invoice.Amount
	if invoice.AmountDue > 0 {
		amountToUse = invoice.AmountDue
	}
	if amountToUse < 0 {
		return "", ParseErr("Could not generate barcode for negative amount.")
	}
	amountString := decimal.NewFromFloat(amountToUse).Mul(decimal.NewFromInt(100)).String()
	amountString = fmt.Sprintf("%010s", amountString)

	barcode := fmt.Sprintf(`%s%s%s%s02%s`, incomeCode, regionCode, ref, amountDigit, amountString)
	return barcode, nil
}

func (s *EonStrategy) EInvoiceNeedsLoadLocations() bool {
	return true
}

func (s *EonStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	accContract := s.accountContractsByRef[received.Identifier]
	uri := fmt.Sprintf(E_INVOICE_ACTIVATE, accContract)
	s.Debug("calling activate e-invoice: ", uri)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Host":         "api.eon.ro",
		"Content-Type": "application/json",
		"Origin":       "https://www.eon.ro",
		"Referer":      "https://www.eon.ro/myline/factura-electronica",
	}).SetBody("{\"termActivateElectronicInvoice\":true}").Post(uri)
	if res == nil {
		return ConnectionErr("no response for " + uri)
	}
	s.Debug(FormatResponse(res))
	if strings.Contains(res.String(), `Electronic invoice is activated`) {
		return nil
	}
	if res.StatusCode() != 200 {
		return ParseErr(res.Status() + " : " + res.String())
	}

	var resJson struct {
		Success bool `json:"success"`
	}
	err = utils.FromJSON(&resJson, strings.NewReader(res.String()))
	if err != nil {
		return ParseErr("parsing ActivateEInvoice json from response: " + res.String())
	}
	if !resJson.Success {
		return ParseErr("ActivateEInvoice response: " + res.String())
	}
	return nil
}

func (s *EonStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		//"Host":       "",
		//"Connection": "keep-alive",
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().Get(LOGIN))
	if ex != nil {
		return nil, ex
	}
	subEls := doc.Find("[data-prop-aem*='subscriptionKey']")
	if subEls.Length() == 0 {
		return nil, ParseErr("no subscription id element found")
	}
	subIdTxt, _ := subEls.Eq(0).Attr("data-prop-aem")
	var subJson struct {
		Options struct {
			SubscriptionKey string `json:"subscriptionKey"`
		} `json:"options"`
	}
	err := utils.FromJSON(&subJson, strings.NewReader(subIdTxt))
	if err != nil {
		return nil, ParseErr("could not parse subscription id from: " + subIdTxt)
	}
	s.subscriptionID = subJson.Options.SubscriptionKey

	escapedPass := strings.ReplaceAll(*account.Password, `\`, `\\`)
	escapedPass = strings.ReplaceAll(escapedPass, `"`, `\"`)
	body := fmt.Sprintf(`{"username":"%s","password":"%s","rememberMe":false}`, account.Username, escapedPass)
	res, err := s.Client().R().
		SetHeaders(map[string]string{
			"Content-Type":   "application/json",
			subKey:           s.subscriptionID,
			"Referer":        "https://www.eon.ro/myline/login",
			"Origin":         "https://www.eon.ro",
			"withAuthBearer": "undefined",
		}).
		SetBody(body).
		Post(LOGIN_AUTH)
	res, ex = Accepted(res, err, 400)
	if ex != nil {
		return nil, ex
	}
	if ex := s.checkLoginAccessToken(res.String()); ex != nil {
		return nil, ex
	}
	return nil, nil
}

func (s *EonStrategy) checkLoginAccessToken(response string) Exception {
	var r struct {
		AccessToken string `json:"accessToken"`
		LegacyId    string `json:"legacyId"`
		Uuid        string `json:"uuid"`
		Description string `json:"description"`
	}
	err := utils.FromJSON(&r, strings.NewReader(response))
	if err != nil {
		return ParseErr("parsing login response: " + response)
	}
	if r.AccessToken == "" || "bad credentials" == strings.ToLower(r.Description) {
		return LoginErr("Eon login error, response=" + response)
	}
	s.accessToken = r.AccessToken
	s.legacyId = r.LegacyId
	s.uuid = r.Uuid

	s.Client().SetHeaders(map[string]string{
		"Accept":         "application/json, text/plain, */*",
		"Authorization":  "Bearer " + s.accessToken,
		subKey:           s.subscriptionID,
		"Origin":         "https://www.eon.ro",
		"Sec-Fetch-Mode": "cors",
	})
	return nil
}

func (s *EonStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *EonStrategy) LoadLocations(account *model.Account) Exception {
	var ex Exception
	s.userDetails, ex = s.getUserDetails()
	if ex != nil {
		return ex
	}
	s.individualPartners, ex = s.partnersRequest(LIST_PARTNERS_INDIVIDUAL)
	if ex != nil {
		return ex
	}
	s.companyPartners, ex = s.partnersRequest(LIST_PARTNERS_COMPANY)
	if ex != nil {
		return ex
	}

	if ex := s.loadLocations(account, s.individualPartners); ex != nil {
		return ex
	}
	if ex := s.loadLocations(account, s.companyPartners); ex != nil {
		return ex
	}
	return nil
}

func (s *EonStrategy) getUserDetails() (UserDetails, Exception) {
	details := UserDetails{}
	res, ex := OkString(s.Client().R().Get(USER_DETAILS2))
	if ex != nil {
		return details, ex
	}
	s.Debug("getUserDetails: ", res)
	err := utils.FromJSON(&details, strings.NewReader(res))
	if err != nil {
		return details, ParseErr("de-serializing user details from: " + res)
	}
	return details, nil
}

func (s *EonStrategy) partnersRequest(uri string) ([]*Partner, Exception) {
	res, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return nil, ex
	}
	s.Debug("partnersRequest uri=" + uri + ": " + res)
	var partners []*Partner
	err := utils.FromJSON(&partners, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("de-serializing user details from: " + res)
	}
	return partners, nil
}

func (s *EonStrategy) loadLocations(account *model.Account, partners []*Partner) Exception {
	if len(partners) == 0 {
		return nil
	}
	accountContracts, ex := s.accountContracts(partners)
	if ex != nil {
		return ex
	}
	for _, contract := range accountContracts {
		accountContractDetails, ex := s.accountContractDetails(contract.AccountContract)
		if ex != nil {
			return ex
		}

		accContract := contract.AccountContract[2:]
		address := contract.Label
		if address == "" {
			address = contract.ConsumptionPointAddress.String()
		}
		var identifier string
		if !accountContractDetails.CollectiveContract && contract.ConsumptionPointCode != "" {
			identifier = contract.ConsumptionPointCode
		} else { //dual
			identifier = accContract
		}
		if !contract.Active || "INACTIVE" == contract.Visibility {
			msg := fmt.Sprintf("inactive contract: identifier=%s, account contract=%s", identifier, accContract)
			s.Warnf(msg)
			unpaid, ex := s.invoiceRequest(contract.AccountContract, "unpaid")
			if ex != nil {
				return ex
			}
			paid, ex := s.invoiceRequest(contract.AccountContract, "paid")
			if ex != nil {
				return ex
			}
			for _, invoiceJson := range unpaid {
				i, ex := s.parseInvoice(invoiceJson)
				if ex != nil {
					return ex
				}
				s.reziliatInvoices[identifier] = append(s.reziliatInvoices[identifier], i)
			}
			for _, invoiceJson := range paid {
				i, ex := s.parseInvoice(invoiceJson)
				if ex != nil {
					return ex
				}
				s.reziliatInvoices[identifier] = append(s.reziliatInvoices[identifier], i)
			}

			continue
		}

		eInvoiceEditable := true
		if accountContractDetails.ElectronicInvoiceModifiedDate != "" {
			eInvoiceLastModified, err := time.Parse(eInvoiceFormat, accountContractDetails.ElectronicInvoiceModifiedDate)
			if err != nil {
				return ParseErr("parsing e-invoice modified date: " + accountContractDetails.ElectronicInvoiceModifiedDate)
			}
			if time.Now().Before(eInvoiceLastModified.AddDate(0, 0, accountContractDetails.ElectronicInvoiceDays)) {
				eInvoiceEditable = false
			}
		}
		eInvoiceInfo := &model.ElectronicInvoiceLocationInfo{
			ElectronicInvoiceType:     model.EMAIL,
			ElectronicInvoiceActive:   accountContractDetails.ElectronicInvoice,
			ElectronicInvoiceEditable: eInvoiceEditable,
		}

		location := &model.Location{
			Service:                        accContract + " " + address,
			Details:                        &accContract,
			Identifier:                     identifier,
			ElectronicInvoiceLocationInfos: []*model.ElectronicInvoiceLocationInfo{eInvoiceInfo},
			AccountHelpers:                 contract.AccountContract,
			ServiceProvided:                &contract.UtilityType,
		}
		exLoc, exists := s.exLocations[identifier]
		s.Debug("identifier=", identifier, "existing location:", exists)
		if !exists || exLoc.LocationAddress == nil || exLoc.LocationAddress.AddressId == nil {
			addr := contract.ConsumptionPointAddress
			location.LocationAddress = &model.LocationAddress{
				AddressId:      nil,
				Street:         &addr.Street.StreetName,
				StreetNumber:   &addr.StreetNumber,
				Building:       &addr.Building,
				BuildingNumber: nil,
				Floor:          addr.Floor,
				Apartment:      &addr.Apartment,
				City:           &addr.Locality.LocalityName,
				County:         &addr.Locality.CountyName,
				PostalCode:     &addr.PostalCode,
			}
		}
		account.AddLocation(location)
		s.accountContractsByRef[location.Identifier] = contract.AccountContract
	}
	return nil
}

func (s *EonStrategy) accountContracts(partners []*Partner) ([]*AccountContractJson, Exception) {
	var partnerCodes []string
	for _, partner := range partners {
		partnerCodes = append(partnerCodes, partner.PartnerCode)
	}
	partnerCodesString := strings.Join(partnerCodes, ",")
	uri := fmt.Sprintf(PARTNER_ACCOUNT_CONTRACTS, partnerCodesString)
	res, ex := OkString(s.Client().R().Get(uri))
	s.Debug("accountContracts=", res)
	if ex != nil {
		return nil, ex
	}
	s.Debug("AccountContractJson: ", res)
	var contracts []*AccountContractJson
	err := utils.FromJSON(&contracts, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("de-serializing json: " + res)
	}
	return contracts, nil
}

func (s *EonStrategy) accountContractDetails(contract string) (AccountContractDetails, Exception) {
	details := AccountContractDetails{}
	uri := fmt.Sprintf(ACC_CONTRACT_DETAILS, contract)
	res, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return details, nil
	}
	s.Debug("AccountContractDetails: ", res)
	err := utils.FromJSON(&details, strings.NewReader(res))
	if err != nil {
		return details, ParseErr("parsing AccountContractDetails : " + res)
	}
	return details, nil
}

func (s *EonStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	s.Debug("load amount for ", location.Identifier)
	accContract := location.AccountHelpers.(string)
	uri := fmt.Sprintf(INVOICES_DAHSBOARD_ACC_CONTRACT, accContract)
	res, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return ex
	}
	s.Debug("sold ", location.Identifier, ": ", res)
	var sold struct {
		Balance struct {
			Balance float64 `json:"balance"`
		} `json:"balance"`
	}
	err := utils.FromJSON(&sold, strings.NewReader(res))
	if err != nil {
		return ParseErr("parsing amount from : " + res)
	}
	location.Amount = &sold.Balance.Balance
	return nil
}

func (s *EonStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	s.Debug("load invoices for ", location.Identifier)
	accContract := location.AccountHelpers.(string)
	unpaid, ex := s.invoiceRequest(accContract, "unpaid")
	if ex != nil {
		return ex
	}
	paid, ex := s.invoiceRequest(accContract, "paid")
	if ex != nil {
		return ex
	}
	for _, invoiceJson := range unpaid {
		i, ex := s.parseInvoice(invoiceJson)
		if ex != nil {
			return ex
		}
		location.AddInvoice(i)
	}
	for _, invoiceJson := range paid {
		i, ex := s.parseInvoice(invoiceJson)
		if ex != nil {
			return ex
		}
		location.AddInvoice(i)
	}

	for _, reziliatInvoice := range s.reziliatInvoices[location.Identifier] {
		found := false
		for _, invoice := range location.Invoices {
			if invoice.Ref == reziliatInvoice.Ref {
				found = true
				break
			}
		}
		if !found {
			location.AddInvoice(reziliatInvoice)
		}
	}

	sort.Slice(location.Invoices, func(i, j int) bool {
		return location.Invoices[i].IssueDate > location.Invoices[j].IssueDate
	})
	//location.SetInvoiceDueFromAmount()
	//allZero:=false
	//for _, i := range location.Invoices {
	//
	//}
	return nil
}

func (s *EonStrategy) invoiceRequest(accContract, paid string) ([]*InvoiceJson, Exception) {
	uri := fmt.Sprintf(INVOICES, accContract) + paid + "&includeSubcontracts=true"
	res, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return nil, ex
	}
	s.Debug("invoices ", paid, ": ", res)
	var invoices []*InvoiceJson
	err := utils.FromJSON(&invoices, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("de-serializing invoices from: " + res)
	}
	return invoices, nil
}

func (s *EonStrategy) parseInvoice(json *InvoiceJson) (*model.Invoice, Exception) {
	due := 0.0
	if json.BalanceValue > 0 {
		due = json.BalanceValue
	}
	issueDate, err := time.Parse(siteFormat, json.EmissionDate)
	if err != nil {
		return nil, ParseErr("parsing issue date from:" + json.EmissionDate)
	}
	dueDate, err := time.Parse(siteFormat, json.MaturityDate)
	if err != nil {
		return nil, ParseErr("parsing due date from :" + json.MaturityDate)
	}
	var barcode *string
	if json.Cb != "" {
		barcode = &json.Cb
	}
	invoice := &model.Invoice{
		Ref:       json.FiscalNumber,
		PdfUri:    &json.InvoiceNumber,
		Amount:    json.IssuedValue,
		AmountDue: due,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
		BarCode:   barcode,
	}
	return invoice, nil
}

func (s *EonStrategy) parsePayment(location *model.Location, json *PaymentJson) Exception {
	date, err := time.Parse(eInvoiceFormat, json.PaymentDate)
	if err != nil {
		return ParseErr("parsing payment date:" + err.Error())
	}
	details := json.PaymentType + ", " + json.PaymentChannel
	payment := &model.Payment{
		Ref:     json.DocumentNumber,
		Amount:  json.Value,
		Details: &details,
		Date:    date.Format(utils.DBDateFormat),
	}
	location.AddPayment(payment)
	return nil
}

func (s *EonStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	s.Debug("load payments for ", location.Identifier)
	accContract := location.AccountHelpers.(string)
	payments, ex := s.paymentRequest(accContract)
	if ex != nil {
		return ex
	}
	for _, payment := range payments {
		if ex := s.parsePayment(location, payment); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *EonStrategy) paymentRequest(contract string) ([]*PaymentJson, Exception) {
	uri := fmt.Sprintf(PAYMENTS, contract)
	res, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return nil, ex
	}
	s.Debug("payments: ", res)
	var payments []*PaymentJson
	err := utils.FromJSON(&payments, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("parsing payments from response: " + res)
	}
	return payments, nil
}

func (s *EonStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *EonStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	uri := fmt.Sprintf(PDF_MIGRATED, *invoice.PdfUri)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Referer": "https://www.eon.ro/myline/facturile-mele",
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("GET " + uri + ": " + err.Error())
	}
	var bodyString struct {
		File string `json:"file"`
	}
	err = utils.FromJSON(&bodyString, bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("could not parse base64 pdf content from response: " + res.String())
	}
	pdfBytes, err := base64.StdEncoding.DecodeString(bodyString.File)
	if err != nil {
		return nil, ParseErr("could not decode pdf bytes from base64 string")
	}
	return pdfBytes, nil
}

func (s *EonStrategy) LoadsInternal() bool {
	return false
}

func (s *EonStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *EonStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
		s.reziliatInvoices[location.Identifier] = make([]*model.Invoice, 0)
	}
}
