package tiscali

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "tiscali.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"annasaviano149@gmail.com", "Tavolone2"}, //login err
		//{"Vincenzo.bottigl@tiscali.it", "onetouchscreenprint"},
		{"agatinopuglia@libero.it", "tino12"},
		//{"francoweb48@gmail.com", "666rebus"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "agatinopuglia@libero.it", Password: "tino12", Uri: "tiscali.crawler",
			Identifier: "440478471",
			Ref:        "marzo2021", PdfUri: "https://selfcare.tiscali.it/unit/ecare/it_file_viewer?file=IIOJAUAA51LAIS-011.pdf&visp=tiscali",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
