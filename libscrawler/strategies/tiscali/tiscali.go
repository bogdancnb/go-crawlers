package tiscali

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	home           = "https://selfcare.tiscali.it/unit/ecare/it_mytiscali?visp=tiscali"
	billingHome    = `https://selfcare.tiscali.it/unit/ecare/it_billing_manager?type=billing_home`
	billingManager = `https://selfcare.tiscali.it/unit/ecare/it_billing_manager?visp=tiscali`
	invoiceManager = `https://selfcare.tiscali.it/unit/ecare/it_invoice_manager?visp=tiscali`
	invoiceFormat  = "02/01/2006"
)

var (
	loginErrMsg    = []string{`La password inserita non è corretta`, `Attenzione! Non sei ancora registrato`}
	confirmMessage = `Gentile Cliente, ti invitiamo a controllare e verificare la correttezza e validità dei tuoi dati e riferimenti personali`

	fullNameRegex = regexp.MustCompile(`fullname="(.+)"`)
	cnumberRegex  = regexp.MustCompile(`cnumber='(.+)'`)
	bankBIc       = "FEBIITM1"

	months = map[string]time.Month{
		"gennaio":   1,
		"febbraio":  2,
		"marzo":     3,
		"aprile":    4,
		"maggio":    5,
		"giugno":    6,
		"luglio":    7,
		"agosto":    8,
		"settembre": 9,
		"ottobre":   10,
		"novembre":  11,
		"dicembre":  12,
	}
)

func isLoginErrMsg(res string) string {
	for _, m := range loginErrMsg {
		if strings.Contains(res, m) {
			return m
		}
	}
	return ""
}

func Init() {
	RegisterFactory("tiscali.crawler", new(TiscaliFactory))
}

type TiscaliFactory struct{}

func (p *TiscaliFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "TiscaliStrategy")
	return &TiscaliStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type TiscaliStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	dataS       string
	UNITSESSION string
}

func (s *TiscaliStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *TiscaliStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *TiscaliStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *TiscaliStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "selfcare.tiscali.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}

	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("parse html for " + home + " : " + err.Error())
	}
	s.dataS = html[strings.Index(html, `data='`)+6:]
	s.dataS = s.dataS[:strings.Index(s.dataS, `';`)]

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "multipart/form-data; boundary=----WebKitFormBoundaryLx2YlPCDeZsNO35B",
		"host":         "selfcare.tiscali.it",
	}).SetFormData(map[string]string{
		"data":     s.dataS,
		"pref":     "/ecare/main.login.login",
		"UserID":   account.Username,
		"Password": *account.Password,
	}).Post(home)
	if res == nil {
		return nil, ConnectionErr("no response for login POST")
	}
	s.doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse login response body: " + err.Error())
	}
	msgTitle := s.doc.Find("div[id='msgTitle']")
	if msgTitle.Length() == 0 {
		html := res.String()
		return &html, nil
	}

	errMsg := isLoginErrMsg(msgTitle.Text())
	if err == nil && res.StatusCode() == 200 && errMsg != "" {
		return nil, LoginErr(errMsg)
	}
	return nil, nil
}

func (s *TiscaliStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *TiscaliStrategy) LoadLocations(account *model.Account) Exception {
	html, _ := s.doc.Html()
	if strings.Contains(html, confirmMessage) {
		return ParseErr(confirmMessage)
	}
	m, err := utils.MatchRegex(html, fullNameRegex)
	if err != nil {
		return ParseErr("match fullname regex")
	}
	fullName := m[0][1]
	m, err = utils.MatchRegex(html, cnumberRegex)
	if err != nil {
		return ParseErr("match cnumberRegex")
	}
	cnumber := m[0][1]
	l := &model.Location{
		Service:    fullName,
		Identifier: cnumber,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            nil,
			BankName:        nil,
			BankBIC:         nil,
			LegalEntityName: nil,
		},
	}
	account.AddLocation(l)

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(billingHome))
	s.Trace(res)
	if ex != nil {
		return ex
	}

	get, err := s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(billingManager)
	doc, ex := OkDocument(get, err)
	if ex != nil {
		return ex
	}
	codeDivSel := `td[class=codeDiv]`
	codeDivEl := doc.Find(codeDivSel)
	if codeDivEl.Length() == 0 {
		return ParseErr("not found:" + codeDivSel)
	}
	codeDiv := codeDivEl.Text()
	html = get.String()
	s.extractDataString(html)
	s.UNITSESSION = HeaderByNameAndValStartsWith(get.Header(), "Set-Cookie", "UNITSESSION")
	s.UNITSESSION = s.UNITSESSION[:strings.Index(s.UNITSESSION, ";")]

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"Cookie":       `INVLGN=""; ` + s.UNITSESSION,
		"Host":         "selfcare.tiscali.it",
		"Origin":       "https://selfcare.tiscali.it",
		"Referer":      "https://selfcare.tiscali.it/unit/ecare/it_billing_manager?visp=tiscali",
	}).SetFormData(map[string]string{
		"pref":       "ecare/billing_manager.list.billings",
		"data":       s.dataS,
		"BACode":     codeDiv,
		"dispatcher": "view",
	}).Post(billingManager))
	if ex != nil {
		return ex
	}
	ibanEl := doc.Find(`div[id=DirectDebitIBANTDL]`)
	if ibanEl.Length() == 0 {
		return ParseErr("not found iban element")
	}
	iban := ibanEl.Text()
	l.ProviderBranch.Iban = &iban
	bankNameEl := doc.Find(`div[id=DirectDebitBankNameTDL]`)
	if bankNameEl.Length() == 0 {
		return ParseErr("not found bankNameEl element")
	}
	bankName := bankNameEl.Text()
	l.ProviderBranch.BankName = &bankName
	l.ProviderBranch.BankBIC = &bankBIc

	return nil
}

func (s *TiscaliStrategy) extractDataString(html string) {
	s.dataS = html[strings.Index(html, `data='`)+6:]
	s.dataS = s.dataS[:strings.Index(s.dataS, `';`)]
}

func (s *TiscaliStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TiscaliStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(invoiceManager))
	if ex != nil {
		return ex
	}
	rows := doc.Find(`tr[class=invoice_tr]`)
	for i := 0; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		ref := cols.Eq(0).Text()
		ref = utils.RemoveUnicodeSpaces([]byte(ref))
		var pdfUri *string
		if !strings.Contains(cols.Eq(1).Text(), `Non disponibile`) {
			uri, ex := Attr(cols.Eq(1).Find("a"), "href")
			if ex != nil {
				return ex
			}
			uri = "https://selfcare.tiscali.it/" + uri
			pdfUri = &uri
		}
		amS := cols.Eq(2).Text()
		amS = utils.RemoveUnicodeSpaces([]byte(amS))
		amS = strings.Replace(amS, "€", "", -1)
		am, ex := AmountFromString(amS)
		if ex != nil {
			return ex
		}
		amDueS := cols.Eq(3).Text()
		amDueS = utils.RemoveUnicodeSpaces([]byte(amDueS))
		amDueS = strings.Replace(amDueS, "€", "", -1)
		amDue, ex := AmountFromString(amDueS)
		if ex != nil {
			return ex
		}
		currentLocation := time.Now().Location()
		var issueDate, dueDate time.Time
		for name, m := range months {
			if strings.Contains(ref, name) {
				yearS := ref[strings.Index(ref, name)+len(name):]
				year, err := strconv.Atoi(yearS)
				if err != nil {
					return ParseErr("parse date from ref " + ref)
				}
				issueDate = time.Date(year, m, 1, 0, 0, 0, 0, currentLocation)
				dueDate = issueDate.AddDate(0, 2, -1)
				break
			}
		}
		if issueDate.IsZero() || dueDate.IsZero() {
			return ParseErr("could not parse dates from ref " + ref)
		}
		i := &model.Invoice{
			Ref:       ref,
			PdfUri:    pdfUri,
			Amount:    am,
			AmountDue: amDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
	}

	return nil
}

func (s *TiscaliStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TiscaliStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *TiscaliStrategy) LoadsInternal() bool {
	return false
}

func (s *TiscaliStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *TiscaliStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
