package telka

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	root     = `https://bok.tvk.wroc.pl/`
	login    = `https://bok.tvk.wroc.pl/?`
	finances = `https://bok.tvk.wroc.pl/?m=finances`
	info     = `https://bok.tvk.wroc.pl/?m=info`

	siteFormat  = "2006/01/02"
	loginErrMsg = "Brak dostępu!"
)

var (
	bankname  = "Bank Polska Kasa Opieki SA"
	bankBIC   = "PKOPPLPW"
	legalName = "TVK Elzbieta Zjawiona"
)

func Init() {
	factory := new(TelkaFactory)
	RegisterFactory("telka_wrocław.crawler", factory)
}

type TelkaFactory struct{}

func (p *TelkaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "TelkaStrategy")
	return &TelkaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type TelkaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	session     string
	html        string
	doc         *goquery.Document
}

func (s *TelkaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *TelkaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *TelkaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *TelkaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Connection": "keep-alive",
	}).Get(root)
	if err != nil {
		return nil, ConnectionErr("GET " + root + ": " + err.Error())
	}
	s.session = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "LMSSESSIONID")
	s.session = s.session[:strings.Index(s.session, ";")]

	res, err = s.Client().R().SetHeaders(map[string]string{
		"Accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.session,
		"Referer":      root,
	}).SetFormData(map[string]string{
		"loginform[login]":  account.Username,
		"loginform[pwd]":    *account.Password,
		"loginform[submit]": "Autentificare",
	}).Post(login)
	if err != nil {
		return nil, ConnectionErr("POST " + login + ": " + err.Error())
	}
	s.html = res.String()
	return &s.html, nil
}

func (s *TelkaStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *TelkaStrategy) LoadLocations(account *model.Account) Exception {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(s.html))
	if err != nil {
		return ParseErr("parse html: " + err.Error())
	}
	infos := doc.Find(`table[class='light'] tr`)
	html, _ := infos.Html()
	s.Debug("info table:", html)
	if infos.Length() == 0 {
		return ParseErr("could not parse info table")
	}
	if infos.Length() != 8 {
		return ParseErr("could not parse info table")
	}

	identifier := strings.TrimSpace(infos.Eq(0).Text())
	address := strings.TrimSpace(infos.Eq(1).Text())
	address = utils.RemoveUnicodeSpaces([]byte(address))
	amountString := strings.TrimSpace(infos.Eq(4).Find("td").Eq(2).Text())
	split := strings.Split(amountString, " ")
	if len(split) < 1 {
		return ParseErr("invalid amount parsed" + amountString)
	}
	amount := strings.ReplaceAll(split[0], `.`, ``)
	amount = strings.ReplaceAll(amount, `,`, `.`)
	amt, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return ParseErr("parse amount " + amountString)
	}
	iban, _ := parseIban(infos.Eq(5))
	if iban == "" {
		iban, _ = parseIban(infos.Eq(6))
	}
	if iban == "" {
		return ParseErr("could not parse iban")
	}
	l := &model.Location{
		Service:    address,
		Identifier: identifier,
		Amount:     &amt,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &bankname,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalName,
		},
	}
	account.AddLocation(l)
	return nil
}

func parseIban(infos *goquery.Selection) (string, Exception) {
	tds := infos.Find("td")
	iban := ""
	if tds.Length() != 3 {
		html, _ := infos.Html()
		return "", ParseErr("invalid row format" + html)
	}
	iban = tds.Eq(2).Text()
	iban = utils.RemoveUnicodeSpaces([]byte(iban))
	return iban, nil
}

func (s *TelkaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TelkaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	var ex Exception
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Cookie":  s.session,
		"Referer": info,
	}).Get(finances))
	if ex != nil {
		return ex
	}

	refs := make(map[string]bool)

	rows := s.doc.Find(`form[name=invoices] table tr`)
	for i := 0; i < rows.Length(); i++ {
		h, _ := rows.Eq(i).Html()
		s.Debug(h)
		cols := rows.Eq(i).Find("td")
		if cols.Length() != 4 {
			continue
		}
		hrefEl := cols.Eq(3).Find("a")
		if hrefEl.Length() == 0 {
			continue
		}
		href, ok := hrefEl.Attr("href")
		if !ok {
			continue
		}
		pdfUri := "https://bok.tvk.wroc.pl/" + href
		ref, ex := Attr(cols.Eq(3).Find("img"), "onmouseover")
		if ex != nil {
			s.Error(ex)
			continue
		}
		prefix := `return overlib('`
		index := strings.Index(ref, prefix)
		if index == -1 {
			continue
		}
		ref = ref[index+len(prefix):]
		ref = ref[:strings.Index(ref, `'`)]
		prefix = `Nr `
		index = strings.Index(ref, prefix)
		if index == -1 {
			continue
		}
		ref = ref[index+len(prefix):]
		if refs[ref] {
			continue
		}
		refs[ref] = true

		issueString := cols.Eq(0).Text()
		split := strings.Split(issueString, " ")
		issueString = split[0]
		issueDate, err := time.Parse(siteFormat, issueString)
		if err != nil {
			return ParseErr("parse date " + issueString + ": " + err.Error())
		}
		dueDate := issueDate.AddDate(0, 0, 14)
		amount := cols.Eq(1).Text()
		split = strings.Split(amount, " ")
		amount = strings.ReplaceAll(split[0], `.`, ``)
		amount = strings.ReplaceAll(amount, `,`, `.`)
		amt, err := strconv.ParseFloat(amount, 64)
		if err != nil {
			return ParseErr("parse amount " + amount)
		}
		invoice := &model.Invoice{
			Ref:       ref,
			PdfUri:    &pdfUri,
			Amount:    -amt,
			AmountDue: 0,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(invoice)
	}

	sort.Slice(location.Invoices, func(i, j int) bool {
		return location.Invoices[i].IssueDate > location.Invoices[j].IssueDate
	})
	location.SetInvoiceDueFromAmount()
	return nil
}

func (s *TelkaStrategy) parseInvoice(location *model.Location, row *goquery.Selection, uri string, selectedOption string) Exception {
	return nil
}

func (s *TelkaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TelkaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		//"Content-type":"application/x-www-form-urlencoded",
	}).Get(pdfUri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	body := res.Body()
	return &model.PdfResponse{
		Content:   body,
		PdfStatus: model.OK.Name,
	}
}

func (s *TelkaStrategy) pdfBytes(uri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		//"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("pdf request: " + uri)
	}

	body := res.Body()
	eof := []byte(`%%EOF`)
	body = body[:bytes.Index(body, eof)+len(eof)]
	return body, nil
}

func (s *TelkaStrategy) LoadsInternal() bool {
	return false
}

func (s *TelkaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *TelkaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
