package telka

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "telka_wrocław.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"30831", "28378"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "30831", Password: "28378", Uri: "telka_wrocław.crawler",
			Identifier: "KŁYK Leszek (30831)",
			Ref:        "FV/DZIERZAWA-TV/0619/06/2021", PdfUri: "https://bok.tvk.wroc.pl/?m=finances&f=invoice&id=3466670",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
