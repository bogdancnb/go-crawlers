package enelitalia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	ROOT             = "https://www.enel.it"
	LOGIN            = "https://www.enel.it/it/login"
	SAMLSSO          = "https://accounts.enel.com/samlsso"
	SAMLAUTH         = "https://www.enel.it/bin/samlauth"
	AREA_CLIENTI     = "https://www.enel.it/it/area-clienti/"
	RESIDENTIAL_INFO = "https://www.enel.it/it/area-clienti/residenziale.getUserInfo.html?_="
	CARDS            = "https://www.enel.it/bin/areaclienti/auth/getCards?skipMerge=true&pageNumber=1&_="
	invoicesPage     = "https://www.enel.it/it/area-clienti/residenziale/bollette"
	invoicesJson     = "https://www.enel.it/bin/areaclienti/auth/getArchivioBollette?"
	pdfUri           = `https://www.enel.it/bin/areaclienti/auth/bollette?action=tipo-fattura&numeroFattura=%s&dataEmissione=%s&_=`

	loginErrMsg  = "TODO"
	siteFormat   = "2006-01-02"
	pdfReqFormat = "02/01/2006"
	jsonFormat   = "02.01.2006"

	pagamentiRequest = `{"cache":true,"canale":"W","cf":"%s","inputList":[{"businessPartner":"%s","contoContrattuale":["%s"]}],"numeroMassimoX2":100,"tipologia":""}`
	invoicesRetries  = 5
	requestError     = `"esito":"KO"`
)

func Init() {
	RegisterFactory("enel_it.crawler", new(EnelItaliaFactory))
}

type EnelItaliaFactory struct{}

func (p *EnelItaliaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EnelItaliaStrategy")
	return &EnelItaliaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type EnelItaliaStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document

	fiscalCode string
}

type cardsResponse struct {
	Data   cardsData      `json:"data"`
	Status responseStatus `json:"status"`
	Meta   responseMeta   `json:"meta"`
}

type cardsData struct {
	Cards   []*card
	Today   string          `json:"today"`
	Message responseMessage `json:"message"`
}

type card struct {
	CodiceFornitura             string `json:"codiceFornitura"`
	Commodity                   string `json:"commodity"`
	ContoContrattuale           string `json:"contoContrattuale"`
	Alias                       string `json:"alias"`
	IndFornitura                string `json:"indFornitura"`
	Stato                       string `json:"stato"`
	Pod                         string `json:"pod"`
	DataAttivazione             string `json:"dataAttivazione"`
	OffertaAttiva               string `json:"offertaAttiva"`
	IsBollettaWebActivated      string `json:"isBollettaWebActivated"`
	IsInfoEnelEnergiaActivated  string `json:"isInfoEnelEnergiaActivated"`
	For_met_pag                 string `json:"for_met_pag"`
	UsoFornitura                string `json:"usoFornitura"`
	IdProdotto                  string `json:"idProdotto"`
	IdListino                   string `json:"idListino"`
	OreFree                     bool   `json:"oreFree"`
	Subscription                bool   `json:"subscription"`
	Rvc                         bool   `json:"rvc"`
	IsOreFree                   bool   `json:"isOreFree"`
	DataInizioCalendario        string `json:"dataInizioCalendario"`
	DataFineCalendario          string `json:"dataFineCalendario"`
	IdAsset                     string `json:"idAsset"`
	OkSap                       bool   `json:"okSap"`
	IsSubscription              bool   `json:"isSubscription"`
	DataInizioFormaContrattuale string `json:"dataInizioFormaContrattuale"`
	IsRvc                       bool   `json:"isRvc"`
	BusinessPartner             string `json:"businessPartner"`
}

type responseMessage struct {
	RichText string `json:"richText"`
}

type responseStatus struct {
	Code        string `json:"code"`
	Result      string `json:"result"`
	Description string `json:"description"`
}

type responseMeta struct {
	Tid               string `json:"tid"`
	Sid               string `json:"sid"`
	Path              string `json:"path"`
	Method            string `json:"method"`
	DataAggiornamento string `json:"dataAggiornamento"`
}

type userInfo struct {
	EnelId          string `json:"enelId"`
	ProxyRemoteUser string `json:"Proxy-Remote-User"`
	FirstName       string `json:"firstName"`
	LastName        string `json:"lastName"`
	FiscalCode      string `json:"fiscalCode"`
	Email           string `json:"email"`
	MobileNumber    string `json:"mobileNumber"`
	PatchedPhone    bool   `json:"patchedPhone"`
	IsBusinessUser  bool   `json:"isBusinessUser"`
	IsAnonymous     bool   `json:"isAnonymous"`
}

type invoiceResponse struct {
	Status struct {
		Codice      string `json:"codice"`
		Descrizione string `json:"descrizione"`
		Esito       string `json:"esito"`
	} `json:"status"`
	Data invoiceData `json:"data"`
}

type invoiceData struct {
	DataAggiornamento string `json:"dataAggiornamento"`
	Results           []*struct {
		ContoContrattuale string         `json:"contoContrattuale"`
		Fatture           []*invoiceJson `json:"fatture"`
	} `json:"results"`
}

type invoiceJson struct {
	Tipodocumento                  string
	Numerodocumento                string
	Dataemissione                  string
	Numerorataidentificativounivno int
	Importofattura                 string
	Datascadenza                   string
	Importoresiduo                 string
	ImportocanoneTV                string
	ImportoresiduocanoneTV         string
	Datapagamento                  string
	Domiciliata                    string
	Esistenzadimostratopagamento   string
	Numeropianorata                string
	Statopagabilita                string
}

func (s *EnelItaliaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EnelItaliaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EnelItaliaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EnelItaliaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().Get(LOGIN))
	if ex != nil {
		return nil, ex
	}
	sessionDataKeyEl := doc.Find("input[name='sessionDataKey']")
	if sessionDataKeyEl.Length() == 0 {
		return nil, ParseErr("no sessionDataKey element found")
	}
	sessionDataKey, ex := Attr(sessionDataKeyEl.Eq(0), "value")
	if ex != nil {
		return nil, ex
	}

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"content-type": ContentTypeFormUrlEncodedHeader,
		"origin":       ROOT,
		"referer":      ROOT + "/",
	}).SetFormData(map[string]string{
		"username":       account.Username,
		"password":       *account.Password,
		"sessionDataKey": sessionDataKey,
		"tocommonauth":   "true",
	}).Post(SAMLSSO)
	if res == nil {
		return nil, ConnectionErr("no response for POST " + SAMLSSO)
	}
	if err != nil {
		return nil, ConnectionErr("POST " + SAMLSSO + ": " + err.Error())
	}
	doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parsing POST " + SAMLSSO + ": " + err.Error())
	}
	loginErrEl := doc.Find("div[id=loginError]")
	if loginErrEl.Length() > 0 {
		msg := strings.TrimSpace(loginErrEl.Eq(0).Text())
		return nil, LoginErr(msg)
	}
	disclamerEl := doc.Find("div[id=generatedConsents]")
	if disclamerEl.Length() > 0 {
		msg := strings.TrimSpace(disclamerEl.Eq(0).Text())
		return nil, ParseErr("disclaimer not accepted " + msg)
	}
	SAMLResponseEl := doc.Find("input[name='SAMLResponse']")
	if SAMLResponseEl.Length() == 0 {
		return nil, ParseErr("no SAMLResponse element found")
	}
	SAMLResponse, _ := Attr(SAMLResponseEl.Eq(0), "value")

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"content-type": ContentTypeFormUrlEncodedHeader,
		"origin":       "https://accounts.enel.com",
		"referer":      "https://accounts.enel.com/samlsso",
	}).SetFormData(map[string]string{
		"SAMLResponse": SAMLResponse,
	}).Post(SAMLAUTH)
	if res == nil {
		return nil, ConnectionErr("no response for POST " + SAMLAUTH)
	}
	//if res.StatusCode() != 302 {
	//	return nil, ConnectionErr("asdasdasd")
	//}

	//doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
	//	"accept":  AcceptHeader,
	//	"referer": "https://accounts.enel.com/",
	//}).Get(AREA_CLIENTI))
	//if ex != nil {
	//	return nil, ex
	//}
	//html, _ := doc.Html()

	return nil, nil
}

func (s *EnelItaliaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *EnelItaliaStrategy) LoadLocations(account *model.Account) Exception {
	res1, ex := OkString(s.Client().R().Get(RESIDENTIAL_INFO + utils.MillisString(time.Now())))
	s.Debug(res1)
	if ex != nil {
		return ConnectionErr("GET " + RESIDENTIAL_INFO + ": " + ex.Error())
	}
	var data struct {
		Data userInfo `json:"data"`
	}
	err := utils.FromJSON(&data, strings.NewReader(res1))
	if err != nil {
		return ParseErr("parsing user info response: " + err.Error())
	}
	s.fiscalCode = data.Data.FiscalCode

	res, ex := OkString(s.Client().R().Get(CARDS + utils.MillisString(time.Now())))
	s.Debug(res)
	if ex != nil {
		return ConnectionErr("GET " + CARDS + ": " + ex.Error())
	}
	var r cardsResponse
	err = utils.FromJSON(&r, strings.NewReader(res))
	if err != nil {
		return ParseErr("parsing cards response: " + err.Error())
	}

	for _, card := range r.Data.Cards {
		details := card.ContoContrattuale + "|" + card.BusinessPartner
		l := &model.Location{
			Service:    card.IndFornitura,
			Identifier: card.CodiceFornitura,
			Details:    &details,
		}
		account.AddLocation(l)
	}
	return nil
}

func (s *EnelItaliaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EnelItaliaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	details := strings.Split(*location.Details, "|")
	if len(details) != 2 {
		return ParseErr("invalid location details: " + *location.Details)
	}
	paymentReq := fmt.Sprintf(pagamentiRequest, s.fiscalCode, details[1], details[0])
	params := url.Values{}
	params.Add("pagamentiRequest", paymentReq)
	params.Add("emailUtente", account.Username)
	//params.Add("_", utils.MillisString(time.Now()))
	uri := invoicesJson + params.Encode()

	r := 0
	res := requestError
	var ex Exception
	for r < invoicesRetries && strings.Contains(res, requestError) {
		res, ex = OkString(s.Client().R().Get(uri))
		s.Debug(res)
		if ex != nil {
			return ConnectionErr("GET " + uri + ": " + ex.Error())
		}
		r++
	}
	if strings.Contains(res, requestError) {
		return ParseErr("could not obtain invoice list, got: " + res)
	}

	var invoices invoiceResponse
	err := utils.FromJSON(&invoices, strings.NewReader(res))
	if err != nil {
		return ParseErr("parsing invoices response: " + err.Error())
	}

	for _, result := range invoices.Data.Results {
		if result.ContoContrattuale == details[0] {
			for _, invoice := range result.Fatture {
				if invoice.Tipodocumento != "F" {
					s.Warn("unkown document type: ", invoice)
					continue
				}

				amt, err := strconv.ParseFloat(invoice.Importofattura, 64)
				if err != nil {
					return ParseErr("parsing amount  " + invoice.Importofattura)
				}
				amtDue, err := strconv.ParseFloat(invoice.Importoresiduo, 64)
				if err != nil {
					return ParseErr("parsing amount due " + invoice.Importoresiduo)
				}

				if amtDue == 0 && invoice.Datapagamento != "" {
					payDate, err := time.Parse(siteFormat, invoice.Datapagamento)
					if err != nil {
						return ParseErr("payment date " + invoice.Datapagamento)
					}
					p := &model.Payment{
						Ref:    invoice.Numerodocumento,
						Amount: amt,
						Date:   payDate.Format(utils.DBDateFormat),
					}
					location.AddPayment(p)
					continue
				}

				if amtDue > 0 {
					issueDate, err := time.Parse(siteFormat, invoice.Dataemissione)
					if err != nil {
						return ParseErr("issue date " + invoice.Dataemissione)
					}
					dueDate, err := time.Parse(siteFormat, invoice.Datascadenza)
					if err != nil {
						return ParseErr("due date " + invoice.Datascadenza)
					}
					issueDatePdf := issueDate.Format(pdfReqFormat)
					pdf := fmt.Sprintf(pdfUri, invoice.Numerodocumento, issueDatePdf)
					i := &model.Invoice{
						Ref:       invoice.Numerodocumento,
						PdfUri:    &pdf,
						Amount:    amt,
						AmountDue: amtDue,
						IssueDate: issueDate.Format(utils.DBDateFormat),
						DueDate:   dueDate.Format(utils.DBDateFormat),
					}
					location.AddInvoice(i)
				}
			}
		}
	}

	return nil
}

func (s *EnelItaliaStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EnelItaliaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EnelItaliaStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EnelItaliaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EnelItaliaStrategy) LoadsInternal() bool {
	return false
}

func (s *EnelItaliaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EnelItaliaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
