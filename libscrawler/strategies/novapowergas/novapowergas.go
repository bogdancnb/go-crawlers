package novapowergas

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	login     = "https://crm.novapg.ro/login"
	postLogin = "https://crmadmin.novapg.ro/webapi/account/postLogin"
	validate  = "https://crmadmin.novapg.ro/webapi/default/validate"
	bills     = "https://crmadmin.novapg.ro/webapi/default/bills"

	cui        = "18680651"
	dateFormat = "2006-01-02"

	pctConsum     = "http://crm.novapg.ro:144/punctConsum/index"
	facturi       = "http://crm.novapg.ro:144/partener/invoices"
	pdfRoot       = "http://crm.novapg.ro:144"
	siteFormat    = "02/01/2006"
	codClientRegS = `COD Client:\s*(\d+)\D+`
)

var (
	loginErrMsgs = []string{"Utilizatorul introdus nu exista!", "Parola introdusa nu este corecta"}
	ipBanMsg     = "Din cauza incercarilor multiple de autentificare, acest IP este blocat temporar."
	codClientReg = regexp.MustCompile(codClientRegS)
)

func Init() {
	f := new(NovaPowerGasFactory)
	RegisterFactory("nova_power_electricity.crawler", f)
}

type NovaPowerGasFactory struct{}

func (p *NovaPowerGasFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "NovaPowerGasStrategy")
	return &NovaPowerGasStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type NovaPowerGasStrategy struct {
	*logrus.Entry
	HttpStrategy

	existingLocsByService map[string]*model.Location
	existingInvoices      map[string]*model.Invoice
	token                 string
	loginRes              loginResponse
}

type loginResponse struct {
	Data   string `json:"data"`
	Status bool   `json:"status"`
}

func (s *NovaPowerGasStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *NovaPowerGasStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *NovaPowerGasStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *NovaPowerGasStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	resString, ex := OkString(s.Client().R().Get(login))
	s.Trace(resString)
	if ex != nil {
		return nil, ex
	}

	payload := fmt.Sprintf(`{"username":"%s","password":"%s"}`, account.Username, *account.Password)
	res, _ := s.Client().R().SetHeaders(map[string]string{
		"accept": "application/json, text/plain, */*",
	}).SetFormData(map[string]string{
		"LoginForm": payload,
	}).Post(postLogin)
	s.Debug("login response:", res.String())
	if res == nil {
		return nil, ConnectionErr("POST " + login + " no response")
	}
	err := utils.FromJSON(&s.loginRes, strings.NewReader(res.String()))
	if err != nil {
		return nil, ParseErr("parse login response " + res.String())
	}
	if !s.loginRes.Status {
		if s.loginRes.Data != "" {
			return nil, LoginErr(s.loginRes.Data)
		} else {
			return nil, ParseErr(res.String())
		}
	}
	s.token = "Bearer " + s.loginRes.Data
	return nil, nil
}

func (s *NovaPowerGasStrategy) CheckLogin(response *string) Exception {
	return nil
}

type validateRes struct {
	User struct {
		PartnerEntityId string
		RegisterNumber  string
		Localitate      string
		Partener        struct {
			Strada              string
			Numar               string
			Bloc                string
			Scara               string
			Apartament          string
			IdUserWeb           string
			BusinessPartnerCode string
		} `json:"partener"`
	} `json:"user"`
}

func (s *NovaPowerGasStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"authorization": s.token,
	}).Get(validate))
	s.Debug("validate response:", res)
	if ex != nil {
		return ex
	}
	var info validateRes
	er := utils.FromJSON(&info, strings.NewReader(res))
	if er != nil {
		return ParseErr("parse validate response " + er.Error())
	}
	p := info.User.Partener
	if p.BusinessPartnerCode == "" {
		return ParseErr("validate response=" + res)
	}
	address := fmt.Sprintf(`%s, str. %s, Nr. %s, bl. %s, sc. %s, ap. %s`,
		info.User.Localitate, p.Strada, p.Numar, p.Bloc, p.Scara, p.Apartament)
	l := &model.Location{
		Service:    address,
		Identifier: p.BusinessPartnerCode,
	}
	account.AddLocation(l)
	return nil
}

func (s *NovaPowerGasStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

type billJson struct {
	IDfg              string `json:"iDfg"`
	IdEntitate        string
	SerieNumar        string
	DataEmitere       string
	DataScadenta      string
	ValoareCuTVA      string
	Sold              string //".00"
	AdresaWebDocument string
}

type billsRes struct {
	Data struct {
		Totals struct {
			Lei struct {
				Sold float64
			}
		} `json:"totals"`
		Bills []*billJson `json:"bills"`
	} `json:"data"`
}

func (s *NovaPowerGasStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"authorization": s.token,
	}).Get(bills))
	s.Debug("bills=", res)
	if ex != nil {
		return ex
	}
	var billsR billsRes
	err := utils.FromJSON(&billsR, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse bills response :" + err.Error())
	}
	if billsR.Data.Totals.Lei.Sold > 0 {
		location.Amount = &billsR.Data.Totals.Lei.Sold
	}

	for _, bill := range billsR.Data.Bills {
		amt := 0.0
		if bill.ValoareCuTVA != ".00" {
			amt, ex = AmountFromString(bill.ValoareCuTVA)
			if ex != nil {
				return ex
			}
		}
		amtDue := 0.0
		if bill.Sold != ".00" {
			amtDue, ex = AmountFromString(bill.Sold)
			if ex != nil {
				return ex
			}
		}
		issue, err := time.Parse(dateFormat, bill.DataEmitere)
		if err != nil {
			return ParseErr("parse date " + bill.DataEmitere)
		}
		due, err := time.Parse(dateFormat, bill.DataScadenta)
		if err != nil {
			return ParseErr("parse date " + bill.DataScadenta)
		}
		barcode, ex := s.getBarcode(bill.IDfg, amt, amtDue, issue)
		if ex != nil {
			return ex
		}
		pdfUri := strings.Replace(bill.AdresaWebDocument, `\`, ``, -1)
		//pdfUri = pdfUri + "|" + bill.IdEntitate
		i := &model.Invoice{
			Ref:       bill.SerieNumar,
			PdfUri:    &pdfUri,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issue.Format(utils.DBDateFormat),
			DueDate:   due.Format(utils.DBDateFormat),
			BarCode:   &barcode,
		}
		location.AddInvoice(i)
	}
	return nil
}

func (s *NovaPowerGasStrategy) getBarcode(id string, amt float64, amtDue float64, issueDate time.Time) (string, Exception) {
	amountToUse := amt
	if amtDue > 0 {
		amountToUse = amtDue
	}
	if amountToUse < 0 {
		return "", ParseErr("Could not generate barcode for negative amount.")
	}
	amountString := decimal.NewFromFloat(amountToUse).Mul(decimal.NewFromInt(100)).String()
	amountString = fmt.Sprintf("%012s", amountString)

	issueString := issueDate.Format(utils.DBDateFormat)
	barcode := fmt.Sprintf(`%011s%s%s0018680651`, id, amountString, issueString)
	return barcode, nil
}

func (s *NovaPowerGasStrategy) parseInvoice(account *model.Account, cols *goquery.Selection, needsPdfParse bool) Exception {
	return nil
}

func (s *NovaPowerGasStrategy) parsePdf(account *model.Account, invoice *model.Invoice) Exception {
	pdfBytes, ex := s.pdfDownload(*invoice.PdfUri)
	if ex != nil {
		return ex
	}
	pdfTxt, err := PlainText(pdfBytes)
	if err != nil {
		return ParseErr("parse pdf text for " + *invoice.PdfUri + " : " + err.Error())
	}
	for _, location := range account.Locations {
		if strings.Contains(pdfTxt, location.Service) {
			if location.Identifier == "" {
				matches, err := utils.MatchRegex(pdfTxt, codClientReg)
				if err != nil {
					return ParseErr("matching client code from pdf text for " + invoice.Ref + " : " + err.Error())
				}
				codClient := matches[0][1]
				if codClient == "" {
					return ParseErr("matching empty client code from pdf text for " + invoice.Ref)
				}
				location.Identifier = codClient
			}
			location.AddInvoice(invoice)
			return nil
		}
	}
	return nil
}

func (s *NovaPowerGasStrategy) pdfDownload(pdfUri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(pdfUri)
	if err != nil {
		return nil, ConnectionErr("GET " + pdfUri + " : " + err.Error())
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *NovaPowerGasStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *NovaPowerGasStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *NovaPowerGasStrategy) Pdf(account *model.Account) *model.PdfResponse {
	uri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := s.pdfDownload(uri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *NovaPowerGasStrategy) LoadsInternal() bool {
	return false
}

func (s *NovaPowerGasStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	s.existingInvoices = make(map[string]*model.Invoice)
	for _, invoice := range existingInvoices {
		s.existingInvoices[invoice.Ref] = invoice
	}
}

func (s *NovaPowerGasStrategy) SetExistingLocations(existingLocations []*model.Location) {
	s.existingLocsByService = make(map[string]*model.Location)
	for _, location := range existingLocations {
		s.existingLocsByService[location.Service] = location
	}
}
