package novapowergas

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	tests := []struct {
		username string
		password string
		uri      string
	}{
		//{"Carmenvaradi@yahoo.com", "Microsoft12", "nova_power_electricity.crawler"},
		{"vasile.spatar@gmail.com", "18AUG59vs!", "nova_power_electricity.crawler"},
		//{"cipri.zare@gmail.com", "vfYVtY2nMKQwByD", "nova_power_electricity.crawler"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, test.uri); err != nil {
			t.Logf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "vasile.spatar@gmail.com", Password: "18AUG59vs!", Uri: "nova_power_electricity.crawler",
			Identifier: "3007230",
			Ref:        "NPG 102116070", PdfUri: "https://crmadmin.novapg.ro/facturi/F_439631_2021102.pdf",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
