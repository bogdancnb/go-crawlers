package play24

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	ACCESS_URL         = "https://24.play.pl/Play24/Welcome"
	NUMBER_LIST_URL    = "https://konto.play.pl/user-gui/myAccountPage/numberList.json?_="
	LOGIN_URL          = "https://logowanie.play.pl/opensso/logowanie"
	LOCATION_URL       = "https://konto.play.pl/user-gui/myAccountPage"
	TOKEN_URL          = "https://24.play.pl/Play24/CsrfJavaScriptServlet"
	BILLS_URL          = "https://24.play.pl/Play24/dwr/call/plaincall/templateRemoteService.sortData.dwr"
	SWITCH_CONTEXT_URL = "https://24.play.pl/Play24/dwr/call/plaincall/accountContextChangeService.changeAccountContext.dwr"
	UNPAID             = "Wszystkie Twoje faktury są opłacone"
	NO_PAYMENTS_MSG    = "Brak płatności dotyczących Twojego konta"

	siteFormat = "02.01.2006"
)

var (
	BANK_NAME         = "DNB Bank Polska SA"
	LEGAL_ENTITY_NAME = "P4 Sp. z o.o."
	BANK_BIC          = "BIGBPLPW"
)

func Init() {
	factory := new(Play24Factory)
	RegisterFactory("play.crawler", factory)
}

type Play24Factory struct{}

func (p *Play24Factory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "Play24Strategy")
	return &Play24Strategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type Play24Strategy struct {
	*logrus.Entry
	HttpStrategy

	nrs             numbersList
	csrfToken       string
	scriptSessionId string
}

func (s *Play24Strategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *Play24Strategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *Play24Strategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

type numbersList struct {
	NumberManagementListDto []numberManagement `json:"numberManagementListDto"`
}

type numberManagement struct {
	Msisdn string `json:"msisdn"`
	Active bool   `json:"active"`
}

func (s *Play24Strategy) Login(acc *model.Account) (*string, Exception) {
	doc, ex := OkDocument(s.Client().R().Get(ACCESS_URL))
	if ex != nil {
		return nil, ex
	}
	redirectUrl, ex := Attr(doc.Find("form"), "action")
	if ex != nil {
		return nil, ex
	}
	samlRequest, ex := Attr(doc.Find("input[name=SAMLRequest]"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetFormData(map[string]string{
		"SAMLRequest": samlRequest,
		"target":      "play24",
	}).Post(redirectUrl))
	if ex != nil {
		return nil, ex
	}
	sunQueryParamsString, ex := Attr(doc.Find("input[name=SunQueryParamsString]"), "value")
	if ex != nil {
		return nil, ex
	}
	encoded, ex := Attr(doc.Find("input[name=encoded]"), "value")
	if ex != nil {
		return nil, ex
	}
	goTo, ex := Attr(doc.Find("input[name=goto]"), "value")
	if ex != nil {
		return nil, ex
	}
	goToOnFail, ex := Attr(doc.Find("input[name=gotoOnFail]"), "value")
	if ex != nil {
		return nil, ex
	}
	gxCharset, ex := Attr(doc.Find("input[name=gx_charset]"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetFormData(map[string]string{
		"IDButton":             "Zaloguj",
		"IDToken1":             acc.Username,
		"IDToken2":             *acc.Password,
		"SunQueryParamsString": sunQueryParamsString,
		"encoded":              encoded,
		"goto":                 goTo,
		"gotoOnFail":           goToOnFail,
		"gx_charset":           gxCharset,
	}).Post(LOGIN_URL))
	if ex != nil {
		return nil, ex
	}
	nextUrlLogin, ex := Attr(doc.Find("form"), "action")
	if ex != nil {
		return nil, ex
	}
	if !strings.Contains(nextUrlLogin, "login") {
		return nil, LoginErr("invalid nextUrlLogin: " + nextUrlLogin)
	}
	samlResponse, ex := Attr(doc.Find("input[name=SAMLResponse]"), "value")
	if ex != nil {
		return nil, ex
	}
	target, ex := Attr(doc.Find("input[name=target]"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetFormData(map[string]string{
		"SAMLResponse": samlResponse,
		"target":       target,
	}).Post(nextUrlLogin))
	if ex != nil {
		return nil, ex
	}
	if ex := s.getCsrfToken(); ex != nil {
		return nil, ex
	}
	if ex := s.getScriptSessionId(); ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("could not parse login response html")
	}
	return &html, nil
}

func (s *Play24Strategy) getCsrfToken() Exception {
	doc, ex := OkString(s.Client().R().SetHeader("FETCH-CSRF-TOKEN", "1").Post(TOKEN_URL))
	if ex != nil {
		return ex
	}
	tokens := strings.Split(doc, "P24_CSRFTOKEN:")
	if len(tokens) < 2 {
		return ParseErr("tokens response: " + doc)
	}
	s.csrfToken = strings.Split(tokens[1], "\n")[0]
	if s.csrfToken == "" {
		return ParseErr("tokens response: " + doc)
	}
	return nil
}

func (s *Play24Strategy) getScriptSessionId() Exception {
	body := "callCount=1\n" +
		"c0-scriptName=__System\n" +
		"c0-methodName=generateId\n" +
		"c0-id=0\n" +
		"batchId=0\n" +
		"instanceId=0\n" +
		"page=%2FPlay24%2FWelcome\n" +
		"scriptSessionId=\n"
	doc, ex := OkDocument(s.Client().R().
		SetHeader("P24_CSRFTOKEN", s.csrfToken).
		SetHeader("Content-Type", "text/plain").
		SetBody([]byte(body)).
		Post("https://24.play.pl/Play24/dwr/call/plaincall/__System.generateId.dwr"))
	if ex != nil {
		return ex
	}
	text := doc.Find("body").Text()
	parts := strings.Split(text, "\"")
	if len(parts) < 6 {
		return ParseErr("getScriptSessionId: parsing response: " + text)
	}
	s.scriptSessionId = parts[5]
	return nil
}

func (s *Play24Strategy) CheckLogin(response *string) Exception {
	if !strings.Contains(*response, "Wyloguj") {
		return LoginErr(`"Wyloguj" not found in page`)
	}
	return nil
}

func (s *Play24Strategy) LoadLocations(account *model.Account) Exception {
	if ex := s.parseNumbers(); ex != nil {
		return ex
	}
	if len(s.nrs.NumberManagementListDto) == 0 {
		s.Warn("empty numbers list")
		return nil
	}
	for _, nr := range s.nrs.NumberManagementListDto {
		if nr.Active {
			if strings.TrimSpace(nr.Msisdn) == "" {
				s.Warn("LoadLocations: got empty phone number: ", nr)
				continue
			}
			location := &model.Location{
				Service:    nr.Msisdn,
				Identifier: nr.Msisdn,
			}
			account.AddLocation(location)
			if ex := s.switchContext(location.Identifier); ex != nil {
				return ex
			}
			if ex := s.loadInvoicesInternal(account, location); ex != nil {
				return ex
			}
			if isTopup := location.AccountHelpers.(bool); isTopup {
				continue
			}
			if ex := s.loadPaymentsInternal(account, location); ex != nil {
				return ex
			}
		}
	}
	i := 0
	for _, location := range account.Locations {
		if isTopup := location.AccountHelpers.(bool); !isTopup {
			account.Locations[i] = location
			i++
		}
	}
	account.Locations = account.Locations[:i]
	return nil
}

func (s *Play24Strategy) parseNumbers() Exception {
	uri := fmt.Sprintf(`%s%d`, NUMBER_LIST_URL, utils.Millis(time.Now()))
	resp, ex := OkString(s.Client().R().Get(uri))
	if ex != nil {
		return ex
	}
	err := json.Unmarshal([]byte(resp), &s.nrs)
	if err != nil {
		return ParseErr("could not parse nrs list")
	}
	return nil
}

func (s *Play24Strategy) switchContext(identifier string) Exception {
	param := "callCount=1\n" +
		"nextReverseAjaxIndex=0\n" +
		"c0-scriptName=accountContextChangeService\n" +
		"c0-methodName=changeAccountContext\n" +
		"c0-id=0\n" +
		"c0-param0=string:" + identifier + "\n" +
		"c0-param1=string:ADMIN_CONTEXT\n" +
		"batchId=5\n" +
		"instanceId=0\n" +
		"page=%2FPlay24%2FInvoices\n" +
		"scriptSessionId=" + s.scriptSessionId + "/5E4GTFm-5zir06px5\n"
	_, ex := Ok(s.Client().R().
		SetHeader("P24_CSRFTOKEN", s.csrfToken).
		SetHeader("Content-Type", "text/plain").
		SetHeader("Cookie", "DWRSESSIONID="+s.scriptSessionId).
		SetBody([]byte(param)).
		Post(BILLS_URL))
	if ex != nil {
		return ex
	}
	return nil
}

func (s *Play24Strategy) loadInvoicesInternal(account *model.Account, location *model.Location) Exception {
	s.Debug("load invoices for ", location.Identifier)
	location.AccountHelpers = false
	doc, ex := OkDocument(s.Client().R().Get("https://24.play.pl/Play24/Invoices"))
	if ex != nil {
		return ex
	}
	if ok := s.isTopuplocation(doc); ok {
		location.AccountHelpers = true
		return nil
	}
	if ex := s.parseAccountNr(location, doc); ex != nil {
		return ex
	}

	var exception Exception
	unpaidInvoices, ex := s.requestData("INVOICES_UNPAID", "Invoices")
	if ex != nil {
		return ex
	}
	if !strings.Contains(unpaidInvoices, UNPAID) {
		doc, err := goquery.NewDocumentFromReader(strings.NewReader(unpaidInvoices))
		if err != nil {
			return ParseErr("parsing unpaid invoices: " + err.Error())
		}
		invoiceList := doc.Find("tr[id^=invoice-]")
		invoiceList.EachWithBreak(func(i int, selection *goquery.Selection) bool {
			if ex := s.parseInvoice(location, selection, false); ex != nil {
				exception = ex
				return false
			}
			return true
		})
	}
	paidInvoices, ex := s.requestData("INVOICES_PAID", "Invoices")
	if ex != nil {
		return ex
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(paidInvoices))
	if err != nil {
		return ParseErr("parsing paid invoices: " + err.Error())
	}
	invoiceList := doc.Find("tr[id^=invoice-]")
	invoiceList.EachWithBreak(func(i int, selection *goquery.Selection) bool {
		if ex := s.parseInvoice(location, selection, true); ex != nil {
			exception = ex
			return false
		}
		return true
	})

	return exception
}

func (s *Play24Strategy) isTopuplocation(doc *goquery.Document) bool {
	sel := doc.Find("a[href='/Play24/Topup']")
	return len(sel.Nodes) > 0
}

func (s *Play24Strategy) parseInvoice(location *model.Location, node *goquery.Selection, paid bool) Exception {
	details := node.Find("td")
	issueDateString := strings.TrimSpace(details.Eq(0).Text())
	issueDate, err := time.Parse(siteFormat, issueDateString)
	if err != nil {
		return &ParseException{Msg: "parse date: " + issueDateString}
	}
	dueDateString := strings.TrimSpace(details.Eq(1).Text())
	dueDate, err := time.Parse(siteFormat, dueDateString)
	if err != nil {
		return &ParseException{Msg: "parse date: " + dueDateString}
	}
	amountString := strings.TrimSpace(details.Eq(2).Text())
	amountString = strings.Split(amountString, " ")[0]
	amountString = strings.ReplaceAll(amountString, ",", ".")
	amount, err := strconv.ParseFloat(amountString, 64)
	if err != nil {
		return &ParseException{Msg: "parsing amount " + amountString}
	}
	amountDue := 0.0
	if !paid {
		amountDue = amount
	}
	ref, ok := details.Eq(3).Find("a").Attr("data-docname")
	if !ok || ref == "" {
		s.Warn("empty ref: ", node.Text())
		return nil
	}

	invoice := &model.Invoice{
		Ref:       ref,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *Play24Strategy) requestData(param string, uri string) (string, Exception) {
	params := "callCount=1\n" +
		"nextReverseAjaxIndex=0\n" +
		"c0-scriptName=templateRemoteService\n" +
		"c0-methodName=sortData\n" +
		"c0-id=0\n" +
		"c0-param0=string:" + param + "\n" +
		"c0-param1=string:trueDown\n" +
		"batchId=3\n" +
		"instanceId=0\n" +
		"page=%2FPlay24%2F" + uri + "Invoices\n" +
		"scriptSessionId=" + s.scriptSessionId + "/lCskOFm-Ik1EjVuam\n"
	resp, ex := OkString(s.Client().R().
		SetHeader("P24_CSRFTOKEN", s.csrfToken).
		SetHeader("Content-Type", "text/plain").
		SetHeader("Cookie", "DWRSESSIONID="+s.scriptSessionId).
		SetBody([]byte(params)).
		Post(BILLS_URL))
	if ex != nil {
		return "", ex
	}
	parts := strings.Split(resp, "view:\"")
	if len(parts) < 2 {
		return "", ParseErr("requestData: pasring response for " + param + ", " + uri)
	}
	result := strings.Split(parts[1], "\"}))")[0]
	//result = html.UnescapeString(result)
	result = regexp.MustCompile(`\\r\\n`).ReplaceAllString(result, "")
	//result = regexp.MustCompile(`\\"`).ReplaceAllString(result, "\"")
	result = regexp.MustCompile(`\\`).ReplaceAllString(result, "")
	return WrapHtmlTableBody(result), nil
}

func (s *Play24Strategy) parseAccountNr(location *model.Location, doc *goquery.Document) Exception {
	errMsg := "parseAccountNr: could not finf acc nr element "
	sel := doc.Find("li:containsOwn('Numer Rachunku Bankowego')")
	if len(sel.Nodes) == 0 {
		s.Error(errMsg, doc.Text())
		return ParseErr(errMsg)
	}
	parts := strings.Split(sel.Text(), ":")
	if len(parts) < 2 {
		return ParseErr(errMsg)
	}
	accNr := utils.RemoveSpaces(parts[1])
	if accNr == "" {
		return ParseErr(errMsg)
	}
	location.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNr,
		BankName:        &BANK_NAME,
		BankBIC:         &BANK_BIC,
		LegalEntityName: &LEGAL_ENTITY_NAME,
	}
	return nil
}

func (s *Play24Strategy) loadPaymentsInternal(account *model.Account, location *model.Location) Exception {
	s.Debug("load payments for ", location.Identifier)
	payments, ex := s.requestData("PAYMENTS", "Payments")
	if ex != nil {
		return ex
	}
	if strings.Contains(payments, NO_PAYMENTS_MSG) {
		s.Debug("no payments for ", location.Identifier)
		return nil
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(payments))
	if err != nil {
		return ParseErr("loadPaymentsInternal:  parsing: " + payments)
	}
	paymentsList := doc.Find("tr")
	paymentsList.Each(func(i int, selection *goquery.Selection) {
		if ex := s.parsePayment(location, selection); ex != nil {
			html, _ := selection.Html()
			s.Warn("error parsing payment from: ", html)
		}
	})
	return nil
}

func (s *Play24Strategy) parsePayment(location *model.Location, selection *goquery.Selection) Exception {
	details := selection.Find("td")
	dateString := strings.TrimSpace(details.Eq(2).Text())
	date, err := time.Parse(siteFormat, dateString)
	if err != nil {
		return &ParseException{Msg: "parse date: " + dateString}
	}
	pDetails := strings.TrimSpace(details.Eq(0).Text())
	amountString := strings.TrimSpace(details.Eq(3).Text())
	amountString = strings.Split(amountString, " ")[0]
	amountString = strings.ReplaceAll(amountString, ",", ".")
	amount, err := strconv.ParseFloat(amountString, 64)
	if err != nil {
		return &ParseException{Msg: "parsing amount " + amountString}
	}
	dbDate := date.Format(utils.DBDateFormat)

	location.AddPayment(&model.Payment{
		Ref:     dbDate + amountString,
		Amount:  amount,
		Date:    dbDate,
		Details: &pDetails,
	})
	return nil
}

func (s *Play24Strategy) LoadAmount(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *Play24Strategy) LoadInvoices(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *Play24Strategy) LoadPayments(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *Play24Strategy) Pdf(a *model.Account) *model.PdfResponse {
	return nil
}

func (s *Play24Strategy) LoadsInternal() bool {
	return true
}

func (s *Play24Strategy) Close(a *model.Account) {
	s.HttpStrategy.Close(a)
}

func (s *Play24Strategy) SendConnectionError() {
}

func (s *Play24Strategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *Play24Strategy) SetExistingLocations(existingLocations []*model.Location) {
}
