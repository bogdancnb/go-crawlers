package vivigas

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	home      = "https://pw.vivigas.it/public/vivigas/"
	prontoweb = "https://pw.vivigas.it/public/prontoweb-web-services"

	loginErrMsg = "Username / Password errati"
	siteFormat  = "2006-01-02"
)

func Init() {
	RegisterFactory("vivigas.crawler", new(VivigasFactory))
}

type VivigasFactory struct{}

func (p *VivigasFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "VivigasStrategy")
	return &VivigasStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type VivigasStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc        *goquery.Document
	JSESSIONID string
}

func (s *VivigasStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *VivigasStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *VivigasStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *VivigasStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "pw.vivigas.it",
	}).Get(home)
	if res == nil {
		return nil, ConnectionErr("no response GET home")
	}
	if err != nil {
		return nil, ConnectionErr("GET home: " + err.Error())
	}
	s.JSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "JSESSIONID")
	s.JSESSIONID = s.JSESSIONID[:strings.Index(s.JSESSIONID, ";")]
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse home html:" + err.Error())
	}
	identifier, ex := Attr(doc.Find("input[name='identifier']"), "value")
	if ex != nil {
		return nil, ex
	}
	loginSessione, ex := Attr(doc.Find("input[name='loginSessione']"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "netaweb.egea.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://netaweb.egea.it",
		"referer":      "https://netaweb.egea.it/public/egea",
	}).SetFormData(map[string]string{
		"operation":     "login",
		"identifier":    identifier,
		"multi-site":    "vivigas",
		"i18n-locale":   "",
		"loginSessione": loginSessione,
		"username":      account.Username,
		"password":      *account.Password,
	}).Post(prontoweb))
	if ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("parse login post html: " + err.Error())
	}
	return &html, nil
}

func (s *VivigasStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return ParseErr("could not obtain information")
}

func (s *VivigasStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *VivigasStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *VivigasStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *VivigasStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *VivigasStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *VivigasStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *VivigasStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *VivigasStrategy) LoadsInternal() bool {
	return false
}

func (s *VivigasStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *VivigasStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
