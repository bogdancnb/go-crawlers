package premiummobile

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "premium_mobile.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"radomiak@gmail.com", "radomiakpremium"},
		{"574492833", "Mojehaslo7"},
		//{"awilkonka@op.pl", "Mojehaslo7"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "574492833", Password: "Mojehaslo7", Uri: "premium_mobile.crawler",
			Identifier: "574492833",
			Ref:        "FV/084532/21/03/3/248816", PdfUri: "FV/084532/21/03/3/248816",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
