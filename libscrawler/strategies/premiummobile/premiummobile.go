package premiummobile

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"sort"
	"strings"
	"time"
)

const (
	root                      = `https://klient.premiummobile.pl/`
	login                     = `https://klient.premiummobile.pl/Account/LogIn`
	changeCustomerDataRequest = `https://klient.premiummobile.pl/Request/ChangeCustomerDataRequest`
	pdf                       = `https://klient.premiummobile.pl/Facture/WizardDocumentsPartial`

	//v2
	login2             = "https://klient.premiummobile.pl/login"
	authLogin          = "https://klient.premiummobile.pl/api/auth/login"
	getMsisdnsList     = "https://klient.premiummobile.pl/api/services/getMsisdnsList"
	getCustomerBalance = "https://klient.premiummobile.pl/api/payments/getCustomerBalance"
	getInvoices        = "https://klient.premiummobile.pl/api/payments/getInvoices"
	getPayments        = "https://klient.premiummobile.pl/api/payments/getPayments"
	getInvoiceDocument = "https://klient.premiummobile.pl/api/payments/getInvoiceDocument"

	dateF = "2006-01-02"

	loginErr       = `Wprowadzono niepoprawną nazwę użytkownika bądź hasło`
	invoiceRowsSel = `tr[id^=Invoice-control_DXDataRow]`
)

var (
	loginControlReg = regexp.MustCompile(`ASPx\.createControl\(MVCxClientPageControl,'Login-Control','',{'stateObject':{'tabsInfo':'(.+)'},'emptyHeight'`)

	bankname  = "ING Bank Śląski SA"
	bankBIC   = "INGBPLPW"
	legalName = "Premium Mobile Sp. z o.o."
)

func Init() {
	RegisterFactory("premium_mobile.crawler", new(PremiumMobileFactory))
}

type PremiumMobileFactory struct{}

func (p *PremiumMobileFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "PremiumMobileStrategy")
	return &PremiumMobileStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type loginResponse struct {
	Email   string `json:"email"`
	Name    string `json:"name"`
	Token   string `json:"token"`
	Message string `json:"message"`
}
type PremiumMobileStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	unpaid      *goquery.Selection
	paid        *goquery.Selection
	payments    *goquery.Selection
	loginRes    loginResponse
}

func (s *PremiumMobileStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}
func (s *PremiumMobileStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PremiumMobileStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *PremiumMobileStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78_2,
	})

	_, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept": AcceptHeader,
	}).Get(login2))
	if ex != nil {
		return nil, ex
	}

	payload := fmt.Sprintf(`{"username":"%s","password":"%s"}`, account.Username, *account.Password)
	loginPost, _ := s.Client().R().SetHeaders(map[string]string{
		"accept":       "application/json, text/plain, */*",
		"content-type": "application/json",
		"referer":      "https://klient.premiummobile.pl/login",
	}).SetBody(payload).Post(authLogin)
	s.Debug("login response=", loginPost.String())
	err := utils.FromJSON(&s.loginRes, strings.NewReader(loginPost.String()))
	if err != nil {
		return nil, ParseErr("parse login response: " + err.Error())
	}
	if loginPost.StatusCode() == 400 {
		if s.loginRes.Message != "" {
			return nil, LoginErr(s.loginRes.Message)
		} else {
			return nil, ParseErr(s.loginRes.Message)
		}
	} else if loginPost.StatusCode() == 200 && s.loginRes.Token != "" {
		return nil, nil
	} else {
		return nil, ConnectionErr("login response status " + loginPost.Status() + " body:" + loginPost.String())
	}
}

func (s *PremiumMobileStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *PremiumMobileStrategy) LoadLocations(account *model.Account) Exception {
	numbers, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"authorization": "Bearer " + s.loginRes.Token,
	}).Get(getMsisdnsList))
	if ex != nil {
		return ex
	}
	s.Debug("numbers response=", numbers)
	var nrs []struct {
		IsMainNumber bool   `json:"isMainNumber"`
		Msisdn       string `json:"msisdn"`
	}
	err := utils.FromJSON(&nrs, strings.NewReader(numbers))
	if err != nil {
		return ParseErr("parse numbers response:" + err.Error())
	}
	var loc *model.Location
	for _, nr := range nrs {
		if nr.IsMainNumber {
			loc = &model.Location{
				Service:    nr.Msisdn,
				Identifier: nr.Msisdn,
			}
			account.AddLocation(loc)
			break
		}
	}
	if loc == nil {
		return ParseErr("no main number found")
	}

	balance, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"authorization": "Bearer " + s.loginRes.Token,
	}).Get(getCustomerBalance))
	if ex != nil {
		return ex
	}
	s.Debug("balance response=", balance)
	var b struct {
		Balance                     string `json:"balance"`
		IndividualBankAccountNumber string `json:"individualBankAccountNumber"`
	}
	err = utils.FromJSON(&b, strings.NewReader(balance))
	if err != nil {
		return ParseErr("parse balance response:" + err.Error())
	}
	if b.IndividualBankAccountNumber == "" {
		return ParseErr("could not parse iban from " + balance)
	}
	loc.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &b.IndividualBankAccountNumber,
		BankName:        &bankname,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}

	if ex := s.LoadInvoicesInternal(account, loc); ex != nil {
		return ex
	}
	if ex := s.LoadPaymentsInternal(account, loc); ex != nil {
		return ex
	}
	return nil
}

func (s *PremiumMobileStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PremiumMobileStrategy) LoadInvoices(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *PremiumMobileStrategy) LoadInvoicesInternal(account *model.Account, location *model.Location) Exception {
	endDate := time.Now()
	starDate := endDate.AddDate(0, -6, 0)
	datePayload := fmt.Sprintf(`{"startDate":"%s","endDate":"%s"}`, starDate.Format(dateF), endDate.Format(dateF))
	bills, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"content-type":  "application/json",
		"authorization": "Bearer " + s.loginRes.Token,
	}).SetBody(datePayload).Post(getInvoices))
	if ex != nil {
		return ex
	}
	s.Debug("bills response=", bills)
	var invs []struct {
		Amount                  float64 //`json:"amount"`
		InvoiceDate             string  //`json:"invoiceDate"`
		InvoiceNumber           string  //`json:"invoiceNumber"`
		PaymentDate             string  //`json:"paymentDate"`
		PaymentDeadlineExceeded bool    //`json:"paymentDeadlineExceeded"`
		Status                  string  //`json:"status"`
	}
	err := utils.FromJSON(&invs, strings.NewReader(bills))
	if err != nil {
		return ParseErr("parse bills response:" + err.Error())
	}
	for _, inv := range invs {
		issue, err := time.Parse(dateF, inv.InvoiceDate)
		if err != nil {
			return ParseErr("parse date " + inv.InvoiceDate)
		}
		dueDate, err := time.Parse(dateF, inv.PaymentDate)
		if err != nil {
			return ParseErr("parse date " + inv.InvoiceDate)
		}
		i := &model.Invoice{
			Ref:       inv.InvoiceNumber,
			Amount:    inv.Amount,
			AmountDue: 0,
			IssueDate: issue.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
	}

	sort.Slice(location.Invoices, func(i, j int) bool {
		return location.Invoices[i].IssueDate > location.Invoices[j].IssueDate
	})
	location.SetInvoiceDueFromAmount()
	return nil
}

func (s *PremiumMobileStrategy) LoadPaymentsInternal(account *model.Account, loc *model.Location) Exception {
	endDate := time.Now()
	starDate := endDate.AddDate(0, -6, 0)
	datePayload := fmt.Sprintf(`{"startDate":"%s","endDate":"%s"}`, starDate.Format(dateF), endDate.Format(dateF))
	payments, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"content-type":  "application/json",
		"authorization": "Bearer " + s.loginRes.Token,
	}).SetBody(datePayload).Post(getPayments))
	if ex != nil {
		return ex
	}
	s.Debug("payments response=", payments)
	var ps []struct {
		Amount      float64
		PaymentDate string
		Title       string
	}
	err := utils.FromJSON(&ps, strings.NewReader(payments))
	if err != nil {
		return ParseErr("parse payments response:" + err.Error())
	}
	for _, p := range ps {
		date, err := time.Parse(dateF, p.PaymentDate)
		if err != nil {
			return ParseErr("parse date" + p.PaymentDate)
		}
		accP := &model.Payment{
			Ref:    p.Title + "_" + p.PaymentDate + "_" + fmt.Sprintf(`%f`, p.Amount),
			Amount: p.Amount,
			Date:   date.Format(utils.DBDateFormat),
		}
		loc.AddPayment(accP)
	}
	return nil
}

func (s *PremiumMobileStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PremiumMobileStrategy) Pdf(account *model.Account) *model.PdfResponse {
	ref := account.Locations[0].Invoices[0].Ref
	pdfBytes, err := s.pdfDownload(ref)
	if err != nil {
		return model.PdfErrorResponse("ref "+ref+": "+err.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *PremiumMobileStrategy) pdfDownload(ref string) ([]byte, error) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"content-type":  "text/plain; charset=UTF-8",
		"authorization": "Bearer " + s.loginRes.Token,
	}).SetBody(ref).Post(getInvoiceDocument)
	if err != nil {
		return nil, err
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *PremiumMobileStrategy) LoadsInternal() bool {
	return true
}

func (s *PremiumMobileStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *PremiumMobileStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
