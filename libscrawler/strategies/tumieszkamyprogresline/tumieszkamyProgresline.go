package tumieszkamyprogresline

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"math"
	"strings"
	"time"
)

const (
	login     = "https://www.strefaklienta24.pl/progresline/content/InetObsKontr/login"
	loginPost = `https://www.strefaklienta24.pl/progresline/iok/Zaloguj`
	finances  = `https://www.strefaklienta24.pl/progresline/iokRozr/DajListeRozrachFin`
	history   = `https://www.strefaklienta24.pl/progresline/iokRozr/DajDrzewoFinHistoria?Rozr=%s&DataOd=%s&DataDo=%s`
	pdfBase   = `https://www.strefaklienta24.pl/progresline/iokRozr/ZalDokFin?Ident=`

	statusSuccess = `"status" : "success"`
)

var (
	bankName  = `ING Bank Śląski SA`
	bankBIC   = `INGBPLPW`
	legalName = `Tumieszkamy Progresline`
)

func Init() {
	factory := new(TumieszkamyProgreslineFactory)
	RegisterFactory("tumieszkamy_progresline.crawler", factory)
}

type TumieszkamyProgreslineFactory struct{}

func (p *TumieszkamyProgreslineFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "TumieszkamyProgreslineStrategy")
	return &TumieszkamyProgreslineStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type TumieszkamyProgreslineStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations  map[string]*model.Location
	exInvoices   map[string]*model.Invoice
	doc          *goquery.Document
	financesResp financesRes
}

func (s *TumieszkamyProgreslineStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *TumieszkamyProgreslineStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
	}).Get(login))
	if ex != nil {
		return nil, ex
	}

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":       "application/json",
		"content-type": "application/x-www-form-urlencoded;charset=UTF-8",
		"referer":      login,
	}).SetFormData(map[string]string{
		"Ident": account.Username,
		"Haslo": *account.Password,
	}).Post(loginPost))
	if ex != nil {
		return nil, ex
	}
	s.Debug("login=", res)
	if strings.Contains(res, `"status" : "fail"`) {
		var loginErrRes = struct {
			Status string
			Data   struct {
				Message string
			}
		}{}
		err := utils.FromJSON(&loginErrRes, strings.NewReader(res))
		if err != nil {
			return nil, ParseErr("parse login response")
		}
		return nil, LoginErr(loginErrRes.Data.Message)
	} else if !strings.Contains(res, statusSuccess) {
		return nil, ParseErr("invalid login response")
	}
	//var loginRes struct{
	//	Status string
	//	Data   interface{} //
	//}
	return nil, nil
}

func (s *TumieszkamyProgreslineStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept": "application/json",
	}).Get(finances))
	s.Debug("finances=", res)
	if ex != nil {
		return ex
	}
	if !strings.Contains(res, statusSuccess) {
		return ParseErr("Error parsing finances response")
	}
	err := utils.FromJSON(&s.financesResp, strings.NewReader(res))
	if err != nil {
		return ParseErr("Error parsing finances response : " + err.Error())
	}
	for _, settlement := range s.financesResp.Data.Rozrachunki {
		identifier := fmt.Sprintf(`%d`, settlement.Ident)
		l := &model.Location{
			Service:    settlement.Opis,
			Identifier: identifier,
			Amount:     &settlement.DoZaplaty,
		}
		account.AddLocation(l)
		now := time.Now()
		startDate := now.AddDate(-1, 0, 0).Format(utils.Formats[`yyyy-MM-dd`])
		endDate := now.AddDate(0, 1, 0).Format(utils.Formats[`yyyy-MM-dd`])
		historyUrl := fmt.Sprintf(history, l.Identifier, startDate, endDate)

		res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
			"accept": "application/json",
		}).Get(historyUrl))
		s.Debug("history=", res)
		if ex != nil {
			return ex
		}
		if !strings.Contains(res, statusSuccess) {
			return ParseErr("Error parsing bill history response")
		}
		var historyResp historyRes
		err := utils.FromJSON(&historyResp, strings.NewReader(res))
		if err != nil {
			return ParseErr("Error parsing bill history response: " + err.Error())
		}
		for _, f := range historyResp.Data.Finanse {
			for _, p := range f.Pozycje {
				for _, item := range p.Pozycje {
					dokument := item.Dokument
					if dokument == nil {
						return ParseErr("Finance item with no document present")
					}
					if dokument.Numer == "" {
						continue
					}

					if dokument.Rodzaj == "RachSprz" || dokument.Rodzaj == "NotaSprz" || "DokOds" == dokument.Rodzaj {
						if ex := s.parseInvoice(dokument, l); ex != nil {
							return ex
						}
					} else if dokument.Rodzaj == "DokBank" {
						if ex := s.parsePayment(dokument, l); ex != nil {
							return ex
						}
					} else {
						return ParseErr("Unknown finance document type: " + dokument.Rodzaj)
					}
				}
			}
		}

	}
	return nil
}

func (s *TumieszkamyProgreslineStrategy) parseInvoice(dokument *dokumentJson, l *model.Location) Exception {
	ref := dokument.Numer
	var pdfUrl *string
	if len(dokument.SklejoneDok) > 0 && dokument.SklejoneDok[0].Ident != "" {
		uri := pdfBase + dokument.SklejoneDok[0].Ident
		pdfUrl = &uri
	}
	amount := math.Abs(dokument.Kwota)
	amountDue := math.Abs(dokument.KwotaDoZapl)
	issueDate, err := time.Parse(time.RFC3339, dokument.DataDok)
	if err != nil {
		return ParseErr("Error parsing date " + dokument.DataDok)
	}
	dueDate, err := time.Parse(time.RFC3339, dokument.TermPl)
	if err != nil {
		return ParseErr("Error parsing date " + dokument.TermPl)
	}
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUrl,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(invoice)
	if l.ProviderBranch == nil && dokument.RachunekB != "" {
		iban := utils.RemoveUnicodeSpaces([]byte(dokument.RachunekB))
		l.ProviderBranch = &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalName,
		}
	}
	return nil
}

func (s *TumieszkamyProgreslineStrategy) parsePayment(dokument *dokumentJson, l *model.Location) Exception {
	ref := dokument.Numer
	amount := math.Abs(dokument.Kwota)
	issueDate, err := time.Parse(time.RFC3339, dokument.DataDok)
	if err != nil {
		return ParseErr("Error parsing date " + dokument.DataDok)
	}
	ap := &model.Payment{
		Ref:    ref,
		Amount: amount,
		Date:   issueDate.Format(utils.DBDateFormat),
	}
	l.AddPayment(ap)
	return nil
}

func (s *TumieszkamyProgreslineStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TumieszkamyProgreslineStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, err := s.Client().R().Get(pdfUri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	body := res.Body()
	return &model.PdfResponse{
		Content:   body,
		PdfStatus: model.OK.Name,
	}
}

func (s *TumieszkamyProgreslineStrategy) LoadsInternal() bool {
	return true
}

func (s *TumieszkamyProgreslineStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *TumieszkamyProgreslineStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
