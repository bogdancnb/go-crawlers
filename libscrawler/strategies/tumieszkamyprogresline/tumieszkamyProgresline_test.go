package tumieszkamyprogresline

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "tumieszkamy_progresline.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"277575", "jEZkw*e9.QKQ!cF"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "277575", Password: "jEZkw*e9.QKQ!cF", Uri: "tumieszkamy_progresline.crawler",
			Identifier: "79461",
			Ref:        "EKSN-2021-10/43", PdfUri: "https://www.strefaklienta24.pl/progresline/iokRozr/ZalDokFin?Ident=3030406",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
