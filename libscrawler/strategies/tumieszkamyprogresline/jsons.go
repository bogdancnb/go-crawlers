package tumieszkamyprogresline

type financesRes struct {
	Status string
	Data   struct {
		Rozrachunki []*struct {
			BrakZaleglosci bool
			Ident          int
			Osoba          string
			Opis           string
			Ulica          string
			NrLok          string
			DoZaplaty      float64
			Nadplata       float64
			//Rejestry       []*struct {
			//	Rej                  int
			//	GrpRej               int
			//	Rozr                 int
			//	Adres                string
			//	NazwaRejestru        string
			//	LiczbaDniDoTermPl    float64
			//	Kwota                float64
			//	Ikona                string
			//	CzyNadplata          bool
			//	RolaWRozrach         string
			//	DodatkoweInfo        string
			//	DataTermPlt          interface{}
			//	CzySaDokPoDacieGrDok bool
			//	KwotaPrzyszlychDok   float64
			//	IOKDokFin            interface{}
			//}
		}
	}
}

type dokumentJson struct {
	Ident          string
	DokId          int
	RejId          int
	RozrId         int
	GrpRejId       int
	Rodzaj         string
	Opis           string
	KontoFin       string
	Numer          string
	DataDok        string
	TermPl         string
	Kwota          float64
	KwotaDoZapl    float64
	Saldo          float64
	RachunekB      string
	OpisPltInet    string
	Ikona          string
	WybraneDoZapl  bool
	CzyObciazenie  bool
	CzyDodDoPar    bool
	CzyNalDoParDod bool
	StrKsg         string
	CzyBO          bool
	CzyKorekta     bool
	CzyDokSym      bool
	Szczegoly      interface{}
	SklejoneDok    []*struct {
		Ident string
		Numer string
		Kwota float64
		Url   string
	}
	SzczegolyDokZak interface{}
}

type historyRes struct {
	Status string
	Data   *struct {
		Ident     int
		Osoba     string
		Opis      string
		Ulica     string
		NrLok     string
		DoZaplaty float64
		Finanse   []*struct {
			Nadplata   float64
			DoZaplaty  float64
			RejIdent   float64
			GrpIdent   float64
			McStanu    string
			Opis       string
			Obciazenia float64
			Uznania    float64
			Saldo      float64
			Pozycje    []*struct {
				Nadplata   float64
				DoZaplaty  float64
				RejIdent   float64
				GrpIdent   float64
				McStanu    string
				Opis       string
				Obciazenia float64
				Uznania    float64
				Saldo      float64
				Pozycje    []*struct {
					Nadplata   float64
					DoZaplaty  float64
					RejIdent   float64
					GrpIdent   float64
					McStanu    string
					Opis       string
					Obciazenia float64
					Uznania    float64
					Saldo      float64
					//Pozycje
					Dokument *dokumentJson
				}
				Dokument interface{}
			}
		}
	}
}
