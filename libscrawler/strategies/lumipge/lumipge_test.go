package lumipge

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestLumiPgeAccount(t *testing.T) {
	crawl.RegisterFactory("lumi.crawler", new(LumiPgeFactory))
	tests := []struct {
		username, password, uri string
	}{
		{"pakii", "Marina46.", "lumi.crawler"},
		//{"vincent.duthel@gmail.com", "Dhju100l", "lumi.crawler"},
		//{"karol@krekora.com", "Pumypumy99"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, test.uri); err != nil {
			t.Error("crawl error ", err)
		}
	}

}
