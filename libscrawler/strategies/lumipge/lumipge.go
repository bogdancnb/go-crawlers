package lumipge

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

const (
	pdfRoot          = "https://moje.lumipge.pl/Invoice/GetDocumentSummary/"
	accessUrl        = "https://moje.lumipge.pl/Account/Login"
	loginErr         = "Niepoprawne login lub hasło"
	invoice          = "https://moje.lumipge.pl/Invoice"
	invoicesUrl      = "https://moje.lumipge.pl/Invoice/GetInvoiceList?sEcho=1&iColumns=11&sColumns=%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C&iDisplayStart=0&iDisplayLength=10&mDataProp_0=0&bSortable_0=false&mDataProp_1=1&bSortable_1=true&mDataProp_2=2&bSortable_2=true&mDataProp_3=3&bSortable_3=false&mDataProp_4=4&bSortable_4=true&mDataProp_5=5&bSortable_5=true&mDataProp_6=6&bSortable_6=true&mDataProp_7=7&bSortable_7=true&mDataProp_8=8&bSortable_8=true&mDataProp_9=9&bSortable_9=true&mDataProp_10=10&bSortable_10=true&iSortCol_0=10&sSortDir_0=asc&iSortingCols=1&periodFrom=&periodTo=&amountFrom=&amountTo=&notPaid=false&mediumStatus=&_="
	paymentsurl      = "https://moje.lumipge.pl/Invoice/GetExtractList?sEcho=1&iColumns=4&sColumns=%2C%2C%2C&iDisplayStart=0&iDisplayLength=10&mDataProp_0=0&bSortable_0=false&mDataProp_1=1&bSortable_1=true&mDataProp_2=2&bSortable_2=true&mDataProp_3=3&bSortable_3=true&iSortCol_0=2&sSortDir_0=desc&iSortingCols=1&_="
	mobileInvoiceUrl = "https://moje.lumipge.pl/Invoice/GetInvoiceMobileList"

	jsonFormat = "02/01/2006"
)

var (
	bankName        = "Powszechna Kasa Oszczednosci Bank Polski Spolka Akcyjna"
	bankBIC         = "BPKOPLPW"
	legalEntityname = "Lumi PGE - Biuro Obsługi Klienta"
)

func Init() {
	factory := new(LumiPgeFactory)
	RegisterFactory("lumi.crawler", factory)
}

type LumiPgeFactory struct{}

func (p *LumiPgeFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "LumiPgeStrategy")
	return &LumiPgeStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type LumiPgeStrategy struct {
	*logrus.Entry
	HttpStrategy
	accDoc *goquery.Document
}

func (s *LumiPgeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *LumiPgeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *LumiPgeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *LumiPgeStrategy) Login(acc *model.Account) (*string, Exception) {
	resp, err := s.Client().R().Get(accessUrl)
	if err != nil {
		return nil, ConnectionErr("Login: " + err.Error())
	}
	verifCookie := resp.Header().Get("Set-Cookie")
	verifCookie = verifCookie[:strings.Index(verifCookie, "; path")]
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(resp.Body()))
	if err != nil {
		return nil, ParseErr("Login: " + err.Error())
	}
	requestVerificationToken, ex := Attr(doc.Find("input[name=__RequestVerificationToken]"), "value")
	if ex != nil {
		return nil, ex
	}

	resp, err = s.Client().R().
		SetHeaders(map[string]string{
			"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
			"content-type": "application/x-www-form-urlencoded",
			"Cookie":       verifCookie,
			"host":         "moje.lumipge.pl",
		}).
		SetFormData(map[string]string{
			"__RequestVerificationToken": requestVerificationToken,
			"Username":                   acc.Username,
			"Password":                   *acc.Password,
			"RememberMe":                 "false",
		}).Post(accessUrl)
	resp, connErr := Accepted(resp, err, 302)
	if connErr != nil {
		return nil, connErr
	}
	s.accDoc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(resp.Body()))
	s.Trace(s.accDoc.Html())
	if err != nil {
		return nil, ParseErr(err.Error())
	}
	html := resp.String()
	return &html, nil
}

func (s *LumiPgeStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErr) {
		return LoginErr(loginErr)
	}
	return nil
}

func (s *LumiPgeStrategy) LoadLocations(account *model.Account) Exception {
	info := s.accDoc.Find("table[class='last-dark platnik-dane'] tbody tr")
	if len(info.Nodes) != 7 {
		return ParseErr("parsing account info")
	}
	pesel := strings.TrimSpace(info.Eq(0).Find("td").Eq(1).Text())
	customerName := strings.TrimSpace(info.Eq(1).Find("td").Eq(1).Text())
	customerNumber := strings.TrimSpace(info.Eq(2).Find("td").Eq(1).Text())

	var branch *model.ProviderBranchDTO
	accNr, ex := s.parseAccNr()
	if ex != nil {
		return ex
	}
	if accNr != "" {
		branch = &model.ProviderBranchDTO{
			Iban:            &accNr,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalEntityname,
		}
	} else {
		s.Debug("no invoice to parse iban from")
	}
	account.AddLocation(&model.Location{
		Service:        customerNumber + ", " + customerName,
		Details:        &customerNumber,
		Identifier:     pesel,
		ProviderBranch: branch,
	})
	return nil
}

func (s *LumiPgeStrategy) parseAccNr() (string, Exception) {
	resp, ex := OkDocument(s.Client().R().Get(invoice))
	if ex != nil {
		return "", ParseErr("parseAccNr: " + ex.Error())
	}
	token, ex := Attr(resp.Find("div form[id='__AjaxAntiForgeryForm'] input"), "value")
	if ex != nil {
		return "", ParseErr("parseAccNr: " + ex.Error())
	}

	res, ex := OkString(s.Client().R().SetFormData(map[string]string{
		"__RequestVerificationToken": token,
		"start":                      "0",
		"take":                       "10",
		"notPaid":                    "false",
	}).SetHeaders(map[string]string{
		"content-type":     "application/x-www-form-urlencoded; charset=UTF-8",
		"host":             "moje.lumipge.pl",
		"origin":           "https://moje.lumipge.pl",
		"X-Requested-With": "XMLHttpRequest",
	}).Post(mobileInvoiceUrl))
	if ex != nil {
		return "", ConnectionErr("parseAccNr, POST " + mobileInvoiceUrl + ": " + ex.Error())
	}
	var info struct {
		Data []*struct{ BankAccountNumberPayer string } `json:"data"`
	}
	err := json.Unmarshal([]byte(res), &info)
	if err != nil {
		s.WithError(err).Errorf("reading POST %s response %s", mobileInvoiceUrl, res)
		return "", ParseErr("parseAccNr, reading response: " + err.Error())
	}
	if len(info.Data) == 0 {
		return "", nil
	}
	accNr := info.Data[0].BankAccountNumberPayer
	if !strings.HasPrefix(accNr, "PL") {
		return "", ParseErr("invalid acc nr: " + accNr)
	}
	return accNr[2:], nil
}

func (s *LumiPgeStrategy) LoadAmount(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *LumiPgeStrategy) LoadInvoices(a *model.Account, l *model.Location) Exception {
	resp, ex := OkString(s.Client().R().Get(invoicesUrl))
	if ex != nil {
		return ex
	}
	s.Trace(resp)
	var invoiceInfo invoiceJson
	err := json.Unmarshal([]byte(resp), &invoiceInfo)
	if err != nil {
		return ParseErr("invoice json: " + resp)
	}
	for _, data := range invoiceInfo.AaData {
		if ex := s.parseInvoice(l, data); ex != nil {
			return ParseErr("LoadInvoices: " + ex.Error())
		}
	}
	return nil
}

type invoiceJson struct {
	AaData [][]string `json:"aaData"`
}

func (s *LumiPgeStrategy) parseInvoice(location *model.Location, data []string) Exception {
	ref := data[1]
	issueDate, err := time.Parse(jsonFormat, data[5])
	if err != nil {
		return ParseErr("parsing issue date " + data[5])
	}
	dueDate, err := time.Parse(jsonFormat, data[6])
	if err != nil {
		return ParseErr("parsing due date " + data[6])
	}
	amount, err := strconv.ParseFloat(strings.ReplaceAll(data[8], ",", "."), 64)
	if err != nil {
		return ParseErr("parsing amount from " + data[8])
	}
	amountDue, err := strconv.ParseFloat(strings.ReplaceAll(data[9], ",", "."), 64)
	if err != nil {
		return ParseErr("parsing amount from " + data[9])
	}
	pdfUri := pdfRoot + data[2]

	location.AddInvoice(&model.Invoice{
		Ref:       ref,
		PdfUri:    &pdfUri,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	})
	return nil
}

type paymentJson struct {
	AaData [][]string `json:"aaData"`
}

func (s *LumiPgeStrategy) LoadPayments(a *model.Account, l *model.Location) Exception {
	resp, ex := OkString(s.Client().R().Get(paymentsurl))
	if ex != nil {
		return ex
	}
	s.Trace(resp)
	var info paymentJson
	err := json.Unmarshal([]byte(resp), &info)
	if err != nil {
		return ParseErr("parsing payments response: " + resp)
	}
	for _, data := range info.AaData {
		if ex := s.parsePayment(l, data); ex != nil {
			return ParseErr("parsing payment: " + ex.Error())
		}
	}
	return nil
}

func (s *LumiPgeStrategy) parsePayment(location *model.Location, data []string) Exception {
	ref := data[1]
	date, err := time.Parse(jsonFormat, data[2])
	if err != nil {
		return ParseErr("parsing date " + data[2])
	}
	amount, err := strconv.ParseFloat(strings.ReplaceAll(data[3], ",", "."), 64)
	if err != nil {
		return ParseErr("parsing amount from " + data[3])
	}
	location.AddPayment(&model.Payment{
		Ref:    ref,
		Amount: amount,
		Date:   date.Format(utils.DBDateFormat),
	})
	return nil
}

func (s *LumiPgeStrategy) Pdf(a *model.Account) *model.PdfResponse {
	pdfBytes, ex := GetBytes(s.Client().R().Get(*a.Locations[0].Invoices[0].PdfUri))
	if ex != nil {
		erMsg := ex.Error()
		return &model.PdfResponse{
			Content:   nil,
			PdfStatus: model.OTHER_EXCEPTION.Name,
			ErrMsg:    &erMsg,
		}
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *LumiPgeStrategy) LoadsInternal() bool {
	return false
}

func (s *LumiPgeStrategy) Close(a *model.Account) {
	s.HttpStrategy.Close(a)
}

func (s *LumiPgeStrategy) SendConnectionError() {
}

func (s *LumiPgeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *LumiPgeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
