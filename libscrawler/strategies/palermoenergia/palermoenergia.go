package palermoenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	home  = "http://www.palermoenergiaspa.it/"
	login = "http://www.palermoenergiaspa.it"

	loginErrMsg = "Nessun utente trovato"
	siteFormat  = "2006-01-02"
)

func Init() {
	RegisterFactory("palermo_energia.crawler", new(PalermoEnergiaFactory))
}

type PalermoEnergiaFactory struct{}

func (p *PalermoEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "PalermoEnergiaStrategy")
	return &PalermoEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type PalermoEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc       *goquery.Document
	PHPSESSID string
}

func (s *PalermoEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PalermoEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.palermoenergiaspa.it",
	}).Get(home)
	if res == nil || err != nil {
		return nil, ConnectionErr("Get " + home)
	}
	s.PHPSESSID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	s.PHPSESSID = s.PHPSESSID[:strings.Index(s.PHPSESSID, ";")]
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse home html: " + err.Error())
	}
	script := doc.Find("script:containsOwn('autenticazione_form_login')")
	if script.Length() == 0 {
		return nil, ParseErr("autenticazione_form_login script not found")
	}
	scriptTxt := script.Text()
	formAction := scriptTxt[strings.Index(scriptTxt, `frm.action = '`)+14:]
	formAction = formAction[:strings.Index(formAction, `';`)]
	uri := login + formAction

	//s.EnableRedirects(false)
	login, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"host":         "www.palermoenergiaspa.it",
		"origin":       "http://www.palermoenergiaspa.it",
	}).SetFormData(map[string]string{
		"autenticazione_form_username": account.Username,
		"autenticazione_form_password": *account.Password,
	}).Post(uri)
	if login == nil {
		return nil, ConnectionErr("no response for login POST")
	}
	if strings.Contains(login.String(), loginErrMsg) {
		return nil, LoginErr(loginErrMsg)
	}
	return nil, ParseErr("could not obtain info")

	//s.EnableRedirects(true)
	//return nil, nil
}

func (s *PalermoEnergiaStrategy) CheckLogin(response *string) Exception {
	return ParseErr("could not obtain information")
}

func (s *PalermoEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *PalermoEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *PalermoEnergiaStrategy) LoadsInternal() bool {
	return false
}

func (s *PalermoEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *PalermoEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
