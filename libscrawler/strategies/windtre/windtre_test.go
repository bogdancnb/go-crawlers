package windtre

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "windtre.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"tiziboncoraglio@gmail.com", "Accesso1973"}, //ok
		//{"pincerchic@hotmail.it", "Ab21022009"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "tiziboncoraglio@gmail.com", Password: "Accesso1973", Uri: "windtre.crawler",
			Identifier: "CWPRA0709661251",
			Ref:        "W2028083660", PdfUri: "https://apigw.windtre.it/api/v1/payment/bills/pdf?contractId=CWPRA0709661251&lineId=0952861200&billNumber=W2028083660",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
