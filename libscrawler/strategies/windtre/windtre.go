package windtre

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

const (
	login       = "https://areaclienti.windtre.it/login"
	credentials = "https://apigw.windtre.it/api/v4/login/credentials"
	bills       = "https://apigw.windtre.it/api/v1/payment/bills?contractId=%s&lineId=%s"
	pdfUri      = `https://apigw.windtre.it/api/v1/payment/bills/pdf?contractId=%s&lineId=%s&billNumber=%s`

	payloadFmt   = `{"username":"%s","password":"%s","rememberMe":false}`
	loginErrCode = "ERR-AUTH"
	jsonFormat   = "2006-01-02"
)

func Init() {
	RegisterFactory("windtre.crawler", new(WindtreFactory))
}

type WindtreFactory struct{}

func (p *WindtreFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "WindtreStrategy")
	return &WindtreStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type WindtreStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document

	fiscalCode  string
	loginRes    loginResponse
	accessToken string
}

type loginResponse struct {
	ErrorCodes               []string                `json:"errorCodes"`
	Messages                 []*loginResponseMessage `json:"messages"`
	Status                   string                  `json:"status"`
	CustomerId               string                  `json:"customerId"`
	CustomerIntegrationStack string                  `json:"customerIntegrationStack"`
	Data                     *loginResponseData      `json:"data"`
}
type loginResponseMessage struct {
	Message string `json:"message"`
	Type    string `json:"type"`
}
type loginResponseData struct {
	Contracts []*jsonContract `json:"contracts"`
}
type jsonContract struct {
	Id             string              `json:"id"`
	CustomerId     string              `json:"customerId"`
	Status         string              `json:"status"`
	Brand          string              `json:"brand"`
	PaymentType    string              `json:"paymentType"`
	Sme            bool                `json:"sme"`
	FlagMigrazione string              `json:"flagMigrazione"`
	Lines          []*jsonContractLine `json:"lines"`
}
type jsonContractLine struct {
	Id                        string   `json:"id"`
	CustomerId                string   `json:"customerId"`
	ContractId                string   `json:"contractId"`
	Alias                     string   `json:"alias"`
	Mobile                    bool     `json:"mobile"`
	Sme                       bool     `json:"sme"`
	PaymentType               string   `json:"paymentType"`
	SimCard                   *simCard `json:"simCard"`
	IntegrationStack          string   `json:"integrationStack"`
	Features                  []string `json:"features"`
	LineAdslId                string   `json:"lineAdslId"`
	FlagMigrazione            string   `json:"flagMigrazione"`
	DataAttivazionePhoenix    string   `json:"dataAttivazionePhoenix"`
	DataAttivazionePeoplesoft string   `json:"dataAttivazionePeoplesoft"`
}
type simCard struct {
	Id      string `json:"id"`
	LineId  string `json:"lineId"`
	SimType string `json:"simType"`
}

func (s *WindtreStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *WindtreStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *WindtreStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *WindtreStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeader("accept", AcceptHeader).Get(login))
	if ex != nil {
		return nil, ex
	}

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"Connection":    "keep-alive",
		"content-type":  "application/json",
		"Host":          "apigw.windtre.it",
		"Origin":        "https://areaclienti.windtre.it",
		"Referer":       "https://areaclienti.windtre.it/login",
		"X-Brand":       "ONEBRAND",
		"X-Wind-Client": "Web",
	}).SetBody(fmt.Sprintf(payloadFmt, account.Username, *account.Password)).Post(credentials)
	if err != nil {
		return nil, ConnectionErr("login request error: " + err.Error())
	}
	s.accessToken = HeaderByNameAndValStartsWith(res.Header(), "X-W3-Token", "")
	err = utils.FromJSON(&s.loginRes, bytes.NewBuffer(res.Body()))
	if err != nil {
		s.Error(err)
		return nil, ParseErr("could not deserialize login response: " + string(res.Body()))
	}
	return nil, nil
}

func (s *WindtreStrategy) CheckLogin(response *string) Exception {
	for _, errorCode := range s.loginRes.ErrorCodes {
		if strings.Contains(errorCode, loginErrCode) {
			return LoginErr("invalid credentials")
		}
	}
	return nil
}

func (s *WindtreStrategy) LoadLocations(account *model.Account) Exception {
	for _, contract := range s.loginRes.Data.Contracts {
		service := ""
		lineId := ""
		for i, line := range contract.Lines {
			if i > 0 {
				service += "| " + line.Alias
				for _, feature := range line.Features {
					service += " " + feature
				}
			} else {
				service += line.Alias
				for _, feature := range line.Features {
					service += " " + feature
				}
			}
			if i > 0 {
				lineId += "|" + line.Id
			} else {
				lineId += line.Id
			}
		}

		l := &model.Location{
			Service:        service,
			Details:        &contract.CustomerId,
			Identifier:     contract.Id,
			AccountHelpers: lineId,
		}
		account.AddLocation(l)
	}
	return nil
}

func (s *WindtreStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

type invoiceJson struct {
	BillNumber     string  `json:"billNumber"`
	Amount         float64 `json:"amount"`
	AmountDue      float64 `json:"amountDue"`
	PeriodStart    string  `json:"periodStart"`
	PeriodEnd      string  `json:"periodEnd"`
	IssueDate      string  `json:"issueDate"`
	ExpirationDate string  `json:"expirationDate"`
	Status         string  `json:"status"`
	Payable        bool    `json:"payable"`
	DownloadUrl    string  `json:"downloadUrl"`
	PaymentProof   bool    `json:"paymentProof"`
}

func (s *WindtreStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	for _, contract := range s.loginRes.Data.Contracts {
		if contract.Id == location.Identifier {
			if contract.PaymentType == "PRE" {
				s.Debug("prepaid contract, no invoices: ", contract)
				return nil
			}
		}
	}

	lineIds := strings.Split(location.AccountHelpers.(string), "|")

	for _, lineId := range lineIds {
		uri := fmt.Sprintf(bills, location.Identifier, lineId)
		res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
			"accept":        "application/json, text/plain, */*",
			"Authorization": "Bearer " + s.accessToken,
			"Connection":    "keep-alive",
			"Content-Type":  "application/json",
			"Host":          "apigw.windtre.it",
			"Origin":        "https://areaclienti.windtre.it",
			"X-W3-Host":     "https://areaclienti.windtre.it",
			"X-Wind-Client": "web",
		}).Get(uri))
		if ex != nil {
			return ex
		}
		s.Debug(res)
		var invoices []*invoiceJson
		err := utils.FromJSON(&invoices, strings.NewReader(res))
		if err != nil {
			return ParseErr("deserialize invoices json: " + err.Error())
		}

		for _, i := range invoices {
			issue, err := time.Parse(jsonFormat, i.IssueDate)
			if err != nil {
				return ParseErr("issue date " + i.IssueDate)
			}
			dueDate, err := time.Parse(jsonFormat, i.ExpirationDate)
			if err != nil {
				return ParseErr("due date " + i.ExpirationDate)
			}
			uri := fmt.Sprintf(pdfUri, location.Identifier, lineId, i.BillNumber)
			inv := &model.Invoice{
				Ref:       i.BillNumber,
				PdfUri:    &uri,
				Amount:    i.Amount,
				AmountDue: i.AmountDue,
				IssueDate: issue.Format(utils.DBDateFormat),
				DueDate:   dueDate.Format(utils.DBDateFormat),
			}
			location.AddInvoice(inv)
		}
	}

	return nil
}

func (s *WindtreStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *WindtreStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *WindtreStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *WindtreStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer " + s.accessToken,
		"Connection":    "keep-alive",
		"Host":          "apigw.windtre.it",
		"Origin":        "https://areaclienti.windtre.it",
		"X-W3-Host":     "https://areaclienti.windtre.it",
		"X-Wind-Client": "web",
	}).Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *WindtreStrategy) LoadsInternal() bool {
	return false
}

func (s *WindtreStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *WindtreStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
