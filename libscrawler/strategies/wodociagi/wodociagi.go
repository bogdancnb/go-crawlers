package wodociagi

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	root       = `https://ebok.wodociagi.chrzanowskie.pl/apex/`
	accessUrl  = `https://ebok.wodociagi.chrzanowskie.pl/apex/f?p=100:102`
	flowAccept = `https://ebok.wodociagi.chrzanowskie.pl/apex/wwv_flow.accept`
	flowAjax   = `https://ebok.wodociagi.chrzanowskie.pl/apex/wwv_flow.ajax`
	p_json     = `{"pageItems":{"itemsToSubmit":[{"n":"P0_ONESIGNAL_USERID","v":"%s"},{"n":"P0_ZMIENIC","v":"","ck":"%s"},{"n":"P102_IP","v":"%s"},{"n":"P102_POMOC","v":"%s"},{"n":"P102_USERNAME","v":"%s"},{"n":"P102_PASSWORD","v":"%s"}],"protected":"%s","rowVersion":"%s","formRegionChecksums":[]},"salt":"%s"}`

	loginErr   = "Podany identyfikator lub hasło są nieprawidłowe"
	siteFormat = "2006-01-02"
)

var (
	accNrRegexp = regexp.MustCompile(`konto bankowe([\d ]+)-`)
	bankName    = "ING Bank Śląski S.A."
	bankBIC     = "INGBPLPW"
	legalName   = "Wodociągi Chrzanowskie Spółka z o.o."
)

func Init() {
	factory := new(WodociagiFactory)
	RegisterFactory("wodociagi_chrzanowskie.crawler", factory)
}

type WodociagiFactory struct{}

func (p *WodociagiFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "WodociagiStrategy")
	return &WodociagiStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}

}

type WodociagiStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc         *goquery.Document
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
}

func (s *WodociagiStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *WodociagiStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *WodociagiStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *WodociagiStrategy) Login(acc *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Connection": "keep-alive",
		"Host":       "bok.wodociagi.chrzanowskie.pl",
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(accessUrl))
	if ex != nil {
		return nil, ex
	}
	p_flow_id, ex := Attr(doc.Find("input[name=p_flow_id]"), "value")
	if ex != nil {
		return nil, ex
	}
	p_flow_step_id, ex := Attr(doc.Find("input[name=p_flow_step_id]"), "value")
	if ex != nil {
		return nil, ex
	}
	p_instance, ex := Attr(doc.Find("input[name=p_instance]"), "value")
	if ex != nil {
		return nil, ex
	}
	p_reload_on_submit, ex := Attr(doc.Find("input[name=p_reload_on_submit]"), "value")
	if ex != nil {
		return nil, ex
	}
	p_page_submission_id, ex := Attr(doc.Find("input[name=p_page_submission_id]"), "value")
	if ex != nil {
		return nil, ex
	}

	P0_ONESIGNAL_USERID, ex := Attr(doc.Find("input[name=P0_ONESIGNAL_USERID]"), "value")
	if ex != nil {
		return nil, ex
	}
	P0_ZMIENIC, ex := Attr(doc.Find("input[data-for=P0_ZMIENIC]"), "value")
	if ex != nil {
		return nil, ex
	}
	P102_POMOC, ex := Attr(doc.Find("input[name=P102_POMOC]"), "value")
	if ex != nil {
		return nil, ex
	}
	pPageItemsProtected, ex := Attr(doc.Find("input[id=pPageItemsProtected]"), "value")
	if ex != nil {
		return nil, ex
	}
	pPageItemsRowVersion, ex := Attr(doc.Find("input[id=pPageItemsRowVersion]"), "value")
	if ex != nil {
		return nil, ex
	}
	pSalt, ex := Attr(doc.Find("input[id=pSalt]"), "value")
	if ex != nil {
		return nil, ex
	}
	ip := "127.0.0.1"
	if s.Proxy() != nil && s.Proxy().Host != "" {
		ip = s.Proxy().Host
	}

	/*
		{"pageItems":{"itemsToSubmit":[{"n":"P0_ONESIGNAL_USERID","v":"%s"},{"n":"P0_ZMIENIC","v":"","ck":"%s"},
		{"n":"P102_IP","v":"%s"},{"n":"P102_POMOC","v":"%s"},{"n":"P102_USERNAME","v":"%s"},{"n":"P102_PASSWORD","v":"%s"}],
		"protected":"%s","rowVersion":"%s","formRegionChecksums":[]},"salt":"%s"}
	*/
	p_jsonFormatted := fmt.Sprintf(p_json,
		P0_ONESIGNAL_USERID, P0_ZMIENIC, ip, P102_POMOC, acc.Username, *acc.Password,
		pPageItemsProtected, pPageItemsRowVersion, pSalt)

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":           "application/json, text/javascript, */*; q=0.01",
		"Content-Type":     "application/x-www-form-urlencoded; charset=UTF-8",
		"Origin":           "https://ebok.wodociagi.chrzanowskie.pl",
		"Referer":          "https://ebok.wodociagi.chrzanowskie.pl/apex/f?p=100:102",
		"X-Requested-With": "XMLHttpRequest",
	}).SetFormData(map[string]string{
		"p_flow_id":            p_flow_id,
		"p_flow_step_id":       p_flow_step_id,
		"p_instance":           p_instance,
		"p_debug":              "",
		"p_request":            "P102_ZALOGUJ",
		"p_reload_on_submit":   p_reload_on_submit,
		"p_page_submission_id": p_page_submission_id,
		"p_json":               p_jsonFormatted,
	}).Post(flowAccept))
	if ex != nil {
		return nil, ex
	}
	var resJson struct {
		RedirectURL string `json:"redirectURL"`
	}
	err := utils.FromJSON(&resJson, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("parsing POST " + flowAccept + " : " + err.Error())
	}

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Referer": "https://ebok.wodociagi.chrzanowskie.pl/apex/f?p=100:102",
	}).Get(root + resJson.RedirectURL))
	if ex != nil {
		return nil, ex
	}
	html, err := s.doc.Html()
	if err != nil {
		return nil, ParseErr("parse html for GET " + root + resJson.RedirectURL + " : " + err.Error())
	}
	return &html, nil
}

func (s *WodociagiStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErr) {
		return LoginErr(loginErr)
	}
	return nil
}

func (s *WodociagiStrategy) LoadLocations(account *model.Account) Exception {
	codClient := s.doc.Find("h3:containsOwn('Identyfikator')")
	if codClient.Length() == 0 {
		return ParseErr("could not find client code element")
	}
	cc := codClient.Eq(0).Next().Text()
	if cc == "" {
		return ParseErr("could not find client code")
	}
	addressEl := s.doc.Find("h3:containsOwn('Adres')")
	if addressEl.Length() == 0 {
		return ParseErr("could not find address element")
	}
	address := addressEl.Eq(0).Next().Text()
	if address == "" {
		return ParseErr("could not find address")
	}
	loc := &model.Location{
		Service:    address,
		Identifier: cc,
	}
	if exL, ok := s.exLocations[cc]; ok && exL.ProviderBranch != nil {
		loc.ProviderBranch = exL.ProviderBranch
	}
	account.AddLocation(loc)
	return nil
}

func (s *WodociagiStrategy) LoadAmount(a *model.Account, l *model.Location) Exception {
	soldEl := s.doc.Find("span[id=N_SALDO_DISPLAY]")
	if soldEl.Length() == 0 {
		s.Warn("could not found amount element")
		return nil
	}
	am := soldEl.Eq(0).Text()
	amParts := strings.Split(am, ` `)
	if len(amParts) < 3 {
		s.Warn("could not found amount element")
		return nil
	}
	am = strings.ReplaceAll(amParts[1], ",", ".")
	locAm, ex := AmountFromString(am)
	if ex != nil {
		s.Warn(ex.Error())
		return nil
	}
	l.Amount = &locAm
	return nil
}

func (s *WodociagiStrategy) LoadInvoices(a *model.Account, l *model.Location) Exception {
	invoicesHrefEl := s.doc.Find("a[title='Rachunki i płatności']")
	if invoicesHrefEl.Length() == 0 {
		return ParseErr("could not find invoices link")
	}
	href, ex := Attr(invoicesHrefEl.Eq(0), "href")
	if ex != nil {
		return ex
	}

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(root + href))
	if ex != nil {
		return ex
	}
	rows := s.doc.Find("table[id='R_RACHUNKI_I_PLATNOSCI_1'] tbody tr")
	if rows.Length() == 0 {
		return ParseErr("no invoice table found")
	}
	for i := 0; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		refTxt := cols.Eq(2).Text()
		if strings.Contains(refTxt, "Faktura") {
			ex = s.parseInvoice(a, l, cols, refTxt)
		} else {
			ex = s.parsePayment(a, l, cols, refTxt)
		}
		if ex != nil {
			return ex
		}
	}
	return nil
}

func (s *WodociagiStrategy) parseInvoice(a *model.Account, l *model.Location, cols *goquery.Selection, refTxt string) Exception {
	ref := refTxt[:strings.Index(refTxt, "Termin")]
	ref = ref[strings.Index(refTxt, "Numer ")+6:]
	issueS := utils.RemoveSpaces(cols.Eq(1).Text())
	issueDate, err := time.Parse(siteFormat, issueS)
	if err != nil {
		return ParseErr("parsing issue date from " + issueS)
	}
	dueS := refTxt[:strings.Index(refTxt, "Sprzedaż Woda")]
	dueS = dueS[len(dueS)-10:]
	dueDate, err := time.Parse(siteFormat, dueS)
	if err != nil {
		return ParseErr("parsing due date from " + dueS)
	}
	amS := utils.RemoveSpaces(cols.Eq(3).Text())
	amt, ex := AmountFromString(amS)
	if ex != nil {
		return ex
	}
	amDueS := utils.RemoveSpaces(cols.Eq(5).Text())
	amDue, ex := AmountFromString(amDueS)
	if ex != nil {
		return ex
	}
	var pdfUri *string
	uriEl := cols.Eq(6).Find("a")
	if uriEl.Length() == 0 {
		return ParseErr("no pdf uri element found for " + ref)
	} else {
		href, ex := Attr(uriEl, "href")
		if ex != nil {
			return ParseErr("no pdf uri element found for " + ref + ": " + ex.Error())
		}
		href = root + href
		pdfUri = &href
	}

	i := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    amt,
		AmountDue: amDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(i)
	if l.ProviderBranch == nil {
		if ex := s.parsePDF(l, i); ex != nil {
			return ParseErr("parse pdf for " + ref + ": " + ex.Error())
		}
	}
	return nil
}

func (s *WodociagiStrategy) parsePDF(l *model.Location, invoice *model.Invoice) Exception {
	pdf, ex := s.pdfDownload(invoice)
	if ex != nil {
		return ConnectionErr("pdfDownload " + invoice.Ref + ": " + ex.Error())
	}
	text, err := PlainText(pdf)
	if err != nil {
		return ParseErr("parsing pdf " + *invoice.PdfUri + ": " + err.Error())
	}
	s.Trace(text)
	m, err := utils.MatchRegex(text, accNrRegexp)
	if err != nil {
		s.Error("pdf text=", text)
		return ParseErr("matching pdf text with regex " + accNrRegexp.String())
	}
	accNr := m[0][1]
	accNr = utils.RemoveSpaces(accNr)
	l.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNr,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}
	return nil
}

func (s *WodociagiStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	}).Get(*invoice.PdfUri))
	if ex != nil {
		return nil, ex
	}
	return pdfBytes, nil
}

func (s *WodociagiStrategy) parsePayment(a *model.Account, l *model.Location, cols *goquery.Selection, refTxt string) Exception {
	issueS := utils.RemoveSpaces(cols.Eq(1).Text())
	issueDate, err := time.Parse(siteFormat, issueS)
	if err != nil {
		return ParseErr("parsing issue date from " + issueS)
	}
	ref := utils.RemoveSpaces(cols.Eq(2).Text())
	amS := utils.RemoveSpaces(cols.Eq(4).Text())
	amt, ex := AmountFromString(amS)
	if ex != nil {
		return ex
	}
	p := &model.Payment{
		Ref:    ref,
		Amount: amt,
		Date:   issueDate.Format(utils.DBDateFormat),
	}
	l.AddPayment(p)
	return nil
}

func (s *WodociagiStrategy) LoadPayments(a *model.Account, l *model.Location) Exception {
	return nil
}

func (s *WodociagiStrategy) Pdf(a *model.Account) *model.PdfResponse {
	l := a.Locations[0]
	ref := l.Invoices[0].Ref
	l.Invoices = make([]*model.Invoice, 0)
	l.ProviderBranch = &model.ProviderBranchDTO{}
	if ex := s.LoadInvoices(a, l); ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	var invoice *model.Invoice
	for _, i := range l.Invoices {
		if i.Ref == ref {
			invoice = i
			break
		}
	}
	if invoice == nil {
		return model.PdfErrorResponse("no invoice parsed with ref "+ref, model.NOT_FOUND)
	}
	pdf, ex := s.pdfDownload(invoice)
	if ex != nil {
		return model.PdfErrorResponse("pdfDownload "+invoice.Ref+": "+ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdf,
		PdfStatus: model.OK.Name,
	}
}

func (s *WodociagiStrategy) LoadsInternal() bool {
	return false
}

func (s *WodociagiStrategy) Close(a *model.Account) {
	s.HttpStrategy.Close(a)
}

func (s *WodociagiStrategy) SendConnectionError() {
}

func (s *WodociagiStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *WodociagiStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
