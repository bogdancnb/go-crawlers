package wodociagi

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	tests := []struct {
		username string
		password string
		uri      string
	}{
		{"207859", "wisla2020", "wodociagi_chrzanowskie.crawler"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, test.uri); err != nil {
			t.Logf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "207859", Password: "wisla2020", Uri: "wodociagi_chrzanowskie.crawler",
			Identifier: "207859",
			Ref:        "02/009153/2020", PdfUri: "xxxx",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
