package energiaazzura

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	home        = "https://netaweb.egea.it/public/energiaazzurra/"
	webServices = "https://netaweb.egea.it/public/prontoweb-web-services"

	loginErrMsg = "Username e Password inserite non corrispondono"
	siteFormat  = "2006-01-02"
)

func Init() {
	RegisterFactory("energia_Azzura.crawler", new(EnergiaAzzuraFactory))
}

type EnergiaAzzuraFactory struct{}

func (p *EnergiaAzzuraFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EnergiaAzzuraStrategy")
	return &EnergiaAzzuraStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type EnergiaAzzuraStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document
}

func (s *EnergiaAzzuraStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EnergiaAzzuraStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "netaweb.egea.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())

	identifier, ex := Attr(doc.Find("input[name='identifier']"), "value")
	if ex != nil {
		return nil, ex
	}
	loginSessione, ex := Attr(doc.Find("input[name='loginSessione']"), "value")
	if ex != nil {
		return nil, ex
	}

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "netaweb.egea.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://netaweb.egea.it",
		"referer":      "https://netaweb.egea.it/public/egea",
	}).SetFormData(map[string]string{
		"operation":     "login",
		"identifier":    identifier,
		"multi-site":    "energiaazzurra",
		"i18n-locale":   "",
		"loginSessione": loginSessione,
		"username":      account.Username,
		"password":      *account.Password,
	}).Post(webServices))
	if ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("parse login post html: " + err.Error())
	}
	return &html, nil
}

func (s *EnergiaAzzuraStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return ParseErr("could not obtain information")
}

func (s *EnergiaAzzuraStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *EnergiaAzzuraStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EnergiaAzzuraStrategy) LoadsInternal() bool {
	return false
}

func (s *EnergiaAzzuraStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EnergiaAzzuraStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
