package aGSMEnergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	home         = "https://countbox.agsm.it/"
	supplies     = "https://countbox.agsm.it/supplies"
	custCodeInfo = `https://countbox.agsm.it/ajax/get-supplies/?customer_code=%s&dashboard=true`
	bills        = "https://countbox.agsm.it/bills"

	loginErrMsg   = `Username o Password errata!`
	invoiceFormat = "02/01/2006"
)

var (
	identRegex = regexp.MustCompile(`FORNITURAN\D*(\d+)`)

	bankName  = "BANCA MEDIOLANUM S.P.A."
	bankBIC   = "MEDBITMM"
	legalName = "Agsm Energia Spa"
)

func Init() {
	RegisterFactory("aGSM_Energia.crawler", new(AGSMEnergiaFactory))
}

type AGSMEnergiaFactory struct{}

func (p *AGSMEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AGSMEnergiaStrategy")
	return &AGSMEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type AGSMEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations  map[string]*model.Location
	exInvoices   map[string]*model.Invoice
	doc          *goquery.Document
	phpsessid    string
	customerCode string
	customerAddr string
}

func (s *AGSMEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *AGSMEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})
	response, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "countbox.agsm.it",
	}).Get(home)
	s.phpsessid = HeaderByNameAndValStartsWith(response.Header(), "Set-Cookie", "PHPSESSID")
	s.phpsessid = s.phpsessid[:strings.Index(s.phpsessid, ";")]
	doc, ex := OkDocument(response, err)
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "countbox.agsm.it",
		"content-type": "application/x-www-form-urlencoded",
		"Cookie":       s.phpsessid,
		"origin":       "https://countbox.agsm.it",
		"referer":      "https://countbox.agsm.it/",
	}).SetFormData(map[string]string{
		"username": account.Username,
		"password": *account.Password,
		"send":     "Login",
	}).Post(home))
	if ex != nil {
		return nil, ex
	}

	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("login response")
	}
	if strings.Contains(html, loginErrMsg) {
		return nil, LoginErr(loginErrMsg)
	}

	custCodeSelector := `span[class=customer_code_plc]`
	custCodeEl := doc.Find(custCodeSelector)
	if custCodeEl.Length() == 0 {
		return nil, ParseErr("not found :" + custCodeSelector)
	}
	s.customerCode = strings.TrimSpace(custCodeEl.Text())
	custAddrEl := doc.Find(`p[class=customer_address]`)
	if custAddrEl.Length() == 0 {
		return nil, ParseErr("address element not found")
	}
	s.customerAddr = strings.TrimSpace(custAddrEl.Text())

	return &html, nil
}

func (s *AGSMEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	loc := &model.Location{
		Service:    s.customerAddr,
		Identifier: s.customerCode,
	}
	account.AddLocation(loc)

	if exception := s.parseIban(account, loc); exception != nil {
		return exception
	}

	if ex := s.parseInvoices(account); ex != nil {
		return ex
	}
	return nil
}

func (s *AGSMEnergiaStrategy) parseIban(account *model.Account, loc *model.Location) Exception {
	url := fmt.Sprintf(custCodeInfo, s.customerCode)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     "*/*",
		"connection": "keep-alive",
		"host":       "countbox.agsm.it",
		"referer":    "https://countbox.agsm.it/dashboard",
		"cookie":     s.phpsessid,
	}).Get(url)
	if err != nil {
		return ConnectionErr(url + " : " + err.Error())
	}
	html := WrapHtmlBody(res.String())
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ParseErr("parse " + url + ": " + err.Error())
	}

	suppLocs := doc.Find(`div[class^=slider_small] div[class^=supply_card]`)
	iban := ""

	for i := 0; i < suppLocs.Length(); i++ {
		var ex Exception
		iban, ex = s.parseLocationIban(account, suppLocs, i)
		if iban != "" {
			break
		}
		if ex != nil {
			s.WithError(ex).Error("parse iban: ")
		}
	}
	if iban == "" {
		return ParseErr("could not parse iban")
	}

	loc.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &iban,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}
	return nil
}

func (s *AGSMEnergiaStrategy) parseLocationIban(account *model.Account, suppLocs *goquery.Selection, i int) (string, Exception) {
	dataEl := suppLocs.Eq(i).Find(`div[class=customer_data] p`)
	if dataEl.Length() != 2 {
		h, _ := dataEl.Html()
		return "", ParseErr("invalid format for div[class=customer_data] p: " + h)
	}
	ident := strings.TrimSpace(dataEl.Eq(0).Text())
	m, err := utils.MatchRegex(ident, identRegex)
	if err != nil {
		return "", ParseErr("parse iban regex: " + err.Error())
	}
	ident = m[0][1]
	//addr := strings.TrimSpace(dataEl.Eq(1).Text())
	//addr = string(utils.RemoveAdjacentUnicodeSpaces([]byte(addr)))
	//loc := &model.Location{
	//	Service:    ident,
	//	Identifier: addr,
	//}
	//account.AddLocation(loc)

	uri := fmt.Sprintf("https://countbox.agsm.it/supplies/%s/%s", s.customerCode, ident)
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "countbox.agsm.it",
		"referer":    "https://countbox.agsm.it/dashboard",
		"cookie":     s.phpsessid,
	}).Get(uri))
	if ex != nil {
		return "", ex
	}
	ibanEl := doc.Find(`div:containsOwn('IBAN')`)
	if ibanEl.Length() == 0 {
		return "", ParseErr("iban element not found")
	}
	ibanEl = ibanEl.Next()
	if ibanEl.Length() == 0 {
		return "", ParseErr("iban element not found")
	}
	iban := strings.TrimSpace(ibanEl.Text())
	return iban, nil
}

func (s *AGSMEnergiaStrategy) parseInvoices(account *model.Account) Exception {
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "countbox.agsm.it",
		"referer":    "https://countbox.agsm.it/dashboard",
		"cookie":     s.phpsessid,
	}).Get(bills))
	if ex != nil {
		return ex
	}
	bills := doc.Find(`table[id=bills_table] tbody tr`)
	for i := 0; i < bills.Length(); i++ {
		tds := bills.Eq(i).Find("td")
		status, ex := Attr(tds.Eq(1).Find("span"), "class")
		if ex != nil {
			return ex
		}

		ref := strings.TrimSpace(tds.Eq(2).Text())
		refP := strings.Split(ref, "/")
		ref = refP[1]
		ams := strings.TrimSpace(tds.Eq(3).Text())
		ams = strings.TrimSpace(ams[strings.Index(ams, " "):])
		am, ex := AmountFromString(ams)
		if ex != nil {
			return ParseErr("parse amount" + ex.Error())
		}

		issueS := strings.TrimSpace(strings.TrimSpace(tds.Eq(4).Text()))
		issueDate, err := time.Parse(invoiceFormat, issueS)
		if err != nil {
			return ParseErr("parse issue date: " + issueS)
		}
		dueS := strings.TrimSpace(strings.TrimSpace(tds.Eq(5).Text()))
		dueDate, err := time.Parse(invoiceFormat, dueS)
		if err != nil {
			return ParseErr("parse due date: " + dueS)
		}

		amDue := 0.0
		if strings.Contains(status, "red") {
			amDue = am
		} else {
			p := &model.Payment{
				Ref:    ref,
				Amount: am,
				Date:   dueDate.Format(utils.DBDateFormat),
			}
			account.Locations[0].AddPayment(p)
		}

		pdfUri, ex := Attr(tds.Eq(8).Find("a"), "href")
		if ex != nil {
			return ex
		}
		pdfUri = "https://countbox.agsm.it" + pdfUri

		inv := &model.Invoice{
			Ref:       ref,
			PdfUri:    &pdfUri,
			Amount:    am,
			AmountDue: amDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}

		account.Locations[0].AddInvoice(inv)
	}
	return nil
}

func (s *AGSMEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AGSMEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := s.downloadPdf(pdfUri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *AGSMEnergiaStrategy) LoadsInternal() bool {
	return true
}

func (s *AGSMEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *AGSMEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}

func (s *AGSMEnergiaStrategy) downloadPdf(uri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "countbox.agsm.it",
		"cookie":     s.phpsessid,
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("GET " + uri + " : " + err.Error())
	}
	return res.Body(), nil
}
