package aGSMEnergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "aGSM_Energia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"spaccapeloz@libero.it", "Ottobre2020"}, //login err
		{"spaccapeloz", "muddyengine70"}, //ok
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "spaccapeloz", Password: "Gentile2013", Uri: "muddyengine70.crawler",
			Identifier: "86242",
			Ref:        "208249", PdfUri: "https://countbox.agsm.it/bills/download/2021/208249/eng",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
