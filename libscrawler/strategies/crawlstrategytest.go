package strategies

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/sirupsen/logrus"
)

var InvalidCrawlError = &crawl.ParseException{
	Msg: "invalid crawl error",
}

func AccountTest(username, pass, uri string) crawl.Exception {
	crawler := crawl.New(logrus.New().WithField("pkg", uri+"_test"), &model.Account{
		Username: username,
		Password: &pass,
		Uri:      uri,
	})
	_, err := crawler.Parse()
	if err == nil {
		return nil
	}
	switch err := err.(type) {
	case *crawl.ParseException:
		return err
	case *crawl.LoginException:
		return err
	case *crawl.ConnectionException:
		return err
	default:
		return InvalidCrawlError
	}
}

func AccountExtractBarcode(username, pass, uri string) crawl.Exception {
	t := true
	crawler := crawl.New(logrus.New().WithField("pkg", uri+"_test"), &model.Account{
		Username:       username,
		Password:       &pass,
		Uri:            uri,
		ExtractBarcode: &t,
	})
	_, err := crawler.Parse()
	if err == nil {
		return nil
	}
	switch err := err.(type) {
	case *crawl.ParseException:
		return err
	case *crawl.LoginException:
		return err
	case *crawl.ConnectionException:
		return err
	default:
		return InvalidCrawlError
	}
}

type PdfRequest struct {
	Username   string
	Password   string
	Uri        string
	Identifier string
	Details    string
	Ref        string
	PdfUri     string
	IssueDate  string
	DueDate    string
}

func PdfTest(pdf *PdfRequest) *model.PdfResponse {
	crawler := crawl.New(logrus.New().WithField("pkg", pdf.Uri+"_test"), &model.Account{
		Username: pdf.Username,
		Password: &pdf.Password,
		Uri:      pdf.Uri,
		Locations: []*model.Location{
			{
				Identifier: pdf.Identifier,
				Details:    &pdf.Details,
				Invoices: []*model.Invoice{
					{
						Ref:       pdf.Ref,
						PdfUri:    &pdf.PdfUri,
						IssueDate: pdf.IssueDate,
						DueDate:   pdf.DueDate,
					},
				},
			},
		},
	})
	res := crawler.Pdf()
	return res
}
