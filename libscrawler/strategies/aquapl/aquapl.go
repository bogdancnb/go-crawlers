package aquapl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	root         = "https://ebok.aqua.com.pl/"
	sessionPHP   = "https://ebok.aqua.com.pl/session.php?"
	apiPHP       = "https://ebok.aqua.com.pl/api.php"
	jsonClient   = "https://ebok.aqua.com.pl/json.php?a=1"
	jsonLocs     = "https://ebok.aqua.com.pl/json.php?a=1&op=103"
	jsonInvoices = "https://ebok.aqua.com.pl/json.php/?a=2"
	jsonPays     = "https://ebok.aqua.com.pl/json.php/?a=4"

	siteFormat = "2006.01.02"
)

var (
	bankName        = "mBank Spolka Akcyjna"
	bankBIC         = "BREXPLPW"
	legalEntityName = "AQUA S.A. 43-300 Bielsko - Biała ul. 1 Maja 23"

	pdfUriRegex = regexp.MustCompile(`<a href="(.+)">.+`)
)

func Init() {
	RegisterFactory("aqua.crawler", new(AquaPLFactory))
}

type AquaPLFactory struct{}

func (p *AquaPLFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AquaPLStrategy")
	return &AquaPLStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type AquaPLStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	phpSession  string
	loginRes    struct {
		LoginOK struct {
			Logged int `json:"logged"`
		} `json:"login_ok"`
		LoginId interface{} `json:"login_id"`
		Admin   interface{} `json:"admin"`
		Message []string    `json:"message"`
	} //ok ex: {"login_ok":{"logged":1},"login_id":"4172","admin":"0","message":[]},  not ok:   {"login_ok":{"logged":0},"login_id":0,"admin":0,"message":["Nieprawid\u0142owy login i\/lub has\u0142o."]}
	jsonClient struct {
		ClientAttrs []*struct {
			Id        string      `json:"id"`
			Input     bool        `json:"input"`
			Label     string      `json:"label"`
			AttrType  string      `json:"type"`
			AttrValue interface{} `json:"value"`
		} `json:"client_attrs"`
	}
	jsonInvoices struct {
		Data []*struct {
			Ref          string `json:"nal_numer"`
			Id           string `json:"nal_id"`
			DueString    string `json:"nal_termin"`
			AmtString    string `json:"nal_kwota"`
			AmtDueString string `json:"nal_pozostalo"`
			PdfUri       string `json:"nal_obraz"`
		} `json:"data"`
	}
	jsonPayments struct {
		Data []*struct {
			Date      string `json:"wpl_data"`
			AmtString string `json:"wpl_kwota"`
			Ref       string `json:"wpl_tytul"`
		} `json:"data"`
	}
}

func (s *AquaPLStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AquaPLStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *AquaPLStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *AquaPLStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	res, ex := OkString(s.Client().R().Get(root))
	s.Trace(res)
	if ex != nil {
		return nil, ex
	}

	res, ex = OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":          "application/json, text/javascript, */*; q=0.01",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"referer":         "https://ebok.aqua.com.pl/",
		"X-Requested-Wit": "XMLHttpRequest",
	}).Get(sessionPHP))
	s.Trace(res)
	if ex != nil {
		return nil, ex
	}

	res, ex = OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":          "application/json, text/javascript, */*; q=0.01",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"content-type":    "application/login_idx-www-form-urlencoded; charset=UTF-8",
		"referer":         "https://ebok.aqua.com.pl/",
		"X-Requested-Wit": "XMLHttpRequest",
	}).SetFormData(map[string]string{
		"login":  account.Username,
		"passwd": *account.Password,
	}).Post(apiPHP))
	s.Debug("login response:", res)
	if ex != nil {
		return nil, ex
	}
	err := utils.FromJSON(&s.loginRes, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("parse login response: " + err.Error())
	}
	if s.loginRes.LoginOK.Logged == 0 {
		msg := "login error"
		if len(s.loginRes.Message) > 0 {
			msg = s.loginRes.Message[0]
		}
		return nil, LoginErr(msg)
	}
	return nil, nil
}

func (s *AquaPLStrategy) CheckLogin(response *string) Exception {
	//if strings.Contains(*response, loginErrMsg) {
	//	return LoginErr(loginErrMsg)
	//}
	return nil
}

func (s *AquaPLStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":          "application/json, text/javascript, */*; q=0.01",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"referer":         "https://ebok.aqua.com.pl/index.html",
		"X-Requested-Wit": "XMLHttpRequest",
	}).Get(jsonClient))
	s.Debug("client info=", res)
	if ex != nil {
		return ex
	}
	err := utils.FromJSON(&s.jsonClient, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse client info:" + err.Error())
	}

	address, iban := "", ""
	var clientCode float64
	for _, attr := range s.jsonClient.ClientAttrs {
		sVal, ok := attr.AttrValue.(string)
		switch attr.Label {
		case "Kod nabywcy:":
			i, okC := attr.AttrValue.(float64)
			if !okC {
				return ParseErr("parse cod client")
			}
			clientCode = i
		case "Adres:":
			if !ok {
				return ParseErr("parse address")
			}
			address = sVal
		case "Konto bankowe:":
			if !ok {
				return ParseErr("parse address")
			}
			iban = sVal
			//case "Saldo za wodę/ścieki:":
			//	if !ok {
			//		return ParseErr("parse address")
			//	}
			//	sold = sVal
		}
	}

	if clientCode == 0 {
		return ParseErr("could not parse client code")
	}
	if iban == "" || address == "" {
		return ParseErr(fmt.Sprintf(`could not find all elements iban=%s, address=%s`, iban, address))
	}
	clientCodeS := fmt.Sprintf("%d", int(clientCode))
	if address == "" {
		address = clientCodeS
	}
	iban = utils.RemoveUnicodeSpaces([]byte(iban))

	l := &model.Location{
		Service:    address,
		Identifier: clientCodeS,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalEntityName,
		},
	}
	account.AddLocation(l)
	return nil
}

func (s *AquaPLStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AquaPLStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":          "application/json, text/javascript, */*; q=0.01",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"referer":         "https://ebok.aqua.com.pl/index.html",
		"X-Requested-Wit": "XMLHttpRequest",
	}).Get(jsonInvoices))
	s.Debug("client info=", res)
	if ex != nil {
		return ex
	}
	err := utils.FromJSON(&s.jsonInvoices, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse invoices json: " + err.Error())
	}

	for _, i := range s.jsonInvoices.Data {
		amt, ex := s.parseAmount(i.AmtString)
		if ex != nil {
			return ex
		}
		amtDue, ex := s.parseAmount(i.AmtDueString)
		if ex != nil {
			return ex
		}
		match, err := utils.MatchRegex(i.PdfUri, pdfUriRegex)
		if err != nil {
			return ParseErr("match pdf uri regex for :" + i.PdfUri)
		}
		m := match[0][1]
		m = "https://ebok.aqua.com.pl/" + m
		dueDate, err := time.Parse(siteFormat, i.DueString)
		if err != nil {
			return ParseErr("due date:" + i.DueString + ": " + err.Error())
		}
		issueDate := utils.FirstOfMonth(&dueDate)
		inv := &model.Invoice{
			Ref:       i.Ref,
			PdfUri:    &m,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(inv)
	}
	return nil
}

func (s *AquaPLStrategy) parseAmount(ams string) (float64, Exception) {
	amp := strings.Split(ams, " ")
	if len(amp) == 0 {
		return 0.0, ParseErr("parse amount from string:" + ams)
	}
	amt, ex := AmountFromString(amp[0])
	if ex != nil {
		return 0.0, ex
	}
	return amt, nil
}

func (s *AquaPLStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":          "application/json, text/javascript, */*; q=0.01",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"referer":         "https://ebok.aqua.com.pl/index.html",
		"X-Requested-Wit": "XMLHttpRequest",
	}).Get(jsonPays))
	s.Debug("client info=", res)
	if ex != nil {
		return ex
	}
	err := utils.FromJSON(&s.jsonPayments, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse payments json:" + err.Error())
	}
	for _, d := range s.jsonPayments.Data {
		date, err := time.Parse(siteFormat, d.Date)
		if err != nil {
			return ParseErr("parse pay date " + d.Date + " : " + err.Error())
		}
		amt, ex := AmountFromString(d.AmtString)
		if ex != nil {
			return ex
		}
		p := &model.Payment{
			Ref:    d.Ref,
			Amount: amt,
			Date:   date.Format(utils.DBDateFormat),
		}
		location.AddPayment(p)
	}
	return nil
}

func (s *AquaPLStrategy) Pdf(account *model.Account) *model.PdfResponse {
	ref := account.Locations[0].Invoices[0].Ref
	account.Locations[0].Invoices = make([]*model.Invoice, 0, 0)
	s.LoadInvoices(account, account.Locations[0])
	pdfUri := ""
	for _, invoice := range account.Locations[0].Invoices {
		if ref == invoice.Ref {
			pdfUri = *invoice.PdfUri
		}
	}
	if pdfUri == "" {
		return model.PdfErrorResponse("not found", model.OTHER_EXCEPTION)
	}
	pdf, ex := s.pdfBytes(pdfUri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	if len(pdf) == 0 {
		return model.PdfErrorResponse("not found", model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdf,
		PdfStatus: model.OK.Name,
	}
}

func (s *AquaPLStrategy) pdfBytes(uri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"connection":      "keep-alive",
		"host":            "ebok.aqua.com.pl",
		"accept-encoding": "gzip, deflate, br",
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("pdf request: " + uri)
	}

	body := res.Body()
	//eof := []byte(`%%EOF`)
	//body = body[:bytes.Index(body, eof)+len(eof)]
	return body, nil
}

func (s *AquaPLStrategy) LoadsInternal() bool {
	return false
}

func (s *AquaPLStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *AquaPLStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
