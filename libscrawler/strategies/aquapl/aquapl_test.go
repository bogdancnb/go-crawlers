package aquapl

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "aqua.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"ghandzlik59@wp.pl", "19611959Gh"},
		//{"bogna_marek@op.pl", "Działka15"},
		{"aro154@interia.pl", "040477arekGg#"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "ghandzlik59@wp.pl", Password: "19611959Gh", Uri: "aqua.crawler",
			Identifier: "171193",
			Ref:        "20331054169", PdfUri: "https://clienti.aquavaslui.ro/page_index.php?action=copie_factura&id_factura=22119286&id_client=90729&tip_client=0&hash=f45b13462e221a2c62dc04fcc3bff391",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
