package zwikszczecin

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	login    = "https://ebok.zwikszczecin.pl/login"
	loginErr = "https://ebok.zwikszczecin.pl/login?error=BLEDNY_LOGIN_LUB_HASLO"
	podmiot  = "https://ebok.zwikszczecin.pl/trust/podmiot"
	facturi  = "https://ebok.zwikszczecin.pl/trust/faktury"

	siteFormat = "2006-01-02"
)

var (
	bankName  = "Powszechna Kasa Oszczędności Bank Polski SA"
	bankBIC   = "BPKOPLPW"
	legalName = "Przedsiębiorstwo Wodociągów i Kanalizacji"

	accNrRegexp = regexp.MustCompile(`Szczecin\s+([\w\s]+)\n`)
)

func Init() {
	RegisterFactory("zwik_szczecin.crawler", new(ZwikSzczecinFactory))
}

type ZwikSzczecinFactory struct{}

func (p *ZwikSzczecinFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "ZwikSzczecinStrategy")
	return &ZwikSzczecinStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type ZwikSzczecinStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	phpSession  string
	pid         string
}

func (s *ZwikSzczecinStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *ZwikSzczecinStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *ZwikSzczecinStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *ZwikSzczecinStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Host":       "ebok.zwikszczecin.pl",
		"Connection": "keep-alive",
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().Get(login)
	if err != nil {
		return nil, ConnectionErr("get " + login + ": " + err.Error())
	}
	s.phpSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	s.phpSession = s.phpSession[:strings.Index(s.phpSession, "; ")]
	doc, err := goquery.NewDocumentFromReader(bytes.NewReader(res.Body()))
	if err != nil {
		return nil, ParseErr("parsing get " + login + ": " + err.Error())
	}
	_csrf, ex := Attr(doc.Find("input[name='_csrf']"), "value")
	if ex != nil {
		return nil, ParseErr("parsing _csrf from GET " + login + ": " + ex.Error())
	}

	s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	res, err = s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.phpSession,
		"Origin":       "https://ebok.zwikszczecin.pl",
		"Refer":        login,
	}).SetFormData(map[string]string{
		"_csrf":    _csrf,
		"username": account.Username,
		"password": *account.Password,
	}).Post(login)
	if res == nil {
		return nil, ConnectionErr("POST " + login + " no response")
	}
	redirect := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	if redirect == loginErr {
		return nil, LoginErr("POST " + login + " redirected to " + loginErr)
	}
	if res.StatusCode() == 403 {
		return nil, LoginErr("POST " + login + " status code not 403")
	}
	s.phpSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	s.phpSession = s.phpSession[:strings.Index(s.phpSession, "; ")]
	if res.StatusCode() != 302 {
		return nil, ConnectionErr("POST " + login + " status code not 302")
	}

	res, err = s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.phpSession,
		"Origin":       "https://ebok.zwikszczecin.pl",
		"Refer":        login,
	}).Get(podmiot)
	if res == nil {
		return nil, ConnectionErr("POST " + login + " no response")
	}
	if res.StatusCode() != 302 {
		return nil, ConnectionErr("POST " + login + " status code not 302")
	}
	redirect = HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	if redirect == "" {
		return nil, ConnectionErr("POST " + login + " no redirect location in response")
	}
	s.pid = redirect[strings.Index(redirect, "podmiot")+len("podmiot"):]

	s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie": s.phpSession,
		"Refer":  login,
	}).Get(redirect))
	if ex != nil {
		return nil, ex
	}
	return nil, nil
}

func (s *ZwikSzczecinStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *ZwikSzczecinStrategy) LoadLocations(account *model.Account) Exception {
	locs := s.doc.Find("select[id='pid'] option")
	if locs.Length() == 0 {
		return ParseErr("LoadLocations: no location options found")
	}
	for i := 0; i < locs.Length(); i++ {
		txt := locs.Eq(i).Text()
		val, ex := Attr(locs.Eq(i), "value")
		if ex != nil {
			return ex
		}
		parts := strings.Split(txt, `|`)
		if len(parts) != 3 {
			return ParseErr("unknown format for location option: " + txt)
		}
		cod := parts[0]
		name := parts[1]
		adres := parts[2]

		location := &model.Location{
			Service:        name + ", " + adres,
			Identifier:     cod,
			AccountHelpers: val,
		}
		if exL, ok := s.exLocations[cod]; ok && exL.ProviderBranch != nil {
			location.ProviderBranch = exL.ProviderBranch
		}
		account.AddLocation(location)
	}
	return nil
}

func (s *ZwikSzczecinStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *ZwikSzczecinStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	var ex Exception
	pid := "?pid=" + location.AccountHelpers.(string)
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie": "CIASTECZKA_ZAAKCEPTOWANE=1; " + s.phpSession,
		//"Referer": "https://ebok.zwikszczecin.pl/trust/podmiot?" + s.pid,
	}).Get(facturi + pid))
	if ex != nil {
		return ex
	}
	rows := s.doc.Find("table[id='fakturyWykaz'] tbody tr")
	if rows.Length() == 0 {
		return ParseErr("load invoices: no invoice rows found")
	}
	for i := 0; i < rows.Length(); i++ {
		if ex := s.parseInvoice(location, rows.Eq(i).Find("td")); ex != nil {
			html, _ := rows.Eq(i).Html()
			return ParseErr("parse invoice " + html + ": " + ex.Error())
		}
	}
	return nil
}

func (s *ZwikSzczecinStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ref := utils.RemoveSpaces(cols.Eq(0).Text())
	issueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(2).Text()))
	if err != nil {
		html, _ := cols.Eq(2).Html()
		return ParseErr("parse issue date: " + html + ": " + err.Error())
	}
	dueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(3).Text()))
	if err != nil {
		html, _ := cols.Eq(3).Html()
		return ParseErr("parse due date: " + html + ": " + err.Error())
	}
	text := cols.Eq(4).Text()
	text = strings.ReplaceAll(text, ",", ".")
	text = utils.RemoveUnicodeSpaces([]byte(text))
	gross, ex := AmountFromString(text)
	if ex != nil {
		return ex
	}
	text = cols.Eq(5).Text()
	text = strings.ReplaceAll(text, ",", ".")
	text = utils.RemoveUnicodeSpaces([]byte(text))
	paid, ex := AmountFromString(text)
	if ex != nil {
		return ex
	}
	amountDue := gross - paid

	var pdfUri *string
	hrefs := cols.Eq(6).Find("a")
	if hrefs.Length() != 2 {
		html, _ := cols.Eq(6).Html()
		return ParseErr("no pdf button found: " + html)
	}
	uri, ex := Attr(hrefs.Eq(1), "href")
	if ex != nil {
		return ex
	}
	uri = "https://ebok.zwikszczecin.pl" + uri
	pdfUri = &uri
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    gross,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	if location.ProviderBranch == nil {
		if ex := s.parsePdf(location, invoice); ex != nil {
			return ParseErr("parse pdf for " + ref + ": " + ex.Error())
		}
	}
	return nil
}

func (s *ZwikSzczecinStrategy) parsePdf(location *model.Location, invoice *model.Invoice) Exception {
	pdfBytes, err := s.pdfDownload(invoice)
	if err != nil {
		return ParseErr("pdf download for " + invoice.Ref + ": " + err.Error())
	}
	txt, err := ParsePlainText(s.Entry, pdfBytes)
	if err != nil {
		return ParseErr("call to pdf parsing app: " + err.Error())
	}
	m, err := utils.MatchRegex(txt, accNrRegexp)
	if err != nil {
		s.Error("pdf text=", txt)
		return ParseErr("matching pdf text with regex " + accNrRegexp.String())
	}
	accNr := m[0][1]
	accNr = utils.RemoveSpaces(accNr)
	location.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNr,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}
	return nil
}

func (s *ZwikSzczecinStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *ZwikSzczecinStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	if err != nil {
		return model.PdfErrorResponse("error", model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *ZwikSzczecinStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Referer": facturi + s.pid,
	}).Get(*invoice.PdfUri)
	if err != nil {
		return nil, ConnectionErr("Get " + *invoice.PdfUri + ": " + err.Error())
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *ZwikSzczecinStrategy) LoadsInternal() bool {
	return false
}

func (s *ZwikSzczecinStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *ZwikSzczecinStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
