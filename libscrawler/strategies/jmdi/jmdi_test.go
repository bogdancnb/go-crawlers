package jmdi

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	tests := []struct {
		username string
		password string
		uri      string
	}{
		//{"057108", "81032900140", "jmdi.crawler"}, //ebok3
		//{"062152", "93100703274", "jmdi.crawler"}, //ebok3
		//{"052759", "68062505813", "jmdi.crawler"}, //ebok2
		//{"050910", "87070908948", "jmdi.crawler"}, //ebok2
		{"027944", "Bwxzde51#", "jmdi.crawler"}, //ebok2
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, test.uri); err != nil {
			t.Logf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
