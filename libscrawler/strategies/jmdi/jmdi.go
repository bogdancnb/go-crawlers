package jmdi

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

const (
	siteFormat    = "2006-01-02"
	selectorFaces = "input[name='javax.faces.ViewState']"
)

var (
	accessUrlTemplate   = "https://%s.jmdi.pl/login.xhtml"
	accountDataTemplate = "https://%s.jmdi.pl/pages/account_data.xhtml"
	invoicesTemplate    = "https://%s.jmdi.pl/pages/invoice.xhtml"
	paymentsTemplate    = "https://%s.jmdi.pl/pages/payment.xhtml"
	hostTemplate        = "%s.jmdi.pl"

	uriMap = map[string]string{
		"jmdi.crawler":  "ebok",
		"jmdi2.crawler": "ebok2",
		"jmdi3.crawler": "ebok3",
	}

	bankName        = "Alior Bank SA"
	bankBIC         = "ALBPPLPW"
	legalEntityName = ""
	loginErr        = []string{"Niepoprawna nazwa użytkownika lub hasło", "Logowanie nieudane"}
)

func Init() {
	f := new(JmdiFactory)
	RegisterFactory("jmdi.crawler", f)
	RegisterFactory("jmdi2.crawler", f)
	RegisterFactory("jmdi3.crawler", f)

}

type JmdiFactory struct{}

func (p *JmdiFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "JmdiStrategy")
	return &JmdiStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type JmdiStrategy struct {
	*logrus.Entry
	HttpStrategy
	accDoc         *goquery.Document
	javaxFacesView string
	doc            *goquery.Document

	accessUrl, accountData, invoices, payments, host string
}

func (s *JmdiStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *JmdiStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *JmdiStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *JmdiStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"host": s.host,
	})

	var html *string
	var ex Exception
	for _, v := range uriMap {
		s.accessUrl = fmt.Sprintf(accessUrlTemplate, v)
		s.accountData = fmt.Sprintf(accountDataTemplate, v)
		s.invoices = fmt.Sprintf(invoicesTemplate, v)
		s.payments = fmt.Sprintf(paymentsTemplate, v)
		s.host = fmt.Sprintf(hostTemplate, v)

		html, ex = s.doLogin(account)
		if ex != nil {
			return nil, ex
		}
		ex = s.CheckLogin(html)
		if ex != nil {
			s.Debug("login error for ", s.accessUrl)
			continue
		} else {
			s.Debug("login ok for ", s.accessUrl)
			break
		}
	}
	return html, ex
}

func (s *JmdiStrategy) doLogin(account *model.Account) (*string, Exception) {
	doc, ex := OkDocument(s.Client().R().Get(s.accessUrl))
	if ex != nil {
		return nil, ex
	}
	s.javaxFacesView, ex = Attr(doc.Find(selectorFaces), "value")
	if ex != nil {
		return nil, ex
	}
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"content-type": "application/x-www-form-urlencoded",
	}).SetFormData(map[string]string{
		"loginForm":               "loginForm",
		"loginForm:loginUserName": account.Username,
		"loginForm:loginPassword": *account.Password,
		"loginForm:loginButton":   "",
		"javax.faces.ViewState":   s.javaxFacesView,
		"hash":                    "null",
	}).Post(s.accessUrl))
	if ex != nil {
		return nil, ex
	}
	s.javaxFacesView, ex = Attr(doc.Find(selectorFaces), "value")
	if ex != nil {
		return nil, ex
	}
	html, _ := s.doc.Html()
	return &html, nil
}

func (s *JmdiStrategy) CheckLogin(response *string) Exception {
	if response == nil {
		return nil
	}
	for _, v := range loginErr {
		if strings.Contains(*response, v) {
			return LoginErr(v)
		}
	}
	return nil
}

func (s *JmdiStrategy) LoadLocations(account *model.Account) Exception {
	//amEl := s.doc.Find("td:containsOwn('Saldo')")
	//if amEl.Length() == 0 {
	//	return ParseErr("not found: td:containsOwn('Saldo')")
	//}
	//amS := strings.TrimSpace(amEl.Next().Text())
	//parts := strings.Split(amS, " ")
	//amS = strings.ReplaceAll(parts[0], ",", ".")
	//amount, err := strconv.ParseFloat(amS, 64)
	//if err != nil {
	//	return ParseErr("could not parse amount from " + amS)
	//}
	doc, ex := OkDocument(s.Client().R().Get(s.accountData))
	if ex != nil {
		return ex
	}
	s.javaxFacesView, ex = Attr(doc.Find(selectorFaces), "value")
	if ex != nil {
		return ex
	}
	ccEl := doc.Find("td:containsOwn('Numer klienta')")
	if ccEl.Length() == 0 {
		return ParseErr("not found: td:containsOwn('Numer klienta')")
	}
	clientCode := utils.RemoveSpaces(ccEl.Next().Text())
	nameEl := doc.Find("td:containsOwn('PESEL')")
	if nameEl.Length() == 0 {
		return ParseErr("not found: td:containsOwn('PESEL')")
	}
	pesel := utils.RemoveSpaces(nameEl.Next().Text())
	accEl := doc.Find("td:containsOwn('Konto bankowe')")
	if accEl.Length() == 0 {
		return ParseErr("not found: td:containsOwn('Konto bankowe')")
	}
	accNr := utils.RemoveSpaces(accEl.Next().Text())
	account.AddLocation(&model.Location{
		//Amount:     &amount,
		Service:    pesel,
		Identifier: clientCode,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &accNr,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalEntityName,
		},
	})
	return nil
}

func (s *JmdiStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *JmdiStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	doc, ex := OkDocument(s.Client().R().Get(s.invoices))
	if ex != nil {
		return ex
	}
	//s.javaxFacesView, ex = Attr(doc.Find(selectorFaces), "value")
	//if ex != nil {
	//	return ex
	//}
	html, _ := doc.Html()
	html = html[strings.Index(html, `<tbody id="invoiceTable_data"`):]
	html = html[:strings.Index(html, `</tbody></table>`)+16]
	doc, ex = DocFromString(`<html><body><table>` + html + `</body></html>`)
	if ex != nil {
		return ex
	}
	s.Trace(doc.Html())
	table := doc.Find("tbody[id='invoiceTable_data']")
	if table.Length() == 0 {
		return ParseErr("invoice table not found")
	}
	rows := table.Find("tr")
	for i := 0; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if cols.Length() != 7 {
			html, _ := rows.Eq(i).Html()
			return ParseErr("invalid invoice row: " + html)
		}
		if ex := s.parseInvoice(location, cols); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *JmdiStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ref := cols.Eq(0).Text()
	amount, ex := s.parseAmount(cols.Eq(1).Text())
	if ex != nil {
		return ex
	}
	amountDue, ex := s.parseAmount(cols.Eq(6).Text())
	if ex != nil {
		return ex
	}
	issueDate, err := time.Parse(siteFormat, cols.Eq(2).Text())
	if err != nil {
		return ParseErr("issue date: " + cols.Eq(2).Text())
	}
	dueDate, err := time.Parse(siteFormat, cols.Eq(3).Text())
	if err != nil {
		return ParseErr("due date: " + cols.Eq(3).Text())
	}
	invoice := &model.Invoice{
		Ref:       ref,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *JmdiStrategy) parseAmount(text string) (float64, Exception) {
	parts := strings.Split(text, " ")
	amS := strings.ReplaceAll(parts[0], ",", ".")
	amount, err := strconv.ParseFloat(amS, 64)
	if err != nil {
		return 0.0, ParseErr("could not parse amount: " + amS)
	}
	return amount, nil
}

func (s *JmdiStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	doc, ex := OkDocument(s.Client().R().Get(s.payments))
	if ex != nil {
		return ex
	}
	html, _ := doc.Html()
	html = html[strings.Index(html, `<tbody id="paymentTable_data"`):]
	html = html[:strings.Index(html, `</tbody></table>`)+16]
	doc, ex = DocFromString(`<html><body><table>` + html + `</body></html>`)
	if ex != nil {
		return ex
	}
	table := doc.Find("tbody[id='paymentTable_data']")
	if table.Length() == 0 {
		return ParseErr("payments table not found")
	}
	rows := table.Find("tr")
	for i := 0; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if ex := s.parsePayment(location, cols); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *JmdiStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	date, err := time.Parse(siteFormat, cols.Eq(0).Text())
	if err != nil {
		return ParseErr("could not parse date: " + cols.Eq(0).Text())
	}
	amount, ex := s.parseAmount(cols.Eq(1).Text())
	if ex != nil {
		return ex
	}
	ref := cols.Eq(2).Text()
	p := &model.Payment{
		Ref:    ref,
		Amount: amount,
		Date:   date.Format(utils.DBDateFormat),
	}
	location.AddPayment(p)
	return nil
}

func (s *JmdiStrategy) Pdf(account *model.Account) *model.PdfResponse {
	return nil
}

func (s *JmdiStrategy) LoadsInternal() bool {
	return false
}

func (s *JmdiStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *JmdiStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
