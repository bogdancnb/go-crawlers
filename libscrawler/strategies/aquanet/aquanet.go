package aquanet

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	login      = "https://ebok.aquanet.pl/user/login"
	switchForm = `https://ebok.aquanet.pl/platnik/zmien`
	faktury    = `https://ebok.aquanet.pl/faktury`

	siteFormat = "02.01.2006"

	loginErrMsg = `Błędne dane logowania`
)

var (
	bankName  = `Bank Gospodarstwa Krajowego`
	bankBIC   = `GOSKPLPW`
	legalName = `Aquanet SA`
)

func Init() {
	factory := new(AquanetFactory)
	RegisterFactory("aquanet.crawler", factory)
}

type AquanetFactory struct{}

func (p *AquanetFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AquanetStrategy")
	return &AquanetStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type AquanetStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *AquanetStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AquanetStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *AquanetStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *AquanetStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().
		SetHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3").
		Get(login))
	if ex != nil {
		return nil, ex
	}
	csrfpToken, ex := Attr(doc.Find(`input[name=csrfp_token]`), "value")
	if ex != nil {
		return nil, ex
	}

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"content-type": "application/x-www-form-urlencoded",
		"referer":      login,
	}).SetFormData(map[string]string{
		"csrfp_token":                csrfpToken,
		"user-login-email[email]":    account.Username,
		"user-login-email[password]": *account.Password,
		"user-login-email[submit]":   "Zaloguj",
	}).Post(login))
	if ex != nil {
		return nil, ex
	}
	html, err := s.doc.Html()
	if err != nil {
		return nil, ParseErr("parse login response: " + err.Error())
	}
	return &html, nil
}

func (s *AquanetStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *AquanetStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"acdept":           "*/*",
		"referer":          "https://ebok.aquanet.pl/",
		"x-requested-with": "XMLHttpRequest",
	}).Get(switchForm))
	if ex != nil {
		return ex
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(WrapHtmlBody(res)))
	if err != nil {
		return ParseErr("parse switchform response: " + err.Error())
	}
	locsEl := doc.Find(`div[class='act-target such-table'] table tbody tr`)

	for i := 0; i < locsEl.Length(); i++ {
		cols := locsEl.Eq(i).Find("td")
		codClient := strings.TrimSpace(cols.Eq(2).Text())
		addr := strings.TrimSpace(cols.Eq(3).Text())
		locId := ""
		locIdEl := cols.Eq(0).Find(`input`)
		if locIdEl.Length() >= 0 {
			locId, _ = Attr(locIdEl.Eq(0), "value")
		}

		if ex := s.switchLoc(ex, locId); ex != nil {
			return ex
		}

		location := &model.Location{
			Service:    addr,
			Identifier: codClient,
		}
		account.AddLocation(location)

		doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
			"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
			"referer": "https://ebok.aquanet.pl/",
		}).Get(faktury))
		if ex != nil {
			return ex
		}
		ibanEl := doc.Find(`span:containsOwn('Numer konta')`)
		if ibanEl.Length() == 0 {
			return ParseErr("iban element not found")
		}
		iban := ibanEl.Next().Text()
		iban = utils.RemoveUnicodeSpaces([]byte(iban))
		location.ProviderBranch = &model.ProviderBranchDTO{
			Iban:            &iban,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalName,
		}

		unpaidEl := doc.Find(`div[alt='Table_Invoice_Unpaid'] table tbody tr`)
		for i := 0; i < unpaidEl.Length(); i++ {
			if ex := s.parseInvoiceUnpaid(unpaidEl.Eq(i), location); ex != nil {
				return ex
			}
		}

		paidEl := doc.Find(`div[alt='Table_Invoice_Paid'] table tbody tr`)
		for i := 0; i < paidEl.Length(); i++ {
			if ex := s.parseInvoicePaid(paidEl.Eq(i), location); ex != nil {
				return ex
			}
		}

	}
	return nil
}

func (s *AquanetStrategy) switchLoc(ex Exception, locId string) Exception {
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"content-type": "application/x-www-form-urlencoded",
		"referer":      "https://ebok.aquanet.pl/",
	}).SetFormData(map[string]string{
		"buyer": locId,
	}).Post(switchForm))
	return ex
}

func (s *AquanetStrategy) parseInvoicePaid(row *goquery.Selection, l *model.Location) Exception {
	cols := row.Find("td")
	if cols.Length() == 1 {
		return nil
	}
	ref := strings.TrimSpace(cols.Eq(1).Text())
	issueString := strings.TrimSpace(cols.Eq(3).Text())
	issueDate, ex := TimeFromString(siteFormat, issueString)
	if ex != nil {
		return ex
	}
	dueString := strings.TrimSpace(cols.Eq(4).Text())
	dueDate, ex := TimeFromString(siteFormat, dueString)
	if ex != nil {
		return ex
	}
	amS := strings.TrimSpace(cols.Eq(5).Text())
	amP := strings.Split(amS, " ")
	if len(amP) != 2 {
		return ParseErr("invalid amount string: " + amS)
	}
	amt, ex := AmountFromString(amP[0])
	if ex != nil {
		return ex
	}

	uri, ex := Attr(cols.Eq(6).Find(`a`), "href")
	if ex != nil {
		return ex
	}
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    &uri,
		Amount:    amt,
		AmountDue: 0,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(invoice)
	return nil
}

func (s *AquanetStrategy) parseInvoiceUnpaid(row *goquery.Selection, l *model.Location) Exception {
	cols := row.Find("td")
	if cols.Length() == 1 {
		return nil
	}
	ref := strings.TrimSpace(cols.Eq(2).Text())
	issueString := strings.TrimSpace(cols.Eq(4).Text())
	issueDate, ex := TimeFromString(siteFormat, issueString)
	if ex != nil {
		return ex
	}
	dueString := strings.TrimSpace(cols.Eq(5).Text())
	dueDate, ex := TimeFromString(siteFormat, dueString)
	if ex != nil {
		return ex
	}
	amS := strings.TrimSpace(cols.Eq(6).Text())
	amP := strings.Split(amS, " ")
	if len(amP) != 2 {
		return ParseErr("invalid amount string: " + amS)
	}
	amt, ex := AmountFromString(amP[0])
	if ex != nil {
		return ex
	}
	amDueS := strings.TrimSpace(cols.Eq(7).Text())
	amDueP := strings.Split(amDueS, " ")
	if len(amP) != 2 {
		return ParseErr("invalid amount due string: " + amDueS)
	}
	amtDue, ex := AmountFromString(amDueP[0])
	if ex != nil {
		return ex
	}
	uri, ex := Attr(cols.Eq(8).Find(`a`), "href")
	if ex != nil {
		return ex
	}
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    &uri,
		Amount:    amt,
		AmountDue: amtDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	l.AddInvoice(invoice)
	return nil
}

func (s *AquanetStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AquanetStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AquanetStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AquanetStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, err := s.Client().R().Get(pdfUri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	body := res.Body()
	return &model.PdfResponse{
		Content:   body,
		PdfStatus: model.OK.Name,
	}
}

func (s *AquanetStrategy) LoadsInternal() bool {
	return true
}

func (s *AquanetStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *AquanetStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
