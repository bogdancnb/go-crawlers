package aquanet

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "aquanet.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"lobes86@gmail.com", "Lech1922"},
		//{"wojciech.krych@gmail.com", "kubA@1983"},
		//{"snopek.jakub@outlook.com", "deWit4none"},
		{"slawomir.walczak@coramico.org", "Hansklos123!"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		/*	{
			Username: "lobes86@gmail.com", Password: "Lech1922", Uri: "aquanet.crawler",
			Identifier: "0375-110-01",
			Ref:        "FRP/21/09/005267", PdfUri: "https://ebok.aquanet.pl/faktury/pobierz?invoice=2109605267",
		},*/{
			Username: "lobes86@gmail.com", Password: "Lech1922", Uri: "aquanet.crawler",
			Identifier: "0375-110-01",
			Ref:        "FRP/21/08/003553", PdfUri: "https://ebok.aquanet.pl/faktury/pobierz?invoice=2108603553",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
