package astanet

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "asta_net.crawler"
	tests := []struct {
		username string
		password string
		uri      string
	}{
		//{"100467", "shotokan24531", uri}, //ok
		{"135624", "BkyinkD6", uri},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, test.uri); err != nil {
			t.Logf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
