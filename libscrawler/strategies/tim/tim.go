package tim

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"math/rand"
	"net/url"
	"regexp"
	"strings"
	"time"
)

const (
	home        = "https://mytim.tim.it/login-authorize.html"
	auth        = `https://api.tim.it/auth/oauth/v2/authorize?`
	authLogin   = "https://api.tim.it/auth/oauth/v2/authorize/login"
	authConsent = "https://api.tim.it/auth/oauth/v2/authorize/consent"
	token       = "https://api.tim.it/auth/oauth/v2/token"
	consistenze = "https://api.tim.it/api/v2/consistenze"
	lst         = "https://api.tim.it/api/fatture/%s/LST"
	pdf         = "https://api.tim.it/api/downloadFatturaPDF/"

	chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz_-"

	siteFormat     = "02.01.2006"
	invoiceFormat  = "02/01/06"
	eInvoiceFormat = "2006-01-02T15:04:05"
)

var (
	r1 = regexp.MustCompile(`=`)
	r2 = regexp.MustCompile(`\+`)
	r3 = regexp.MustCompile(`\/`)
)

func Init() {
	RegisterFactory("tim.crawler", new(TimFactory))
}

type TimFactory struct{}

func (p *TimFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "TimStrategy")
	return &TimStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type TimStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	tokenJson
}

func (s *TimStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *TimStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *TimStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *TimStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().Get(home))
	if ex != nil {
		return nil, ex
	}
	loginAuthorizeForm := doc.Find("form[id=loginAuthorizeForm]")
	if loginAuthorizeForm.Length() == 0 {
		return nil, ConnectionErr("loginAuthorizeForm not found")
	}
	caringChallengeCode, params, ex := s.loginFormQueryParams(loginAuthorizeForm)
	if ex != nil {
		return nil, ex
	}
	uri := auth + params.Encode()
	s.Debug("auth uri: ", uri)

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Connection":                "keep-alive",
		"Host":                      "api.tim.it",
		"Referer":                   "https://mytim.tim.it/login-authorize.html",
		"Upgrade-Insecure-Requests": "1",
	}).Get(uri))
	//if err != nil {
	//	return nil, ConnectionErr("GET " + uri + " : " + err.Error())
	//}
	if ex != nil {
		return nil, ex
	}
	caringLoginForm := doc.Find("form[id=caringLoginForm]")
	if caringLoginForm.Length() == 0 {
		return nil, ParseErr("caringLoginForm not found")
	}
	sessionID, ex := Attr(caringLoginForm.Find("input[name=sessionID]"), "value")
	if ex != nil {
		return nil, ex
	}
	sessionData, ex := Attr(caringLoginForm.Find("input[name=sessionData]"), "value")
	if ex != nil {
		return nil, ex
	}
	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":       AcceptHeader,
		"Connection":   "keep-alive",
		"Content-Type": "application/x-www-form-urlencoded",
		"Host":         "api.tim.it",
		"Origin":       "https://api.tim.it",
	}).SetFormData(map[string]string{
		"sessionID":     sessionID,
		"sessionData":   sessionData,
		"username":      account.Username,
		"password":      *account.Password,
		"remember":      "true",
		"response_type": "",
		"response_mode": "",
		"action":        "login",
	}).Post(authLogin))
	if ex != nil {
		if strings.Contains(ex.Error(), "401") {
			return nil, LoginErr(ex.Error())
		}
		return nil, ex
	}
	form := doc.Find("form")
	if form.Length() == 0 {
		return nil, ParseErr("no consent form found")
	}
	sessionID, ex = Attr(form.Find("input[name=sessionID]"), "value")
	if ex != nil {
		return nil, ex
	}
	sessionData, ex = Attr(form.Find("input[name=sessionData]"), "value")
	if ex != nil {
		return nil, ex
	}

	s.EnableRedirects(false)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":       AcceptHeader,
		"Connection":   "keep-alive",
		"Content-Type": "application/x-www-form-urlencoded",
		"Host":         "api.tim.it",
		"Origin":       "https://api.tim.it",
	}).SetFormData(map[string]string{
		"action":        "grant",
		"sessionID":     sessionID,
		"sessionData":   sessionData,
		"response_type": "code",
		"response_mode": "query",
	}).Post(authConsent)
	s.Trace(err)
	if res == nil {
		return nil, ConnectionErr("no response POST " + authConsent)
	}
	location := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
	code := location[43:]
	code = code[:strings.Index(code, "&")]

	_, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":  AcceptHeader,
		"Referer": "https://api.tim.it/auth/oauth/v2/authorize/login",
	}).Get(location))
	if ex != nil {
		return nil, ex
	}

	resS, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":       "application/json, text/javascript, */*; q=0.01",
		"Connection":   "keep-alive",
		"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
		"Host":         "api.tim.it",
		"Origin":       "https://mytim.tim.it",
		"Referer":      location,
	}).SetFormData(map[string]string{
		"grant_type":    "authorization_code",
		"code":          code,
		"redirect_uri":  "https://mytim.tim.it/login-token.html",
		"client_id":     params["client_id"][0],
		"code_verifier": caringChallengeCode,
	}).Post(token))
	s.Debug(resS)
	if ex != nil {
		return nil, ex
	}
	s.EnableRedirects(true)
	err = utils.FromJSON(&s.tokenJson, strings.NewReader(resS))
	if err != nil {
		return nil, ParseErr("deserialize token response " + err.Error())
	}

	return nil, nil
}

type tokenJson struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string
	IdToken      string `json:"id_token"`
	IdTokenType  string `json:"id_token_type"`
}

func (s *TimStrategy) loginFormQueryParams(form *goquery.Selection) (string, url.Values, Exception) {
	responseType, ex := Attr(form.Find("input[name=response_type]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	clientid, ex := Attr(form.Find("input[name=client_id]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	redirectUri, ex := Attr(form.Find("input[name=redirect_uri]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	scope, ex := Attr(form.Find("input[name=scope]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	if scope == "" {
		scope = "openid oob"
	}
	prompt, ex := Attr(form.Find("input[name=prompt]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	if prompt == "" {
		prompt = "none SSO"
	}
	//nonce, ex := Attr(form.Find("input[name=nonce]"), "value")
	//if ex != nil {
	//	return nil, ex
	//}
	state, ex := Attr(form.Find("input[name=state]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	if state == "" {
		state = "https://mytim.tim.it/" //"https:\/\/mytim.tim.it\/"
	}

	//codeChallenge, ex := Attr(form.Find("input[name=code_challenge]"), "value")
	//if ex != nil {
	//	return nil, ex
	//}
	rString := generateRandomString(128)
	codeChallenge := generateCodeChallenge(rString)
	//codeChallenge := "_SD1sKm7NhcXFVF6SQkFvIF_bATpdQKA5U_9FwldIio"

	codeChallengeMethod, ex := Attr(form.Find("input[name=code_challenge_method]"), "value")
	if ex != nil {
		return "", nil, ex
	}
	params := url.Values{
		"response_type":         []string{responseType},
		"client_id":             []string{clientid},
		"redirect_uri":          []string{redirectUri},
		"scope":                 []string{scope},
		"prompt":                []string{prompt},
		"nonce":                 []string{buildNonce()},
		"state":                 []string{state},
		"code_challenge":        []string{codeChallenge},
		"code_challenge_method": []string{codeChallengeMethod},
	}
	return rString, params, nil
}

func buildNonce() string {
	nonce := ""
	rand.Seed(time.Now().Unix())
	for i := 0; i < 12; i++ {
		rNum := rand.Intn(len(chars))
		nonce += string(chars[rNum])
	}
	return nonce
}

func generateCodeChallenge(verifier string) string {
	sum256 := sha256.Sum256([]byte(verifier))
	h := sum256[:]
	enc := hex.EncodeToString(h)

	enc = base64.StdEncoding.EncodeToString(h)

	enc = string(r1.ReplaceAll([]byte(enc), []byte("")))
	enc = string(r2.ReplaceAll([]byte(enc), []byte("-")))
	enc = string(r3.ReplaceAll([]byte(enc), []byte("_")))
	return enc
}

func generateRandomString(length int) string {
	var text = ""
	rand.Seed(time.Now().Unix())
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	for i := 0; i < length; i++ {
		text += string(possible[rand.Intn(len(possible))])
	}
	return text
}

func (s *TimStrategy) CheckLogin(response *string) Exception {
	if s.AccessToken == "" {
		return LoginErr("no access token: " + fmt.Sprintf("%q", s.tokenJson))
	}
	return nil
}

func (s *TimStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json",
		"Authorization": "Bearer " + s.AccessToken,
		"Channel":       "MYTIMWEB",
		"Connection":    "keep-alive",
		"Host":          "api.tim.it",
		"Origin":        "https://mytim.tim.it",
		"Referer":       "https://mytim.tim.it/it.html",
		"SourceSystem":  "WEB",
		//"InteractionDate-Date":"2020-10-26",
		//"InteractionDate-Time":"07:21:08.004",
		"BusinessID":    "c0b2429e1f924f40868aa136",
		"MessageID":     "c351ec8891994b2c9784f917",
		"SessionID":     "a1c550b0174c4139ace4b91a",
		"TransactionID": "1041CFECF53D4C7F81B7E5B2",
	}).Get(consistenze))
	s.Debug(res)
	if !strings.Contains(res, `"status":"OK"`) {
		return ParseErr("invalid location response: " + res)
	}
	if ex != nil {
		return ex
	}
	var locJson struct {
		Sim []struct {
			Msisdn              string
			Type                string
			Ambito              string
			Stato               string
			ElegibleForFavorite int
			Migrata             string
			Prefix              string
		}
	}
	err := utils.FromJSON(&locJson, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserialize locations json: " + err.Error())
	}
	for _, l := range locJson.Sim {
		loc := &model.Location{
			Service:        l.Msisdn + " " + l.Ambito,
			Details:        nil,
			Identifier:     l.Msisdn,
			AccountHelpers: l.Type,
		}
		account.AddLocation(loc)
	}
	return nil
}

func (s *TimStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TimStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	if location.AccountHelpers.(string) == "PP" {
		s.Debug("prepay location, no invoices")
		return nil
	}

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json",
		"Authorization": "Bearer " + s.AccessToken,
		"Channel":       "MYTIMWEB",
		"Connection":    "keep-alive",
		"Host":          "api.tim.it",
		"Origin":        "https://mytim.tim.it",
		"Referer":       "https://mytim.tim.it/it.html",
		"SourceSystem":  "WEB",
		//"InteractionDate-Date":"2020-10-26",
		//"InteractionDate-Time":"07:21:08.004",
		"BusinessID":    "321fe31745ae481dad2649a8",
		"MessageID":     "c8ce9948ab4b4b0e8952ee4d",
		"SessionID":     "b0e3040d40b6474ab49a3d47",
		"TransactionID": "A0B5AE548ADB4FD4AD03E69A",
	}).Get(fmt.Sprintf(lst, location.Identifier))
	s.Trace(res)
	if err != nil {
		return ConnectionErr("GET " + lst + " : " + err.Error())
	}
	if !strings.Contains(res.String(), `"status":"OK"`) {
		return ParseErr("invalid invoices response: " + res.String())
	}
	var invoiceJson struct {
		RifCliente string
		Fattura    []*struct {
			NumeroFattura     string
			NomeFattura       string
			DataEmissione     string
			DataScadenza      string
			ImportoFattura    string
			StatoPagamento    string
			MetodoDiPagamento string
			AnnoEmissione     string
			MeseEmissione     string
			DownloadPDF       bool
			InfoDashBoard     struct {
				Importo   string
				LabelDash string
			}
			FatturaNavigabile bool
		}
	}
	err = utils.FromJSON(&invoiceJson, strings.NewReader(res.String()))
	if err != nil {
		s.Debug(res.String())
		return ParseErr("parse json response: " + err.Error())
	}
	location.Details = &invoiceJson.RifCliente
	for _, f := range invoiceJson.Fattura {
		amt, ex := AmountFromString(f.ImportoFattura)
		if ex != nil {
			return ex
		}
		issueDate, err := time.Parse(invoiceFormat, f.DataEmissione)
		if err != nil {
			return ParseErr("issue date " + f.DataEmissione)
		}
		dueDate, err := time.Parse(invoiceFormat, f.DataScadenza)
		if err != nil {
			return ParseErr("due date " + err.Error())
		}
		amtDue := 0.0
		if strings.ToLower(f.StatoPagamento) == "da pagare" {
			amtDue = amt
		}
		uri := f.AnnoEmissione + "|" + f.MeseEmissione
		i := &model.Invoice{
			Ref:       f.NumeroFattura,
			PdfUri:    &uri,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
	}
	return nil
}

func (s *TimStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *TimStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	parts := strings.Split(pdfUri, "|")
	if len(parts) != 2 {
		return model.PdfErrorResponse("invalid pdf uri "+pdfUri, model.OTHER_EXCEPTION)
	}

	body := `{"anno":"%s","mese":"%s","numeroFattura":"%s"}`
	body = fmt.Sprintf(body, parts[0], parts[1], invoice.Ref)
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json",
		"Authorization": "Bearer " + s.AccessToken,
		"BusinessID":    "625f1428a0004f8db686883c",
		"Channel":       "MYTIMWEB",
		"Content-Type":  "application/json;charset=UTF-8",
		//"InteractionDate-Date": "2020-10-26",
		//"InteractionDate-Time": "08:32:35.543",
		"MessageID":     "70e17ab1fe9b4ca08ed749aa",
		"SessionID":     "b55c4b342943473bb8f250b3",
		"SourceSystem":  "WEB",
		"TransactionID": "2DC3903307A54B15B1F7DCAE",
		"Connection":    "keep-alive",
		"Host":          "api.tim.it",
		"Origin":        "https://mytim.tim.it",
		"Referer":       "https://mytim.tim.it/it/fatture.html",
	}).SetBody(body).Post(pdf + account.Locations[0].Identifier))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *TimStrategy) LoadsInternal() bool {
	return false
}

func (s *TimStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *TimStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
