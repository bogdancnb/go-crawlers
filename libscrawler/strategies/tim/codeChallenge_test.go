package tim

import "testing"

func Test_generateCodeChallenge(t *testing.T) {
	tests := []struct {
		caringChallengeCode string
		codeChallenge       string
	}{
		{
			caringChallengeCode: "n7RtVbcDNk0nswcZll2T0juxNjOmM0zHhDtsJBSkbsqnv3uuTsxQk3jSdidXvRnUn03zqAo1UR3fnDLmOfbm6w9k8YA8Eib3YiokMzKzqzOSt7my3zAq8mSCGaY6qmLK",
			codeChallenge:       "ZWua2LdNUvk6PUHc183AA-yPAgUx6ETuRO3HW5B-QVE",
		},
	}
	for _, tt := range tests {
		got := generateCodeChallenge(tt.caringChallengeCode)
		t.Logf("\ngot =%s, \nwant=%s", got, tt.codeChallenge)
		if got != tt.codeChallenge {
			t.Error("code challenge failed")
		}
	}
}
