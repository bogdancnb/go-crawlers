package tim

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "tim.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"marianobarbara48@gmail.com", "Atnsi200348!"}, //ok
		{"favasole@gmail.com", "Giuliano123@"}, //ok
		//{"luigi.casciotti@gmail.com", "@Tim2020.."}, //ok due
		//{"perazzimassimo3@gmail.com", "AreCChinO3"}, //login err
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "marianobarbara48@gmail.com", Password: "Atnsi200348!", Uri: "tim.crawler",
			Identifier: "0918146019",
			Ref:        "RV04847765", PdfUri: "2020|10",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
