package hera

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	login    = "https://servizionline.gruppohera.it/auth/hera/login"
	apiLogin = "https://servizionline.gruppohera.it/api/v1/user/login"

	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("hera.crawler", new(HeraFactory))
}

type HeraFactory struct{}

func (p *HeraFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "HeraStrategy")
	return &HeraStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type HeraStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *HeraStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *HeraStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *HeraStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *HeraStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "servizionline.gruppohera.it",
	}).Get(login))
	if ex != nil {
		return nil, ex
	}

	payload := fmt.Sprintf(`{"username":"%s","password":"%s"}`, account.Username, *account.Password)
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"connection":       "keep-alive",
		"content-type":     "application/json;charset=UTF-8",
		"host":             "servizionline.gruppohera.it",
		"origin":           "https://servizionline.gruppohera.it",
		"referer":          "https://servizionline.gruppohera.it/auth/hera/login",
		"X-Bwb-PlatformId": "web",
		"X-Bwb-Referer":    "HERA",
	}).SetBody(payload).Post(apiLogin)
	if res == nil {
		return nil, ConnectionErr("no repsonse for POST " + apiLogin)
	}
	if err != nil {
		return nil, ConnectionErr("POST  " + apiLogin + " : " + err.Error())
	}
	s.Debug(res.String())
	s.Error(err)
	var loginRes struct {
		Reason  string
		Code    int
		Message string
	}
	err = utils.FromJSON(&loginRes, strings.NewReader(res.String()))
	if err != nil {
		return nil, ParseErr(fmt.Sprintf("parse login response body %v : %v", loginRes, err))
	}
	if loginRes.Code == 404 || loginRes.Code == 401 {
		return nil, LoginErr(loginRes.Message)
	}
	return nil, nil
}

func (s *HeraStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *HeraStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *HeraStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *HeraStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *HeraStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *HeraStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *HeraStrategy) LoadsInternal() bool {
	return false
}

func (s *HeraStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *HeraStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
