package aquavas

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "aquavas.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"RusuMarius", "787880"},
		//{"MilenaMarusac", "Milena1!"},
		{"Olarucamelia", "edutdragut"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestAccountHusi(t *testing.T) {
	Init()
	uri := "aquavas.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"lucianicoleta", "e74Yem7V"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "DAZ", Password: "adriandragos", Uri: "aquavas_husi_.crawler",
			Identifier: "921203",
			Ref:        "10198863", PdfUri: "https://clienti.aquavaslui.ro/page_index.php?action=copie_factura&id_factura=22370302&id_client=55420&tip_client=5&hash=ce71ad237aa13f811632d743786b87ba",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
