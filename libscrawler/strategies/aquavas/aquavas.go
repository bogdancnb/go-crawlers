package aquavas

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/andybalholm/brotli"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"strings"
	"time"
)

const (
	login     = "https://clienti.aquavaslui.ro/index.php?action=login"
	clients   = "https://clienti.aquavaslui.ro/index.php?action=clienti_ace"
	invoices  = "https://clienti.aquavaslui.ro/index.php?action=facturi"
	invoices2 = "https://clienti.aquavaslui.ro/index.php?action=facturi&step=2"
	plati     = "https://clienti.aquavaslui.ro/index.php?action=incasari"
	plati2    = "https://clienti.aquavaslui.ro/index.php?action=incasari&step=2"
	contact   = "https://clienti.aquavaslui.ro/index.php?action=contact"

	siteFormat  = "02.01.2006"
	loginErrMsg = "Combinatia utilizator/parola nu exista in baza de date sau contul este inactiv"
)

var (
	barcodes = map[string]struct {
		cui  string
		name string
	}{
		"aquavas.crawler":        {"22586122", "Vaslui"},
		"aquavas_husi_.crawler":  {"27913802", "Husi"},
		"aquavas_barlad.crawler": {"22586149", "Barlad"},
	}
)

func Init() {
	factory := new(AquavasFactory)
	RegisterFactory("aquavas.crawler", factory)
	RegisterFactory("aquavas_husi_.crawler", factory)
}

type AquavasFactory struct{}

func (p *AquavasFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AquavasStrategy")
	return &AquavasStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type AquavasStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations    map[string]*model.Location
	exInvoices     map[string]*model.Invoice
	phpSession     string
	clientInfos    map[string]*clientInfo
	doc            *goquery.Document
	selectedOption string
}

func (s *AquavasStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AquavasStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *AquavasStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

type clientInfo struct {
	option                                string
	hash                                  string
	start, start_zi, start_luna, start_an string
	stop, stop_zi, stop_luna, stop_an     string
}

func (s *AquavasStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.9",
		"Connection":      "keep-alive",
		"Host":            "clienti.aquavaslui.ro",
		"user-agent":      AgentChrome78,
	})

	res, err := s.Client().R().Get(login)
	if err != nil {
		return nil, ConnectionErr(err.Error())
	}
	s.phpSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "PHPSESSID")
	s.phpSession = s.phpSession[:strings.Index(s.phpSession, "; ")]

	res, err = s.Client().R().
		SetHeaders(map[string]string{
			"referer": "https://clienti.aquavaslui.ro/index.php?action=login",
			"Cookie":  "GDPR=3",
		}).Get(login)
	if err != nil {
		return nil, ConnectionErr(err.Error())
	}

	resp, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       "GDPR=3; " + s.phpSession,
		"Referer":      "https://clienti.aquavaslui.ro/index.php?action=login",
	}).SetFormData(map[string]string{
		"action":     "login",
		"utilizator": account.Username,
		"parola":     *account.Password,
	}).Post(login))
	if ex != nil {
		return nil, ex
	}

	return &resp, nil
}

func (s *AquavasStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *AquavasStrategy) LoadLocations(account *model.Account) Exception {
	//s.Client().SetHeader("Cookie", "GDPR=3; "+s.phpSession)
	doc, ex := OkDocument(s.Client().R().
		SetHeaders(map[string]string{
			"referer": "https://clienti.aquavaslui.ro/index.php?action=login",
			"Cookie":  "GDPR=3; " + s.phpSession,
		}).
		Get("https://clienti.aquavaslui.ro/index.php?action=status"))
	if ex != nil {
		return ex
	}
	response, err := s.Client().R().
		SetHeaders(map[string]string{
			"referer": "https://clienti.aquavaslui.ro/index.php?action=status",
			"Cookie":  "GDPR=3; " + s.phpSession,
		}).
		Get(clients)
	if err != nil {
		return ConnectionErr(clients + ": " + err.Error())
	}
	//respBody, ex := decodeBrotli(bytes.NewBuffer(response.Body()))
	//if ex != nil {
	//	return ex
	//}
	doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(response.Body()))
	if err != nil {
		return ParseErr("clients doc: " + err.Error())
	}

	locs := doc.Find("table[class='table'] tbody tr[class^='tableRow']")
	if locs.Length() == 0 {
		return ParseErr("no locations table found")
	}
	for i := 0; i < locs.Length(); i++ {
		cols := locs.Eq(i).Find("td")
		details := strings.TrimSpace(cols.Eq(2).Text())
		location := &model.Location{
			Service:    strings.TrimSpace(cols.Eq(1).Text()) + ", " + strings.TrimSpace(cols.Eq(3).Text()),
			Details:    &details,
			Identifier: strings.TrimSpace(cols.Eq(0).Text()),
		}
		account.AddLocation(location)
	}

	if ex := s.getSelectedOption(account.Uri); ex != nil {
		return ex
	}
	utils.SleepMillis(3000, 4000)
	if ex := s.getDocument(invoices, contact); ex != nil {
		return ex
	}
	return nil
}

func (s *AquavasStrategy) getDocument(url string, referer string) Exception {
	response, err := s.Client().R().SetHeaders(map[string]string{
		"Cookie":  "GDPR=3; " + s.phpSession,
		"Referer": referer,
	}).Get(url)
	if err != nil {
		return ConnectionErr(url + ":" + err.Error())
	}
	//respBody, ex := decodeBrotli(bytes.NewBuffer(response.Body()))
	//if ex != nil {
	//	return ex
	//}
	s.doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(response.Body()))
	if err != nil {
		return ParseErr(url + " doc: " + err.Error())
	}
	return nil
}

func decodeBrotli(src io.Reader) ([]byte, Exception) {
	reader := brotli.NewReader(src)
	respBody, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, ParseErr("brotli reader: " + err.Error())
	}
	return respBody, nil
}

func (s *AquavasStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AquavasStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	utils.SleepMillis(4000, 4500)

	if len(s.clientInfos) > 0 {
		s.Debug("client options already parsed")
	} else {
		if ex := s.parseClientOptions(s.doc); ex != nil {
			return ex
		}
	}
	info := s.clientInfos[location.Identifier]

	response, err := s.Client().R().SetHeaders(map[string]string{
		"referer":      "https://clienti.aquavaslui.ro/index.php?action=facturi",
		"Content-Type": "application/x-www-form-urlencoded",
		"origin":       "https://clienti.aquavaslui.ro",
		"Cookie":       "GDPR=3", /* + s.phpSession*/
	}).SetFormData(map[string]string{
		"id_client_ace": info.option,
		"start":         info.start,
		"start_zi":      info.start_zi,
		"start_luna":    info.start_luna,
		"start_an":      info.start_an,
		"stop":          info.stop,
		"stop_zi":       info.stop_zi,
		"stop_luna":     info.stop_luna,
		"stop_an":       info.stop_an,
		"hash":          info.hash,
	}).Post(invoices2)
	if err != nil {
		return ConnectionErr(invoices2 + ":" + err.Error())
	}
	//respBody, ex = decodeBrotli(bytes.NewBuffer(response.Body()))
	//if ex != nil {
	//	return ex
	//}
	s.doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(response.Body()))
	if err != nil {
		return ParseErr("invoices2 doc: " + err.Error())
	}
	rows := s.doc.Find("tr[class^='tableRow']")
	if rows.Length() == 0 {
		s.Warn("no invoices found:")
		s.Warn(s.doc.Html())
		return nil
	}

	for i := 0; i < rows.Length(); i++ {
		if ex := s.parseInvoice(location, rows.Eq(i), account.Uri, s.selectedOption); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *AquavasStrategy) getSelectedOption(uri string) Exception {
	res, err := s.Client().R().
		SetHeaders(map[string]string{
			"Cookie":  s.phpSession,
			"referer": "https://clienti.aquavaslui.ro/index.php?action=clienti_ace",
			"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		}).Get(contact)

	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return ParseErr("contact doc: " + err.Error())
	}

	options := doc.Find("select[id^='id_centru'] option")

	options.Each(func(i int, selection *goquery.Selection) {
		_, exists := selection.Attr("selected")
		if exists == true {
			s.selectedOption = selection.Text()
			s.Info("selectedOption=", s.selectedOption)
		}
	})

	errMsg := `Ați înregistrat un cont de Aquavas %s, însă contul dumneavoastră este asociat pe site-ul furnizorului pentru %s`
	if s.selectedOption == "Husi" && uri != "aquavas_husi_.crawler" {
		return ParseErr(fmt.Sprintf(errMsg, barcodes[uri].name, s.selectedOption))
	}
	if s.selectedOption == "Vaslui" && uri != "aquavas.crawler" {
		return ParseErr(fmt.Sprintf(errMsg, barcodes[uri].name, s.selectedOption))
	}
	if s.selectedOption == "Barlad" && uri != "aquavas_barlad.crawler" {
		return ParseErr(fmt.Sprintf(errMsg, barcodes[uri].name, s.selectedOption))
	}
	return nil
}

func (s *AquavasStrategy) parseInvoice(location *model.Location, row *goquery.Selection, uri string, selectedOption string) Exception {
	cols := row.Find("td")
	ref := strings.TrimSpace(cols.Eq(0).Text())
	issueString := strings.TrimSpace(cols.Eq(1).Text())
	issueDate, err := time.Parse(siteFormat, issueString)
	if err != nil {
		return ParseErr("parse date: " + issueString)
	}
	dueDate := issueDate.Add(15 * 60 * 24 * time.Minute)
	amount, ex := AmountFromSelection(cols.Eq(4))
	if ex != nil {
		return ex
	}
	amountDue, ex := AmountFromSelection(cols.Eq(5))
	if ex != nil {
		return ex
	}
	pdfUri, ex := Attr(cols.Eq(6).Find("a"), "href")
	if ex != nil {
		return ex
	}
	pdfUri = "https://clienti.aquavaslui.ro/" + pdfUri
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    &pdfUri,
		Amount:    amount,
		AmountDue: amountDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	barcode, ex := s.generateBarcode(uri, location, invoice, issueDate, selectedOption)
	if ex != nil {
		s.WithError(ex).Error("barcode generate error")
	} else {
		invoice.BarCode = &barcode
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *AquavasStrategy) generateBarcode(uri string, location *model.Location, invoice *model.Invoice, issueDate time.Time, selectedOption string) (string, Exception) {
	info := barcodes[uri]

	if selectedOption == "Husi" {
		info = barcodes["aquavas_husi_.crawler"]
	}

	if selectedOption == "Vaslui" {
		info = barcodes["aquavas.crawler"]
	}

	if selectedOption == "Barlad" {
		info = barcodes["aquavas_barlad.crawler"]
	}

	identifier := location.Identifier
	identifier = fmt.Sprintf(`%06s`, identifier)

	ref := invoice.Ref
	ref = fmt.Sprintf(`%08s`, ref)

	dateString := issueDate.Format("02012006")

	amountToUse := invoice.Amount
	if invoice.AmountDue > 0 {
		amountToUse = invoice.AmountDue
	}
	if amountToUse < 0 {
		return "", ParseErr("Could not generate barcode for negative amount.")
	}
	amountString := decimal.NewFromFloat(amountToUse).Mul(decimal.NewFromInt(100)).String()
	amountString = fmt.Sprintf("%010s", amountString)

	barcode := fmt.Sprintf(`%s%s%s%s%s`, info.cui, identifier, ref, dateString, amountString)
	return barcode, nil
}

func (s *AquavasStrategy) parseClientOptions(doc *goquery.Document) Exception {
	s.clientInfos = make(map[string]*clientInfo)
	options := doc.Find("select[id='id_client_ace'] option")
	options.Each(func(i int, selection *goquery.Selection) {
		val, ok := selection.Attr("value")
		if !ok {
			s.Error("could not parse client options")
			s.Error(selection.Html())
			return
		}
		id := strings.Split(selection.Text(), ` - `)[0]
		s.clientInfos[id] = &clientInfo{option: val}
	})
	if len(s.clientInfos) != options.Length() {
		return ParseErr("not all client options parsed")
	}
	html, err := doc.Html()
	if err != nil {
		return ParseErr("parsing invoices html: " + err.Error())
	}
	start, ex := Attr(doc.Find("input[id='start']"), "value")
	if ex != nil {
		return ex
	}
	start_zi, ex := Attr(doc.Find("input[id='start_zi']"), "value")
	if ex != nil {
		return ex
	}
	start_luna, ex := Attr(doc.Find("input[id='start_luna']"), "value")
	if ex != nil {
		return ex
	}
	start_an, ex := Attr(doc.Find("input[id='start_an']"), "value")
	if ex != nil {
		return ex
	}
	stop, ex := Attr(doc.Find("input[id='stop']"), "value")
	if ex != nil {
		return ex
	}
	stop_zi, ex := Attr(doc.Find("input[id='stop_zi']"), "value")
	if ex != nil {
		return ex
	}
	stop_luna, ex := Attr(doc.Find("input[id='stop_luna']"), "value")
	if ex != nil {
		return ex
	}
	stop_an, ex := Attr(doc.Find("input[id='stop_an']"), "value")
	if ex != nil {
		return ex
	}
	for id := range s.clientInfos {
		begin := fmt.Sprintf(`optionsHash[%s] ="`, s.clientInfos[id].option)
		end := `";`
		match := html[strings.Index(html, begin)+len(begin):]
		match = match[:strings.Index(match, end)]
		s.clientInfos[id].hash = match
		s.clientInfos[id].start = start
		s.clientInfos[id].start_zi = start_zi
		s.clientInfos[id].start_luna = start_luna
		s.clientInfos[id].start_an = start_an
		s.clientInfos[id].stop = stop
		s.clientInfos[id].stop_zi = stop_zi
		s.clientInfos[id].stop_luna = stop_luna
		s.clientInfos[id].stop_an = stop_an
	}
	return nil
}

func (s *AquavasStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	if ex := s.getDocument(plati, "https://clienti.aquavaslui.ro/index.php?action=facturi&step=2"); ex != nil {
		return ex
	}

	info := s.clientInfos[location.Identifier]

	response, err := s.Client().R().SetHeaders(map[string]string{
		"referer":      "https://clienti.aquavaslui.ro/index.php?action=incasari",
		"Content-Type": "application/x-www-form-urlencoded",
		"origin":       "https://clienti.aquavaslui.ro",
		"Cookie":       "GDPR=3", /* + s.phpSession*/
	}).SetFormData(map[string]string{
		"id_client_ace": info.option,
		"start":         info.start,
		"start_zi":      info.start_zi,
		"start_luna":    info.start_luna,
		"start_an":      info.start_an,
		"stop":          info.stop,
		"stop_zi":       info.stop_zi,
		"stop_luna":     info.stop_luna,
		"stop_an":       info.stop_an,
		"hash":          info.hash,
	}).Post(plati2)
	if err != nil {
		return ConnectionErr(plati2 + ":" + err.Error())
	}
	//respBody, ex := decodeBrotli(bytes.NewBuffer(response.Body()))
	//if ex != nil {
	//	return ex
	//}
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(response.Body()))
	if err != nil {
		return ParseErr("plati2 doc: " + err.Error())
	}

	rows := doc.Find("tr[class^='tableRow']")
	if rows.Length() == 0 {
		s.Warn("no payments found:")
		s.Warn(doc.Html())
		return nil
	}
	for i := 0; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if cols.Length() == 4 {
			dateS := strings.TrimSpace(cols.Eq(0).Text())
			date, err := time.Parse(siteFormat, dateS)
			if err != nil {
				return ParseErr("parisng date " + dateS)
			}
			ref := strings.TrimSpace(cols.Eq(1).Text())
			amS := strings.TrimSpace(cols.Eq(3).Text())
			amt, ex := AmountFromString(amS)
			if ex != nil {
				return ex
			}
			details := strings.TrimSpace(cols.Eq(2).Text())
			p := &model.Payment{
				Ref:     ref,
				Amount:  amt,
				Date:    date.Format(utils.DBDateFormat),
				Details: &details,
			}
			location.AddPayment(p)
		}
	}

	if ex := s.getDocument(invoices, "https://clienti.aquavaslui.ro/index.php?action=incasari&step=2"); ex != nil {
		return ex
	}

	return nil
}

func (s *AquavasStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, err := s.Client().R().SetHeader("Cookie", "GDPR=3"+s.phpSession).Get(pdfUri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	//resBody, ex := decodeBrotli(bytes.NewBuffer(res.Body()))
	//if ex != nil {
	//	return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	//}
	resp := string(res.Body())
	begin := `window.location="`
	end := `"`
	uri := resp[strings.Index(resp, begin)+len(begin):]
	uri = uri[:strings.Index(uri, end)]
	uri = "https://clienti.aquavaslui.ro/" + uri
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Cookie":  "GDPR=3" + s.phpSession,
		"referer": pdfUri,
	}).Get(uri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	body := res.Body()
	return &model.PdfResponse{
		Content:   body,
		PdfStatus: model.OK.Name,
	}
}

func (s *AquavasStrategy) pdfBytes(uri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"accept-encoding": "gzip, deflate, br",
		"cache-control":   "max-age=0",
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("pdf request: " + uri)
	}

	body := res.Body()
	eof := []byte(`%%EOF`)
	body = body[:bytes.Index(body, eof)+len(eof)]
	return body, nil
}

func (s *AquavasStrategy) LoadsInternal() bool {
	return false
}

func (s *AquavasStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *AquavasStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
