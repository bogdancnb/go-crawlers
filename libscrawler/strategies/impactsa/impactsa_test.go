package impactsa

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "impact_developer.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"andhangan@gmail.com", "blocc52ap42"},
		//{"andrei.costea@icloud.com","I3rheD8pYU"},
		{"bogdan.udriste@gmail.com", "264326Bogdan"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "andhangan@gmail.com", Password: "blocc52ap42", Uri: "impact_developer.crawler",
			Identifier: "P5C52_AP42",
			Ref:        "FIMP.21.22035.BU", PdfUri: "https://helpdesk.impactsa.ro/invoices/download/FIMP.21.22035.BU",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
