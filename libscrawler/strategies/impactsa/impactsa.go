package impactsa

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	login    = `https://helpdesk.impactsa.ro/login`
	invoices = `https://helpdesk.impactsa.ro/invoices/list`
	home     = `https://helpdesk.impactsa.ro/home`
	ticket   = `https://helpdesk.impactsa.ro/ticket/list`
	payments = `https://helpdesk.impactsa.ro/payments/list`

	siteFormat  = "02.01.2006"
	loginErrMsg = "Parola sau utilizator incorecte"
	allPaidMsg  = `Momentan nu figurați cu facturi neachitate. Vă mulțumim!`
)

var codPartnerRegex = regexp.MustCompile(`Cod partner:\s*(\w+)\s*`)

func Init() {
	factory := new(ImpactSAFactory)
	RegisterFactory("impact_developer.crawler", factory)
}

type ImpactSAFactory struct{}

func (p *ImpactSAFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "ImpactSAStrategy")
	return &ImpactSAStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type ImpactSAStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations   map[string]*model.Location
	exInvoices    map[string]*model.Invoice
	token         string
	xsrfToken     string
	impactSession string
	html          string
	doc           *goquery.Document
}

func (s *ImpactSAStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *ImpactSAStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *ImpactSAStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *ImpactSAStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
	}).Get(login)
	if err != nil {
		return nil, ConnectionErr("GET login " + err.Error())
	}
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(res.String()))
	if err != nil {
		return nil, ParseErr("parse login response: " + err.Error())
	}
	var ex Exception
	s.token, ex = Attr(doc.Find(`input[name='_token']`), "value")
	if ex != nil {
		return nil, ex
	}
	s.parseCookieHeaders(res)

	s.EnableRedirects(false)

	loginPost, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":       AcceptHeader,
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.xsrfToken + "; " + s.impactSession,
	}).SetFormData(map[string]string{
		"_token":   s.token,
		"email":    account.Username,
		"password": *account.Password,
	}).Post(login)
	if loginPost == nil {
		return nil, ConnectionErr("no response for login post")
	}
	s.parseCookieHeaders(loginPost)
	if loginPost.StatusCode() != 302 {
		return nil, ConnectionErr("response for login post got status " + loginPost.Status())
	}
	location := HeaderByNameAndValStartsWith(loginPost.Header(), "Location", "")
	if location == login {
		return nil, LoginErr(loginErrMsg)
	}
	if location != home {
		return nil, ConnectionErr("response for login post got redirect ro " + location)
	}

	homeGet, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
		"Cookie": s.xsrfToken + "; " + s.impactSession,
	}).Get(home)
	if homeGet == nil {
		return nil, ConnectionErr("no response for home request")
	}
	s.parseCookieHeaders(homeGet)
	if homeGet.StatusCode() != 302 {
		return nil, ConnectionErr("response for home request got status " + homeGet.Status())
	}
	location = HeaderByNameAndValStartsWith(homeGet.Header(), "Location", "")
	if location != ticket {
		return nil, ConnectionErr("response for home request got redirect ro " + location)
	}

	ticketGet, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
		"Cookie": s.xsrfToken + "; " + s.impactSession,
	}).Get(ticket)
	if ticketGet == nil {
		return nil, ConnectionErr("no response for ticket request")
	}
	s.parseCookieHeaders(ticketGet)
	if ticketGet.StatusCode() != 200 {
		return nil, ConnectionErr("response for home ticket got status " + ticketGet.Status())
	}

	s.EnableRedirects(true)

	html := ticketGet.String()
	return &html, nil
}

func (s *ImpactSAStrategy) parseCookieHeaders(res *resty.Response) {
	s.xsrfToken = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "XSRF-TOKEN")
	s.xsrfToken = s.xsrfToken[:strings.Index(s.xsrfToken, `;`)]
	s.impactSession = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "impact_developer_contractor_sa_helpdesk_session")
	s.impactSession = s.impactSession[:strings.Index(s.impactSession, `;`)]
}

func (s *ImpactSAStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *ImpactSAStrategy) LoadLocations(account *model.Account) Exception {
	get, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
		"Cookie": s.xsrfToken + "; " + s.impactSession,
	}).Get(invoices)
	s.parseCookieHeaders(get)
	var ex Exception
	s.doc, ex = OkDocument(get, err)
	if ex != nil {
		return ex
	}

	s.html, err = s.doc.Html()
	if err != nil {
		return ParseErr("parse invoices response html: " + err.Error())
	}
	matches, err := utils.MatchRegex(s.html, codPartnerRegex)
	if err != nil {
		return ParseErr("could not parse codPartner regex: " + err.Error())
	}
	codPartner := matches[0][1]
	l := &model.Location{
		Service:    account.Username + ", " + codPartner,
		Identifier: codPartner,
	}
	account.AddLocation(l)
	return nil
}

func (s *ImpactSAStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	amt := 0.0
	if strings.Contains(s.html, allPaidMsg) {
		location.Amount = &amt
		return nil
	}
	//todo caz total de plata
	location.Amount = &amt
	return nil
}

func (s *ImpactSAStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	tables := s.doc.Find(`.table`)
	if tables.Length() == 2 {
		unpaidRows := tables.Eq(0).Find("tbody tr")
		if ex := s.parseUnpaid(location, unpaidRows); ex != nil {
			return ex
		}
		paidRows := tables.Eq(1).Find("tbody tr")
		if ex := s.parsePaid(location, paidRows); ex != nil {
			return ex
		}
	} else if tables.Length() == 1 {
		paidRows := tables.Eq(0).Find("tbody tr")
		if ex := s.parsePaid(location, paidRows); ex != nil {
			return ex
		}
	} else {
		s.Warnf("no invoice rows found")
	}
	return nil
}

func (s *ImpactSAStrategy) parseUnpaid(location *model.Location, rows *goquery.Selection) Exception {
	for i := 1; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if cols.Length() != 6 {
			html, _ := rows.Eq(i).Html()
			return ParseErr("invalid invoice row: " + html)
		}
		ref := cols.Eq(0).Text()
		issueString := cols.Eq(1).Text()
		issueDate, err := time.Parse(siteFormat, issueString)
		if err != nil {
			return ParseErr("parse date " + issueString)
		}
		dueString := cols.Eq(2).Text()
		dueDate, err := time.Parse(siteFormat, dueString)
		if err != nil {
			return ParseErr("parse date " + dueString)
		}

		amString := cols.Eq(3).Text()
		amt, ex := AmountFromString(amString)
		if ex != nil {
			return ex
		}

		var pdfUri *string
		href, ex := Attr(cols.Eq(5).Find("a"), "href")
		if ex != nil || href == "" {
			html, _ := rows.Eq(i).Html()
			s.Error("no pdf link found: " + html)
		} else {
			pdfUri = &href
		}

		invoice := &model.Invoice{
			Ref:       ref,
			PdfUri:    pdfUri,
			Amount:    amt,
			AmountDue: amt,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(invoice)
	}
	return nil
}

func (s *ImpactSAStrategy) parsePaid(location *model.Location, rows *goquery.Selection) Exception {
	for i := 1; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		if cols.Length() != 4 {
			html, _ := rows.Eq(i).Html()
			return ParseErr("invalid invoic row: " + html)
		}
		ref := cols.Eq(0).Text()
		issueString := cols.Eq(1).Text()
		issueDate, err := time.Parse(siteFormat, issueString)
		if err != nil {
			return ParseErr("parse date " + issueString)
		}
		dueString := cols.Eq(2).Text()
		dueDate, err := time.Parse(siteFormat, dueString)
		if err != nil {
			return ParseErr("parse date " + dueString)
		}

		var pdfUri *string
		href, ex := Attr(cols.Eq(3).Find("a"), "href")
		if ex != nil || href == "" {
			html, _ := rows.Eq(i).Html()
			s.Error("no pdf link found: " + html)
		} else {
			pdfUri = &href
		}

		invoice := &model.Invoice{
			Ref:       ref,
			PdfUri:    pdfUri,
			Amount:    0,
			AmountDue: 0,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(invoice)
	}
	return nil
}

func (s *ImpactSAStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	get, err := s.Client().R().SetHeaders(map[string]string{
		"Accept": AcceptHeader,
		"Cookie": s.xsrfToken + "; " + s.impactSession,
	}).Get(payments)
	s.parseCookieHeaders(get)
	var ex Exception
	s.doc, ex = OkDocument(get, err)
	if ex != nil {
		return ex
	}
	s.html, err = s.doc.Html()
	if err != nil {
		return ParseErr("parse payments response html: " + err.Error())
	}

	table := s.doc.Find(`table[class='table']`)
	if table.Length() == 0 {
		s.Warnf("payments table not found")
		return nil
	}
	rows := table.Find("tbody tr")
	if rows.Length() < 2 {
		s.Warnf("payments rows not found")
		return nil
	}
	for i := 1; i < rows.Length(); i++ {
		cols := rows.Eq(i).Find("td")
		html, _ := cols.Eq(i).Html()
		if cols.Length() != 6 {
			s.Warnf("invalid payment row " + html)
		}
		ref := cols.Eq(3).Text()
		ref = utils.RemoveUnicodeSpaces([]byte(ref))
		amString := cols.Eq(4).Text()
		amt, ex := AmountFromString(amString)
		if ex != nil {
			return ex
		}
		dateString := cols.Eq(1).Text()
		dateP := strings.Split(dateString, " ")
		if len(dateP) != 2 {
			return ParseErr("invalid date format " + dateString)
		}
		dateString = dateP[0]
		date, err := time.Parse(siteFormat, dateString)
		if err != nil {
			return ParseErr("parse date string " + dateString)
		}
		p := &model.Payment{
			Ref:    ref,
			Amount: amt,
			Date:   date.Format(utils.DBDateFormat),
		}
		location.AddPayment(p)
	}
	return nil
}

func (s *ImpactSAStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	res, err := s.Client().R().SetHeaders(map[string]string{
		//"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		//"Content-type":"application/x-www-form-urlencoded",
	}).Get(pdfUri)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	body := res.Body()
	return &model.PdfResponse{
		Content:   body,
		PdfStatus: model.OK.Name,
	}
}

func (s *ImpactSAStrategy) LoadsInternal() bool {
	return false
}

func (s *ImpactSAStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *ImpactSAStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
