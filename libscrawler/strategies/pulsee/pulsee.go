package pulsee

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const (
	home               = "https://pulsee.it/energia/login"
	createToken        = "https://pulsee.it/middleware/api/publicArea/createToken"
	login              = "https://pulsee.it/middleware/api/publicArea/login"
	getAlertDisservizi = "https://pulsee.it/configuration/getAlertDisservizi?"
	mulesoftRequest    = "https://pulsee.it/middleware/api/privateArea/mulesoftRequest"
	sapRequest         = "https://pulsee.it/middleware/api/privateArea/sapRequest"
	pdfUri             = "https://pulsee.it/middleware/api/privateArea/createTokenDownloadDocument/"

	accountPayload = `{"transactionId":"%s","flowCode":"WEB","serviceCode":"0060","requestJson":[{"accountId":"%s","company":"PULSEE","infoType":"profile;supply"}]}`
	invReqFormat   = "2006-01-02"
)

func Init() {
	RegisterFactory("pulsee.crawler", new(PulseeFactory))
}

type PulseeFactory struct{}

func (p *PulseeFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "PulseeStrategy")
	return &PulseeStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type PulseeStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc                   *goquery.Document
	accessToken           string
	publicAreaTokenValue  string
	privateAreaTokenValue string
	loginRes              loginResponse
	info                  accountInfo
}

func (s *PulseeStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *PulseeStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *PulseeStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

type loginResponse struct {
	CorrelationId string `json:"correlationId"`
	StatusCode    int    `json:"statusCode"`
	StatusMessage string `json:"statusMessage"`
	Data          struct {
		TransactionId string `json:"transactionId"`
		FlowCode      string `json:"flowCode"`
		ResponseJson  []struct {
			AccountId   string `json:"accountId"`
			Company     string `json:"company"`
			Description string `json:"description"`
			Code        string `json:"code"`
			Result      string `json:"result"`
		} `json:"responseJson"`
		ServiceCode string `json:"serviceCode"`
	} `json:"data"`
	Token struct {
		AccessToken string `json:"access_token"`
	} `json:"token"`
}

func (s *PulseeStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":     AcceptHeader,
		"Connection": "keep-alive",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}

	httpRes, err := s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer null",
		"Connection":    "keep-alive",
		"Content-Type":  "application/json",
		"Host":          "pulsee.it",
		"Origin":        "https://pulsee.it",
		"Referer":       "https://pulsee.it/energia/login",
	}).SetBody(`{}`).Put(createToken)
	if err != nil {
		return nil, ConnectionErr("PUT " + createToken + ": " + err.Error())
	}
	var resBody struct {
		AccessToken string `json:"access_token"`
	}
	err = utils.FromJSON(&resBody, bytes.NewBuffer(httpRes.Body()))
	if err != nil {
		return nil, ParseErr("deserializing token response " + err.Error())
	}
	s.accessToken = resBody.AccessToken
	s.publicAreaTokenValue = HeaderByNameAndValStartsWith(httpRes.Header(), "Set-Cookie", "publicAreaTokenValue")
	s.publicAreaTokenValue = s.publicAreaTokenValue[:strings.Index(s.publicAreaTokenValue, "; ")]

	hash := md5.Sum([]byte(*account.Password))
	encodedPass := hex.EncodeToString(hash[:])
	payload := fmt.Sprintf(`{"username":"%s","password":"%s","company":"PULSEE"}`, account.Username, encodedPass)
	httpRes, err = s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer " + s.accessToken,
		"Connection":    "keep-alive",
		"Host":          "pulsee.it",
		"Origin":        "https://pulsee.it",
		"Referer":       "https://pulsee.it/energia/login",
	}).SetBody(payload).Post(login)
	if err != nil {
		return nil, ConnectionErr("POST " + login + ": " + err.Error())
	}
	s.privateAreaTokenValue = HeaderByNameAndValStartsWith(httpRes.Header(), "Set-Cookie", "privateAreaTokenValue")
	s.privateAreaTokenValue = s.privateAreaTokenValue[:strings.Index(s.privateAreaTokenValue, "; ")]
	err = utils.FromJSON(&s.loginRes, bytes.NewBuffer(httpRes.Body()))
	if err != nil {
		return nil, ParseErr("deserialize login response: " + err.Error())
	}
	res := httpRes.String()
	return &res, nil
}

func (s *PulseeStrategy) CheckLogin(response *string) Exception {
	if s.loginRes.Token.AccessToken == "" {
		if s.loginRes.Data.ResponseJson[0].Description == "Invalid Password" {
			return LoginErr("Invalid Password")
		} else {
			return ParseErr("unknown invalid response " + *response)
		}
	}
	return nil
}

type accountInfo struct {
	CorrelationId string `json:"correlationId"`
	StatusCode    int    `json:"statusCode"`
	DebugMessage  string `json:"debugMessage"`
	StatusMessage string `json:"statusMessage"`
	Data          struct {
		TransactionId string `json:"transactionId"`
		FlowCode      string `json:"flowCode"`
		ServiceCode   string `json:"serviceCode"`
		ResponseJson  []*struct {
			AccountId string `json:"accountId"`
			Code      string `json:"code"`
			InfoType  struct {
				Profile struct {
					InfoAccount struct {
						AccountId          string `json:"accountId"`
						BusinessPartnerKey string `json:"businessPartnerKey"`
						ClientType         string `json:"clientType"`
						Company            string `json:"company"`
						CustomerCode       string `json:"customerCode"`
						Email              string `json:"email"`
						FiscalCode         string `json:"fiscalCode"`
						LastName           string `json:"lastName"`
						MobilePhone        string `json:"mobilePhone"`
						Name               string `json:"name"`
					} `json:"infoAccount"`
				} `json:"profile"`
				Supply []*struct {
					InfoAsset         interface{} `json:"infoAsset"`
					InfoCadastral     interface{} `json:"infoCadastral"`
					InfoOffer         interface{} `json:"infoOffer"`
					InfoPaymentMethod struct {
						BillingProfileId string `json:"billingProfileId"`
						BillingType      string `json:"billingType"`
						Sdd              struct {
							Iban string `json:"iban"`
						} `json:"sdd"`
					} `json:"infoPaymentMethod"`
					InfoPoint struct {
						AcciseValue        string      `json:"acciseValue"`
						AssetId            string      `json:"assetId"`
						BillingType        string      `json:"billingType"`
						CommodityType      string      `json:"commodityType"`
						DocIdList          interface{} `json:"docIdList"`
						IsResidentAtSupply bool        `json:"isResidentAtSupply"`
						OrderItemId        string      `json:"orderItemId"`
						PodPdr             string      `json:"podPdr"`
						SerialNumber       string      `json:"serialNumber"`
						StatusOrderItem    string      `json:"statusOrderItem"`
						SupplyAddress      string      `json:"supplyAddress"`
						SupplyCap          string      `json:"supplyCap"`
						SupplyCity         string      `json:"supplyCity"`
						SupplyCivic        string      `json:"supplyCivic"`
						SupplyNation       string      `json:"supplyNation"`
						SupplyProvince     string      `json:"supplyProvince"`
					} `json:"infoPoint"`
				} `json:"supply"`
			} `json:"infoType"`
		} `json:"responseJson"`
	} `json:"data"`
}

func (s *PulseeStrategy) LoadLocations(account *model.Account) Exception {
	accountId := s.loginRes.Data.ResponseJson[0].AccountId //0013Y00002dgzpKQAQ
	transactionId := buildTransactionId()
	payload := fmt.Sprintf(accountPayload, transactionId, accountId)
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"accountId":     accountId,
		"Authorization": "Bearer " + s.loginRes.Token.AccessToken,
		"Cookie":        s.privateAreaTokenValue,
		"Connection":    "keep-alive",
		"Content-Type":  "application/json",
		"Host":          "pulsee.it",
		"Origin":        "https://pulsee.it",
		"Referer":       "https://pulsee.it/energia/login",
	}).SetBody(payload).Post(mulesoftRequest))
	if ex != nil {
		return ex
	}
	s.Debug(res)
	err := utils.FromJSON(&s.info, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserialize account info json: " + err.Error())
	}

	if len(s.info.Data.ResponseJson) == 0 && s.info.StatusCode != 200 {
		return ParseErr("account info request error: " + s.info.StatusMessage + ", " + s.info.DebugMessage)
	}

	customerCode := ""
	businessPartnerKey := ""
	for _, acc := range s.info.Data.ResponseJson {
		customerCode = acc.InfoType.Profile.InfoAccount.CustomerCode
		businessPartnerKey = acc.InfoType.Profile.InfoAccount.BusinessPartnerKey
		for _, supply := range acc.InfoType.Supply {
			addr := supply.InfoPoint.SupplyAddress + " " + supply.InfoPoint.SupplyCivic + " " + supply.InfoPoint.SupplyCity +
				" " + supply.InfoPoint.SupplyProvince + " " + supply.InfoPoint.SupplyCap
			l := &model.Location{
				Service:        addr,
				Details:        &customerCode,
				Identifier:     supply.InfoPoint.PodPdr,
				AccountHelpers: businessPartnerKey,
			}
			account.AddLocation(l)
		}
	}

	if ex := s.loadInvoicesInternal(account, accountId, customerCode, businessPartnerKey); ex != nil {
		return ex
	}
	return nil
}

type invoiceInfo struct {
	CustomerCode  string `json:"CUSTOMER_CODE"`
	ServiceCode   string `json:"serviceCode"`
	TransactionID string `json:"transactionID"`
	ResponseJson  struct {
		Invoices []struct {
			Id               string      `json:"@id"`
			AUGBT            string      `json:"AUGBT"`
			BETRH            string      `json:"BETRH"`
			BUDAT            string      `json:"BUDAT"`
			EXT_UI           string      `json:"EXT_UI"`
			FAEDN            string      `json:"FAEDN"`
			OPBEL            interface{} `json:"OPBEL"`
			PYMET            string      `json:"PYMET"`
			SPART            string      `json:"SPART"`
			TOTAL_AMNT       string      `json:"TOTAL_AMNT"`
			VERTRAG          string      `json:"VERTRAG"`
			XBLNR            string      `json:"XBLNR"`
			ZAUGBDT          string      `json:"ZAUGBDT"`
			ZCOMP            string      `json:"ZCOMP"`
			ZCOUNT           string      `json:"ZCOUNT"`
			ZMOR             interface{} `json:"ZMOR"`
			ZRAT             interface{} `json:"ZRAT"`
			ZRATE            string      `json:"ZRATE"`
			ZRATEB           interface{} `json:"ZRATEB"`
			ZRATEIZ          interface{} `json:"ZRATEIZ"`
			ZSOLL            interface{} `json:"ZSOLL"`
			ZTIPO_FAT        string      `json:"ZTIPO_FAT"`
			INVOICE_DOC_LIST []struct {
				DOC_ID   string `json:"DOC_ID"`
				DOC_NAME string `json:"DOC_NAME"`
				DOC_TYPE string `json:"DOC_TYPE"`
			} `json:"INVOICE_DOC_LIST"`
		} `json:"INVOICES"`
	} `json:"responseJson"`
}

func (s *PulseeStrategy) loadInvoicesInternal(account *model.Account, accountId string, customerCode string, businessPartnerKey string) Exception {
	format := `{"transactionId":"%s","flowCode":"LDGR","SAP_FUNCTION_MODULE":"ZFI_CRM_FAT_H","serviceCode":"0055","CUSTOMER_CODE":"%s","requestJson":[{"X_GPART":"%s"}]}`
	payload := fmt.Sprintf(
		format,
		buildTransactionId(), customerCode, businessPartnerKey,
	)
	res, ex := s.invoiceReq(accountId, payload)
	if ex != nil {
		return ex
	}
	//format = `{"transactionId":"%s","flowCode":"LDGR","SAP_FUNCTION_MODULE":"ZFI_CRM_FAT_H","serviceCode":"0055","CUSTOMER_CODE":"%s","requestJson":[{"X_GPART":"%s","X_BUDAT_A":"%s","X_BUDAT_DA":"%s"}]}`
	//now := time.Now()
	//payload = fmt.Sprintf(
	//	format,
	//	buildTransactionId(), customerCode, businessPartnerKey,
	//	now.AddDate(-1, 0, 0).Format(invReqFormat), now.Format(invReqFormat),
	//)
	//res, ex = s.invoiceReq(accountId, payload)
	//if ex != nil {
	//	return ex
	//}

	var info invoiceInfo
	err := utils.FromJSON(&info, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserialize invoice json " + err.Error())
	}
	for _, i := range info.ResponseJson.Invoices {
		issueDate, err := time.Parse(invReqFormat, i.BUDAT)
		if err != nil {
			return ParseErr("issue date: " + i.BUDAT)
		}
		dueDate, err := time.Parse(invReqFormat, i.FAEDN)
		if err != nil {
			return ParseErr("due date " + i.FAEDN)
		}
		amt, err := strconv.ParseFloat(i.TOTAL_AMNT, 64)
		if err != nil {
			return ParseErr("amount " + i.TOTAL_AMNT)
		}
		amtDue, err := strconv.ParseFloat(i.BETRH, 64)
		if err != nil {
			return ParseErr("amount due " + i.BETRH)
		}
		var pUri *string
		for _, doc := range i.INVOICE_DOC_LIST {
			if doc.DOC_TYPE == "Sintesi" {
				uri := pdfUri + doc.DOC_ID + "|" + doc.DOC_NAME
				pUri = &uri
				break
			}
		}
		inv := &model.Invoice{
			Ref:       i.XBLNR,
			PdfUri:    pUri,
			Amount:    amt,
			AmountDue: amtDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		l := s.locationByPod(account, i.EXT_UI)
		if l == nil {
			return ParseErr("no location matched for POD " + i.EXT_UI)
		}
		l.AddInvoice(inv)
	}
	return nil
}

func (s *PulseeStrategy) locationByPod(account *model.Account, pod string) *model.Location {
	if pod == "" {
		return nil
	}
	for _, location := range account.Locations {
		if pod == location.Identifier {
			return location
		}
	}
	return nil
}

func (s *PulseeStrategy) invoiceReq(accountId string, payload string) (string, Exception) {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"accountId":     accountId,
		"Authorization": "Bearer " + s.loginRes.Token.AccessToken,
		"Cookie":        s.privateAreaTokenValue,
		"Connection":    "keep-alive",
		"Content-Type":  "application/json",
		"Host":          "pulsee.it",
		"Origin":        "https://pulsee.it",
	}).SetBody(payload).Post(sapRequest))
	if ex != nil {
		return res, ex
	}
	s.Debug(res)
	if strings.Contains(res, `"statusCode": 500`) {
		return res, ParseErr("invalid invoice response " + res)
	}
	return res, nil
}

func buildTransactionId() interface{} {
	n := ""
	e := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	rand.Seed(time.Now().Unix())
	for t := 0; t < 15; t++ {
		n = n + string(e[rand.Intn(len(e))])
	}
	return n
}

func (s *PulseeStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PulseeStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PulseeStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *PulseeStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *PulseeStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *PulseeStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	parts := strings.Split(pdfUri, "|")
	if len(parts) != 2 {
		return model.PdfErrorResponse("invalid pdf uri, must have 2 parts separated by |", model.OTHER_EXCEPTION)
	}
	accountId := s.loginRes.Data.ResponseJson[0].AccountId
	var token struct {
		Token string
	}
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"accountId":     accountId,
		"Authorization": "Bearer " + s.loginRes.Token.AccessToken,
		"Cookie":        s.privateAreaTokenValue,
		"Connection":    "keep-alive",
		"Content-Type":  "application/json",
		"Host":          "pulsee.it",
		"Origin":        "https://pulsee.it",
	}).SetBody(`{}`).SetResult(&token).Put(parts[0]))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	s.Debug(res)

	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"Accept":     AcceptHeader,
		"Connection": "keep-alive",
		"Cookie":     s.privateAreaTokenValue,
		"Host":       "pulsee.it",
	}).Get("https://pulsee.it/middleware/private/download/" + token.Token + "/" + parts[1]))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.NOT_FOUND)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *PulseeStrategy) LoadsInternal() bool {
	return true
}

func (s *PulseeStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *PulseeStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
