package pulsee

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "pulsee.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"a.to@hotmail.it", "!Antonio92"}, //ok

	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "a.to@hotmail.it", Password: "!Antonio92", Uri: "pulsee.crawler",
			Identifier: "IT001E86363527",
			Ref:        "202011803250", PdfUri: "https://pulsee.it/middleware/api/privateArea/createTokenDownloadDocument/AXPODT2020-261241683-3225362|202011803250_Sintesi.pdf",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
