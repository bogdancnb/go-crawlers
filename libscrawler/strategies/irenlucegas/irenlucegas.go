package irenlucegas

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"encoding/base64"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

const (
	home         = "https://clienti.irenlucegas.it/azioni-iren"
	authenticate = "https://clienti.irenlucegas.it/v2/api/authenticate"
	profile      = "https://clienti.irenlucegas.it/v3/api/profile"
	invoices     = `https://clienti.irenlucegas.it/v3/api/archivio-bollette?flagHP=N&page=1&contracts=%s`
	pdf          = "https://clienti.irenlucegas.it/v3/api/downloadpdf"
	pdfPayload   = `{"codFattura":"%s","sistema":"%s","tipo_utenza":"%s","commodity":"%s","anno":"%s"}`

	yyyyMMdd = "2006-01-02"
	ddMMyyyy = "02.01.2006"
)

func Init() {
	RegisterFactory("iren_Luce_Gas.crawler", new(IrenLuceGasFactory))
}

type IrenLuceGasFactory struct{}

func (p *IrenLuceGasFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "IrenLuceGasStrategy")
	return &IrenLuceGasStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type IrenLuceGasStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc      *goquery.Document
	loginRes loginResponse
}

func (s *IrenLuceGasStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *IrenLuceGasStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *IrenLuceGasStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

type loginResponse struct {
	Data struct {
		IdToken  string `json:"id_token"`
		Username string `json:"username"`
	} `json:"data"`
	Status statusJson `json:"status"`
}
type statusJson struct {
	Codice      string `json:"codice"`
	Descrizione string `json:"descrizione"`
	Esito       string `json:"esito"`
}

func (s *IrenLuceGasStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})
	_, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Accept":     AcceptHeader,
		"Connection": "keep-alive",
		"Host":       "clienti.irenlucegas.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":       "application/json, text/plain, */*",
		"Connection":   "keep-alive",
		"Content-Type": "application/json",
		"Host":         "clienti.irenlucegas.it",
		"Origin":       "https://clienti.irenlucegas.it",
		"Referer":      "https://clienti.irenlucegas.it/login?returnUrl=%2Fazioni-iren",
	}).SetBody(fmt.Sprintf(`{"email":"%s","password":"%s"}`, account.Username, *account.Password)).
		SetResult(&s.loginRes).
		Post(authenticate))
	if ex != nil {
		return nil, ex
	}
	s.Debug(res)
	return nil, nil
}

func (s *IrenLuceGasStrategy) CheckLogin(response *string) Exception {
	if s.loginRes.Status.Codice == "401" {
		return LoginErr("Unauthorized access")
	}
	return nil
}

type profileJson struct {
	Data struct {
		ListAccount    string        `json:"listAccount"`
		ListAccountDWH string        `json:"listAccountDWH"`
		ListaTitolari  []interface{} `json:"listaTitolari"`
		Uuid           string        `json:"uuid"`
		UserProfile    struct {
			AccountId                  string `json:"accountId"`
			BpSap                      string `json:"bpSap"`
			Cap                        string `json:"cap"`
			Citta                      string `json:"citta"`
			Civico                     string `json:"civico"`
			CodiceIstat                string `json:"codiceIstat"`
			DbProvenienza              string `json:"dbProvenienza"`
			FiscalCode                 string `json:"fiscalCode"`
			FlagPersonaFisicaGiuridica string `json:"flagPersonaFisicaGiuridica"`
			Indirizzo                  string `json:"indirizzo"`
			MobilePhone                string `json:"mobilePhone"`
			Name                       string `json:"name"`
			Provincia                  string `json:"provincia"`
			SubjectType                string `json:"subjectType"`
			Surname                    string `json:"surname"`
			Contact                    struct {
				CanaleContattoPreferito string `json:"canaleContattoPreferito"`
				CellulareRef            string `json:"cellulareRef"`
				CodiceFiscaleRef        string `json:"codiceFiscaleRef"`
				CognomeReferente        string `json:"cognomeReferente"`
				IdReferente             string `json:"idReferente"`
				NomeReferente           string `json:"nomeReferente"`
			} `json:"contact"`
		} `json:"userProfile"`
		IdmUser struct {
			UserId string `json:"userId"`
		} `json:"idmUser"`
		Contracts []*contractJson `json:"contracts"`
	} `json:"data"`
	Status statusJson `json:"status"`
}
type contractJson struct {
	IdContratto             string `json:"idContratto"`
	IdFornitura             string `json:"idFornitura"`
	IndirizzoDiFatturazione string `json:"indirizzoDiFatturazione"`
	IrenContractNumber      string `json:"irenContractNumber"`
	NomeIntestatario        string `json:"nomeIntestatario"`
	PersonaFisicaGiuridica  string `json:"personaFisicaGiuridica"`
	PodPdr                  string `json:"pod_Pdr"`
	StatoServizio           string `json:"statoServizio"`
	TipologiaCommodity      string `json:"tipologiaCommodity"`
	DbProvenienza           string `json:"dbProvenienza"`
}

func (s *IrenLuceGasStrategy) LoadLocations(account *model.Account) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer " + s.loginRes.Data.IdToken,
		"Connection":    "keep-alive",
		"Host":          "clienti.irenlucegas.it",
		"Referer":       "https://clienti.irenlucegas.it/login?returnUrl=%2Fazioni-iren",
	}).Get(profile))
	s.Debug(res)
	if ex != nil {
		return ex
	}
	var info profileJson
	err := utils.FromJSON(&info, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserialize profile response: " + err.Error())
	}

	if info.Status.Descrizione != "Success" {
		return ParseErr(fmt.Sprintf("profile info error: %v", info.Status))
	}
	for _, contract := range info.Data.Contracts {
		if contract.StatoServizio != "Attivo" {
			s.Debugf("inactive contract %v", contract)
			continue
		}
		l := &model.Location{
			Service:        contract.NomeIntestatario + " " + contract.IndirizzoDiFatturazione,
			Details:        &contract.IdContratto,
			Identifier:     contract.IrenContractNumber,
			AccountHelpers: contract.DbProvenienza,
		}
		account.AddLocation(l)
	}
	return nil
}

func (s *IrenLuceGasStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

type invoiceJson struct {
	Status statusJson
	Data   struct {
		Uuid    string
		Fatture []struct {
			Commodity                              string
			ConPagOnline                           bool
			CondizioneEsistenzaDimostratoPagamento bool
			DataEmissione                          string
			DataPagamento                          string
			FiltroStato                            string
			IdFornituraDwh                         string
			ImportoFattura                         float64
			ImportoPagato                          float64
			ImportoResiduo                         float64
			LinkPdf                                string
			Nome                                   string
			NumeroContratto                        string
			PuntoFornitura                         string
			RifPdf                                 string
			Scadenza                               string
			StatoFatt                              string
			StatoPareggio                          string
			TipoDocumento                          string
			SistemaProvenienza                     string
		}
	}
}

func (s *IrenLuceGasStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer " + s.loginRes.Data.IdToken,
		"Connection":    "keep-alive",
		"Host":          "clienti.irenlucegas.it",
		"Referer":       "https://clienti.irenlucegas.it/info-contratto",
	}).Get(fmt.Sprintf(invoices, *location.Details)))
	s.Debug(res)
	if ex != nil {
		return ex
	}
	var info invoiceJson
	err := utils.FromJSON(&info, strings.NewReader(res))
	if err != nil {
		return ParseErr("deserialize invoice response " + res + ": " + err.Error())
	}
	if info.Status.Descrizione != "Success" {
		return ParseErr("invoice request error: " + res)
	}

	for _, f := range info.Data.Fatture {
		issueDate, err := time.Parse(ddMMyyyy, f.DataEmissione)
		if err != nil {
			return ParseErr(f.DataEmissione)
		}
		dueDate, err := time.Parse(ddMMyyyy, f.Scadenza)
		if err != nil {
			return ParseErr(f.Scadenza)
		}
		uri := ""
		if f.RifPdf != "" {
			uri += f.RifPdf
		} else {
			uri += f.Nome
		}
		uri += "|" + f.SistemaProvenienza + "|" + location.AccountHelpers.(string) + "|" + f.Commodity + "|" + strconv.Itoa(issueDate.Year())

		i := &model.Invoice{
			Ref:       f.Nome,
			PdfUri:    &uri,
			Amount:    f.ImportoFattura,
			AmountDue: f.ImportoResiduo,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)

		if f.ImportoPagato > 0 && f.DataPagamento != "" {
			payDate, err := time.Parse(yyyyMMdd, f.DataPagamento)
			if err != nil {
				return ParseErr(f.DataPagamento)
			}
			p := &model.Payment{
				Ref:    f.Nome,
				Amount: f.ImportoPagato,
				Date:   payDate.Format(utils.DBDateFormat),
			}
			location.AddPayment(p)
		}
	}
	return nil
}

func (s *IrenLuceGasStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

type pdfJson struct {
	Status statusJson
	Data   struct {
		Document string
	}
}

func (s *IrenLuceGasStrategy) Pdf(account *model.Account) *model.PdfResponse {
	uri := *account.Locations[0].Invoices[0].PdfUri
	parts := strings.Split(uri, "|")
	if len(parts) != 5 {
		return model.PdfErrorResponse("invalid uri format: "+uri, model.OTHER_EXCEPTION)
	}
	payload := fmt.Sprintf(pdfPayload, parts[0], parts[1], parts[2], parts[3], parts[4])
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Accept":        "application/json, text/plain, */*",
		"Authorization": "Bearer " + s.loginRes.Data.IdToken,
		"Connection":    "keep-alive",
		"Content-Type":  "application/json",
		"Host":          "clienti.irenlucegas.it",
		"Origin":        "https://clienti.irenlucegas.it",
	}).SetBody(payload).Post(pdf))
	s.Debug(res)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	var resp pdfJson
	err := utils.FromJSON(&resp, strings.NewReader(res))
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	pdfBytes, err := base64.StdEncoding.DecodeString(resp.Data.Document)
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *IrenLuceGasStrategy) LoadsInternal() bool {
	return false
}

func (s *IrenLuceGasStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *IrenLuceGasStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
