package irenlucegas

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "iren_Luce_Gas.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"sonia.ottonello30@gmail.com", "Sonia1976."}, // disclameer not accepted
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "sonia.ottonello30@gmail.com", Password: "Sonia1976.", Uri: "iren_Luce_Gas.crawler",
			Identifier: "3425817",
			Ref:        "060000020200003652300", PdfUri: "060000020200003652300|NETAH|DWH|H2O|2020",
		},

		//{
		//	Username: "sonia.ottonello30@gmail.com", Password: "Sonia1976.", Uri: "iren_Luce_Gas.crawler",
		//	Identifier: "0005541192",
		//	Ref:        "0000002043287558", PdfUri: "0000002043287558|SAPISU|DWH|GAS|2020 ",
		//},

		//{
		//	Username: "sonia.ottonello30@gmail.com", Password: "Sonia1976.", Uri: "iren_Luce_Gas.crawler",
		//	Identifier: "0005527779",
		//	Ref:        "0000002043834105", PdfUri: "0000002043834105|SAPISU|DWH|ELE|2020",
		//},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
