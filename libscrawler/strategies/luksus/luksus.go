package luksus

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"math"
	"strings"
	"time"
)

const (
	login      = "https://bok.luksus.net.pl/"
	info       = "https://bok.luksus.net.pl/?m=info"
	finances   = "https://bok.luksus.net.pl/?m=finances"
	siteFormat = "2006/01/02 15:04"
)

var (
	bankName  = "Bank Millennium SA"
	bankBIC   = "BIGBPLPW"
	legalName = "lms.luksus.net.pl"

	accNrSelector = "strong:containsOwn('Konta bankowe')"
	peselSelect   = "strong:containsOwn('PESEL')"
	adresSelect   = `img[src='modules/info/style/default/address.gif']`
)

func Init() {
	RegisterFactory("luksus.crawler", new(LuksusFactory))
}

type LuksusFactory struct{}

func (p *LuksusFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "LuksusStrategy")
	return &LuksusStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type LuksusStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations  map[string]*model.Location
	exInvoices   map[string]*model.Invoice
	doc          *goquery.Document
	lmsSESSIONID string
}

func (s *LuksusStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *LuksusStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *LuksusStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *LuksusStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Host":       "bok.luksus.net.pl",
		"Connection": "keep-alive",
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().Get(login)
	if err != nil {
		return nil, ConnectionErr("GET " + login + ": " + err.Error())
	}
	s.lmsSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "LMSSESSIONID")
	s.lmsSESSIONID = s.lmsSESSIONID[:strings.Index(s.lmsSESSIONID, "; ")]

	s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	res, err = s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
		"Cookie":       s.lmsSESSIONID,
		"Origin":       "https://bok.luksus.net.pl",
		"Referer":      "https://bok.luksus.net.pl/",
	}).SetFormData(map[string]string{
		"loginform[login]":  account.Username,
		"loginform[pwd]":    *account.Password,
		"loginform[submit]": "Login",
	}).Post(login)
	if res == nil {
		return nil, ConnectionErr("POST " + login + " got no response")
	}
	if len(res.Body()) > 0 && (strings.Contains(res.String(), "Brak dostępu!") || strings.Contains(res.String(), "Access denied")) {
		return nil, LoginErr("Access denied")
	}
	s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	return nil, nil
}

func (s *LuksusStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *LuksusStrategy) LoadLocations(account *model.Account) Exception {
	var ex Exception
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Cookie": s.lmsSESSIONID,
	}).Get(info))
	if ex != nil {
		return ex
	}

	addresEl := s.doc.Find(adresSelect)
	if addresEl.Length() == 0 {
		return ParseErr("no adress element found")
	}
	address := utils.RemoveAdjacentUnicodeSpaces([]byte(addresEl.Parent().Next().Text()))
	addresS := strings.ReplaceAll(string(address), "\n", "")
	peselEl := s.doc.Find(peselSelect)
	if peselEl.Length() == 0 {
		return ParseErr("no PESEL element found")
	}
	pesel := utils.RemoveSpaces(peselEl.Parent().Next().Text())
	accNrEl := s.doc.Find(accNrSelector)
	if accNrEl.Length() == 0 {
		return ParseErr("no acc nr element found")
	}
	accNR := accNrEl.Parent().Next().Text()
	accNR = utils.RemoveUnicodeSpaces([]byte(accNR))

	location := &model.Location{
		Service:    addresS,
		Identifier: pesel,
		ProviderBranch: &model.ProviderBranchDTO{
			Iban:            &accNR,
			BankName:        &bankName,
			BankBIC:         &bankBIC,
			LegalEntityName: &legalName,
		},
	}
	account.AddLocation(location)
	return nil
}

func (s *LuksusStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *LuksusStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	var ex Exception
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"Content-Type": "application/x-www-form-urlencoded",
	}).Get(finances))
	if ex != nil {
		return ex
	}
	rows := s.doc.Find("form[name='invoices'] table tr")
	if rows.Length() <= 1 {
		return ParseErr("no invoice/payments rows found")
	}
	limit := math.Min(12, float64(rows.Length()))
	for i := 1; i < int(limit); i++ {
		cols := rows.Eq(i).Find("td")
		desc := cols.Eq(4).Text()
		if strings.Contains(desc, "RRN") {
			if ex := s.parsePayment(location, cols); ex != nil {
				return ParseErr("parse payment: " + ex.Error())
			}
		} else {
			if ex := s.parseInvoice(location, cols); ex != nil {
				return ParseErr("parse payment: " + ex.Error())
			}
		}
	}

	//selectorSold := "a[href='?m=finances&f=transferform']"
	selectorSold := "table:contains('Saldo')"
	soldEl := s.doc.Find(selectorSold)
	if soldEl.Length() == 0 {
		return ParseErr("no sold element found on site")
	}
	soldEl = soldEl.Next().Find("h1")
	if soldEl.Length() == 0 {
		return ParseErr("no sold element found on site")
	}
	soldParts := strings.Split(soldEl.Text(), " ")
	if len(soldParts) == 0 {
		return ParseErr("no sold parts found: " + soldEl.Text())
	}
	locAmount, ex := AmountFromString(soldParts[0])
	if ex != nil {
		return ParseErr("could not parse location amount: " + ex.Error())
	}
	if locAmount < 0 {
		locAmount = math.Abs(locAmount)
	}

	location.Amount = &locAmount
	location.Invoices[0].AmountDue = locAmount
	return nil
}

func (s *LuksusStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ds := strings.TrimSpace(cols.Eq(0).Text())
	date, err := time.Parse(siteFormat, ds)
	if err != nil {
		return ParseErr("parsing date from " + cols.Eq(0).Text())
	}
	am, exception := s.parseAmount(cols)
	if exception != nil {
		return exception
	}
	desc := strings.TrimSpace(cols.Eq(4).Text())
	var pdfUri *string
	uri, _ := Attr(cols.Eq(5).Find("a"), "href")
	if uri != "" {
		uri = strings.ReplaceAll(uri, "amp;", "")
		uri = "https://bok.luksus.net.pl/" + uri
		pdfUri = &uri
	}
	invoice := &model.Invoice{
		Ref:       desc,
		PdfUri:    pdfUri,
		Amount:    am,
		AmountDue: 0,
		IssueDate: date.Format(utils.DBDateFormat),
		DueDate:   date.AddDate(0, 0, 14).Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *LuksusStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	ds := strings.TrimSpace(cols.Eq(0).Text())
	date, err := time.Parse(siteFormat, ds)
	if err != nil {
		return ParseErr("parsing date from " + cols.Eq(0).Text())
	}
	am, exception := s.parseAmount(cols)
	if exception != nil {
		return exception
	}
	dateString := date.Format(utils.DBDateFormat)
	id := cols.Eq(4).Text()
	id = id[strings.Index(id, "id: ")+4:]
	id = id[:strings.Index(id, " ")]
	payment := &model.Payment{
		Ref:    id,
		Amount: am,
		Date:   dateString,
	}
	location.AddPayment(payment)
	return nil
}

func (s *LuksusStrategy) parseAmount(cols *goquery.Selection) (float64, Exception) {
	amParts := strings.Split(cols.Eq(1).Text(), " ")
	if len(amParts) == 0 {
		return 0, ParseErr("no valid amount: " + cols.Eq(1).Text())
	}
	am, ex := AmountFromString(amParts[0])
	if ex != nil {
		return 0, ex
	}
	if am < 0 {
		am = math.Abs(am)
	}
	return am, nil
}

func (s *LuksusStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *LuksusStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	if err != nil {
		return model.PdfErrorResponse("error", model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *LuksusStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"Cookie": s.lmsSESSIONID,
	}).Get(*invoice.PdfUri)
	if err != nil {
		return nil, ConnectionErr("Get " + *invoice.PdfUri + ": " + err.Error())
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *LuksusStrategy) LoadsInternal() bool {
	return false
}

func (s *LuksusStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *LuksusStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
