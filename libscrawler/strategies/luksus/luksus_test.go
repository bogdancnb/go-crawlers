package luksus

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
	"time"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "luksus.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"445884","445884"},//login error
		{"102", "445884"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "102", Password: "445884", Uri: "luksus.crawler",
			Identifier: "90051514861",
			Ref:        "90051514861", PdfUri: "https://bok.luksus.net.pl/?m=finances&f=invoice&id=190406",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}

func TestParseDate(t *testing.T) {
	date, err := time.Parse(siteFormat, "2020/06/17 09:57")
	t.Log(date, err)
}
