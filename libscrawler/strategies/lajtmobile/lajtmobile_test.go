package lajtmobile

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	tests := []struct {
		username string
		password string
		uri      string
	}{
		{"786816840", "Marcin125p", "lajt_mobile.crawler"},
		//{"539210027", "Arkadiusz93", "lajt_mobile.crawler"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, test.uri); err != nil {
			t.Logf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
