package lajtmobile

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

const (
	home               = "https://lajt-online.pl/"
	theme              = "https://lajt-online.pl/api/theme"
	authenticateNumber = "https://lajt-online.pl/api/authenticate?number="
	authenticate       = "https://lajt-online.pl/api/authenticate"
	paymentsUrl        = "https://lajt-online.pl/api/payments"
	invoicesUrl        = "https://lajt-online.pl/api/invoices"
	checkIsExist       = "https://lajt-online.pl/api/invoice/checkIsExist"
	pdfDownload        = `https://lajt-online.pl/api/invoice/download?year=%s&month=%s`

	jsonFormat = `2006-01-02`
)

var (
	ibanRegex = regexp.MustCompile(`Numer\s*rachunku\s*do\s*wpłaty\s*:\s*(\d+)\D`)
	bankName  = "Bank Zachodni WBK SA"
	bankBIC   = "WBKPPLPP"
	legalName = "Lajt Mobile"
)

func Init() {
	f := new(LajtMobileFactory)
	RegisterFactory("lajt_mobile.crawler", f)
}

type LajtMobileFactory struct{}

func (p *LajtMobileFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "LajtMobileStrategy")
	return &LajtMobileStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type LajtMobileStrategy struct {
	*logrus.Entry
	HttpStrategy

	existingLocsByService map[string]*model.Location
	existingInvoices      map[string]*model.Invoice
	lajtmobile_session    string
	token                 string
}

func (s *LajtMobileStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *LajtMobileStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *LajtMobileStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *LajtMobileStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"Connection": "keep-alive",
		"Host":       "lajt-online.pl",
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept": AcceptHeader,
	}).Get(home)
	s.Trace(res)
	if err != nil {
		return nil, ConnectionErr(home + " : " + err.Error())
	}

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"Brand":            "lajt-online.pl",
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(theme)
	s.Trace(res)
	if err != nil {
		return nil, ConnectionErr(theme + " : " + err.Error())
	}
	s.parseSessionCookie(res)

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"Brand":            "lajt-online.pl",
		"content-type":     "application/json;charset=UTF-8",
		"Cookie":           s.lajtmobile_session,
		"origin":           "https://lajt-online.pl",
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(authenticateNumber + account.Username)
	if res == nil {
		return nil, ConnectionErr("no reponse: " + authenticateNumber)
	}
	s.Debug("authenticateNumber:", res.String())
	if strings.Contains(res.String(), `"message": "not_acceptable"`) || strings.Contains(res.String(), `"message":"not_acceptable"`) {
		return nil, LoginErr("username not acceptable")
	}
	if err != nil {
		return nil, ConnectionErr(authenticateNumber + " : " + err.Error())
	}
	if res.StatusCode() != 200 {
		return nil, ConnectionErr(authenticateNumber + " : status code " + res.Status())
	}
	s.parseSessionCookie(res)

	payload := fmt.Sprintf(`{"password":"%s","number":"%s"}`, *account.Password, account.Username)
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"Brand":            "lajt-online.pl",
		"content-type":     "application/json;charset=UTF-8",
		"Cookie":           s.lajtmobile_session,
		"origin":           "https://lajt-online.pl",
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).SetBody(payload).Post(authenticate)
	if res == nil {
		return nil, ConnectionErr("no reponse: " + authenticate)
	}
	s.Debug("authenticate:", res.String())
	if strings.Contains(res.String(), `"message": "not_acceptable"`) || strings.Contains(res.String(), `"message":"not_acceptable"`) {
		return nil, LoginErr("password not acceptable")
	}
	if err != nil {
		return nil, ConnectionErr(authenticate + " : " + err.Error())
	}
	if res.StatusCode() != 200 {
		return nil, ConnectionErr(authenticate + " : status code " + res.Status())
	}
	s.parseSessionCookie(res)
	var loginRes struct {
		Data struct {
			Token string
		}
		Code    int
		Message string
	}
	err = utils.FromJSON(&loginRes, bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse login response: " + err.Error())
	}
	s.token = loginRes.Data.Token
	if s.token == "" {
		return nil, LoginErr("no token: " + res.String())
	}

	return nil, nil
}

func (s *LajtMobileStrategy) parseSessionCookie(res *resty.Response) {
	s.lajtmobile_session = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "lajtmobile_session")
	s.lajtmobile_session = s.lajtmobile_session[:strings.Index(s.lajtmobile_session, ";")]
}

func (s *LajtMobileStrategy) CheckLogin(response *string) Exception {
	return nil
}

type paymentsJson struct {
	Data struct {
		Payments struct {
			List []*struct {
				Posting_date    string `json:"posting_date"`
				Description     string
				Amount          string
				Account_balance string `json:"account_balance"`
				Interest        string
				PaymentType     string `json:"type"`
			}
			Count int
		}
		Correction_invoices struct {
			List  []*interface{}
			Count int
		}
	}
	Code    int
	Message string
}

type invoiceJson struct {
	Code int
	Data struct {
		Count int
		List  []*struct {
			Amount      string
			Description string
			IssueDate   string `json:"issue_date"`
			DueDate     string `json:"posting_date"`
		}
	}
	Message string
}

func (s *LajtMobileStrategy) LoadLocations(account *model.Account) Exception {
	l := &model.Location{
		Service: account.Username,
	}
	account.AddLocation(l)

	ex := s.parseInvoices(l)
	if ex != nil {
		return ex
	}

	ex = s.parsePayments(l)
	if ex != nil {
		return ex
	}

	if l.Amount != nil && *l.Amount > 0 {
		l.SetInvoiceDueFromAmount()
	}
	return nil
}

func (s *LajtMobileStrategy) parseInvoices(l *model.Location) Exception {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"authorization":    "Bearer " + s.token,
		"Brand":            "lajt-online.pl",
		"Cookie":           s.lajtmobile_session,
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(invoicesUrl)
	s.Debug("invoicesUrl: ", res.String())
	if err != nil {
		return ConnectionErr(invoicesUrl + " : " + err.Error())
	}
	var invoices invoiceJson
	err = utils.FromJSON(&invoices, bytes.NewBuffer(res.Body()))
	if err != nil {
		return ParseErr("invoicesUrl json: " + err.Error())
	}
	if invoices.Message != "success" {
		return ParseErr("invoicesUrl json: " + res.String())
	}
	s.parseSessionCookie(res)

	for _, i := range invoices.Data.List {
		amt, ex := AmountFromString(i.Amount)
		if ex != nil {
			return ParseErr("invoice amount " + i.Amount + ": " + ex.Error())
		}
		issue, err := time.Parse(jsonFormat, i.IssueDate)
		if err != nil {
			return ParseErr("issue date:" + i.IssueDate)
		}
		due, err := time.Parse(jsonFormat, i.DueDate)
		if err != nil {
			return ParseErr("due date: " + i.DueDate)
		}
		refp := strings.Split(i.Description, " ")
		if len(refp) < 2 {
			return ParseErr("invalid ref format : " + i.Description)
		}
		inv := &model.Invoice{
			Ref:       i.Description,
			PdfUri:    &refp[1],
			Amount:    amt,
			AmountDue: 0,
			IssueDate: issue.Format(utils.DBDateFormat),
			DueDate:   due.Format(utils.DBDateFormat),
		}
		l.AddInvoice(inv)

		if l.Identifier == "" {
			refp = strings.Split(refp[1], "/")
			if len(refp) != 3 {
				return ParseErr("invalid ref format: " + i.Description)
			}
			l.Identifier = refp[0]

		}

		if l.ProviderBranch == nil {
			if exLoc, ok := s.existingLocsByService[l.Service]; ok {
				l.ProviderBranch = exLoc.ProviderBranch
			} else if ex = s.parsePdfInfo(l, inv); ex != nil {
				return ParseErr("parse pdf location info: " + ex.Error())
			}
		}
	}
	return nil
}

func (s *LajtMobileStrategy) parsePdfInfo(l *model.Location, inv *model.Invoice) Exception {
	pdfBody, ex := s.pdfDownload(*inv.PdfUri)
	if ex != nil {
		return ex
	}
	text, err := PlainText(pdfBody)
	if err != nil {
		return ParseErr("parse pdf text: " + err.Error())
	}
	s.Debug("pdf: ", text)

	m, err := utils.MatchRegex(text, ibanRegex)
	if err != nil {
		return ParseErr("could not match iban regex: " + err.Error())
	}
	iban := m[0][1]
	l.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &iban,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalName,
	}
	return nil
}

func (s *LajtMobileStrategy) parsePayments(l *model.Location) Exception {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"authorization":    "Bearer " + s.token,
		"Brand":            "lajt-online.pl",
		"Cookie":           s.lajtmobile_session,
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).Get(paymentsUrl)
	s.Debug("paymentsUrl: ", res.String())
	if err != nil {
		return ConnectionErr(paymentsUrl + " : " + err.Error())
	}
	var payments paymentsJson
	err = utils.FromJSON(&payments, bytes.NewBuffer(res.Body()))
	if err != nil {
		return ParseErr("paymentsUrl json: " + err.Error())
	}
	if payments.Message != "success" {
		return ParseErr("paymentsUrl json: " + res.String())
	}
	s.parseSessionCookie(res)

	for i, p := range payments.Data.Payments.List {
		if i == 0 {
			balance, ex := AmountFromString(p.Account_balance)
			if ex != nil {
				return ParseErr("account balance:" + ex.Error())
			}
			s.Debug("account balance=", balance)
		}

		paydate, err := time.Parse(jsonFormat, p.Posting_date)
		if err != nil {
			return ParseErr("pay date: " + p.Posting_date)
		}
		amt, ex := AmountFromString(p.Amount)
		if ex != nil {
			return ParseErr("payment amount:" + ex.Error())
		}
		pay := &model.Payment{
			Ref:    p.Description,
			Amount: amt,
			Date:   paydate.Format(utils.DBDateFormat),
		}
		l.AddPayment(pay)
	}
	return nil
}

func (s *LajtMobileStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *LajtMobileStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {

	return nil
}

func (s *LajtMobileStrategy) pdfDownload(pdfUri string) ([]byte, Exception) {
	refp := strings.Split(pdfUri, "/")
	if len(refp) != 3 {
		return nil, ParseErr("invalid ref format: " + pdfUri)
	}
	payload := fmt.Sprintf(`{"year":"%s","month":"%s"}`, refp[2], refp[1])

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":           "application/json, text/plain, */*",
		"authorization":    "Bearer " + s.token,
		"content-type":     "application/json;charset=UTF-8",
		"Brand":            "lajt-online.pl",
		"Cookie":           s.lajtmobile_session,
		"origin":           "https://lajt-online.pl",
		"referer":          "https://lajt-online.pl/",
		"X-Requested-With": "XMLHttpRequest",
	}).SetBody(payload).Post(checkIsExist)
	if err != nil {
		return nil, ConnectionErr(checkIsExist + " " + payload + " : " + err.Error())
	}
	if res.StatusCode() != 200 || !strings.Contains(res.String(), "success") {
		return nil, ParseErr("checkIsExist " + payload + " : " + res.String())
	}

	uri := fmt.Sprintf(pdfDownload, refp[2], refp[1])
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"Cookie": s.lajtmobile_session,
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr(uri + " : " + res.String() + " : " + err.Error())
	}
	if res.StatusCode() != 200 {
		return nil, ConnectionErr(uri + " : " + res.String() + " : " + res.Status())
	}
	body := res.Body()
	body = TrimPdf(body)
	return body, nil
}

func (s *LajtMobileStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *LajtMobileStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	return nil
}

func (s *LajtMobileStrategy) Pdf(account *model.Account) *model.PdfResponse {
	uri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := s.pdfDownload(uri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *LajtMobileStrategy) LoadsInternal() bool {
	return true
}

func (s *LajtMobileStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	s.existingInvoices = make(map[string]*model.Invoice)
	for _, invoice := range existingInvoices {
		s.existingInvoices[invoice.Ref] = invoice
	}
}

func (s *LajtMobileStrategy) SetExistingLocations(existingLocations []*model.Location) {
	s.existingLocsByService = make(map[string]*model.Location)
	for _, location := range existingLocations {
		s.existingLocsByService[location.Service] = location
	}
}
