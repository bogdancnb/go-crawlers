package eonenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "eon_Energia.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"cometa", "Bianca2017."}, //ok 2 locs
		//{"cometa.laura@alice.it", "Bianca2017."}, //not ok
		//{"pippopd", "$aliva76"},
		//{"PetrovSlavko", "Bambini*3"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "cometa", Password: "Bianca2017.", Uri: "eon_Energia.crawler",
			Identifier: "PF-2565941",
			Ref:        "155390|03/04/2017", PdfUri: "&subscription-key=951d6fb5e1584cbc9f723725055831c5&account-id=A301877737&invoice-date=03/04/2017&installation-type=POWER",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
