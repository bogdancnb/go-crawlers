package eonenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"errors"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"sort"
	"strings"
	"time"
)

const (
	login = "https://myeon.eon-energia.com/it/login.html"
	oauth = "https://api-mmi.eon.it/mmi/oauth2/login/v1.0"
	token = "https://api-mmi.eon.it/mmi/oauth2/token/v1.0"

	redirect   = "https://myeon.eon-energia.com/it/redirect.html?id_token="
	dashboard  = "https://myeon.eon-energia.com/it/dashboard.html?userLoggingIn=true&userName="
	billing    = "https://api-mmi.eon.it/scsi/billing-profiles/v1.0"
	deliveries = "https://api-mmi.eon.it/scsi/point-of-deliveries/v1.0"
	invoices   = "https://api-mmi.eon.it/scsi/invoices/v1.0/?apiversion=v1.2"
	pdf        = "https://api-mmi.eon.it/scsi/invoices/v1.0/invoicePDFs/"

	invoiceFormat  = "02/01/2006"
	eInvoiceFormat = "2006-01-02T15:04:05"

	subKey       = "Ocp-Apim-Subscription-Key"
	clientId     = "441d18a5-1245-4633-a8c4-a0b1d9a07223"
	clientSecret = "#v,[\\F7Jf4NF`3b8"
)

func Init() {
	RegisterFactory("eon_Energia.crawler", new(EonEnergiaFactory))
}

type EonEnergiaFactory struct{}

func (p *EonEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EonEnergiaStrategy")
	return &EonEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type EonEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations                        map[string]*model.Location
	exInvoices                         map[string]*model.Invoice
	doc                                *goquery.Document
	subscriptionID, subscriptionID2    string
	accessToken, accessToken2, idToken string
}

func (s *EonEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EonEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return true
}

func (s *EonEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EonEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})
	s.subscriptionID = "14e73bcf51f0403cb5aa85914cdb4441"
	s.subscriptionID2 = "951d6fb5e1584cbc9f723725055831c5"

	doc, ex := OkDocument(s.Client().R().Get(login))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Text())

	//res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
	//	"accept":     "*/*",
	//	"connection": "keep-alive",
	//	"host":       "api-mmi.eon.it",
	//	"origin":     "https://myeon.eon-energia.com",
	//	"referer":    login,
	//}).Options(oauth))
	//s.Trace(res)
	//if ex != nil {
	//	return nil, ex
	//}

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"Authorization": "bearer null",
		"connection":    "keep-alive",
		"content-type":  "application/x-www-form-urlencoded",
		"host":          "api-mmi.eon.it",
		subKey:          s.subscriptionID,
		"origin":        "https://myeon.eon-energia.com",
		"Referer":       login,
	}).SetFormData(map[string]string{
		"grant_type":    "password",
		"username":      account.Username,
		"password":      *account.Password,
		"scope":         "openid profile",
		"client_id":     clientId,
		"client_secret": clientSecret,
		"response_type": "token_id",
	}).Post(oauth)
	s.Debug("oauth response:", res.String())
	var loginRes struct {
		AccessToken string `json:"access_token"`
		IdToken     string `json:"id_token"`
		Error       string `json:"error"`
		ErrorDesc   string `json:"error_description"`
	}
	errJson := utils.FromJSON(&loginRes, strings.NewReader(res.String()))
	if errJson != nil {
		return nil, ParseErr("parse login response: " + errJson.Error())
	}

	//if strings.Contains(res, "invalid_grant") {
	if loginRes.Error == "invalid_grant" {
		return nil, LoginErr(loginRes.ErrorDesc)
	}
	if err != nil {
		return nil, ConnectionErr(err.Error())
	}

	if loginRes.AccessToken == "" {
		return nil, LoginErr("no access token")
	}
	s.accessToken = loginRes.AccessToken
	s.idToken = loginRes.IdToken

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":        "application/json, text/plain, */*",
		"Authorization": "bearer null",
		"connection":    "keep-alive",
		"content-type":  "application/x-www-form-urlencoded",
		"host":          "api-mmi.eon.it",
		subKey:          s.subscriptionID,
		"origin":        "https://myeon.eon-energia.com",
		"Referer":       login,
	}).SetFormData(map[string]string{
		"grant_type":    "client_credentials",
		"client_id":     clientId,
		"client_secret": clientSecret,
	}).Post(token)
	s.Debug("token res", res.String())
	var resJson struct {
		AccessToken string `json:"access_token"`
	}
	err = utils.FromJSON(&resJson, strings.NewReader(res.String()))
	if err != nil {
		return nil, ParseErr("token response:" + err.Error())
	}
	s.accessToken2 = resJson.AccessToken

	return nil, nil
}

func (s *EonEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *EonEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	//redirectUrl := redirect + s.idToken + "&username=" + account.Username
	//res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
	//	"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	//	"referer": "https://myeon.eon-energia.com/it/login.html",
	//}).Get(redirectUrl))
	//s.Trace(res)
	//if ex != nil {
	//	return ex
	//}
	//
	//urlDashboard := dashboard + account.Username
	//res, ex = OkString(s.Client().R().SetHeaders(map[string]string{
	//	"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	//	"referer": redirectUrl,
	//}).Get(urlDashboard))
	//s.Trace(res)
	//if ex != nil {
	//	return ex
	//}
	//
	//res, ex = OkString(s.Client().R().SetHeaders(map[string]string{
	//	"accept":        "application/json, text/plain, */*",
	//	"Authorization": "bearer null",
	//	"connection":    "keep-alive",
	//	"content-type":  "application/x-www-form-urlencoded",
	//	"host":          "api-mmi.eon.it",
	//	subKey:          s.subscriptionID,
	//	"origin":        "https://myeon.eon-energia.com",
	//	"Referer":       login,
	//}).SetFormData(map[string]string{
	//	"grant_type":    "client_credentials",
	//	"client_id":     clientId,
	//	"client_secret": clientSecret,
	//}).Post(token))
	//s.Trace(res)
	//var resJson struct {
	//	AccessToken string `json:"access_token"`
	//}
	//err := utils.FromJSON(&resJson, strings.NewReader(res))
	//if err != nil {
	//	return ParseErr("token response:" + err.Error())
	//}
	//s.accessToken2 = resJson.AccessToken

	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Authorization":             "bearer " + s.idToken,
		"Ocp-Apim-Subscription-Key": "951d6fb5e1584cbc9f723725055831c5",
		"accept":                    "application/json, text/plain, */*",
		"connection":                "keep-alive",
		"host":                      "api-mmi.eon.it",
		//"origin":                    "https://myeon.eon-energia.com",
		//"referer":                   urlDashboard,
	}).Get(billing))
	s.Debug("billing:", res)
	if ex != nil {
		return ex
	}
	var billingJson []*struct {
		BillingProfileID       string
		Commodity              string
		CustomerElectricityID  string
		CustomerGasID          string
		FirstName, LastName    string
		Status                 string
		InvoiceDeliveryAddress struct {
			City    string
			Number  string
			Street  string
			ZIPCode string
		}
	}
	err := utils.FromJSON(&billingJson, strings.NewReader(res))
	if err != nil {
		return ParseErr("billing json:" + err.Error())
	}
	for _, b := range billingJson {
		addr := b.FirstName + " " + b.LastName + ", " +
			b.InvoiceDeliveryAddress.City + " " + b.InvoiceDeliveryAddress.Street + " " + b.InvoiceDeliveryAddress.Number
		l := &model.Location{
			Service: addr,
			//Details:    b,
			Identifier: b.BillingProfileID,
		}
		account.AddLocation(l)
	}

	if ex := s.parseInvoices(account, res); ex != nil {
		return ex
	}

	for _, location := range account.Locations {
		sort.Slice(location.Invoices, func(i, j int) bool {
			return location.Invoices[i].IssueDate > location.Invoices[j].IssueDate
		})
	}

	return nil
}

func (s *EonEnergiaStrategy) parseInvoices(account *model.Account, res string) Exception {
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"Authorization":             "bearer " + s.idToken,
		"Ocp-Apim-Subscription-Key": "951d6fb5e1584cbc9f723725055831c5",
		"accept":                    "application/json, text/plain, */*",
		"connection":                "keep-alive",
		"host":                      "api-mmi.eon.it",
		//"origin":                    "https://myeon.eon-energia.com",
		//"referer":                   urlDashboard,
	}).Get(invoices))
	s.Debug("invoices:", res)
	if ex != nil {
		return ex
	}
	var invoices []*struct {
		AccountID        string
		BillingProfileID string
		Competence       struct {
			PeriodFrom string
			PeriodTo   string
		}
		InstallationType string
		Invoice          struct {
			Amount        string
			Date          string
			DueDate       string
			IsExtra       string
			Number        string
			PaymentDate   string
			PaymentStatus string
		}
		PODIDList []string
	}
	err := utils.FromJSON(&invoices, strings.NewReader(res))
	if err != nil {
		return ParseErr("parse invoice response: " + err.Error())
	}
	for _, i := range invoices {
		loc := s.getLocationById(account.Locations, i.BillingProfileID)
		if loc == nil {
			s.Warn("location not parsed: ", i.BillingProfileID)
			continue
		}

		am, ex := AmountFromString(i.Invoice.Amount)
		if ex != nil {
			return ex
		}
		amDue := 0.0
		if "NOT_PAID" == i.Invoice.PaymentStatus {
			amDue = am
		}
		issueDate, err := time.Parse(invoiceFormat, i.Invoice.Date)
		if err != nil {
			return ParseErr("parse date " + i.Invoice.Date)
		}
		dueDate, err := time.Parse(invoiceFormat, i.Invoice.DueDate)
		if err != nil {
			return ParseErr("parse date " + i.Invoice.DueDate)
		}
		uri := "&subscription-key=951d6fb5e1584cbc9f723725055831c5&account-id=" + i.AccountID +
			"&invoice-date=" + i.Invoice.Date +
			"&installation-type=" + i.InstallationType
		pdfUri := &uri
		inv := &model.Invoice{
			Ref:       i.Invoice.Number + "|" + i.Invoice.DueDate,
			PdfUri:    pdfUri,
			Amount:    am,
			AmountDue: amDue,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		loc.AddInvoice(inv)

		if "PAID" == i.Invoice.PaymentStatus && i.Invoice.PaymentDate != "" {
			payDate, err := time.Parse(invoiceFormat, i.Invoice.PaymentDate)
			if err != nil {
				return ParseErr("parse date " + i.Invoice.PaymentDate)
			}
			p := &model.Payment{
				Ref:    i.Invoice.Number,
				Amount: am,
				Date:   payDate.Format(utils.DBDateFormat),
			}
			loc.AddPayment(p)
		}
	}
	return nil
}

func (s *EonEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EonEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EonEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EonEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, err := s.pdfDownload(account.Locations[0].Invoices[0])
	if err != nil {
		return model.PdfErrorResponse(err.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
	}
}

func (s *EonEnergiaStrategy) pdfDownload(invoice *model.Invoice) ([]byte, error) {
	refP := strings.Split(invoice.Ref, "|")
	if len(refP) != 2 {
		return nil, errors.New("invalid pdf uri: " + *invoice.PdfUri)
	}
	uri := pdf + refP[0] + "?id_token=" + s.idToken + *invoice.PdfUri
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"connection": "keep-alive",
		"host":       "api-mmi.eon.it",
	}).Get(uri)
	if err != nil {
		return nil, err
	}
	return res.Body(), nil
}

func (s *EonEnergiaStrategy) LoadsInternal() bool {
	return true
}

func (s *EonEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *EonEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}

func (s *EonEnergiaStrategy) getLocationById(locs []*model.Location, id string) *model.Location {
	for _, loc := range locs {
		if id == loc.Identifier {
			return loc
		}
	}
	return nil
}
