package a2aenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "a2aenergy.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"rosanna.floridia@fastwebnet.it", "james025408"}, //login err
		{"simogrido@gmail.com", "Ottaviano_07"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "Gregorio61", Password: "Gentile2013", Uri: "estra_Energia.crawler",
			Identifier: "10400001013534",
			Ref:        "0000201902408871", PdfUri: "http://10.128.1.185:8080/urlsearchprod.jsp?type=BOLLETTA20&i1=EXBEL&s1=21&v1=201902408871",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
