package a2aenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
)

const (
	home                  = "https://www.a2aenergia.eu/area_clienti/"
	convertusername       = "https://casa.a2aenergia.eu/drupal-login-manager/convertusername"
	loginwidgetcontroller = "https://areaclienti.a2aenergia.eu/servlet/servlet.loginwidgetcontroller?type=login"
	oauth2                = "/services/oauth2/authorize?response_type=token&client_id=3MVG9fTLmJ60pJ5Ju.bDhhxrt9qt8JPmj4NW9qkQoH18W_PfMNFu1ECzCMI3I2tCkZiJP7aMGga4hg6VA7N71&redirect_uri=https%3A%2F%2Fwww.a2aenergia.eu%2F_callback.html&state=https%3A%2F%2Fwww.a2aenergia.eu%2Farea_clienti%2F"
	formPostUrl           = "https://areaclienti.a2aenergia.eu/loginflow/lightningLoginFlow.apexp"
	callback              = "https://www.a2aenergia.eu/_callback.html"
	areaClienti           = "https://www.a2aenergia.eu/area_clienti/"

	loginErrMsg   = `we could not authenticate you`
	invoiceFormat = "02/01/2006"
)

var (
	hrefRegex         = regexp.MustCompile(`window\.location\.href=\"(.+)\"}`)
	viewStateR        = regexp.MustCompile(`name=\"com\.salesforce\.visualforce\.ViewState\"\s*value=\"(.*)\"\s*\/><input\s*type="hidden"\s*id="com\.salesforce\.visualforce\.ViewStateVersion"`)
	viewStateVersionR = regexp.MustCompile(`name=\"com\.salesforce\.visualforce\.ViewStateVersion\"\s*value=\"(.*)\"\s*\/><input\s*type="hidden"\s*id="com\.salesforce\.visualforce\.ViewStateMAC"`)
	viewStateMACR     = regexp.MustCompile(`name=\"com\.salesforce\.visualforce\.ViewStateMAC\"\s*value=\"(.*)\"\s*\/><input\s*type="hidden"\s*id="com\.salesforce\.visualforce\.ViewStateCSRF"`)
	viewStateCSRFR    = regexp.MustCompile(`name=\"com\.salesforce\.visualforce\.ViewStateCSRF\"\s*value=\"(.*)\"\s*\/>`)
	hrefR             = regexp.MustCompile(`window\.location\.href\s*='(.+)'`)
)

func Init() {
	RegisterFactory("a2aenergy.crawler", new(A2AEnergiaFactory))
}

type A2AEnergiaFactory struct{}

func (p *A2AEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "A2AEnergiaStrategy")
	return &A2AEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type A2AEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *A2AEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *A2AEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.a2aenergia.eu",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	s.Debug(doc.Html())

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       "*/*",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded; charset=UTF-8",
		"host":         "casa.a2aenergia.eu",
		"origin":       "https://www.a2aenergia.eu",
		"referer":      "https://www.a2aenergia.eu/area_clienti/",
	}).SetFormData(map[string]string{
		"sourceusername": account.Username,
	}).Post(convertusername)
	if res == nil {
		return nil, ConnectionErr("no response for login post")
	}
	s.Debugf(`login response=%q, login error=%v`, res.String(), err)
	if res.StatusCode() == 200 {
		var data struct {
			Status      int
			Newusername *string
			Firstaccess interface{}
			Message     string
		}
		err = utils.FromJSON(&data, strings.NewReader(res.String()))
		if err != nil {
			return nil, ParseErr("parse login response: " + err.Error())
		}

		if data.Status != 0 {
			if data.Newusername == nil {
				return nil, LoginErr("wronmg username: " + FormatResponse(res))
			} else {
				res, err = s.Client().R().SetHeaders(map[string]string{
					"Accept":          "*/*",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Content-type":    "application/x-www-form-urlencoded",
					"Host":            "areaclienti.a2aenergia.eu",
					"Origin":          "https://www.a2aenergia.eu",
					"Referer":         "https://www.a2aenergia.eu/area_clienti/",
				}).SetFormData(map[string]string{
					"username":      *data.Newusername,
					"password":      *account.Password,
					"startURL":      "/services/oauth2/authorize?response_type=token&client_id=3MVG9fTLmJ60pJ5Ju.bDhhxrt9qt8JPmj4NW9qkQoH18W_PfMNFu1ECzCMI3I2tCkZiJP7aMGga4hg6VA7N71&redirect_uri=https%3A%2F%2Fwww.a2aenergia.eu%2F_callback.html&state=https%3A%2F%2Fwww.a2aenergia.eu%2Farea_clienti%2F",
					"mode":          "inline",
					"maskRedirects": "false",
				}).Post(loginwidgetcontroller)
				if res == nil {
					return nil, ConnectionErr("no response for " + loginwidgetcontroller)
				}
				s.Debug(loginwidgetcontroller, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(loginwidgetcontroller + " : " + err.Error())
				}
				var urlJson struct {
					Result string
				}
				err = utils.FromJSON(&urlJson, bytes.NewBuffer(res.Body()))
				if err != nil {
					return nil, ParseErr("parse loginwidgetcontroller response: " + res.String())
				}
				if urlJson.Result == "invalid" {
					return nil, LoginErr("invalid password")
				}
				/*{
				   "result": "https://areaclienti.a2aenergia.eu/secur/frontdoor.jsp?sid=00D1t000000D4H8%21ARQAQKiKXm6Ifwph7cIp89Dmh3Wi.qil68drc1jH_EN9B9BSUtd_Hi_DC7MGrAkRlFSri_K9Q_U1nx2tixiEVLPNNBRYrxGx&retURL=%2Fservices%2Foauth2%2Fauthorize%3Fresponse_type%3Dtoken%26client_id%3D3MVG9fTLmJ60pJ5Ju.bDhhxrt9qt8JPmj4NW9qkQoH18W_PfMNFu1ECzCMI3I2tCkZiJP7aMGga4hg6VA7N71%26redirect_uri%3Dhttps%253A%252F%252Fwww.a2aenergia.eu%252F_callback.html%26state%3Dhttps%253A%252F%252Fwww.a2aenergia.eu%252Farea_clienti%252F&apv=1&allp=1&untethered=&cshc=Y00000AQiJDt000000D4H8&refURL=https%3A%2F%2Fareaclienti.a2aenergia.eu%2Fsecur%2Ffrontdoor.jsp"
				}*/

				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"Referer":         "https://www.a2aenergia.eu/area_clienti/",
				}).Get(urlJson.Result)
				if res == nil {
					return nil, ConnectionErr("no response for " + urlJson.Result)
				}
				s.Debug(urlJson.Result, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(urlJson.Result + " : " + err.Error())
				}
				m, er := utils.MatchRegex(res.String(), hrefRegex)
				if er != nil {
					return nil, ParseErr("match href regex: " + res.String())
				}
				href := m[0][1]
				//https://areaclienti.a2aenergia.eu/loginflow/lightningLoginFlow.apexp?retURL=%2Fservices%2Foauth2%2Fauthorize%3Fresponse_type%3Dtoken%26client_id%3D3MVG9fTLmJ60pJ5Ju.bDhhxrt9qt8JPmj4NW9qkQoH18W_PfMNFu1ECzCMI3I2tCkZiJP7aMGga4hg6VA7N71%26redirect_uri%3Dhttps%253A%252F%252Fwww.a2aenergia.eu%252F_callback.html%26state%3Dhttps%253A%252F%252Fwww.a2aenergia.eu%252Farea_clienti%252F&sparkID=Login_Flow

				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"referer":         urlJson.Result,
				}).Get(href)
				if res == nil {
					return nil, ConnectionErr("no response for " + href)
				}
				s.Trace(href, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(href + " : " + err.Error())
				}
				doc, er := goquery.NewDocumentFromReader(strings.NewReader(res.String()))
				if er != nil {
					return nil, ParseErr("parse document for " + href + " :  " + err.Error())
				}
				form := doc.Find("form")
				if form.Length() == 0 {
					return nil, ParseErr("form not found in doc")
				}
				formId, _ := form.Attr("id")

				viewState, er := utils.MatchRegex(res.String(), viewStateR)
				if er != nil {
					return nil, ParseErr("parse view state regex" + err.Error())
				}
				viewStateVersione, er := utils.MatchRegex(res.String(), viewStateVersionR)
				if er != nil {
					return nil, ParseErr("parse view state version regex" + err.Error())
				}
				viewStateMAC, er := utils.MatchRegex(res.String(), viewStateMACR)
				if er != nil {
					return nil, ParseErr("parse viewStateMACR regex" + err.Error())
				}
				viewStateCSRF, er := utils.MatchRegex(res.String(), viewStateCSRFR)
				if er != nil {
					return nil, ParseErr("parse viewStateCSRF regex" + err.Error())
				}

				thePageScriptId, exx := Attr(doc.Find(`script[id^=thePage]`), "id")
				if exx != nil {
					return nil, exx
				}

				res, err = s.Client().R().SetHeaders(map[string]string{
					"Accept":          "*/*",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Content-type":    "application/x-www-form-urlencoded; charset=UTF-8",
					"Host":            "areaclienti.a2aenergia.eu",
					"Origin":          "https://www.a2aenergia.eu",
					"Referer":         href,
				}).SetFormData(map[string]string{
					"AJAXREQUEST":                          "_viewRoot",
					formId:                                 formId,
					"com.salesforce.visualforce.ViewState": viewState[0][1],
					"com.salesforce.visualforce.ViewStateVersion": viewStateVersione[0][1],
					"com.salesforce.visualforce.ViewStateMAC":     viewStateMAC[0][1],
					"com.salesforce.visualforce.ViewStateCSRF":    viewStateCSRF[0][1],
					thePageScriptId: thePageScriptId,
					"url":           oauth2,
				}).Post(formPostUrl)
				if res == nil {
					return nil, ConnectionErr("no response for " + formPostUrl)
				}
				s.Debug(formPostUrl, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(formPostUrl + " : " + err.Error())
				}
				oauthHref, er := utils.MatchRegex(res.String(), hrefR)
				if er != nil {
					return nil, ParseErr("parse oauthHref regex: " + err.Error())
				}

				s.EnableRedirects(false)

				//https://areaclienti.a2aenergia.eu/services/oauth2/authorize?response_type=token&client_id=3MVG9fTLmJ60pJ5Ju.bDhhxrt9qt8JPmj4NW9qkQoH18W_PfMNFu1ECzCMI3I2tCkZiJP7aMGga4hg6VA7N71&redirect_uri=https%3A%2F%2Fwww.a2aenergia.eu%2F_callback.html&state=https%3A%2F%2Fwww.a2aenergia.eu%2Farea_clienti%2F
				url := "https://areaclienti.a2aenergia.eu" + oauthHref[0][1]
				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"referer":         href,
				}).Get(url)
				if res == nil {
					return nil, ConnectionErr("no response for " + url)
				}
				s.Debug(url, FormatResponse(res))
				if res.StatusCode() != 302 {
					return nil, ConnectionErr(url + " no redirect")
				}
				location := HeaderByNameAndValStartsWith(res.Header(), "Location", "https")
				s.Trace(location)

				s.EnableRedirects(true)

				//https://areaclienti.a2aenergia.eu/setup/secur/RemoteAccessAuthorizationPage.apexp?source=CAAAAXeCmWW4ME8wM1kwMDAwMDA0QzlHAAAA5GAHAQWOMbxikUj8YOAdLRkGVfu1Cr5X0hpDYNjH9JpNAuZ0ySgNBXyTfmgJIIi2QjvmlWyq7ty8SIFvmmO0V-REipUrHHzswxlA2uqnYtgnuUDHDK08-ISzTwN4sjdnziv8HcNg8fb50GW7RtByhFJKkC5n2z3cMDg5WqHUD395UTMps9gQsGHgDvTJ1TQYYMbZDPDdurGMOX7xHd9GbJ24Ze35poQ5E3W5wX-QJNQU4Yzp6QiibahDMk-ncYR7Qs9mxUk9ZHdlQl1WMO9EBZ3raw_a0VFD3WQ5hotFSOr9Bv2pYB9b1zqk3aX3XGQ16fZTZyQ_TemKsapn__KsSaKwQygBZr-QPH7vIWlBGTZWRf9vtFesgJ8TBy2MUApcZdSWjBodeV7pkk8_BtNXSVsQJr4Gu_mKDNEHSa1XJEOCNZ4AK39bdgSGM4OOEYkquUhZi4WR2Uzd7nzrr2hs8lTZqt62jWcAKi91Bj6YXYo-4u5zxXIH8_sNXPIA03onNHAZ8aHmrmR9joxcSK1WTeQllPJADxvnwJ5u0fTXbqXtd-DbKPwXc2Vn699NUXM62jSPkKnewmPtP8MBSONF6ypw796EeHH1RCqIYfdk73fJReJ2FqTets8lpTLfWDcY1J_0kht__Ft2o7yweNLMcWuNDFez4XZGzGAXGceLst5DRS0QzF6J949dgp4jwAjylQ%3D%3D
				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"referer":         href,
				}).Get(location)
				if res == nil {
					return nil, ConnectionErr("no response for " + location)
				}
				s.Debug(location, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(location + " : " + err.Error())
				}
				callbackHref, er := utils.MatchRegex(res.String(), hrefR)
				if er != nil {
					return nil, ParseErr("parse oauthHref regex: " + err.Error())
				}
				callbackUrl := callbackHref[0][1]
				s.Debug(callbackUrl)

				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"referer":         "https://areaclienti.a2aenergia.eu/",
				}).Get(callback /*callbackUrl*/)
				if res == nil {
					return nil, ConnectionErr("no response for " + callbackUrl)
				}
				s.Debug(callbackUrl, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(callbackUrl + " : " + err.Error())
				}

				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
				}).Get(areaClienti)
				if res == nil {
					return nil, ConnectionErr("no response for " + callbackUrl)
				}
				s.Debug(callbackUrl, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(callbackUrl + " : " + err.Error())
				}

				//https://areaclienti.a2aenergia.eu/s/
				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "areaclienti.a2aenergia.eu",
					"referer":         "https://www.a2aenergia.eu/area_clienti/",
				}).Get(areaClienti)
				if res == nil {
					return nil, ConnectionErr("no response for " + callbackUrl)
				}
				s.Debug(callbackUrl, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(callbackUrl + " : " + err.Error())
				}

				//https://sportello.a2aenergia.eu/miserviWeb/sportello/embedCustomerGraficiPlancia
				res, err = s.Client().R().SetHeaders(map[string]string{
					"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
					"Accept-Encoding": "gzip, deflate, br",
					"Connection":      "keep-alive",
					"Host":            "sportello.a2aenergia.eu",
					"referer":         "https://areaclienti.a2aenergia.eu/",
				}).Get("https://sportello.a2aenergia.eu/miserviWeb/sportello/embedCustomerGraficiPlancia")
				if res == nil {
					return nil, ConnectionErr("no response for " + callbackUrl)
				}
				s.Debug(callbackUrl, FormatResponse(res))
				if err != nil {
					return nil, ConnectionErr(callbackUrl + " : " + err.Error())
				}

				//return nil, ParseErr(s.formatResponse(res))
			}
		} else {
			return nil, ParseErr(FormatResponse(res))
		}
	} else {
		return nil, ParseErr(FormatResponse(res))
	}

	return nil, nil
}

func (s *A2AEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *A2AEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *A2AEnergiaStrategy) LoadsInternal() bool {
	return false
}

func (s *A2AEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *A2AEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
