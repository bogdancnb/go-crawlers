package aceaenergia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"net/url"
	"strings"
)

const (
	root        = "https://www.acea.it/"
	myacea      = "https://my.acea.it/myacea/it/"
	ondemand    = "https://au5nhmzph.accounts.ondemand.com/saml2/idp/sso/au5nhmzph.accounts.ondemand.com?"
	home        = "https://au5nhmzph.accounts.ondemand.com/"
	sso         = "https://au5nhmzph.accounts.ondemand.com/saml2/idp/sso"
	ssoRedirect = "https://au5nhmzph.accounts.ondemand.com/saml2/idp/sso?redirect=true"
	samlSSO     = "https://my.acea.it/myacea/saml/SSO"
	invoices    = "https://my.acea.it/myacea/it/invoices"
	balance     = "https://my.acea.it/myacea/it/invoices/balance/ML/%s?fromDate=&toDate="
	invoiceList = "https://my.acea.it/myacea/it/invoices/ML/%s?fromDate=&toDate="

	loginErrMsg   = `we could not authenticate you`
	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("aceaEnergia.crawler", new(AceaEnergiaFactory))
}

type AceaEnergiaFactory struct{}

func (p *AceaEnergiaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "AceaEnergiaStrategy")
	return &AceaEnergiaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type AceaEnergiaStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations                                                                                      map[string]*model.Location
	exInvoices                                                                                       map[string]*model.Invoice
	doc                                                                                              *goquery.Document
	HYB_JSESSIONID, NSC_izcdpn, acceleratorSecureGUID, ACEA_USER_COOKIE, HYB_JS_SESSION, ACEA_ASM_ID string
}

func (s *AceaEnergiaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *AceaEnergiaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *AceaEnergiaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *AceaEnergiaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
	}).Get(root)
	if err != nil {
		return nil, ConnectionErr("Get" + root + ": " + err.Error())
	}

	s.EnableRedirects(false)
	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
	}).Get(myacea)
	if res == nil || res.StatusCode() != 302 {
		return nil, ConnectionErr("Get" + myacea)
	}
	loc1 := HeaderByNameAndValStartsWith(res.Header(), "Location", "")
	if loc1 == "" {
		return nil, ConnectionErr("Get" + myacea + " no location header found")
	}
	if !strings.Contains(loc1, ondemand) {
		return nil, ParseErr("redirect location invalid: " + loc1)
	}
	locQueryParams := loc1[strings.Index(loc1, ondemand)+len(ondemand):]
	locQueryParams, err = url.QueryUnescape(locQueryParams)
	if err != nil {
		return nil, ParseErr("redirect location invalid: " + loc1)
	}
	SAMLRequest := locQueryParams[strings.Index(locQueryParams, "SAMLRequest=")+12 : strings.Index(locQueryParams, "&SigAlg")]
	SigAlg := locQueryParams[strings.Index(locQueryParams, "SigAlg=")+7 : strings.Index(locQueryParams, "&Signature")]
	Signature := locQueryParams[strings.Index(locQueryParams, "Signature=")+10:]
	s.HYB_JSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "HYB_JSESSIONID")
	s.HYB_JSESSIONID = s.HYB_JSESSIONID[:strings.Index(s.HYB_JSESSIONID, "; ")]
	NSC_izcdpn := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "NSC_izcdpn")
	NSC_izcdpn = NSC_izcdpn[:strings.Index(NSC_izcdpn, ";")]

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "au5nhmzph.accounts.ondemand.com",
	}).Get(loc1)
	if res == nil {
		return nil, ConnectionErr("Get " + home + " no response")
	}
	if err != nil {
		return nil, ConnectionErr("Get " + home + " : " + err.Error())
	}
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse html document for " + home + " : " + err.Error())
	}
	authenticity_token, ex := Attr(doc.Find("input[name='authenticity_token']"), "value")
	if ex != nil {
		return nil, ex
	}
	xsrfProtection, ex := Attr(doc.Find("input[name='xsrfProtection']"), "value")
	if ex != nil {
		return nil, ex
	}
	idpSSOEndpoint, ex := Attr(doc.Find("input[name='idpSSOEndpoint']"), "value")
	if ex != nil {
		return nil, ex
	}
	//sp, ex := Attr(doc.Find("input[name='sp']"), "value")
	//if ex != nil {
	//	sp = ""
	//}
	//RelayState, ex := Attr(doc.Find("input[name='RelayState']"), "value")
	//if ex != nil {
	//	RelayState = ""
	//}
	spId, ex := Attr(doc.Find("input[name='spId']"), "value")
	if ex != nil {
		return nil, ex
	}
	spName, ex := Attr(doc.Find("input[name='spName']"), "value")
	if ex != nil {
		return nil, ex
	}
	mobileSSOToken, ex := Attr(doc.Find("input[name='mobileSSOToken']"), "value")
	if ex != nil {
		return nil, ex
	}
	tfaToken, ex := Attr(doc.Find("input[name='tfaToken']"), "value")
	if ex != nil {
		return nil, ex
	}
	XSRF_COOKIE := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "XSRF_COOKIE")
	XSRF_COOKIE = XSRF_COOKIE[:strings.Index(XSRF_COOKIE, "; ")]
	JSESSIONID := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "JSESSIONID")
	JSESSIONID = JSESSIONID[:strings.Index(JSESSIONID, "; ")]

	post, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"cookie":       XSRF_COOKIE + "; " + JSESSIONID,
		"content-type": "application/x-www-form-urlencoded",
		"host":         "au5nhmzph.accounts.ondemand.com",
		"origin":       "https://au5nhmzph.accounts.ondemand.com",
		"referer":      "https://au5nhmzph.accounts.ondemand.com/",
	}).SetFormData(map[string]string{
		"utf8":               "\n",
		"authenticity_token": authenticity_token,
		"xsrfProtection":     xsrfProtection,
		"method":             "GET",
		"idpSSOEndpoint":     idpSSOEndpoint,
		//"sp":                 sp,
		//"RelayState":         RelayState,
		"SAMLRequest":    SAMLRequest,
		"Signature":      Signature,
		"SigAlg":         SigAlg,
		"targetUrl":      "",
		"sourceUrl":      "",
		"org":            "",
		"spId":           spId,
		"spName":         spName,
		"mobileSSOToken": mobileSSOToken,
		"tfaToken":       tfaToken,
		"css":            "",
		"j_username":     account.Username,
		"j_password":     *account.Password,
	}).Post(loc1)
	if post == nil {
		return nil, ConnectionErr("no response for POST " + loc1)
	}
	if post.StatusCode() == 200 && strings.Contains(post.String(), loginErrMsg) {
		return nil, LoginErr(loginErrMsg)
	}
	if post.StatusCode() != 302 {
		return nil, ConnectionErr("POST " + sso + " got status code " + post.Status())
	}
	loc2 := HeaderByNameAndValStartsWith(post.Header(), "Location", "")
	if loc2 == "" {
		return nil, ConnectionErr("POST " + sso + " no redirect location")
	}
	XSRF_COOKIE = HeaderByNameAndValStartsWith(post.Header(), "Set-Cookie", "XSRF_COOKIE")
	XSRF_COOKIE = XSRF_COOKIE[:strings.Index(XSRF_COOKIE, "; ")]
	IDP_USER := HeaderByNameAndValStartsWith(post.Header(), "Set-Cookie", "IDP_USER")
	IDP_USER = IDP_USER[:strings.Index(IDP_USER, "; ")]
	IDP_J_COOKIE := HeaderByNameAndValStartsWith(post.Header(), "Set-Cookie", "IDP_J_COOKIE")
	IDP_J_COOKIE = IDP_J_COOKIE[:strings.Index(IDP_J_COOKIE, "; ")]
	//JSESSIONID := HeaderByNameAndValStartsWith(post.Header(), "Set-Cookie", "JSESSIONID")
	//JSESSIONID = JSESSIONID[:strings.Index(JSESSIONID, "; ")]
	//X-IDS-ID

	res, err = s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie":     IDP_USER + "; " + IDP_J_COOKIE + "; " + XSRF_COOKIE,
		"host":       "au5nhmzph.accounts.ondemand.com",
		"referer":    "https://au5nhmzph.accounts.ondemand.com/",
	}).Get(loc2)
	if res == nil {
		return nil, ConnectionErr("no repsonse for " + "GET " + loc2)
	}
	JSESSIONID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "JSESSIONID")
	JSESSIONID = JSESSIONID[:strings.Index(JSESSIONID, "; ")]
	doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(res.Body()))
	if err != nil {
		return nil, ParseErr("parse html document for " + home + " : " + err.Error())
	}
	authenticity_token, ex = Attr(doc.Find("input[name='authenticity_token']"), "value")
	if ex != nil {
		return nil, ex
	}
	SAMLResponse, ex := Attr(doc.Find("input[id='SAMLResponse']"), "value")
	if ex != nil {
		return nil, ex
	}

	post2, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"cookie":       s.HYB_JSESSIONID + "; " + NSC_izcdpn,
		"host":         "my.acea.it",
		"origin":       "https://au5nhmzph.accounts.ondemand.com",
		"referer":      "https://au5nhmzph.accounts.ondemand.com/",
	}).SetFormData(map[string]string{
		"utf8":               "\n",
		"authenticity_token": authenticity_token,
		"SAMLResponse":       SAMLResponse,
	}).Post(samlSSO)
	if post2 == nil {
		return nil, ConnectionErr("no response for POST" + samlSSO)
	}
	if post2.StatusCode() != 302 {
		return nil, ConnectionErr("POST" + samlSSO + " got status=" + post2.Status())
	}
	NSC_izcdpn = HeaderByNameAndValStartsWith(post2.Header(), "Set-Cookie", "NSC_izcdpn")
	NSC_izcdpn = NSC_izcdpn[:strings.Index(NSC_izcdpn, ";")]
	acceleratorSecureGUID := HeaderByNameAndValStartsWith(post2.Header(), "Set-Cookie", "acceleratorSecureGUID")
	acceleratorSecureGUID = acceleratorSecureGUID[:strings.Index(acceleratorSecureGUID, ";")]
	s.acceleratorSecureGUID = acceleratorSecureGUID

	get2, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie":     s.HYB_JSESSIONID + "; " + NSC_izcdpn + "; " + acceleratorSecureGUID,
		"host":       "my.acea.it",
		"referer":    "https://au5nhmzph.accounts.ondemand.com/",
	}).Get(myacea)
	if get2 == nil {
		return nil, ConnectionErr("no response for GET" + myacea)
	}
	if get2.StatusCode() != 200 {
		return nil, ConnectionErr("GET" + myacea + " got status=" + get2.Status())
	}
	ACEA_USER_COOKIE := HeaderByNameAndValStartsWith(get2.Header(), "Set-Cookie", "ACEA_USER_COOKIE")
	ACEA_USER_COOKIE = ACEA_USER_COOKIE[:strings.Index(ACEA_USER_COOKIE, ";")]
	s.ACEA_USER_COOKIE = ACEA_USER_COOKIE
	HYB_JS_SESSION := HeaderByNameAndValStartsWith(get2.Header(), "Set-Cookie", "HYB_JS_SESSION")
	HYB_JS_SESSION = HYB_JS_SESSION[:strings.Index(HYB_JS_SESSION, ";")]
	s.HYB_JS_SESSION = HYB_JS_SESSION
	NSC_izcdpn = HeaderByNameAndValStartsWith(get2.Header(), "Set-Cookie", "NSC_izcdpn")
	NSC_izcdpn = NSC_izcdpn[:strings.Index(NSC_izcdpn, ";")]
	s.NSC_izcdpn = NSC_izcdpn
	s.EnableRedirects(true)

	s.doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer(get2.Body()))
	if err != nil {
		return nil, ParseErr("could not parse html for main page: " + err.Error())
	}
	html := get2.String()

	return &html, nil
}

func (s *AceaEnergiaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *AceaEnergiaStrategy) LoadLocations(account *model.Account) Exception {
	contractsTable := s.doc.Find("table[id='contractsTable']")
	if contractsTable.Length() == 0 {
		return ParseErr("contractsTable not found")
	}
	rows := contractsTable.Find("tbody tr")
	for i := 0; i < rows.Length(); i++ {
		row := rows.Eq(i)
		details, ex := Attr(row, "data-moredetails-id")
		if ex != nil {
			return ex
		}
		cols := row.Find("td")
		if cols.Length() != 6 {
			return ParseErr("invalid row format for " + details)
		}
		divs := cols.Eq(1).Find("div")
		codLoc := strings.TrimSpace(divs.Eq(1).Text())
		addr := string(utils.RemoveAdjacentUnicodeSpaces([]byte(strings.TrimSpace(cols.Eq(2).Text()))))
		addr = strings.ReplaceAll(addr, "\n", " ")
		l := &model.Location{
			Service:    addr,
			Details:    &details,
			Identifier: codLoc,
		}
		account.AddLocation(l)
	}

	get, err := s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie": s.HYB_JSESSIONID + "; " + s.NSC_izcdpn + "; " + s.acceleratorSecureGUID + "; " +
			s.ACEA_USER_COOKIE + "; " + s.HYB_JS_SESSION,
		"host": "my.acea.it",
	}).Get(invoices)
	_, ex := OkDocument(get, err)
	if ex != nil {
		return ex
	}
	s.NSC_izcdpn = HeaderByNameAndValStartsWith(get.Header(), "Set-Cookie", "NSC_izcdpn")
	s.NSC_izcdpn = s.NSC_izcdpn[:strings.Index(s.NSC_izcdpn, ";")]
	return nil
}

func (s *AceaEnergiaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	uri := fmt.Sprintf(balance, *location.Details)
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":     "*/*",
		"connection": "keep-alive",
		"cookie": s.HYB_JSESSIONID + "; " + s.NSC_izcdpn + "; " + s.acceleratorSecureGUID +
			s.ACEA_USER_COOKIE + "; " + s.HYB_JS_SESSION,
		"host":    "my.acea.it",
		"referer": "https://my.acea.it/myacea/it/invoices",
	}).Get(uri))
	s.Trace(res)
	if ex != nil {
		return ex
	}
	html := WrapHtmlBody(res)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ParseErr("parse html for GET " + uri)
	}

	amEl := doc.Find("div:containsOwn('Totale da pagare')")
	if amEl.Length() == 0 {
		return ParseErr("amount element not found")
	}
	amEl = amEl.Next()
	amS := strings.TrimSpace(amEl.Text())
	amP := strings.Split(amS, "€")
	if len(amP) == 2 {
		amS = amP[1]
		amS = strings.ReplaceAll(amS, `,`, `.`)
		amS = utils.RemoveUnicodeSpaces([]byte(amS))
		am, ex := AmountFromString(amS)
		if ex != nil {
			return ParseErr("parse location amount: " + amS)
		}
		location.Amount = &am
	}
	return nil
}

func (s *AceaEnergiaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	uri := fmt.Sprintf(invoiceList, *location.Details)
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":     "*/*",
		"connection": "keep-alive",
		"cookie": s.HYB_JSESSIONID + "; " + s.NSC_izcdpn + "; " + s.acceleratorSecureGUID +
			s.ACEA_USER_COOKIE + "; " + s.HYB_JS_SESSION,
		"host":    "my.acea.it",
		"referer": "https://my.acea.it/myacea/it/invoices",
	}).Get(uri))
	s.Trace(res)
	if ex != nil {
		return ex
	}
	html := WrapHtmlBody(res)
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ParseErr("parse html for GET " + uri)
	}

	invoicesTable := doc.Find("table[id='invoicesTable']")
	if invoicesTable.Length() == 0 {
		return ParseErr("invoicesTable not found")
	}
	rows := invoicesTable.Find("tbody tr")
	for i := 0; i < rows.Length(); i++ {
		row := rows.Eq(i)
		cols := row.Find("td")
		if cols.Length() != 9 {
			html, _ = row.Html()
			return ParseErr("invalid invoice row format: " + html)
		}
		if ex := s.parseInvoice(location, cols); ex != nil {
			return ex
		}
	}
	ok := location.SetInvoiceDueFromAmount()
	if !ok {
		return ParseErr("could not set invoices due from location amount")
	}
	return nil
}

func (s *AceaEnergiaStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	ref := strings.TrimSpace(cols.Eq(3).Text())
	issue, ex := Attr(cols.Eq(4), "data-orderable")
	if ex != nil {
		return ex
	}
	due, ex := Attr(cols.Eq(5), "data-orderable")
	if ex != nil {
		return ex
	}
	amS := strings.TrimSpace(cols.Eq(6).Text())
	amS = amS[:strings.Index(amS, `€`)]
	amount, ex := AmountFromString(amS)
	if ex != nil {
		return ex
	}
	var pdfUri *string
	pdfEl := cols.Eq(7).Find("div")
	if pdfEl.Length() == 0 {
		s.Warn("pdf link not found")
	} else {
		href, ex := Attr(pdfEl.Eq(0), "data-invoiceajaxtest-link")
		if ex != nil {
			return ex
		}
		pathParts := strings.Split(href, "/")
		lastPart := pathParts[len(pathParts)-1]
		escaped := url.PathEscape(lastPart)
		href = strings.ReplaceAll(href, lastPart, escaped)
		href = "https://my.acea.it" + href
		pdfUri = &href
	}
	i := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    amount,
		AmountDue: 0,
		IssueDate: issue,
		DueDate:   due,
	}
	location.AddInvoice(i)
	return nil
}

func (s *AceaEnergiaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *AceaEnergiaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"cookie": s.HYB_JSESSIONID + "; " + s.NSC_izcdpn + "; " + s.acceleratorSecureGUID +
			s.ACEA_USER_COOKIE + "; " + s.HYB_JS_SESSION,
		"host":    "my.acea.it",
		"referer": "https://my.acea.it/myacea/it/invoices",
	}).Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *AceaEnergiaStrategy) LoadsInternal() bool {
	return false
}

func (s *AceaEnergiaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *AceaEnergiaStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
