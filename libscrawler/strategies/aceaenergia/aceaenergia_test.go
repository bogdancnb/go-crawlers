package aceaenergia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "aceaEnergia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"sebdim46@live.it", "seba1946"}, //login err
		{"cristianaluly@yahoo.it", "Malu-0314"}, //ok
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "cristianaluly@yahoo.it", Password: "Malu-0314", Uri: "aceaEnergia.crawler",
			Identifier: "asdasd",
			Ref:        "922001618475", PdfUri: "https://my.acea.it/myacea/it/invoices/download?c=ML&contr=IT002E4404948A&code=5Rp0LkBCGXiOAidiF2cqIGroW2MMdFU-584PqQZUiOk=&odn=922001618475&nbr=922001618475&iit=Fatture/Rate%20IS-U&m=s",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
