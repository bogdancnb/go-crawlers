package wadowicenet

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "wadowice_net.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"8325", "oZce"}, //ok
		//{"pawnowicki1977@gmail.com", "slugasandry1977"}, //?
		//{"pawel530@gmeil. com", "slugasandry1977"}, //?

	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "8325", Password: "oZce", Uri: "wadowice_net.crawler",
			Identifier: "8325",
			Ref:        "4376/10/Z/2020", PdfUri: "https://ebok.wad.pl/userpanel/documents/get/ID/2742316",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
