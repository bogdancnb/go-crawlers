package wadowicenet

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

const (
	root  = "https://ebok.wad.pl"
	auth  = "https://ebok.wad.pl/userpanel/auth"
	login = "https://ebok.wad.pl/userpanel/auth/login"
	pay   = "https://ebok.wad.pl/userpanel/pay"

	acceptHeader = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
	loginErrMsg  = "Błędny login lub hasło"
	accNrSel     = "p[class='bigNumber']" //"h4:containsOwn('Numer konta')"

	siteFormat = "2006-01-02"
)

var (
	legalEntity = "WadowiceNET Sp. z o.o."
	bankName    = "Bank Millennium SA"
	bankBIC     = "BIGBPLPW"
)

func Init() {
	RegisterFactory("wadowice_net.crawler", new(WadowiceNetFactory))
}

type WadowiceNetFactory struct{}

func (p *WadowiceNetFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "WadowiceNetStrategy")
	return &WadowiceNetStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type WadowiceNetStrategy struct {
	*logrus.Entry
	HttpStrategy
	doc *goquery.Document
}

func (s *WadowiceNetStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *WadowiceNetStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *WadowiceNetStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *WadowiceNetStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	_, ex := OkDocument(s.Client().R().SetHeader("accept", acceptHeader).Get(auth))
	if ex != nil {
		return nil, ex
	}

	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       acceptHeader,
		"content-type": "application/x-www-form-urlencoded",
		"origin":       root,
		"referer":      auth,
	}).SetFormData(map[string]string{
		"username": account.Username,
		"password": *account.Password,
	}).Post(login))
	if ex != nil {
		return nil, ex
	}

	txt, err := s.doc.Html()
	if err != nil {
		return nil, ParseErr("parsing html for " + login)
	}

	return &txt, nil
}

func (s *WadowiceNetStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *WadowiceNetStrategy) LoadLocations(account *model.Account) Exception {
	nrClientEl := s.doc.Find("h5:containsOwn('Numer klienta')")
	if nrClientEl.Length() == 0 {
		return ParseErr("no client nr element found")
	}
	clientNr := nrClientEl.Next().Text()
	clientNr = utils.RemoveSpaces(clientNr)
	service := clientNr
	detailsEl := s.doc.Find("h5:containsOwn('Dane właściciela')")
	if detailsEl.Length() != 0 {
		service = detailsEl.Next().Text()
		service = string(utils.RemoveAdjacentUnicodeSpaces([]byte(strings.TrimSpace(service))))
	}
	loc := &model.Location{
		Service:    service,
		Identifier: clientNr,
	}
	account.AddLocation(loc)
	return nil
}

func (s *WadowiceNetStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *WadowiceNetStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	var ex Exception
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":  acceptHeader,
		"referer": "https://ebok.wad.pl/userpanel/panel",
	}).Get(pay))
	if ex != nil {
		return ex
	}
	accNrEl := s.doc.Find(accNrSel)
	if accNrEl.Length() == 0 {
		return ParseErr("could not find account number element: " + accNrSel)
	}
	accNr, _ := accNrEl.Html()
	accNr = utils.RemoveUnicodeSpaces([]byte(accNr))
	//accNr = utils.RemoveNonASCII(accNr)
	//l := len(accNr)
	//s.Debug(l)
	location.ProviderBranch = &model.ProviderBranchDTO{
		Iban:            &accNr,
		BankName:        &bankName,
		BankBIC:         &bankBIC,
		LegalEntityName: &legalEntity,
	}

	unpaidInvoices := s.doc.Find("h2:containsOwn('Dokumenty nieopłacone')").Next().Find("tbody tr")
	//paidInvoices := s.doc.Find("h2:containsOwn('Dokumenty opłacone')").Next().Find("tbody tr")
	for i := 0; i < unpaidInvoices.Length(); i++ {
		cols := unpaidInvoices.Eq(i).Find("td")
		if ex := s.parseInvoice(location, cols); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *WadowiceNetStrategy) parseInvoice(location *model.Location, cols *goquery.Selection) Exception {
	dueString := strings.TrimSpace(cols.Eq(5).Text())
	if dueString == "" {
		return nil
	}
	amtS := strings.TrimSpace(cols.Eq(3).Text())
	amtP := strings.Split(amtS, " ")
	amt, ex := AmountFromString(amtP[0])
	if ex != nil {
		return ex
	}
	if amt == 0 {
		return nil
	}

	amDueS := strings.TrimSpace(cols.Eq(4).Text())
	amtP = strings.Split(amDueS, " ")
	amtDue, ex := AmountFromString(amtP[0])
	if ex != nil {
		return ex
	}

	ref := strings.TrimSpace(cols.Eq(2).Text())
	issueString := strings.TrimSpace(cols.Eq(1).Text())
	issueDate, err := time.Parse(siteFormat, issueString)
	if err != nil {
		return ParseErr("parse issue date : " + issueString)
	}
	dueDate, err := time.Parse(siteFormat, dueString)
	if err != nil {
		return ParseErr("parse due date " + dueString)
	}
	var pdfUri *string
	href, ex := Attr(cols.Eq(6).Find("a"), "href")
	if ex != nil {
		s.Debug("could not parse pdf uri for " + ref)
	} else {
		uri := "https://ebok.wad.pl" + href
		pdfUri = &uri
	}
	invoice := &model.Invoice{
		Ref:       ref,
		PdfUri:    pdfUri,
		Amount:    amt,
		AmountDue: amtDue,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *WadowiceNetStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	payments := s.doc.Find("div[id='nav-history']").Find("table tbody tr")
	for i := 0; i < payments.Length(); i++ {
		cols := payments.Eq(i).Find("td")
		if ex := s.parsePayment(location, cols); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *WadowiceNetStrategy) parsePayment(location *model.Location, cols *goquery.Selection) Exception {
	dateS := strings.TrimSpace(cols.Eq(1).Text())
	date, err := time.Parse(siteFormat, dateS)
	if err != nil {
		return ParseErr("parse date : " + dateS)
	}
	ref := strings.TrimSpace(cols.Eq(3).Text())
	amtS := strings.TrimSpace(cols.Eq(4).Text())
	amtP := strings.Split(amtS, " ")
	amt, ex := AmountFromString(amtP[0])
	if ex != nil {
		return ex
	}
	p := &model.Payment{
		Ref:    ref,
		Amount: amt,
		Date:   date.Format(utils.DBDateFormat),
	}
	location.AddPayment(p)
	return nil
}

func (s *WadowiceNetStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, ex := GetBytes(s.Client().R().Get(*account.Locations[0].Invoices[0].PdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *WadowiceNetStrategy) LoadsInternal() bool {
	return false
}

func (s *WadowiceNetStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *WadowiceNetStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
