package reginamaria

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "regina_maria.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"daniela.canelea88@gmail.com", "140119Ema!"}, //ok
		//{"alexandrairina.noaghi@yahoo.ro", "Gameloft2015"}, //login err
		//	{"RM55109_1", "Regina55109_1"}, //HR
		//{"lasc.andrei@gmail.com", "3turmeddfeO!"}, //pacient nevalidat
		//{"RM53292_1", "D$fJHTgK!QTw8bc7"},
		//{"rrotter@deviqon.com", "!OG*8iwB7dwnLUJS"}, //choose account
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
