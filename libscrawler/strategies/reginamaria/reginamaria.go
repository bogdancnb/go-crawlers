package reginamaria

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	root                 = "https://contulmeu.reginamaria.ro/"
	LOGIN                = "https://contulmeu.reginamaria.ro/Account/Login"
	FINANCIAL            = "https://contulmeu.reginamaria.ro/Patient/FinancialArea/Get?FirstResult=0&MaxResults=20&Date=&DueDate=&ProviderCode=&State=&requestTimeStamp="
	HR                   = "https://contulmeu.reginamaria.ro/V1/HR/FinancialArea/Get?FirstResult=0&MaxResults=20&Date=&DueDate=&ProviderCode=&State=&requestTimeStamp="
	PATIENT              = "https://contulmeu.reginamaria.ro/V1/Patient/FinancialArea/Get?FirstResult=0&MaxResults=20&Date=&DueDate=&ProviderCode=&State=&requestTimeStamp"
	PDF                  = "https://contulmeu.reginamaria.ro/User/CurrentUser/Report?commArgEnc="
	financialAreaHR      = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHtml?path=Views%2FHR%2FFinancialArea%2FfinancialArea.html"
	financialAreaPatient = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHtml?path=Views%2FPatient%2FFinancialArea%2FfinancialArea.html"
	getUserRoles         = "https://contulmeu.reginamaria.ro/V1/User/Roles/GetUserRoles?requestTimeStamp"
	setUserRole          = "https://contulmeu.reginamaria.ro/V1/User/Roles/SetRole"

	selectCompany = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHtml?path=Views%2FAccount%2FselectCompany.html"
	hrCompanyGet  = "https://contulmeu.reginamaria.ro/V1/HR/Company/Get?requestTimeStamp="
	hrCompanySet  = "https://contulmeu.reginamaria.ro/V1/HR/Company/SetCompany"
	hrUserRights  = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHrUserRights?requestTimeStamp="
	hrData        = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHrData?requestTimeStamp="

	navBar   = "https://contulmeu.reginamaria.ro/V1/User/CurrentUser/GetHtml?path=Views%2FHR%2Fnavbar.html"
	loginErr = "Adresa de email si/sau parola nu sunt corecte"

	roleHR        = "HR"
	rolePacient   = "Pacient"
	roleVizitator = "Vizitator"
)

func Init() {
	RegisterFactory("regina_maria.crawler", new(ReginaFactory))
}

type ReginaFactory struct{}

func (p *ReginaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "ReginaStrategy")
	return &ReginaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type ReginaStrategy struct {
	*logrus.Entry
	HttpStrategy
	aSPXAUTH      string
	aspSessionID  string
	loginResponse loginResponse
	//accDoc         *goquery.Document
	//javaxFacesView string
	//doc            *goquery.Document

	multipleAccounts bool
	userRoles        []userRole
}

func (s *ReginaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *ReginaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *ReginaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

type loginResponse struct {
	PatientCode  string
	FirstName    string
	LastName     string
	Subscription string
	RoleName     string
}
type userRole struct {
	UserRoleId  string
	RoleName    string
	PatientName string
	Description string
	Enable      bool
	MedisId     int
}

type invoiceJson struct {
	CommArgEnc     string
	DataFactura    string
	DataScadenta   string
	NrFactura      string
	RestPlata      float64
	Status         int
	ValoareFactura float64
}

func (s *ReginaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent":       AgentChrome78,
		"referer":          root,
		"x-requested-with": "XMLHttpRequest",
	})
	res, err := s.Client().R().SetFormData(map[string]string{
		"username":   account.Username,
		"password":   *account.Password,
		"rememberMe": "false",
	}).SetResult(&s.loginResponse).Post(LOGIN)
	if err != nil {
		return nil, ConnectionErr(err.Error())
	}
	resBody := res.String()
	if strings.Contains(resBody, loginErr) || strings.Contains(resBody, "\"ErrorCode\":\"500\"") {
		return nil, LoginErr(resBody)
	}
	s.aSPXAUTH = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", ".ASPXAUTH")
	if s.aSPXAUTH == "" {
		return nil, ParseErr("not set-cookie found")
	}
	s.aSPXAUTH = s.aSPXAUTH[:strings.Index(s.aSPXAUTH, "; expires")]
	s.aspSessionID = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "ASP.NET_SessionId")
	s.aspSessionID = s.aspSessionID[:strings.Index(s.aspSessionID, "; path")]
	txt := res.String()
	return &txt, nil
}

func (s *ReginaStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, "\"ErrorCode\":\"500\"") {
		return LoginErr(*response)
	}
	if s.loginResponse.PatientCode == "" {
		if s.loginResponse.RoleName == roleVizitator {
			return nil
		}
		return ParseErr("no patient code found: " + *response)
	}
	return nil
}

func (s *ReginaStrategy) LoadLocations(account *model.Account) Exception {
	if s.loginResponse.RoleName != roleVizitator {
		loc := &model.Location{
			Service:    s.loginResponse.FirstName + " " + s.loginResponse.LastName + " " + s.loginResponse.Subscription,
			Identifier: s.loginResponse.PatientCode,
		}
		account.AddLocation(loc)
		if ex := s.loadInvoicesInternal(account, loc); ex != nil {
			return ex
		}
		return nil
	}

	s.multipleAccounts = true
	_, err := s.Client().R().SetResult(&s.userRoles).Get(getUserRoles)
	if err != nil {
		return ConnectionErr(getUserRoles + ": " + err.Error())
	}
	for _, role := range s.userRoles {
		s.loginResponse = loginResponse{}
		res, err := s.Client().R().SetFormData(map[string]string{
			"id": role.UserRoleId,
		}).SetResult(&s.loginResponse).Post(setUserRole)
		if err != nil {
			return ConnectionErr(setUserRole + ": " + err.Error())
		}
		s.aSPXAUTH = HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", ".ASPXAUTH")
		if s.aSPXAUTH == "" {
			return ParseErr("not set-cookie found")
		}
		s.aSPXAUTH = s.aSPXAUTH[:strings.Index(s.aSPXAUTH, "; expires")]

		loc := &model.Location{
			Service:    s.loginResponse.FirstName + " " + s.loginResponse.LastName + " " + s.loginResponse.Subscription,
			Identifier: s.loginResponse.PatientCode + "_" + s.loginResponse.RoleName,
		}
		account.AddLocation(loc)
		if ex := s.loadInvoicesInternal(account, loc); ex != nil {
			return ex
		}
	}
	return nil
}

func (s *ReginaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *ReginaStrategy) loadInvoicesInternal(account *model.Account, location *model.Location) Exception {
	uri, financialArea := "", ""
	switch t := strings.ToLower(s.loginResponse.RoleName); t {
	case "pacient":
		uri = FINANCIAL
		financialArea = financialAreaHR
	case "pacient nevalidat":
		uri = PATIENT
		financialArea = financialAreaPatient
	case "hr":
		uri = HR
		financialArea = financialAreaPatient

		res, err := s.Client().R().
			SetHeader("cookie", s.aspSessionID+"; "+s.aSPXAUTH).
			Get(selectCompany)
		if err != nil {
			return ConnectionErr(selectCompany + ": " + err.Error())
		}
		resS := res.String()
		_xsrf := s.xsrf(resS)
		_xsrf_token, exception := s.xsrfToken(res)
		if exception != nil {
			return exception
		}

		var companyRes []struct {
			Id   string
			Name string
		}
		resS, ex := OkString(s.Client().R().SetHeaders(map[string]string{
			"_xsrf":  _xsrf,
			"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
		}).SetResult(&companyRes).Get(hrCompanyGet))
		if ex != nil {
			return ex
		}
		if len(companyRes) == 0 {
			return ParseErr("no company returned: " + resS)
		}
		resS, ex = OkString(s.Client().R().SetHeaders(map[string]string{
			"_xsrf":  _xsrf,
			"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
		}).SetFormData(map[string]string{
			"companyId": companyRes[0].Id,
		}).Post(hrCompanySet))
		if ex != nil {
			return ex
		}
		if !strings.Contains(resS, `"ResponseCode":"200"`) {
			return ParseErr("company set failed: " + resS)
		}

		var userRightsRes struct {
			FinancialArea        bool `json:"financialArea"`
			OccupationalMedicine bool `json:"occupationalMedicine"`
			SubscribersList      bool `json:"subscribersList"`
		}
		resS, ex = OkString(s.Client().R().SetHeaders(map[string]string{
			"_xsrf":  _xsrf,
			"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
		}).SetResult(&userRightsRes).Get(hrUserRights))
		if ex != nil {
			return ex
		}
		if !userRightsRes.FinancialArea {
			return ParseErr("no access right to financial area: " + resS)
		}

		//res, err = s.Client().R().SetHeaders(map[string]string{
		//	"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
		//}).Get(navBar)
		//if err != nil {
		//	return ConnectionErr(err.Error())
		//}
		//resS = res.String()
		//_xsrf = s.xsrf(resS)
		//_xsrf_token, exception = s.xsrfToken(res)
		//if exception != nil {
		//	return exception
		//}

		//resS, ex = OkString(s.Client().R().SetHeaders(map[string]string{
		//	"_xsrf":  _xsrf,
		//	"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
		//}).Get(hrData))
		//if ex != nil {
		//	return ex
		//}
		//s.Trace(resS)

	default:
		return ParseErr("unknown user role: " + t)
	}

	res, err := s.Client().R().SetHeaders(map[string]string{
		"cookie": s.aspSessionID + "; " + s.aSPXAUTH,
	}).Get(financialArea)
	if err != nil {
		return ConnectionErr(financialAreaHR + ": " + err.Error())
	}
	resS := res.String()
	_xsrf := s.xsrf(resS)
	_xsrf_token, exception := s.xsrfToken(res)
	if exception != nil {
		return exception
	}

	resp, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"_xsrf":  _xsrf,
		"cookie": s.aspSessionID + "; " + s.aSPXAUTH + "; " + _xsrf_token,
	}).Get(uri))
	if ex != nil {
		return ex
	}
	if strings.Contains(strings.ToLower(resp), "error") {
		return ParseErr("invoice request: " + resp)
	}
	var invoiceList struct {
		List []*invoiceJson
	}
	err = json.Unmarshal([]byte(resp), &invoiceList)
	if err != nil {
		return ParseErr("parsing invoice response: " + resp)
	}
	for _, inv := range invoiceList.List {
		if ex := s.parseInvoice(location, inv); ex != nil {
			return ParseErr("parseInvoice: " + ex.Error())
		}
	}
	return nil
}

func (s *ReginaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *ReginaStrategy) xsrfToken(res *resty.Response) (string, Exception) {
	_xsrf_token := HeaderByNameAndValStartsWith(res.Header(), "Set-Cookie", "_xsrf_token")
	if _xsrf_token == "" {
		return "", ParseErr("no _xsrf_token found in headers")
	}
	_xsrf_token = _xsrf_token[:strings.Index(_xsrf_token, "; expires")]
	return _xsrf_token, nil
}

func (s *ReginaStrategy) xsrf(resS string) string {
	begin := "id=\\\"_xsrf\\\" type=\\\"hidden\\\" value=\\\""
	end := `\"`
	_xsrf := resS[strings.Index(resS, begin)+len(begin) : strings.LastIndex(resS, end)]
	return _xsrf
}

func (s *ReginaStrategy) parseInvoice(location *model.Location, jsonInvoice *invoiceJson) Exception {
	s.Debug("parsing invoice: ", *jsonInvoice)
	issueS := jsonInvoice.DataFactura[strings.Index(jsonInvoice.DataFactura, "(")+1 : strings.Index(jsonInvoice.DataFactura, ")")]
	issueDate, err := utils.FromMillisString(issueS)
	if err != nil {
		return ParseErr("issue date:  " + jsonInvoice.DataFactura)
	}
	dueS := jsonInvoice.DataScadenta[strings.Index(jsonInvoice.DataScadenta, "(")+1 : strings.Index(jsonInvoice.DataScadenta, ")")]
	dueDate, err := utils.FromMillisString(dueS)
	if err != nil {
		return ParseErr("due date: " + jsonInvoice.DataScadenta)
	}
	var pdfUri *string
	if jsonInvoice.CommArgEnc != "" {
		uri := PDF + jsonInvoice.CommArgEnc
		pdfUri = &uri
	}
	invoice := &model.Invoice{
		Ref:       jsonInvoice.NrFactura,
		PdfUri:    pdfUri,
		Amount:    jsonInvoice.ValoareFactura,
		AmountDue: jsonInvoice.RestPlata,
		IssueDate: issueDate.Format(utils.DBDateFormat),
		DueDate:   dueDate.Format(utils.DBDateFormat),
	}
	location.AddInvoice(invoice)
	return nil
}

func (s *ReginaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *ReginaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, ex := GetBytes(s.Client().R().Get(*account.Locations[0].Invoices[0].PdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *ReginaStrategy) LoadsInternal() bool {
	return true
}

func (s *ReginaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *ReginaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
