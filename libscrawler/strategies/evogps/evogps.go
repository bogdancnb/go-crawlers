package evogps

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"bytes"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	login      = "https://live.evogps.ro/Web/login.aspx?ReturnUrl=%2Fweb"
	invoices   = "https://live.evogps.ro/web/settings/Invoices.aspx"
	pdfBase    = "http://live.evogps.ro/web/settings/"
	siteFormat = "02.01.2006"
)

var cifRegex = regexp.MustCompile(`C.I.F.:\s*(\w+)IBAN:`)
var addressRegex = regexp.MustCompile(`Adresa facturare:\s*(.+?)\s`)

func Init() {
	RegisterFactory("evo_gps.crawler", new(EvoGPSFactory))
}

type EvoGPSFactory struct{}

func (p *EvoGPSFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EvoGPSStrategy")
	return &EvoGPSStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type EvoGPSStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
}

func (s *EvoGPSStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EvoGPSStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EvoGPSStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EvoGPSStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
	})
	doc, ex := OkDocument(s.Client().R().Get(login))
	if ex != nil {
		return nil, ex
	}
	viewState, ex := Attr(doc.Find("input[id='__VIEWSTATE']"), "value")
	if ex != nil {
		return nil, ex
	}
	viewStateGenerator, ex := Attr(doc.Find("input[id='__VIEWSTATEGENERATOR']"), "value")
	if ex != nil {
		return nil, ex
	}
	eventValidation, ex := Attr(doc.Find("input[id='__EVENTVALIDATION']"), "value")
	if ex != nil {
		return nil, ex
	}
	bodyAntiForgery, ex := Attr(doc.Find("input[id='ctl00_body_antiforgery']"), "value")
	if ex != nil {
		return nil, ex
	}
	s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	res, _ := s.Client().R().SetHeaders(map[string]string{
		"accept":                    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"accept-encoding":           "gzip, deflate, br",
		"accept-language":           "en-US,en;q=0.9",
		"cache-control":             "max-age=0",
		"content-type":              "application/x-www-form-urlencoded",
		"origin":                    "https://live.evogps.ro",
		"referer":                   "https://live.evogps.ro/Web/login.aspx?ReturnUrl=%2Fweb",
		"sec-fetch-mode":            "navigate",
		"sec-fetch-site":            "same-origin",
		"sec-fetch-user":            "?1",
		"upgrade-insecure-requests": "1",
	}).SetFormData(map[string]string{
		"__VIEWSTATE":            viewState,
		"__VIEWSTATEGENERATOR":   viewStateGenerator,
		"__EVENTVALIDATION":      eventValidation,
		"authUser":               account.Username,
		"authPasswd":             *account.Password,
		"keepLogged":             "1",
		"ctl00$body$antiforgery": bodyAntiForgery,
	}).Post(login)
	if strings.Contains(res.String(), "incorrect data") {
		return nil, LoginErr("incorrect data")
	}
	if strings.Contains(res.String(), "Locked User") {
		return nil, ParseErr("Locked User, try after 30 minutes")
	}
	if res.StatusCode() != 302 {
		return nil, ConnectionErr("POST:" + login)
	}
	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"referer": "https://live.evogps.ro/Web/login.aspx?ReturnUrl=%2Fweb",
	}).Get("https://live.evogps.ro/web/"))
	if ex != nil {
		return nil, ex
	}

	html, _ := doc.Html()
	s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
	return &html, nil
}

func (s *EvoGPSStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *EvoGPSStrategy) LoadLocations(account *model.Account) Exception {
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":  "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"referer": "https://live.evogps.ro/Web/login.aspx?ReturnUrl=%2Fweb",
	}).Get(invoices))
	if ex != nil {
		return ex
	}
	//viewState, ex := Attr(doc.Find("form[name='form1'] input[id='__VIEWSTATE']"), "value")
	//if ex != nil {
	//	return ex
	//}
	//viewStateGenerator, ex := Attr(doc.Find("input[id='__VIEWSTATEGENERATOR']"), "value")
	//if ex != nil {
	//	return ex
	//}
	//eventValidation, ex := Attr(doc.Find("input[id='__EVENTVALIDATION']"), "value")
	//if ex != nil {
	//	return ex
	//}
	//antiforgery, ex := Attr(doc.Find("input[id='antiforgery']"), "value")
	//if ex != nil {
	//	return ex
	//}

	invoices := doc.Find("table[class='bdn_table'] tr")
	if invoices.Length() == 0 {
		return ParseErr("no invoices table found")
	}

	var loc *model.Location
	if len(s.exLocations) > 0 {
		for _, val := range s.exLocations {
			loc = &model.Location{
				Service:    val.Service,
				Identifier: val.Identifier,
			}
			break
		}
	} else {
		loc = &model.Location{}
	}
	if loc == nil {
		return ParseErr("no location")
	}
	account.AddLocation(loc)

	for i := 1; i < invoices.Length(); i++ {
		row := invoices.Eq(i)
		cols := row.Find("td")
		if cols.Length() < 7 {
			break
		}
		ref := utils.RemoveSpaces(cols.Eq(1).Text() + cols.Eq(2).Text())
		if i, ok := s.exInvoices[ref]; !ok || i.AmountDue > 0 {
			issueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(3).Text()))
			if err != nil {
				return ParseErr("parse issue date " + cols.Eq(3).Text())
			}
			dueDate, err := time.Parse(siteFormat, utils.RemoveSpaces(cols.Eq(0).Text()))
			if err != nil {
				return ParseErr("parse due date " + cols.Eq(0).Text())
			}
			amountS := utils.RemoveSpaces(cols.Eq(4).Find("span").Eq(0).Text())
			amountS = strings.ReplaceAll(amountS, ".", "")
			amountS = strings.ReplaceAll(amountS, ",", ".")
			amount, err := strconv.ParseFloat(amountS, 64)
			if err != nil {
				return ParseErr("parse " + amountS)
			}
			amountDueS := utils.RemoveSpaces(cols.Eq(5).Find("span").Eq(0).Text())
			amountDueS = strings.ReplaceAll(amountDueS, ".", "")
			amountDueS = strings.ReplaceAll(amountDueS, ",", ".")
			amountDue, err := strconv.ParseFloat(amountDueS, 64)
			if err != nil {
				return ParseErr("parse " + amountDueS)
			}

			var pdfUri *string
			aEl := cols.Eq(6).Find("a")
			if aEl.Length() == 0 {
				s.Warn("no pdf link for " + ref)
			} else {
				uri, ok := aEl.Attr("onclick")
				if !ok {
					html, _ := aEl.Html()
					s.Warn("no pdf uri found in ", html)
				}
				begin := `javascript:window.open(`
				end := `)`
				uri = uri[strings.Index(uri, begin)+len(begin) : strings.Index(uri, end)]
				uri = strings.ReplaceAll(uri, `amp;`, ``)
				uri = strings.ReplaceAll(uri, `"`, ``)
				uri = pdfBase + uri

				//href, _ := aEl.Attr("href")
				//begin = `__doPostBack('`
				//end = `'`
				//href = href[strings.Index(href, begin)+len(begin):]
				//href = href[:strings.Index(href, end)]
				//uri = uri + "||" + href
				uri = strings.ReplaceAll(uri, `"`, ``)
				pdfUri = &uri
			}

			barcode := fmt.Sprintf(`%s%s%s`, loc.Identifier, ref, amountS)
			invoice := &model.Invoice{
				Ref:       ref,
				PdfUri:    pdfUri,
				Amount:    amount,
				AmountDue: amountDue,
				IssueDate: issueDate.Format(utils.DBDateFormat),
				DueDate:   dueDate.Format(utils.DBDateFormat),
				BarCode:   &barcode,
			}
			loc.AddInvoice(invoice)
			if loc.Identifier == "" && pdfUri != nil {
				if ex := s.parsePdf(loc, invoice /*, viewState, viewStateGenerator, eventValidation, antiforgery*/); ex != nil {
					return ParseErr("parse pdf for " + ref + ": " + ex.Error())
				}
			}
		}
	}
	return nil
}

func (s *EvoGPSStrategy) parsePdf(loc *model.Location, invoice *model.Invoice /*, viewState, viewStateGenerator, eventValidation, antiforgery string*/) Exception {
	//parts := strings.Split(*invoice.PdfUri, `||`)
	//if len(parts) != 2 {
	//	return ParseErr("invalid pdf uri " + *invoice.PdfUri)
	//}
	//res, err := s.Client().R().SetHeaders(map[string]string{
	//	"accept":       "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
	//	"content-type": "application/x-www-form-urlencoded",
	//	"origin":       "https://live.evogps.ro",
	//	"referer":      "https://live.evogps.ro/web/settings/Invoices.aspx",
	//}).SetFormData(map[string]string{
	//	"__EVENTTARGET":        parts[1],
	//	"__EVENTARGUMENT":      "",
	//	"__VIEWSTATE":          viewState,
	//	"__VIEWSTATEGENERATOR": viewStateGenerator,
	//	"__EVENTVALIDATION":    eventValidation,
	//	"antiforgery":          antiforgery,
	//}).Post(invoices)
	//if err != nil {
	//	return ConnectionErr("post " + invoices + ": " + err.Error())
	//}
	//s.Trace(res.String())
	pdfBytes, ex := s.pdfBytes(*invoice.PdfUri)
	if ex != nil {
		return ex
	}
	text, err := PlainText(pdfBytes)
	if err != nil {
		return ParseErr("parsing pdf: " + err.Error())
	}
	matches, err := utils.MatchRegex(text, cifRegex)
	if err != nil {
		return ParseErr(err.Error())
	}
	loc.Identifier = matches[0][1]
	matches, err = utils.MatchRegex(text, addressRegex)
	if err != nil {
		s.Warn("could not parse address, default identifier")
		loc.Service = loc.Identifier
	} else {
		loc.Service = matches[0][1]
	}
	return nil
}

func (s *EvoGPSStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EvoGPSStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EvoGPSStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EvoGPSStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfBytes, ex := s.pdfBytes(*account.Locations[0].Invoices[0].PdfUri)
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EvoGPSStrategy) pdfBytes(uri string) ([]byte, Exception) {
	res, err := s.Client().R().SetHeaders(map[string]string{
		"accept":          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"accept-encoding": "gzip, deflate, br",
		"cache-control":   "max-age=0",
	}).Get(uri)
	if err != nil {
		return nil, ConnectionErr("pdf request: " + uri)
	}

	body := res.Body()
	eof := []byte(`%%EOF`)
	body = body[:bytes.Index(body, eof)+len(eof)]
	return body, nil
}

func (s *EvoGPSStrategy) LoadsInternal() bool {
	return true
}

func (s *EvoGPSStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *EvoGPSStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
