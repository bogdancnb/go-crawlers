package evogps

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "evo_gps.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"graingps", "grainexpress"},
	}
	for _, test := range tests {
		if err := AccountExtractBarcode(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}
