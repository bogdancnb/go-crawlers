package olimontel

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	home  = "https://www.olimontel.it/palog.php"
	login = "https://www.olimontel.it/utenti/contlogin.php"

	loginErrMsg   = `Combinazione username/password non corretta`
	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("olimontel.crawler", new(OlimontelFactory))
}

type OlimontelFactory struct{}

func (p *OlimontelFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "OlimontelStrategy")
	return &OlimontelStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type OlimontelStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
}

func (s *OlimontelStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *OlimontelStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *OlimontelStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *OlimontelStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.olimontel.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())

	doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "www.olimontel.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://www.olimontel.it",
		"referer":      "https://www.olimontel.it/palog.php",
	}).SetFormData(map[string]string{
		"idutente":   account.Username,
		"idpassword": *account.Password,
		"Submit":     "ACCEDI",
	}).Post(login))
	if ex != nil {
		return nil, ex
	}
	html, err := doc.Html()
	if err != nil {
		return nil, ParseErr("login response")
	}
	return &html, nil
}

func (s *OlimontelStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *OlimontelStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *OlimontelStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *OlimontelStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *OlimontelStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *OlimontelStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().
		Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *OlimontelStrategy) LoadsInternal() bool {
	return false
}

func (s *OlimontelStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *OlimontelStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
