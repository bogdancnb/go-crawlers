package olimontel

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "olimontel.crawler"
	tests := []struct {
		username string
		password string
	}{
		{"lillo.51@hotmail.com ", "alice2015"}, //login err
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "Gregorio61", Password: "Gentile2013", Uri: "estra_Energia.crawler",
			Identifier: "10400001013534",
			Ref:        "0000201902408871", PdfUri: "http://10.128.1.185:8080/urlsearchprod.jsp?type=BOLLETTA20&i1=EXBEL&s1=21&v1=201902408871",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
