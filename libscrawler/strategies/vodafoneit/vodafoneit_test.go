package vodafoneit

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "vodafone_italia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//{"mohssin zitouni", "maryam@16"}, //login err
		//{"papy-67@hotmail.it", "Aprilia.67"}, //ok no locs
		{"andrea1074@outlook.com", "Marco.1067"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "andrea1074@outlook.com", Password: "Marco.1067", Uri: "vodafone_italia.crawler",
			Identifier: "0828031315",
			Ref:        "AM18731114", PdfUri: "1.13094568",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
