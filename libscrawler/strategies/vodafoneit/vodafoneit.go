package vodafoneit

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

const (
	home                    = "https://www.vodafone.it/area-utente/fai-da-te/Common/PaginaUnicadiLogin.html"
	privat                  = "http://www.vodafone.it/portal/Privati"
	downloadDocumentServlet = "https://www.vodafone.it/area-utente/DownloadDocumentServlet"

	loginErrMsg   = "Username / Password non corretta"
	invoiceFormat = "02/01/2006"
)

func Init() {
	RegisterFactory("vodafone_italia.crawler", new(VodafoneITFactory))
}

type VodafoneITFactory struct{}

func (p *VodafoneITFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "VodafoneITStrategy")
	return &VodafoneITStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
		exLocations:  make(map[string]*model.Location),
		exInvoices:   make(map[string]*model.Invoice),
	}
}

type VodafoneITStrategy struct {
	*logrus.Entry
	HttpStrategy
	exLocations map[string]*model.Location
	exInvoices  map[string]*model.Invoice
	doc         *goquery.Document
	currentSim  string
	PICWLPSIDB  string
}

func (s *VodafoneITStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *VodafoneITStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *VodafoneITStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *VodafoneITStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"User-Agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.vodafone.it",
	}).Get(home))
	if ex != nil {
		return nil, ex
	}
	form := doc.Find("form[id$=':account']")
	if form.Length() == 0 {
		return nil, ParseErr("login form not found")
	}
	formId, ex := Attr(form.Eq(0), "id")
	if ex != nil {
		return nil, ex
	}
	loginBtn := doc.Find("a[id$=':account:loginBtn']")
	if loginBtn.Length() == 0 {
		return nil, ParseErr("login btn element not found")
	}
	loginId, ex := Attr(loginBtn.Eq(0), "id")
	if ex != nil {
		return nil, ex
	}

	s.EnableRedirects(false)
	post, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"host":         "www.vodafone.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://www.vodafone.it",
		"referer":      "https://www.vodafone.it/area-utente/fai-da-te/Common/PaginaUnicadiLogin.html",
	}).SetFormData(map[string]string{
		formId:                  formId,
		"username":              account.Username,
		"password":              *account.Password,
		"javax.faces.ViewState": "j_id1:j_id2",
		loginId:                 loginId,
		"actionName":            "solicitedLogin",
	}).Post(home)
	if post == nil {
		return nil, ConnectionErr("no response for POST " + home)
	}
	if strings.Contains(post.String(), loginErrMsg) {
		return nil, LoginErr(loginErrMsg)
	}
	loc := HeaderByNameAndValStartsWith(post.Header(), "Location", "https")
	if loc == "" {
		return nil, ConnectionErr("no location redirect header")
	}
	s.PICWLPSIDB = HeaderByNameAndValStartsWith(post.Header(), "Set-Cookie", "PICWLPSIDB")
	s.PICWLPSIDB = s.PICWLPSIDB[:strings.Index(s.PICWLPSIDB, ";")]

	get, err := s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"connection":   "keep-alive",
		"cookie":       s.PICWLPSIDB,
		"host":         "www.vodafone.it",
		"content-type": "application/x-www-form-urlencoded",
		"origin":       "https://www.vodafone.it",
		"referer":      "https://www.vodafone.it/area-utente/fai-da-te/Common/PaginaUnicadiLogin.html",
	}).Get(loc)
	if get == nil {
		return nil, ConnectionErr("no response for get " + loc)
	}
	s.EnableRedirects(true)

	if get.StatusCode() != 200 {
		loc = HeaderByNameAndValStartsWith(get.Header(), "Location", "")
		if loc == "" {
			return nil, ConnectionErr("no location redirect header")
		}
		if !strings.HasPrefix(loc, "https") {
			loc = "https://www.vodafone.it" + loc
		}
		doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
			"accept":     AcceptHeader,
			"connection": "keep-alive",
			"host":       "www.vodafone.it",
			"cookie":     s.PICWLPSIDB,
		}).Get(loc))
		if ex != nil {
			return nil, ex
		}
		html, err := doc.Html()
		if err != nil {
			s.WithError(err).Debug(html)
			return nil, ParseErr("parsing login response html: " + err.Error())
		}
		return &html, nil
	} else {
		s.PICWLPSIDB = HeaderByNameAndValStartsWith(get.Header(), "Set-Cookie", "PICWLPSIDB")
		s.PICWLPSIDB = s.PICWLPSIDB[:strings.Index(s.PICWLPSIDB, ";")]

		doc, ex = OkDocument(get, err)
		if ex != nil {
			return nil, ex
		}
		html, err := doc.Html()
		if err != nil {
			s.WithError(err).Debug(html)
			return nil, ParseErr("parsing login response html: " + err.Error())
		}
		return &html, nil
	}
}

func (s *VodafoneITStrategy) CheckLogin(response *string) Exception {
	if strings.Contains(*response, loginErrMsg) {
		return LoginErr(loginErrMsg)
	}
	return nil
}

func (s *VodafoneITStrategy) LoadLocations(account *model.Account) Exception {
	var ex Exception
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.vodafone.it",
		"cookie":     s.PICWLPSIDB,
	}).Get(privat))
	if ex != nil {
		return ex
	}
	swapForm := s.doc.Find("form[id='swapForm']")
	if swapForm.Length() == 0 {
		return ParseErr("swapForm not found")
	}
	aEl := swapForm.Find("a")
	for i := 0; i < aEl.Length(); i++ {
		//divs := aEl.Eq(i).Find("div")
		//if divs.Length() != 2 {
		//	html, err := aEl.Html()
		//	return ParseErr("invalid loginOverlayerSim content: " + html + ", html parse err=" + err.Error())
		//}
		loginOverlayerNumero := aEl.Eq(i).Find("div[class='loginOverlayerNumero']")
		if loginOverlayerNumero.Length() == 0 {
			return ParseErr("loginOverlayerNumero not found")
		}
		nr := strings.TrimSpace(loginOverlayerNumero.Text())
		loginOverlayerTipo := aEl.Eq(i).Find("div[class='loginOverlayerTipo']")
		if loginOverlayerTipo.Length() == 0 {
			return ParseErr("loginOverlayerTipo not found")
		}
		tip := strings.TrimSpace(loginOverlayerTipo.Text())
		l := &model.Location{
			Service:    nr + " " + tip,
			Details:    &tip,
			Identifier: nr,
		}
		account.AddLocation(l)
		aClass, ex := Attr(aEl.Eq(i), "class")
		if ex != nil {
			return ex
		}
		if strings.Contains(aClass, "currentSim") {
			s.currentSim = nr
		}
	}
	return nil
}

func (s *VodafoneITStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	s.Debugf("currentSim=%s, location identifier=%s", s.currentSim, location.Identifier)
	if location.Identifier != s.currentSim {
		//todo switch post
		s.currentSim = location.Identifier
	}
	loginContainer := s.doc.Find("div[id='loginContainer']")
	if loginContainer.Length() == 0 {
		return ParseErr("loginContainer not found")
	}
	href, ex := Attr(loginContainer.Find("a"), "href")
	if ex != nil {
		return ex
	}
	s.doc, ex = OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.vodafone.it",
		"cookie":     s.PICWLPSIDB,
	}).Get(href))
	if ex != nil {
		return ex
	}
	return nil
}

func (s *VodafoneITStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	lastInv := s.doc.Find("a:containsOwn('La mia ultima fattura')")
	if lastInv.Length() == 0 {
		return ParseErr("La mia ultima fattura not found")
	}
	href, ex := Attr(lastInv, "href")
	if ex != nil {
		return ex
	}
	if !strings.HasPrefix(href, "http") {
		href = "https://www.vodafone.it" + href
	}
	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     AcceptHeader,
		"connection": "keep-alive",
		"host":       "www.vodafone.it",
		"cookie":     s.PICWLPSIDB,
	}).Get(href))
	if ex != nil {
		return ex
	}
	rows := doc.Find("table[id='tbl'] tr")
	if rows.Length() < 2 {
		return ParseErr("no invoice rows")
	}
	for i := 1; i < rows.Length(); i++ {
		ths := rows.Eq(i).Find("th")
		ref := ths.Eq(2).Text()
		s.Debug("parsing ref=", ref)
		issueDate, err := time.Parse(invoiceFormat, strings.TrimSpace(ths.Eq(0).Text()))
		if err != nil {
			return ParseErr("issue date: " + err.Error())
		}
		dueDate, err := time.Parse(invoiceFormat, strings.TrimSpace(ths.Eq(1).Text()))
		if err != nil {
			return ParseErr("due date " + err.Error())
		}
		ams := strings.TrimSpace(ths.Eq(3).Text())
		amp := strings.Split(ams, "€")
		am, ex := AmountFromString(amp[0])
		if ex != nil {
			return ex
		}
		due := am
		status := strings.TrimSpace(ths.Eq(5).Text())
		if status == "Pagato" {
			due = 0
		}
		invoiceId := doc.Find(fmt.Sprintf(`input[name='invoiceId'][value='%s']`, ref))
		if invoiceId.Length() == 0 {
			return ParseErr("invoiceId not found")
		}
		custCodeEl := invoiceId.Prev()
		name, ex := Attr(custCodeEl, "name")
		if ex != nil {
			return ex
		}
		value, ex := Attr(custCodeEl, "value")
		if ex != nil {
			return ex
		}
		if name != "custCode" || value == "" {
			return ParseErr("pdf custCode value not found")
		}
		i := &model.Invoice{
			Ref:       ref,
			PdfUri:    &value,
			Amount:    am,
			AmountDue: due,
			IssueDate: issueDate.Format(utils.DBDateFormat),
			DueDate:   dueDate.Format(utils.DBDateFormat),
		}
		location.AddInvoice(i)
	}
	return nil
}

func (s *VodafoneITStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *VodafoneITStrategy) Pdf(account *model.Account) *model.PdfResponse {
	invoice := account.Locations[0].Invoices[0]
	pdfUri := *invoice.PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().SetHeaders(map[string]string{
		"accept":       AcceptHeader,
		"host":         "www.vodafone.it",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded",
		"cookie":       s.PICWLPSIDB,
	}).SetFormData(map[string]string{
		"custCode":  pdfUri,
		"invoiceId": invoice.Ref,
	}).Post(downloadDocumentServlet))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *VodafoneITStrategy) LoadsInternal() bool {
	return false
}

func (s *VodafoneITStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
	for _, invoice := range existingInvoices {
		s.exInvoices[invoice.Ref] = invoice
	}
}

func (s *VodafoneITStrategy) SetExistingLocations(existingLocations []*model.Location) {
	for _, location := range existingLocations {
		s.exLocations[location.Identifier] = location
	}
}
