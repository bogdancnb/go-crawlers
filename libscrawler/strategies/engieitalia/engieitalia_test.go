package engieitalia

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies"
	"testing"
)

func TestAccount(t *testing.T) {
	Init()
	uri := "engie_italia.crawler"
	tests := []struct {
		username string
		password string
	}{
		//*/{"event":{"descriptor":"markup://aura:clientOutOfSync","eventDef":{"descriptor":"markup://aura:clientOutOfSync","t":"APPLICATION","xs":"I"}},"exceptionEvent":true}/*ERROR*/
		//{"sonia.ottonello30@gmail.com", "Sonia1976"},

		//*/{"event":{"descriptor":"markup://aura:clientOutOfSync","eventDef":{"descriptor":"markup://aura:clientOutOfSync","t":"APPLICATION","xs":"I"}},"exceptionEvent":true}/*ERROR*/
		{"giuseppeamandonico", "600peppo"},
	}
	for _, test := range tests {
		if err := AccountTest(test.username, test.password, uri); err != nil {
			t.Errorf("crawl error %[1]T:  %[1]v", err)
		}
	}
}

func TestPdf(t *testing.T) {
	Init()
	tests := []*PdfRequest{
		{
			Username: "tiziboncoraglio@gmail.com", Password: "accesso1973", Uri: "enel_it.crawler",
			Identifier: "629155058",
			Ref:        "0000004059299965", PdfUri: "https://www.enel.it/bin/areaclienti/auth/bollette?action=tipo-fattura&numeroFattura=0000004059299965&dataEmissione=07/09/2020&_=1603197781217",
		},
	}
	for _, test := range tests {
		pdf := PdfTest(test)
		t.Log(string(pdf.Content))
	}
}
