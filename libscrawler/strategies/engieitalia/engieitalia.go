package engieitalia

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"
)

const (
	dashboard     = "https://casa.engie.it/spazio-clienti?content=ACMADashboard"
	loginSiteUser = "https://web-gdfsuezenergieb2c.force.com/acma/aura?r=2&other.ACMAAuthenticationCnt.loginSiteUser=1"

	loginErrMsg = "TODO"
	siteFormat  = "2006-01-02"
)

func Init() {
	RegisterFactory("engie_italia.crawler", new(EngieItaliaFactory))
}

type EngieItaliaFactory struct{}

func (p *EngieItaliaFactory) NewStrategy(log *logrus.Entry) Strategy {
	l := log.WithField("type", "EngieItaliaStrategy")
	return &EngieItaliaStrategy{
		Entry:        l,
		HttpStrategy: NewHttpStrategy(l),
	}
}

type EngieItaliaStrategy struct {
	*logrus.Entry
	HttpStrategy
}

func (s *EngieItaliaStrategy) GetBarcode(account *model.Account, location *model.Location, invoice *model.Invoice) Exception {
	return nil
}

func (s *EngieItaliaStrategy) EInvoiceNeedsLoadLocations() bool {
	return false
}

func (s *EngieItaliaStrategy) ActivateEInvoice(account *model.Account, received *model.Location, electronicInvoiceType model.ElectronicInvoiceType) Exception {
	return nil
}

func (s *EngieItaliaStrategy) Login(account *model.Account) (*string, Exception) {
	s.Client().SetHeaders(map[string]string{
		"user-agent": AgentChrome78,
	})

	doc, ex := OkDocument(s.Client().R().SetHeaders(map[string]string{
		"accept":     "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
		"connection": "keep-alive",
		"host":       "casa.engie.it",
	}).Get(dashboard))
	if ex != nil {
		return nil, ex
	}
	s.Trace(doc.Html())

	message := `{"actions":[{"id":"78;a","descriptor":"apex://ACMAAuthenticationCnt/ACTION$loginSiteUser","callingDescriptor":"markup://c:ACMALogin","params":{"params":"{\"EMAIL\":\"%s\",\"PASSWORD\":\"%s\",\"REMEMBER_ME\":null,\"memberId\":\"\",\"__LNTG_CMP\":\"c:ACMALogin\",\"SOURCE_CHANNEL\":\"ACMAWEB\"}"},"version":null}]}`
	message = fmt.Sprintf(message, account.Username, *account.Password)
	res, ex := OkString(s.Client().R().SetHeaders(map[string]string{
		"accept":       "*/*",
		"connection":   "keep-alive",
		"content-type": "application/x-www-form-urlencoded; charset=UTF-8",
		"host":         "web-gdfsuezenergieb2c.force.com",
		//"origin": "https://web-gdfsuezenergieb2c.force.com",
		//"referer":"https://web-gdfsuezenergieb2c.force.com/acma/ACMAAuthentication?startURL=%2Facma%2FACMADashboard%3Ftp%3DRES",
	}).SetFormData(map[string]string{
		"message":      message,
		"aura.context": `{"mode":"PROD","fwuid":"uB7Kis-nrXhbA1D0ce6Sog","app":"c:ACMAAuthenticationApp","loaded":{"APPLICATION@markup://c:ACMAAuthenticationApp":"lwYYYceHBYS4UAkIRqIx1Q"},"dn":[],"globals":{},"uad":true}`,
		"aura.pageURI": `/acma/ACMAAuthentication?startURL=%2Facma%2FACMADashboard%3Ftp%3DRES`,
		"aura.token":   "undefined",
	}).Post(loginSiteUser))
	s.Debug("login response: ", res)
	if ex != nil {
		return nil, ex
	}
	var loginRes loginResJson
	err := utils.FromJSON(&loginRes, strings.NewReader(res))
	if err != nil {
		return nil, ParseErr("parsing login response: " + err.Error())
	}
	if len(loginRes.Actions) > 0 && len(loginRes.Actions[0].ReturnValue.Errors) > 0 {
		errMsg := loginRes.Actions[0].ReturnValue.Errors[0]
		return nil, LoginErr(errMsg)
	} else {
		return nil, ParseErr("could not perform login")
	}
	return nil, nil
}

type loginResJson struct {
	Actions []*struct {
		Error       []interface{}
		Id          string
		ReturnValue struct {
			Errors []string
			Params map[string]map[string]interface{}
			Result string
		}
		State string
	}
	Context struct {
		Apce                 int
		App                  string
		ContextPath          string
		DescriptorUids       interface{}
		Dns                  string
		EnableAccessChecks   bool
		Fwuid                string
		GlobalValueProviders interface{}
		Loaded               struct {
			AppMarkup string `json:"APPLICATION@markup://c:ACMAAuthenticationApp"`
		}
		Ls         int
		Lv         string
		Mlr        int
		Mna        interface{}
		Mode       string
		PathPrefix string
		Services   []string
		Uad        int
	}
	PerfSummary struct {
		Actions      interface{}
		ActionsTotal int
		Overhead     int
		Request      int
		Version      string
	}
}

func (s *EngieItaliaStrategy) CheckLogin(response *string) Exception {
	return nil
}

func (s *EngieItaliaStrategy) LoadLocations(account *model.Account) Exception {
	return nil
}

func (s *EngieItaliaStrategy) LoadAmount(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EngieItaliaStrategy) LoadInvoices(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EngieItaliaStrategy) LoadPayments(account *model.Account, location *model.Location) Exception {
	return nil
}

func (s *EngieItaliaStrategy) Pdf(account *model.Account) *model.PdfResponse {
	pdfUri := *account.Locations[0].Invoices[0].PdfUri
	pdfBytes, ex := GetBytes(s.Client().R().Get(pdfUri))
	if ex != nil {
		return model.PdfErrorResponse(ex.Error(), model.OTHER_EXCEPTION)
	}
	return &model.PdfResponse{
		Content:   pdfBytes,
		PdfStatus: model.OK.Name,
		ErrMsg:    nil,
	}
}

func (s *EngieItaliaStrategy) LoadsInternal() bool {
	return false
}

func (s *EngieItaliaStrategy) SetExistingInvoices(existingInvoices map[string]*model.Invoice) {
}

func (s *EngieItaliaStrategy) SetExistingLocations(existingLocations []*model.Location) {
}
