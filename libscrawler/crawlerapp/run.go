package crawlerapp

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appserver"
	"bitbucket.org/bogdancnb/go-crawlers/libs/config"
	. "bitbucket.org/bogdancnb/go-crawlers/libs/endpoints"
	"bitbucket.org/bogdancnb/go-crawlers/libs/eureka"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/profiling"

	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/endpoints"
)

func Run(initFuncs ...func()) {
	shutdownBroadcast := make(chan struct{})
	netAddress := make(chan string)
	actuatorRefresh := make(chan bool)

	config.Init()
	for _, initFunc := range initFuncs {
		initFunc()
	}

	crawl.StartWorkers(shutdownBroadcast)

	go func() {
		for event := range actuatorRefresh {
			if event {
				//todo send to all interested parties
			}
		}
	}()

	go eureka.Register(netAddress, shutdownBroadcast)

	router := NewRouter(
		[]*RouteGroup{endpoints.Crawler()},
		actuatorRefresh,
	)
	err := appserver.Start(
		router,
		[]appserver.BeforeShutdownCall{
			eureka.OutOfService,
			profiling.Heap,
		},
		netAddress,
		shutdownBroadcast,
	)
	if err != nil {
		logging.Instance().WithError(err).Error("server error")
	}
}
