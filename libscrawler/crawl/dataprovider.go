package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/uaa"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	crawlerUtils "bitbucket.org/bogdancnb/go-crawlers/libscrawler/utils"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type SendWorkModel struct {
	Account   *Account `json:"account"`
	StartDate int64    `json:"startDate"`
	EndDate   int64    `json:"endDate"`
	ExitIP    *string  `json:"exitIp"`
}

type SendErrorModel struct {
	Error     string  `json:"error"`
	Username  string  `json:"username"`
	Uri       string  `json:"uri"`
	StartDate int64   `json:"startDate"`
	EndDate   int64   `json:"endDate"`
	ExitIP    *string `json:"exitIp"`
}

var nonOkAccountRes = errors.New("account request error")

func fetchAccounts(logGW *logrus.Entry) ([]*Account, error) {
	l := logGW.WithField("method", "fetchAccounts")
	l.Debugf("requesting %d accounts", cfg.GetWorkSize)

	uri := strings.ReplaceAll(cfg.GetWorkUrl, `{crawler}`, cfg.CrawlerUri)
	uri = strings.ReplaceAll(uri, `{size}`, strconv.Itoa(cfg.GetWorkSize))

	resBody := crawlerUtils.DoRequest(l,
		crawlerUtils.HttpRequestUtil{
			Method: http.MethodGet,
			Url:    uri,
			Body:   nil,
		},
		uaa.Authenticate,
	)
	if resBody == nil {
		return nil, nonOkAccountRes
	}
	accs := make([]*Account, 0)
	err := utils.FromJSON(&accs, resBody)
	if err != nil {
		logGW.WithError(err).Error("deserialize accounts")
		return nil, fmt.Errorf("")
	}
	_ = resBody.Close()
	return accs, nil
}

func sendError(l *logrus.Entry, acc *Account, err error, start time.Time) {
	l = l.WithField("method", "sendError")
	l.WithError(err).Errorf("crawl error for %s: %v, sending error", acc.Username, err)
	errMsg := ""
	switch e := err.(type) {
	case Exception:
		errMsg = e.CrawlerErrorMsg()
	default:
		errMsg = e.Error()
	}
	body := SendErrorModel{
		Error:     errMsg,
		Username:  acc.Username,
		Uri:       acc.Uri,
		StartDate: utils.Millis(start),
		EndDate:   utils.Millis(time.Now()),
		ExitIP:    acc.ExitIP,
	}
	b, err := json.Marshal(body)
	if err != nil {
		l.WithError(err).Error("serializing ", acc)
		return
	}

	resBody := crawlerUtils.DoRequest(l, crawlerUtils.HttpRequestUtil{
		Method: http.MethodPost,
		Url:    cfg.SendErrorUrl,
		Body:   b,
	}, uaa.Authenticate)
	if resBody != nil {
		_ = resBody.Close()
	}
}

func sendWork(l *logrus.Entry, res *Account, start time.Time) {
	l = l.WithField("method", "sendWork")
	l.Debugf("crawl success for %s, sending work", res.Username)

	body := SendWorkModel{
		Account:   res,
		StartDate: utils.Millis(start),
		EndDate:   utils.Millis(time.Now()),
		ExitIP:    res.ExitIP,
	}
	b, err := json.Marshal(body)
	if err != nil {
		l.WithError(err).Error("serializing ", res)
		return
	}

	resBody := crawlerUtils.DoRequest(l, crawlerUtils.HttpRequestUtil{
		Method: http.MethodPost,
		Url:    cfg.SendWorkUrl,
		Body:   b,
	}, uaa.Authenticate)
	if resBody != nil {
		_ = resBody.Close()
	}
}
