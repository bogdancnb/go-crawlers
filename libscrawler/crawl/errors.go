package crawl

import (
	"errors"
	"fmt"
)

type Exception interface {
	error
	CrawlerErrorMsg() string
	SetExitIP(ip *string)
}

type (
	LoginException struct {
		Msg    string
		ExitIP *string
	}
	ParseException struct {
		Msg    string
		ExitIP *string
	}
	ConnectionException struct {
		Msg    string
		ExitIP *string
	}
)

var invalidPdfRequest = errors.New("invalid pdf request, missing invoice")

const format = `{"pagocrawlererror":"%s","exitIP":"%v"}`
const formatNoIP = `{"pagocrawlererror":"%s","exitIP":null}`

func (ex *LoginException) CrawlerErrorMsg() string {
	if ex.ExitIP != nil {
		return fmt.Sprintf(format, "LoginException: "+ex.Msg, *ex.ExitIP)
	}
	return fmt.Sprintf(formatNoIP, "LoginException: "+ex.Msg)
}

func (ex *ParseException) CrawlerErrorMsg() string {
	if ex.ExitIP != nil {
		return fmt.Sprintf(format, "ParseException: "+ex.Msg, *ex.ExitIP)
	}
	return fmt.Sprintf(formatNoIP, "ParseException: "+ex.Msg)
}

func (ex *ConnectionException) CrawlerErrorMsg() string {
	if ex.ExitIP != nil {
		return fmt.Sprintf(format, "ConnectionException: "+ex.Msg, *ex.ExitIP)
	}
	return fmt.Sprintf(formatNoIP, "ConnectionException: "+ex.Msg)
}

func (ex *LoginException) Error() string {
	return ex.Msg
}

func (ex *ParseException) Error() string {
	return ex.Msg
}

func (ex *ConnectionException) Error() string {
	return ex.Msg
}

func (ex *LoginException) SetExitIP(ip *string) {
	ex.ExitIP = ip
}

func (ex *ParseException) SetExitIP(ip *string) {
	ex.ExitIP = ip
}
func (ex *ConnectionException) SetExitIP(ip *string) {
	ex.ExitIP = ip
}

func ConnectionErr(err string) *ConnectionException {
	return &ConnectionException{
		Msg:    err,
		ExitIP: nil,
	}
}

func LoginErr(err string) *LoginException {
	return &LoginException{
		Msg:    err,
		ExitIP: nil,
	}
}

func ParseErr(err string) *ParseException {
	return &ParseException{
		Msg:    err,
		ExitIP: nil,
	}
}
