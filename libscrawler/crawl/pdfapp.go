package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/uaa"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/utils"
	"encoding/json"
	"errors"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"strings"
)

var noResErr = errors.New("empty response body")

type pdfExtractRequest struct {
	PdfBytes []byte `json:"pdfBytes"`
}

func ParsePlainText(log *logrus.Entry, pdfBytes []byte) (string, error) {
	req := pdfExtractRequest{pdfBytes}
	bytes, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	resBody := utils.DoRequest(log, utils.HttpRequestUtil{
		Method: http.MethodPost,
		Url:    cfg.PdfPlainTextUrl,
		Body:   bytes,
	}, uaa.Authenticate)
	if resBody == nil {
		return "", noResErr
	}
	b := new(strings.Builder)
	_, err = io.Copy(b, resBody)
	if err != nil {
		return "", err
	}
	return b.String(), nil
}
