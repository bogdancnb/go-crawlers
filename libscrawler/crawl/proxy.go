package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/uaa"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type Proxy struct {
	Host string
	Port int
}

func (p *Proxy) String() string {
	return fmt.Sprintf(`{"hostname":%s, "port":%d}`, p.Host, p.Port)
}

// GetProxy returns a nil proxy + non nil error if the call fails, otherwise a valid proxy and nil error
func GetProxy() (*Proxy, error) {
	log := logging.Instance().WithField("pgk", "crawl")
	res, err := proxyRequest(http.MethodGet, nil)
	if err != nil {
		log.WithError(err).Error("proxy request")
		return nil, err
	}

	defer res.Body.Close()

	proxy := new(Proxy)
	err = utils.FromJSON(proxy, res.Body)
	if err != nil {
		log.WithError(err).Error("deserialize proxy response")
		return nil, err
	}

	return proxy, nil
}

type proxyLoginResult struct {
	Host       string `json:"host"`
	Port       int    `json:"port"`
	LoginError bool   `json:"loginError"`
}

func SendConnectionError(proxy *Proxy) {
	log := logging.Instance().WithField("pgk", "crawl")
	log.Debug("sending connection error for ", proxy)
	body := proxyLoginResult{
		Host:       proxy.Host,
		Port:       proxy.Port,
		LoginError: true,
	}
	b, err := json.Marshal(body)
	if err != nil {
		log.WithError(err).Error("serializing")
		return
	}
	res, err := proxyRequest(http.MethodPut, b)
	if err != nil {
		log.WithError(err).Error("send connection error")
	}
	if res != nil {
		_ = res.Body.Close()
	}
}

func proxyRequest(method string, b []byte) (*http.Response, error) {
	log := logging.Instance().WithField("pgk", "crawl")
	headers, err := uaa.Authenticate()
	if err != nil {
		log.WithError(err).Error("auth error")
		return nil, err
	}
	uri := strings.ReplaceAll(cfg.GetProxyUrl, `{crawler}`, cfg.CrawlerUri)

	var body io.Reader
	if len(b) > 0 {
		body = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, uri, body)
	if err != nil {
		log.WithError(err).Error("create request ", cfg.GetProxyUrl)
		return nil, err
	}
	req.Header = headers
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("proxy response status=%d, body=%q", res.StatusCode, string(b))
	}
	return res, nil
}
