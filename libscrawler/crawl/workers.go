package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"fmt"
	"github.com/sirupsen/logrus"
	"time"
)

var (
	crawlQueue chan *Account
)

type fetchResult struct {
	*Account
	error
}

func startWorkers(shutdown <-chan struct{}) {
	log := logging.Instance().WithField("pgk", "crawl")
	if cfg.ThreadCount <= 0 {
		log.Warn("no workers, config thread count=", cfg.ThreadCount)
		return
	}

	crawlQueue = make(chan *Account, cfg.QueueSize)

	log.Debug("starting account fetch thread")
	go getWork(log, shutdown)

	log.Debugf("starting %d crawler threads", cfg.ThreadCount)
	for i := 0; i < cfg.ThreadCount; i++ {
		go crawl(log, i, shutdown)
	}
}

func getWork(log *logrus.Entry, shutdown <-chan struct{}) {
	logGW := log.WithField("thread", "getWork")
	defer func() {
		logGW.Info("shutdown, closing crawlQueue")
		close(crawlQueue)
		for range crawlQueue { //drain queue
		}
	}()

	isShutdown := func() bool {
		select {
		case <-shutdown:
			return true
		default:
			return false
		}
	}

	for {
		if isShutdown() {
			return
		}
		result, err := fetchAccounts(logGW)

		if err != nil {
			logGW.WithError(err).Errorf("requesting accounts, sleeping %d ms", cfg.GetWorkErrorSleepMs)
			select {
			case <-shutdown:
				return
			case <-time.After(time.Duration(cfg.GetWorkErrorSleepMs) * time.Millisecond):
				continue
			}
		}

		logGW.Debugf("received %d accounts", len(result))
		if len(result) == 0 {
			logGW.Debugf("no accounts received, sleeping %d ms", cfg.GetWorkIdleSleepMs)
			select {
			case <-shutdown:
				return
			case <-time.After(time.Duration(cfg.GetWorkIdleSleepMs) * time.Millisecond):
				continue
			}
		}

		for _, account := range result {
			select {
			case <-shutdown:
				return
			case crawlQueue <- account:
				logGW.Debugf("queued account %s", account.Username)
			}
		}
	}
}

func crawl(log *logrus.Entry, index int, shutdown <-chan struct{}) {
	logC := log.WithField("thread", fmt.Sprintf("crawl-%d", index))
	logC.Debug("starting")

	for {
		select {
		case <-shutdown:
			logC.Info("shutdown")
			return
		case acc, ok := <-crawlQueue:
			if !ok {
				logC.Infof("crawlQueue closed  ok=%v, account=%v", ok, acc)
				return
			}
			logC.Debugf("de-queued account %s", acc.Username)
			t := true
			acc.ExtractBarcode = &t
			start := time.Now()
			crawler := New(logC.WithField("user", acc.Username), acc)
			res, err := crawler.Parse()
			if err == nil {
				sendWork(logC, res, start)
				time.Sleep(time.Duration(cfg.WorkSleepMs) * time.Millisecond)
			} else {
				sendError(logC, acc, err, start)
				time.Sleep(time.Duration(cfg.ErrorSleepMs) * time.Millisecond)
			}
		}
	}
}
