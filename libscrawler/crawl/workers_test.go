package crawl

import (
	config2 "bitbucket.org/bogdancnb/go-crawlers/libs/config"
	"testing"
)

var _ = func() bool {
	testing.Init()
	return true
}()

func Test_getAccounts(t *testing.T) {
	config2.LoadCloud()
	initConfig()

	accs, err := fetchAccounts(nil)
	if err != nil {
		t.Error(err.Error())
	}
	if len(accs) == 0 {
		t.Error("no accounts received")
	}
}
