package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/uaa"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	crawlerUtils "bitbucket.org/bogdancnb/go-crawlers/libscrawler/utils"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"time"
)

const secretApiErr = "secrets-api-error"

var factories = make(map[string]Factory)

type Factory interface {
	NewStrategy(l *logrus.Entry) Strategy
}

type Crawler interface {
	Parse() (*Account, error)
	Pdf() *PdfResponse
	ActivateEInvoice(eInvoiceType *ElectronicInvoiceType) error
	Close()
}

func New(log *logrus.Entry, account *Account) Crawler {
	factory := factories[account.Uri]
	return &PagoCrawler{
		Entry: log.WithFields(logrus.Fields{
			"pkg":  "crawl",
			"type": "PagoCrawler",
		}),
		Strategy: factory.NewStrategy(log),
		Account:  account,
		Validate: validator.New(),
	}
}

func RegisterFactory(uri string, f Factory) {
	log := logging.Instance().WithField("pgk", "crawl")
	registered, ok := factories[uri]
	if ok {
		log.Warnf("%q replacing registered %T", uri, registered)
	}
	factories[uri] = f
	log.Debugf("registered %q with %T", uri, f)
}

type PagoCrawler struct {
	*logrus.Entry
	*validator.Validate
	Strategy         Strategy
	Account          *Account
	existingInvoices map[string]*Invoice
}

func (c *PagoCrawler) ActivateEInvoice(eInvoiceType *ElectronicInvoiceType) error {
	start := time.Now()
	defer func() {
		c.Debugf("ActivateEInvoice done in  %d ms", time.Since(start).Milliseconds())
	}()

	err := c.login()
	if err != nil {
		return err
	}
	received := c.Account.Locations[0]
	if c.Strategy.EInvoiceNeedsLoadLocations() {
		c.Account.Locations = make([]*Location, 0)
		c.Debug("loading locations...")
		if ex := c.Strategy.LoadLocations(c.Account); ex != nil {
			return ex
		}
	}
	if ex := c.Strategy.ActivateEInvoice(c.Account, received, *eInvoiceType); ex != nil {
		return ex
	}
	return nil
}

func (c *PagoCrawler) Parse() (resultAccount *Account, err error) {
	start := time.Now()
	defer func() {
		c.Close()
		if e, ok := err.(Exception); ok && c.Account.ExitIP != nil {
			e.SetExitIP(c.Account.ExitIP)
		}
		go c.checkConnectionError(err)
		c.Debugf("parse done in %d ms,  error= %T : %v", time.Since(start).Milliseconds(), err, err)
		if resultAccount != nil {
			_ = resultAccount.PrettyPrint(c.Logger.Writer())
		}
	}()

	err = c.Struct(c.Account)
	if err != nil {
		//todo validation.go
		c.Error("validating account: ", err)
		err = &ParseException{Msg: "error validating account: " + err.Error()}
		return
	}

	err = c.login()
	if err != nil {
		return
	}

	c.Strategy.SetExistingLocations(c.Account.Locations)
	c.existingInvoices = existingInvoices(c.Account)
	c.Strategy.SetExistingInvoices(c.existingInvoices)
	c.Account.Locations = c.Account.Locations[:0]
	c.Debug("loading locations...")
	err = c.Strategy.LoadLocations(c.Account)
	if err != nil {
		return
	}
	c.Debugf("loaded %d locations", len(c.Account.Locations))

	if !c.Strategy.LoadsInternal() {
		for _, location := range c.Account.Locations {
			c.Debug("loading amount for ", location.Identifier)
			err = c.Strategy.LoadAmount(c.Account, location)
			if err != nil {
				return
			}

			c.Debug("loading invoices for ", location.Identifier)
			err = c.Strategy.LoadInvoices(c.Account, location)
			if err != nil {
				return
			}
			sort.Slice(location.Invoices, func(i, j int) bool {
				return location.Invoices[i].IssueDate > location.Invoices[j].IssueDate
			})

			if c.Account.ExtractBarcode != nil && *c.Account.ExtractBarcode {
				for _, parsedInvoice := range location.Invoices {
					if (parsedInvoice.BarCode != nil && *parsedInvoice.BarCode != "") || parsedInvoice.AmountDue <= 0 {
						continue
					}
					existingInvoice, ok := c.existingInvoices[parsedInvoice.Ref]
					if !ok || (ok && (existingInvoice.BarCode == nil || *existingInvoice.BarCode == "")) {
						c.Debug("get barcode for", parsedInvoice.Ref)
						bex := c.Strategy.GetBarcode(c.Account, location, parsedInvoice)
						if bex != nil {
							c.Error("get barcode error:", bex.Error())
						}
					}
				}
			}

			c.Debug("loading payments for ", location.Identifier)
			err = c.Strategy.LoadPayments(c.Account, location)
			if err != nil {
				return
			}
			sort.Slice(location.Payments, func(i, j int) bool {
				return location.Payments[i].Date > location.Payments[j].Date
			})
		}
	}
	resultAccount = c.Account
	return
}

func (c *PagoCrawler) login() Exception {
	c.Debug("start login for ", c.Account.Username)
	if c.Account.Password == nil || *c.Account.Password == "" {
		if ex := c.decodeSecret(); ex != nil {
			return ex
		}
	}
	loginResp, err := c.Strategy.Login(c.Account)
	if err != nil {
		return err
	}
	c.Debug("login done")
	err = c.Strategy.CheckLogin(loginResp)
	if err != nil {
		return err
	}
	c.Account.Password = nil
	return nil
}

func (c *PagoCrawler) decodeSecret() Exception {
	c.Debug("decode secret call for account ", c.Account.Username)
	key := fmt.Sprintf(`%s:%s`, c.Account.Uri, c.Account.Username)
	key = url.QueryEscape(key)

	secret, ex := c.getSecret(key)
	if ex != nil {
		return ex
	}
	if secret != "" {
		c.Account.Password = &secret
		return nil
	}

	key = url.QueryEscape(key)
	secret, ex = c.getSecret(key)
	if ex != nil {
		return ex
	}
	if secret == "" {
		return ParseErr("got empty secret response")
	}
	c.Account.Password = &secret
	return nil
}

func (c *PagoCrawler) getSecret(key string) (string, Exception) {
	uaa.Init()
	uri := viper.GetString("secrets.secretServiceUri")
	uri += "?key=" + key
	c.Debug("calling secret uri=", uri)
	resBody := crawlerUtils.DoRequest(c.Entry,
		crawlerUtils.HttpRequestUtil{
			Method: http.MethodGet,
			Url:    uri,
			Body:   nil,
		},
		uaa.AuthenticateSecret,
	)
	if resBody == nil {
		args := uri + " :got no response for secret"
		c.Warn(args)
		return "", ParseErr(args)
	}
	secretBytes, err := ioutil.ReadAll(resBody)
	_ = resBody.Close()
	if err != nil {
		args := uri + " :reading secret response"
		c.Error(args)
		return "", ParseErr(args)

	}
	if len(secretBytes) > 0 {
		secret := string(secretBytes)
		if secret == secretApiErr {
			args := uri + " :got secret api error response: "
			c.Error(args, secretApiErr)
			return "", ParseErr(args + secretApiErr)
		}
		c.Debug("got secret ", secret)
		return secret, nil
	} else {
		args := uri + " :got empty secret response"
		c.Debug(args)
		return "", nil
	}
}

func (c *PagoCrawler) checkConnectionError(err error) {
	if err != nil {
		if _, ok := err.(*ConnectionException); ok {
			c.Strategy.SendConnectionError()
		}
	}
}

var loginErr = "LoginException"

func (c *PagoCrawler) Pdf() *PdfResponse {
	start := time.Now()
	defer func() { c.Debugf("pdf done in %d ms", time.Since(start).Milliseconds()) }()

	err := c.login()
	if err != nil {
		msg := loginErr + ": " + err.Error()
		return &PdfResponse{
			Content:   nil,
			PdfStatus: LOGIN_EXCEPTION.Name,
			ErrMsg:    &msg,
		}
	}
	if len(c.Account.Locations) == 0 || len(c.Account.Locations[0].Invoices) == 0 {
		msg := invalidPdfRequest.Error()
		return &PdfResponse{
			Content:   nil,
			PdfStatus: OTHER_EXCEPTION.Name,
			ErrMsg:    &msg,
		}
	}
	pdf := c.Strategy.Pdf(c.Account)
	if pdf.PdfStatus != OK.Name {
		c.Errorf("pdf error %s: %s", pdf.PdfStatus, pdf.ErrMsg)
	} else {
		c.Debug("pdf success")
	}
	return pdf
}

func (c *PagoCrawler) Close() {
	c.Strategy.Close(c.Account)
}

func existingInvoices(acc *Account) map[string]*Invoice {
	invs := make(map[string]*Invoice)
	for _, location := range acc.Locations {
		for _, invoice := range location.Invoices {
			invs[invoice.Ref] = invoice
		}
	}
	return invs
}
