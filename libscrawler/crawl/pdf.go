package crawl

import (
	"bytes"
	"errors"
	"github.com/ledongthuc/pdf"
	"io"
	"regexp"
	"strings"
)

var NoMatchErr = errors.New("no barcode matched for given patterns")

func PlainText(src []byte) (string, error) {
	bytesReader := bytes.NewReader(src)
	reader, err := pdf.NewReader(bytesReader, bytesReader.Size())
	if err != nil {
		return "", err
	}
	r, err := reader.GetPlainText()
	if err != nil {
		return "", err
	}
	var txt strings.Builder
	_, err = io.Copy(&txt, r)
	if err != nil {
		return "", err
	}
	return txt.String(), nil
}

func TextByRows(src []byte, pageNr int) (*pdf.Rows, error) {
	bytesReader := bytes.NewReader(src)
	reader, err := pdf.NewReader(bytesReader, bytesReader.Size())
	if err != nil {
		return nil, err
	}
	p := reader.Page(pageNr)
	rows, err := p.GetTextByRow()
	if err != nil {
		return nil, err
	}
	return &rows, nil
}

func TrimPdf(body []byte) []byte {
	eof := []byte(`%%EOF`)
	body = body[:bytes.LastIndex(body, eof)+len(eof)]
	return body
}

/*MatchBarcodePatterns returns matched barcode by one of the given patterns, or empty string and an error*/
func MatchBarcodePatterns(src []byte, patterns []*regexp.Regexp) (string, error) {
	txt, err := PlainText(src)
	if err != nil {
		return "", errors.New("parsing PDF text: " + err.Error())
	}

	match := ""
	for _, pattern := range patterns {
		matches := pattern.FindAllStringSubmatch(txt, -1)
		if len(matches) == 0 || len(matches[0]) == 0 {
			continue
		}
		match = matches[0][0]
		break
	}
	if match == "" {
		return "", NoMatchErr
	}
	return match, nil
}
