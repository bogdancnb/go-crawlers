package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	. "bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"crypto/tls"
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const ProxyFormat = `http://%s:%d`
const nrRedirects = 20

type Strategy interface {
	Login(a *Account) (*string, Exception)
	CheckLogin(response *string) Exception
	LoadLocations(a *Account) Exception
	LoadAmount(a *Account, l *Location) Exception
	LoadInvoices(a *Account, l *Location) Exception
	LoadPayments(a *Account, l *Location) Exception
	Pdf(a *Account) *PdfResponse
	LoadsInternal() bool
	Close(a *Account)
	SendConnectionError()
	SetExistingInvoices(existingInvoices map[string]*Invoice)
	SetExistingLocations(existingLocations []*Location)
	EInvoiceNeedsLoadLocations() bool
	ActivateEInvoice(account *Account, received *Location, electronicInvoiceType ElectronicInvoiceType) Exception
	GetBarcode(account *Account, location *Location, invoice *Invoice) Exception
}

type HttpStrategy interface {
	Client() *resty.Client
	SendConnectionError()
	Close(*Account)
	Proxy() *Proxy
	EnableRedirects(enabled bool)
}

func NewHttpStrategy(l *logrus.Entry) HttpStrategy {
	return &HttpStrategyDefault{Entry: l}
}

type HttpStrategyDefault struct {
	*logrus.Entry
	once   sync.Once //used to lazy init client
	client *resty.Client
	proxy  *Proxy
}

func (s *HttpStrategyDefault) EnableRedirects(enabled bool) {
	if enabled {
		s.Client().SetRedirectPolicy(resty.FlexibleRedirectPolicy(nrRedirects))
	} else {
		s.Client().SetRedirectPolicy(resty.NoRedirectPolicy())
	}
}

func (h *HttpStrategyDefault) Proxy() *Proxy {
	return h.proxy
}

func (h *HttpStrategyDefault) SendConnectionError() {
	l := h.WithField("method", "SendConnectionError")
	if h.proxy == nil {
		l.Debug("no proxy used")
		return
	}
	SendConnectionError(h.proxy)
}

func (h *HttpStrategyDefault) Close(acc *Account) {
	if h.proxy != nil {
		acc.ExitIP = &h.proxy.Host
	}
}

func (h *HttpStrategyDefault) Client() *resty.Client {
	h.once.Do(h.initClient)
	return h.client
}

//func (h *HttpStrategyDefault) Proxy() *Proxy {
//	return h.proxy
//}

func (h *HttpStrategyDefault) initClient() {
	p, err := GetProxy()
	if err != nil {
		h.WithError(err).Error("obtaining proxy")
	} else {
		h.Debug("got proxy: ", p)
		h.proxy = p
	}

	client := resty.New()
	client.SetAllowGetMethodPayload(true)
	client.SetHeader("user-agent", AgentChrome78)

	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	if files := strings.Split(*appflags.Certs, ","); *appflags.Certs != "" && len(files) > 0 {
		h.Debug("adding certificates ", files)
		basepath := viper.GetString("certs.basepath")
		for _, file := range files {
			fullPath := filepath.Join(basepath, file)
			client.SetRootCertificate(fullPath)
		}
	}

	if p != nil {
		client.SetProxy(fmt.Sprintf(ProxyFormat, p.Host, p.Port))
	}

	client.SetTimeout(2 * time.Minute)
	client.SetRedirectPolicy(resty.FlexibleRedirectPolicy(nrRedirects))
	//client.
	//	// Set retry count to non zero to enable retries
	//	SetRetryCount(1).
	//	// You can override initial retry wait time.
	//	// Default is 100 milliseconds.
	//	SetRetryWaitTime(5 * time.Second).
	//	// MaxWaitTime can be overridden as well.
	//	// Default is 2 seconds.
	//	SetRetryMaxWaitTime(20 * time.Second)

	//client.SetLogger(logging.Instance())
	if *appflags.LogLevel == "" || *appflags.LogLevel == "trace" || appflags.Localhost.Active() {
		client.SetDebug(true)
	}

	h.client = client
}
