package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/go-resty/resty/v2"
	"net/http"
	"strings"
)

const (
	AcceptHeader                    = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
	ContentTypeFormUrlEncodedHeader = "application/x-www-form-urlencoded"
	AgentChrome78                   = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
	AgentChrome78_2                 = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36"
)

// Ok checks if response has any error, or status code > 299
func Ok(resp *resty.Response, err error) (*resty.Response, Exception) {
	if err != nil {
		return resp, &ConnectionException{Msg: "http client error: " + err.Error()}
	}
	if resp.StatusCode() > 299 {
		return resp, &ConnectionException{Msg: fmt.Sprintf("status code %d", resp.StatusCode())}
	}
	return resp, nil
}

func OkString(resp *resty.Response, err error) (string, Exception) {
	resp, conEx := Ok(resp, err)
	if conEx != nil {
		return "", conEx
	}
	return resp.String(), nil
}

func OkDocument(resp *resty.Response, err error) (*goquery.Document, Exception) {
	log := logging.Instance().WithField("pgk", "crawl")
	resp, conEx := Ok(resp, err)
	if conEx != nil {
		return nil, conEx
	}
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(resp.Body()))
	if err != nil {
		msg := fmt.Sprintf("parsing body of %s %s", resp.Request.Method, resp.Request.URL)
		log.WithError(err).Error(msg)
		log.WithError(err).Error(resp.String())
		return nil, &ParseException{Msg: msg}
	}
	return doc, nil
}

// Accepted if response has any error, or status code not included in whitelist
func Accepted(resp *resty.Response, err error, acceptedCodes ...int) (*resty.Response, Exception) {
	if err != nil {
		return resp, &ConnectionException{Msg: "http client error: " + err.Error()}
	}
	if resp.StatusCode() <= 299 {
		return resp, nil
	}
	ok := false
	for _, code := range acceptedCodes {
		if code == resp.StatusCode() {
			ok = true
			break
		}
	}
	if ok {
		return resp, nil
	} else {
		return resp, &ConnectionException{Msg: fmt.Sprintf("status code %d", resp.StatusCode())}
	}
}

func GetBytes(resp *resty.Response, err error) ([]byte, Exception) {
	resp, conEx := Ok(resp, err)
	if conEx != nil {
		return nil, conEx
	}
	return resp.Body(), nil
}

//Attr returns the specified attribute value for the given selection, otherwise empty string and a descriptive ParseException
func Attr(s *goquery.Selection, attr string) (string, Exception) {
	if s == nil || s.Length() == 0 {
		return "", ParseErr("empty selection")
	}
	val, ok := s.Attr(attr)
	if !ok {
		html, _ := s.Html()
		return "", ParseErr("could not find attribute=" + attr + " for selection: " + html)
	}
	return val, nil
}

func CookiesByName(name string, cookies []*http.Cookie) []*http.Cookie {
	var res []*http.Cookie
	for _, cookie := range cookies {
		if cookie.Name == name {
			res = append(res, cookie)
		}
	}
	return res
}

func HeaderByNameAndValStartsWith(headers http.Header, name, prefix string) string {
	for key, values := range headers {
		if strings.ToLower(name) == strings.ToLower(key) {
			for _, val := range values {
				if prefix == "" {
					return val
				}
				if strings.HasPrefix(val, prefix) {
					return val
				}
			}
		}
	}
	return ""
}

func FormatResponse(res *resty.Response) string {
	return fmt.Sprintf(`statusCode=%d, body=%v`, res.StatusCode(), res.String())
}
