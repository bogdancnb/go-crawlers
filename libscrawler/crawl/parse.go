package crawl

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
	"time"
)

func WrapHtmlTableBody(content string) string {
	return `<html><body><table><tbody>` + content + `</tbody></table></body></html>`
}

func WrapHtmlBody(content string) string {
	return `<html><body>` + content + `</body></html>`
}

func DocFromBytes(body []byte) (*goquery.Document, Exception) {
	log := logging.Instance().WithField("pgk", "crawl")
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(body))
	if err != nil {
		msg := fmt.Sprintf("parsing body: %v", err)
		log.WithError(err).Errorf("%s", body)
		return nil, &ParseException{Msg: msg}
	}
	log.Trace(doc.Html())
	return doc, nil
}

func DocFromString(body string) (*goquery.Document, Exception) {
	log := logging.Instance().WithField("pgk", "crawl")
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		msg := fmt.Sprintf("parsing body: %v", err)
		log.WithError(err).Errorf("%s", body)
		return nil, &ParseException{Msg: msg}
	}
	log.Trace(doc.Html())
	return doc, nil
}

func AmountFromSelection(selection *goquery.Selection) (float64, Exception) {
	amountS := strings.TrimSpace(selection.Text())
	amountS = strings.ReplaceAll(amountS, ",", ".")
	amount, err := strconv.ParseFloat(amountS, 64)
	if err != nil {
		html, _ := selection.Html()
		return 0.0, ParseErr("parse amount: " + html)
	}
	return amount, nil
}

func AmountFromString(src string) (float64, Exception) {
	amountS := strings.TrimSpace(src)
	amountS = strings.ReplaceAll(amountS, ",", ".")
	amount, err := strconv.ParseFloat(amountS, 64)
	if err != nil {
		return 0.0, ParseErr("parse amount: " + src)
	}
	return amount, nil
}

func TimeFromString(format, dateString string) (*time.Time, Exception) {
	t, err := time.Parse(format, dateString)
	if err != nil {
		return nil, ParseErr(fmt.Sprintf(`parsing date string %q with format %q: %v`, dateString, format, err))
	}
	return &t, nil
}
