package crawl

import (
	"github.com/spf13/viper"
	"math"
)

type crawlerConfig struct {
	CrawlerUri          string
	ThreadCount         int
	QueueSize           int
	GetWorkSize         int
	ErrorSleepMs        int64
	WorkSleepMs         int64
	GetWorkErrorSleepMs int64
	GetWorkIdleSleepMs  int64
	GetWorkUrl          string
	GetWorkUrlMulti     string
	SendWorkUrl         string
	SendErrorUrl        string
	GetProxyUrl         string
	PdfPlainTextUrl     string
	CrawlTimeoutMillis  int64
}

var cfg crawlerConfig

func Config() *crawlerConfig {
	return &cfg
}

func StartWorkers(shutdown <-chan struct{}) {
	initConfig()
	startWorkers(shutdown)
}

func initConfig() {
	viper.SetDefault("crawler.threadCount", 10)
	viper.SetDefault("crawler.errorSleepMillis", 20000)
	viper.SetDefault("crawler.getWork.errorSleepMillis", 20000)
	viper.SetDefault("crawler.getWork.idleSleepMillis", 300000)
	viper.SetDefault("crawler.timeoutMillis", 180000)
	viper.SetDefault("crawler.pdf.plainText.url", "${pago.edgeServer}/pdf-app/extract/text-plain")

	cfg = crawlerConfig{
		CrawlerUri:          viper.GetString("crawler.uri"),
		ThreadCount:         viper.GetInt("crawler.threadCount"),
		QueueSize:           viper.GetInt("crawler.queueSize"),
		GetWorkSize:         viper.GetInt("crawler.getWork.size"),
		ErrorSleepMs:        viper.GetInt64("crawler.errorSleepMillis"),
		WorkSleepMs:         viper.GetInt64("crawler.workSleepMillis"),
		GetWorkErrorSleepMs: viper.GetInt64("crawler.getWork.errorSleepMillis"),
		GetWorkIdleSleepMs:  viper.GetInt64("crawler.getWork.idleSleepMillis"),
		GetWorkUrl:          viper.GetString("crawler.getWork.url"),
		GetWorkUrlMulti:     viper.GetString("crawler.getWork.urlMulti"),
		SendWorkUrl:         viper.GetString("crawler.sendWork.url"),
		SendErrorUrl:        viper.GetString("crawler.sendError.url"),
		GetProxyUrl:         viper.GetString("crawler.getProxy.url"),
		PdfPlainTextUrl:     viper.GetString("crawler.pdf.plainText.url"),
		CrawlTimeoutMillis:  viper.GetInt64("crawler.timeoutMillis"),
	}

	if cfg.QueueSize == 0 {
		cfg.QueueSize = cfg.ThreadCount * 5
	}
	if cfg.GetWorkSize == 0 {
		cfg.GetWorkSize = int(math.Max(1, float64(cfg.QueueSize/5)))
	}
}
