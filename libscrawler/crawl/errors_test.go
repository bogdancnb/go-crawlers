package crawl

import "testing"

func TestLoginException_Response(t *testing.T) {
	got := LoginException{
		"this is a login error",
		"0.0.0.0",
	}.Response()
	want := "{\"pagocrawlererror\":\"LoginException: this is a login error\",\"exitIP\":\"0.0.0.0\"}"
	if got != want {
		t.Error()
	}
}
