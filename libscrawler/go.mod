module bitbucket.org/bogdancnb/go-crawlers/libscrawler

go 1.14

replace bitbucket.org/bogdancnb/go-crawlers/libs => ../libs

require (
	bitbucket.org/bogdancnb/go-crawlers/libs v0.0.0-00010101000000-000000000000
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/andybalholm/brotli v1.0.1
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-resty/resty/v2 v2.3.0
	github.com/gorilla/mux v1.7.4
	github.com/ledongthuc/pdf v0.0.0-20200323191019-23c5852adbd2
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
