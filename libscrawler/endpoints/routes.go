package endpoints

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libs/endpoints"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/handlers"
	"net/http"
)

func Crawler() *RouteGroup {
	accountHandler := handlers.NewAccount()

	crawlerRoutes := RouteGroup{
		Prefix:    "/crawl",
		Validator: accountHandler.Validate,
		List: []*Route{
			{
				Method:      http.MethodPost,
				Pattern:     "/complete",
				HandlerFunc: accountHandler.Crawl,
			}, {
				Method:      http.MethodPost,
				Pattern:     "/pdfbytes",
				HandlerFunc: accountHandler.Pdf,
			}, {
				Method:      http.MethodPost,
				Pattern:     "/e-invoice/{electronicInvoiceType}",
				HandlerFunc: accountHandler.ActivateEInvoice,
			},
		},
	}
	return &crawlerRoutes
}
