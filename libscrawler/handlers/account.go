package handlers

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawl"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/model"
	"context"
	"fmt"
	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

//AccountHandler is the handler for Account related requests
type AccountHandler struct {
	*logrus.Entry
	validate *validator.Validate
}

type AccountKey struct{}

func NewAccount() *AccountHandler {
	return &AccountHandler{
		logging.Instance().WithField("pkg", "handlers").WithField("type", "AccountHandler"),
		validator.New(),
	}
}

func (ah *AccountHandler) Validate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		acc := &model.Account{}
		err := utils.FromJSON(acc, r.Body)
		if err != nil {
			ah.Error("deserialize account: ", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = ah.validate.Struct(acc)
		if err != nil {
			//todo validation.go
			ah.Error("validating account: ", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ctx := context.WithValue(r.Context(), AccountKey{}, acc)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

func (ah *AccountHandler) Crawl(w http.ResponseWriter, r *http.Request) {
	acc := r.Context().Value(AccountKey{}).(*model.Account)
	l := ah.WithField("thread", "http:"+acc.Username)
	l.Debug("received crawl request")

	crawler := crawl.New(logging.Instance().WithField("thread", "http:"+acc.Username), acc)
	res, err := crawler.Parse()
	if err != nil {
		l.Errorf("error crawl request %T : %v", err, err)
		ah.handleError(w, err)
		return
	}

	err = utils.ToJSON(res, w)
	if err != nil {
		l.Error("writing account response: ", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (ah *AccountHandler) Pdf(writer http.ResponseWriter, r *http.Request) {
	acc := r.Context().Value(AccountKey{}).(*model.Account)
	l := ah.WithField("thread", "pdf:"+acc.Username)
	l.Debug("received pdf request")
	crawler := crawl.New(logging.Instance().WithField("thread", "pdf:"+acc.Username), acc)
	pdf := crawler.Pdf()
	err := utils.ToJSON(pdf, writer)
	if err != nil {
		l.Error("serializing pdf response: ", err)
		http.Error(writer, err.Error(), http.StatusInternalServerError)
	}
}

func (ah *AccountHandler) ActivateEInvoice(w http.ResponseWriter, r *http.Request) {
	acc := r.Context().Value(AccountKey{}).(*model.Account)
	l := ah.WithField("thread", "activateEInvoice:"+acc.Username)
	l.Debug("received ActivateEInvoice request")
	vars := mux.Vars(r)
	cat := vars["electronicInvoiceType"]
	if cat != string(model.EMAIL) && cat != string(model.SMS) {
		http.Error(w, fmt.Sprintf("wrong electronicInvoiceType:%s", cat), http.StatusBadRequest)
		return
	}
	crawler := crawl.New(logging.Instance().WithField("thread", "activateEInvoice:"+acc.Username), acc)
	category := model.ElectronicInvoiceType(cat)
	err := crawler.ActivateEInvoice(&category)
	if err != nil {
		l.Errorf("error ActivateEInvoice request %T : %v", err, err)
		ah.handleError(w, err)
		return
	}
	l.Debug("ActivateEInvoice request successful")

}

func (ah *AccountHandler) handleError(w http.ResponseWriter, err error) {
	switch err := err.(type) {
	case *crawl.LoginException:
		http.Error(w, err.CrawlerErrorMsg(), 411)
	case *crawl.ParseException:
		http.Error(w, err.CrawlerErrorMsg(), 410)
	case *crawl.ConnectionException:
		http.Error(w, err.CrawlerErrorMsg(), 412)
	default:
		http.Error(w, err.Error(), 409)
	}
}
