package model

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"encoding/json"
	"fmt"
	"io"
	"text/template"
)

type Account struct {
	Id               *int                 `json:"id"`
	Username         string               `json:"username" validate:"required"`
	Password         *string              `json:"password"`
	Uri              string               `json:"uri" validate:"required"`
	Locations        []*Location          `json:"locations"`
	ExitIP           *string              `json:"exitIP"`
	ProviderBranches []*ProviderBranchDTO `json:"providerBranches"`
	CrawlerRunId     *int                 `json:"crawlerRunId"`
	ExtractBarcode   *bool                `json:"extractBarcode"`

	//Active           bool                 `json:"active"`
	//Enrolment        bool                 `json:"enrolment"`
	//Principal        string               `json:"principal"`
	//NewPdfs          []*InvoicePdf        `json:"newPdfs"`
	//NewInvoices      int                  `json:"newInvoices"`
	//NewLocations     int                  `json:"newLocations"`
	//NextCrawl        time.Time            `json:"nextCrawl"`
}

func (a *Account) AddLocation(l *Location) {
	if len(a.Locations) == 0 {
		a.Locations = make([]*Location, 0, 1)
	}
	a.Locations = append(a.Locations, l)
}

func (a *Account) String() string {
	s, err := json.MarshalIndent(*a, "", "  ")
	if err != nil {
		return fmt.Sprintf("{\"username\":%q, \"uri\":%q, \"password\":%q}", a.Username, a.Uri, a.Password)
	}
	return string(s)
}

func (a *Account) PrettyPrint(w io.Writer) error {
	err := prettyPrint.Execute(w, a)
	return err
}

type ProviderBranchDTO struct {
	BranchId        *int    `json:"branchId"`
	Cui             *string `json:"cui"`
	Iban            *string `json:"iban"`
	BankName        *string `json:"bankName"`
	BankBIC         *string `json:"bankBIC"`
	LegalEntityName *string `json:"legalEntityName"`
	ProviderId      *int    `json:"providerId"`
}

func (p *ProviderBranchDTO) String() string {
	cui := ""
	if p.Cui != nil {
		cui = *p.Cui
	}
	return fmt.Sprintf(`{iban:%s, bankName:%s, bankBIC:%s, cui:%s, legalEntityName:%s}`, *p.Iban, *p.BankName, *p.BankBIC, cui, *p.LegalEntityName)
}

type InvoicePdf struct {
	Id         *int     `json:"id"`
	InvoiceRef *string  `json:"invoiceRef"`
	Content    []byte   `json:"content"`
	Uri        *string  `json:"uri"`
	Invoice    *Invoice `json:"invoice"`
}

const templ = `
============={{.Username | printf "%.30s"}}======={{.Uri | printf "%.30s"}}========================================
CrawlerRunId {{.CrawlerRunId}}

{{range .Locations}}------------------------------------------------------------------------------------------------------------------------
Identifier   {{.Identifier}}
Details      {{.Details | stringVal}}
Service      {{.Service}}
Amount       {{.Amount}}
Branch       {{.ProviderBranch}}

Invoices 
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Ref                                                                    |IssueDate |DueDate   |Amount   |AmountDue|PdfUri                                                                  |BarCode
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------{{range .Invoices}}
{{.Ref | printf "%-70.70s"}} | {{.IssueDate | printf "%8s"}} | {{.DueDate | printf "%8s"}} | {{.Amount | printf "%7.2f"}} | {{.AmountDue | printf "%7.2f"}} | {{.PdfUri | stringVal | printf "%-70.70s"}} | {{.BarCode | stringVal | printf "%-40.40s"}}{{end}}

Payments {{range .Payments}}
{{.Ref | printf "%-70.70s"}} | {{.Date | printf "%8s"}} | {{.Amount | printf "%7.2f"}}{{end}}
------------------------------------------------------------------------------------------------------------------------
{{end}}
`

var prettyPrint = template.Must(template.New("prettyPrint").
	Funcs(template.FuncMap{"stringVal": utils.StringVal}).
	Parse(templ))
