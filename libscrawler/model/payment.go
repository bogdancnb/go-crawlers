package model

import (
	"fmt"
)

type Payment struct {
	Id      *int    `json:"id"`
	Ref     string  `json:"ref"`
	Amount  float64 `json:"amount"`
	Date    string  `json:"date"`
	Details *string `json:"details"`
}

func (p *Payment) String() string {
	return fmt.Sprintf(`{"id":%d, "ref":"%s", "details":"%s", "date":"%s", "amount":%f}`, p.Id, p.Ref, p.Details, p.Date, p.Amount)
}
