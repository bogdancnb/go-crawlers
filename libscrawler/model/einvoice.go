package model

type ElectronicInvoiceType string

const (
	EMAIL ElectronicInvoiceType = "EMAIL"
	SMS   ElectronicInvoiceType = "SMS"
)

type ElectronicInvoiceLocationInfo struct {
	ElectronicInvoiceType     ElectronicInvoiceType `json:"electronicInvoiceType"`
	ElectronicInvoiceActive   bool                  `json:"electronicInvoiceActive"`
	ElectronicInvoiceEditable bool                  `json:"electronicInvoiceEditable"`
}
