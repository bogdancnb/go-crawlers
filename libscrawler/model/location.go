package model

import (
	"fmt"
	"github.com/shopspring/decimal"
	"sort"
	"strings"
)

type Location struct {
	Id                             *int                             `json:"id"`
	Service                        string                           `json:"service"`
	Details                        *string                          `json:"details"`
	Identifier                     string                           `json:"identifier"`
	LocationStatus                 *LocationStatus                  `json:"locationStatus"`
	AccountUri                     *string                          `json:"accountUri"`
	AmountUri                      *string                          `json:"amountUri"`
	PaymentUri                     *string                          `json:"paymentUri"`
	Amount                         *float64                         `json:"amount"`
	DueDate                        *string                          `json:"dueDate"`
	Invoices                       []*Invoice                       `json:"invoices"`
	Payments                       []*Payment                       `json:"acceptedPayments"`
	ProviderBranch                 *ProviderBranchDTO               `json:"providerBranch"`
	ElectronicInvoiceLocationInfos []*ElectronicInvoiceLocationInfo `json:"electronicInvoiceLocationInfos"`
	LocationAddress                *LocationAddress                 `json:"locationAddress"`
	ServiceProvided                *string                          `json:"serviceProvided"`

	AccountHelpers interface{} `json:"-"`
}

func (l *Location) String() string {
	return fmt.Sprintf(`{"identifier":"%s", "service":"%s"}`, l.Identifier, l.Service)
}

func (l *Location) AccountHelper() interface{}         { return l.AccountHelpers }
func (l *Location) SetAccountHelper(value interface{}) { l.AccountHelpers = value }

func (l *Location) AddInvoice(i *Invoice) {
	if len(l.Invoices) == 0 {
		l.Invoices = make([]*Invoice, 0, 1)
	}
	l.Invoices = append(l.Invoices, i)
}

func (l *Location) AddPayment(p *Payment) {
	if len(l.Payments) == 0 {
		l.Payments = make([]*Payment, 0, 1)
	}
	l.Payments = append(l.Payments, p)
}

type LocationStatus string

const (
	all        LocationStatus = "All"
	perService LocationStatus = "PerService"
)

func (l *LocationStatus) UnmarshalJSON(bytes []byte) error {
	if len(bytes) == 0 {
		l = nil
		return nil
	}
	locationStatus := LocationStatus(strings.Trim(string(bytes), `"`))
	switch locationStatus {
	case all, perService:
		*l = locationStatus
		return nil
	}
	return fmt.Errorf("invalid value for LocationStatus: %q", bytes)
}

func (l *LocationStatus) MarshalJSON() ([]byte, error) {
	if l == nil {
		return nil, nil
	}
	return []byte(*l), nil
}

func (l *Location) SetInvoiceDueFromAmount() bool {
	if l.Amount == nil || *l.Amount <= 0 {
		return true
	}
	sort.Slice(l.Invoices, func(i, j int) bool {
		return l.Invoices[i].IssueDate > l.Invoices[j].IssueDate
	})

	due := *l.Amount
	for _, i := range l.Invoices {
		if due == 0 {
			return true
		}
		if i.Amount < due {
			i.AmountDue = i.Amount
			left := decimal.NewFromFloat(due).Sub(decimal.NewFromFloat(i.Amount))
			d, _ := left.Float64()
			//if !ok {
			//	return false
			//}
			due = d
		} else {
			i.AmountDue = due
			break
		}
	}
	return true
}

type LocationAddress struct {
	AddressId      *float64 `json:"addressId"`
	Street         *string  `json:"street"`
	StreetNumber   *string  `json:"streetNumber"`
	Building       *string  `json:"building"`
	BuildingNumber *string  `json:"buildingNumber"`
	Floor          *string  `json:"floor"`
	Apartment      *string  `json:"apartment"`
	City           *string  `json:"city"`
	County         *string  `json:"county"`
	PostalCode     *string  `json:"postalCode"`
}
