package model

type PdfStatus struct {
	code int
	Name string
}

var (
	OK                = PdfStatus{200, "OK"}
	LOGIN_EXCEPTION   = PdfStatus{407, "LOGIN_EXCEPTION"}
	OTHER_EXCEPTION   = PdfStatus{406, "OTHER_EXCEPTION"}
	NOT_GENERATED     = PdfStatus{204, "NOT_GENERATED"}
	DOWNLOADING       = PdfStatus{425, "DOWNLOADING"}
	INVOICE_NOT_FOUND = PdfStatus{400, "INVOICE_NOT_FOUND"}
	NO_PDF_URI        = PdfStatus{405, "NO_PDF_URI"}
	NOT_FOUND         = PdfStatus{404, "NOT_FOUND"}
	INVALID_CONTENT   = PdfStatus{406, "INVALID_CONTENT"}
)

type PdfResponse struct {
	Content   []byte  `json:"content"`
	PdfStatus string  `json:"pdfStatus"`
	ErrMsg    *string `json:"errMsg"`
}

func PdfErrorResponse(msg string, status PdfStatus) *PdfResponse {
	return &PdfResponse{
		Content:   nil,
		PdfStatus: status.Name,
		ErrMsg:    &msg,
	}
}
