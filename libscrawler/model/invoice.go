package model

import (
	"fmt"
)

type Invoice struct {
	Id        *int    `json:"id"`
	Ref       string  `json:"ref"`
	PdfUri    *string `json:"pdfUri"`
	Amount    float64 `json:"amount"`
	AmountDue float64 `json:"amountDue"`
	IssueDate string  `json:"issueDate"`
	DueDate   string  `json:"dueDate"`
	BarCode   *string `json:"barCode"`
	HasPdf    *bool   `json:"hasPdf"`

	//Status                   string  `json:"status"`
	//NeedsRefresh             bool    `json:"needsRefresh"`
	//ProviderConfirmedPayment bool    `json:"providerConfirmedPayment"`
}

func (i *Invoice) String() string {
	return fmt.Sprintf(`{"ref":"%s", "id":%d, "pdfUri":"%s", "amount":%f, "amountDue":%f, "issueDate":"%s", "dueDate":"%s", "barCode":"%s"}`,
		i.Ref, i.Id, i.PdfUri, i.Amount, i.AmountDue, i.IssueDate, i.DueDate, i.BarCode)
}
