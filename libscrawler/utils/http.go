package utils

import (
	"bytes"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

var httpClient = &http.Client{
	Timeout: 2 * time.Minute,
}

type HttpRequestUtil struct {
	Method string
	Url    string
	Body   []byte
}

//DoRequest returns response body if status code is 200 (must be closed after usage), otherwise nil
func DoRequest(l *logrus.Entry, request HttpRequestUtil, authenticator func() (http.Header, error)) io.ReadCloser {
	var body io.Reader
	if len(request.Body) > 0 {
		body = bytes.NewBuffer(request.Body)
	}
	req, err := http.NewRequest(request.Method, request.Url, body)
	if err != nil {
		l.WithError(err).Errorf("creating request %s", request.Url)
		return nil
	}
	headers, err := authenticator()
	if err != nil {
		l.WithError(err).Error("auth")
		return nil
	}
	req.Header = headers
	response, err := httpClient.Do(req)
	if err != nil {
		l.WithError(err).Errorf("doing request %s", request.Url)
		return nil
	}

	if response.StatusCode != 200 {
		b, _ := ioutil.ReadAll(response.Body)
		l.Errorf("response: status=%d, body=%s", response.StatusCode, b)
		_ = response.Body.Close()
		return nil
	}
	return response.Body
}
