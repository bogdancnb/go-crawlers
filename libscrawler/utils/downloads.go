package utils

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"path/filepath"
	"strings"
)

func CheckDownloads(downloadPath string, filename string, log *logrus.Entry) ([]byte, error) {
	files, err := ioutil.ReadDir(downloadPath)
	if err != nil {
		return nil, fmt.Errorf("could not read downloads folder")
	}
	for _, file := range files {
		if strings.Contains(file.Name(), filename) {
			log.Debug("found downloaded file ", file.Name())
			bytes, err := ioutil.ReadFile(filepath.Join(downloadPath, file.Name()))
			return bytes, err
		}
	}
	return nil, nil
}
