package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/pwikrybnik"
)

func main() {
	crawlerapp.Run(pwikrybnik.Init)
}
