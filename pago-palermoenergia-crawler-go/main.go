package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/palermoenergia"
)

func main() {
	crawlerapp.Run(palermoenergia.Init)
}
