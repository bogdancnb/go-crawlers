package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/blocexpert"
)

func main() {
	crawlerapp.Run(blocexpert.Init)
}
