package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/aceaenergia"
)

func main() {
	crawlerapp.Run(aceaenergia.Init)
}
