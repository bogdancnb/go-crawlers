package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/strategies/engieitalia"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
)

func main() {
	crawlerapp.Run(engieitalia.Init)
}
