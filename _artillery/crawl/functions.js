module.exports = {
    setJSONBody: setJSONBody,
    logHeaders: logHeaders
}

function setJSONBody(requestParams, context, ee, next) {
    return next(); // MUST be called for the scenario to continue
}

function logHeaders(requestParams, response, context, ee, next) {
    if (response.statusCode != "200") {
        console.log("=============STATUS CODE: " + response.statusCode + "===================");
        console.log(requestParams.url);
        console.log("Response:");
        console.log(response.body);
        console.log("=============DONE LOG FOR STATUS CODE: " + response.statusCode + "===================");
    }
    return next(); // MUST be called for the scenario to continue
}