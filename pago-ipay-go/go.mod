module bitbucket.org/bogdancnb/go-crawlers/pago-ipay-go

go 1.14

require (
	github.com/go-resty/resty/v2 v2.3.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.6.0

)
