package main

import (
	"crypto/tls"
	"github.com/go-resty/resty/v2"
	gorillactx "github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"time"
)

const timeout = 2 * time.Minute

var (
	client *resty.Client
	log    *logrus.Logger
)

func main() {
	initClient()
	initLog()

	r := mux.NewRouter()
	r.HandleFunc("/post", handlerPost).Methods(http.MethodPost)
	r.HandleFunc("/get", handlerGet).Methods(http.MethodPost)

	cors := handlers.CORS(handlers.AllowedOrigins([]string{"*"}))
	listener, err := net.Listen("tcp", ":9020")
	if err != nil {
		log.Fatal("could not create net listener")
	}
	server := http.Server{
		Handler:      cors(gorillactx.ClearHandler(r)), // set the default handlerGet
		ReadTimeout:  timeout,                          // max time to read request from the client
		WriteTimeout: timeout,                          // max time to write response to the client
		IdleTimeout:  timeout,                          // max time for connections using TCP Keep-Alive
	}
	err = server.Serve(listener)
	if err != nil {
		if err != http.ErrServerClosed {
			log.Fatalf("Serve error (!= http.ErrServerClosed): %v", err)
		} else {
			log.Info(err)
		}
	}
}

func handlerGet(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	log.Info("Processing request", r)
	if err != nil {
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	res, err := client.R().Get(string(body))
	if err != nil {
		http.Error(w, "GET url error: "+err.Error(), 409)
		return
	}
	log.Info("Responding ", res.String())
	_, _ = io.WriteString(w, res.String())
}

func handlerPost(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	log.Info("Processing request ", r)
	if err != nil {
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}
	res, err := client.R().Post(string(body))
	if err != nil {
		http.Error(w, "GET url error: "+err.Error(), 409)
		return
	}
	log.Info("Responding ", res.String())
	_, _ = io.WriteString(w, res.String())
}

func initLog() {
	log = logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:          true,
		TimestampFormat:        "2006-01-02 15:04:05.000",
		DisableLevelTruncation: false,
		PadLevelText:           true,
	})
	log.SetLevel(logrus.TraceLevel)
}

func initClient() {
	client = resty.New()
	client.SetAllowGetMethodPayload(true)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetTimeout(timeout)
	client.SetRedirectPolicy(resty.FlexibleRedirectPolicy(15))
}
