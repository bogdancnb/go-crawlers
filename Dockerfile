FROM iron/base
RUN mkdir -p /usr/local/app
ADD . /usr/local/app/
WORKDIR /usr/local/app
RUN pwd
RUN chmod +x /usr/local/app/*
RUN ls -l
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
CMD ["/usr/local/app/run.sh"]