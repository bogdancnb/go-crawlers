package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/enelitalia"
)

func main() {
	crawlerapp.Run(enelitalia.Init)
}
