package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/premiummobile"
)

func main() {
	crawlerapp.Run(premiummobile.Init)
}
