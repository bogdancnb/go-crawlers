package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/energiaazzura"
)

func main() {
	crawlerapp.Run(energiaazzura.Init)
}
