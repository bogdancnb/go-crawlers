package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/gkpge"
)

func main() {
	crawlerapp.Run(gkpge.Init)
}
