package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/jmdi"
)

func main() {
	crawlerapp.Run(jmdi.Init)
}
