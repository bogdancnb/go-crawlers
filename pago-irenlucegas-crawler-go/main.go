package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/irenlucegas"
)

func main() {
	crawlerapp.Run(irenlucegas.Init)
}
