package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/a2aenergia"
)

func main() {
	crawlerapp.Run(a2aenergia.Init)
}
