package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/postemobile"
)

func main() {
	crawlerapp.Run(postemobile.Init)
}
