package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/aquavas"
)

func main() {
	crawlerapp.Run(aquavas.Init)
}
