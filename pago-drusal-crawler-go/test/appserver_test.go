package test

//
//var crawlUrl = "http://localhost:8989/enel/crawl/complete"
//var reader = strings.NewReader("{\"username\": \"bogdan\",\"uri\": \"vodafone.crawler\"}")
//var server *http.Server
//
////func BenchmarkServerParaller(b *testing.B) {
////	if server == nil {
////		server = startServer()
////		time.Sleep(time.Second)
////	}
////	b.ReportAllocs()
////	b.ResetTimer()
////	b.ReportAllocs()
////	//b.SetParallelism(50)
////	b.RunParallel(func(pb *testing.PB) {
////		client := &http.Client{}
////
////		for pb.Next() {
////			res, err := client.Post(crawlUrl, "application/json", reader)
////			if err != nil {
////				b.Errorf("post error: %v", err)
////				continue
////			}
////			defer res.Body.Close()
////			data, err := ioutil.ReadAll(res.Body)
////			if err != nil {
////				b.Errorf("response body read error: %v", err)
////				continue
////			}
////			b.Log(string(data))
////		}
////	})
////}
//
////BenchmarkServerSequence is used to benchmark cpu/memory/concurrency for the server
////
//// ex memory:
////
////  go test -run=NONE -bench=BenchmarkServerSequence -memprofile=mem.out bitbucket.org/bogdancnb/go-crawlers/libs/web
////	go tool pprof -text -nodecount=50 ./web.test mem.out
//func BenchmarkServerSequence(b *testing.B) {
//	router := endpoints.NewRouter([]*endpoints.RouteGroup{endpoints2.Crawler()})
//
//	if server == nil {
//		server = appServer.StartServer(router, make(chan string, 1))
//		time.Sleep(time.Second)
//	}
//
//	client := &http.Client{}
//
//	for n := 0; n < b.N; n++ {
//		res, err := client.Post(crawlUrl, "application/json", reader)
//		if err != nil {
//			b.Errorf("post error: %v", err)
//			continue
//		}
//		defer res.Body.Close()
//		data, err := ioutil.ReadAll(res.Body)
//		if err != nil {
//			b.Errorf("response body read error: %v", err)
//			continue
//		}
//		b.Log(string(data))
//	}
//}
