package eureka

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/config"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"errors"
	"fmt"
	"github.com/ArthurHlt/go-eureka-client/eureka"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	outOfService = "OUT_OF_SERVICE"
)

var errCodeInstanceNotFound = errors.New("Instance resource not found when sending heartbeat")

type eurekaInfo struct {
	ip      string
	port    int
	appId   string
	appName string
}

var info eurekaInfo
var client *eureka.Client
var instance *eureka.InstanceInfo
var heartbeat *time.Ticker

// Register uses eureka client library (https://github.com/ArthurHlt/go-eureka-client)
func Register(addr <-chan string, shutdown <-chan struct{}) {
	log := logging.Instance().WithField("pkg", "eureka")
	initConfig()
	address := <-addr
	//log.Debug("waiting before registering with eureka")
	//time.Sleep(5 * time.Second)

	info = getEurekaInfo(log, address)
	log.Debugf("registering with eureka for %s\n", info.appId)

	//From a spring boot based eureka server
	client = eureka.NewClient([]string{cfg.clientServiceUrl})
	//Create a new instance to register
	instance = eureka.NewInstanceInfo(
		info.ip,
		info.appName,
		info.ip,
		info.port,
		30,
		false,
	)
	instance.InstanceID = info.appId
	instance.SecurePort = &eureka.Port{
		Port:    443,
		Enabled: false,
	}
	instance.HomePageUrl = "http://" + (config.ApplicationName()) + ":" + strconv.Itoa(info.port)
	instance.StatusPageUrl = instance.HomePageUrl + cfg.infoPath
	instance.HealthCheckUrl = instance.HomePageUrl + cfg.healthPath
	instance.VipAddress = config.ApplicationName()
	instance.SecureVipAddress = config.ApplicationName()
	instance.Metadata = &eureka.MetaData{
		Map: map[string]string{
			//"management.port":         strconv.Itoa(info.port),
			"health.path":             cfg.healthPath,
			"management.context-path": cfg.managementContextPath,
			"metrics.path":            cfg.metricsPath,
		},
	}

	err := client.RegisterInstance(info.appName, instance)
	if err != nil {
		log.Fatalf("eureka register: %v", err)
	}
	log.Debug("registering with eureka done")
	sendHeartbeats(log, shutdown)
}

func sendHeartbeats(log *logrus.Entry, shutdown <-chan struct{}) {
	interval := 30
	if cfg.renewalIntervalInSecs > 0 {
		interval = cfg.renewalIntervalInSecs
	}
	heartbeat = time.NewTicker(time.Duration(interval*1000) * time.Millisecond)
	for {
		select {
		case <-heartbeat.C:
			err := client.SendHeartbeat(instance.App, instance.InstanceID)
			if err != nil {
				log.Errorf("eureka heartbeat: %v\n", err)
			}
		case <-shutdown:
			log.Debug("stop heartbeats")
			return
		}
	}
}

func Deregister(log *logrus.Entry) {
	log.Debugf("de-registering with eureka for %s\n", info.appId)
	heartbeat.Stop()
	instance.Status = outOfService
	err := client.UnregisterInstance(instance.App, instance.InstanceID)
	if err != nil {
		log.Errorf("eureka de-register: %v\n", err)
	}
}

func OutOfService() {
	log := logging.Instance().WithField("pkg", "eureka")
	log.Debugf("de-registering with eureka for %s\n", info.appId)
	heartbeat.Stop()
	instance.Status = outOfService
	values := []string{"apps", instance.App, instance.InstanceID, "status?value=OUT_OF_SERVICE"}
	path := strings.Join(values, "/")
	resp, err := client.Put(path, nil)
	if err != nil {
		log.Errorf("eureka de-register: %v\n", err)
		return
	}
	switch resp.StatusCode {
	case http.StatusNotFound:
		log.Error(errCodeInstanceNotFound)
	}
}

func getEurekaInfo(log *logrus.Entry, address string) eurekaInfo {
	var myIp string
	var err error
	if !appflags.Localhost.Active() {
		myIp, err = utils.ResolveIPFromHostsFile()
		if err != nil {
			log.WithError(err).Fatal()
		}
	} else {
		myIp, err = utils.GetIP()
		if err != nil {
			log.WithError(err).Fatal("could not obtain ip")
		}
	}
	p, err := utils.Port(address)
	if err != nil {
		log.Fatal("could not obtain server port")
	}
	return eurekaInfo{
		ip:      myIp,
		port:    p,
		appId:   fmt.Sprintf("%s:%s:%d", myIp, config.ApplicationName(), p),
		appName: strings.ToUpper(config.ApplicationName()),
	}
}
