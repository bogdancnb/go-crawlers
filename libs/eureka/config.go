package eureka

import (
	"github.com/spf13/viper"
	"strings"
)

type eurekaConfig struct {
	clientServiceUrl      string
	infoPath              string
	healthPath            string
	managementContextPath string
	metricsPath           string //todo
	renewalIntervalInSecs int
}

var cfg eurekaConfig

func initConfig() {
	viper.SetDefault("eureka.client.serviceUrl.defaultZone", "http://localhost:8761/eureka/")
	viper.SetDefault("eureka.instance.metadata-map.info.path", "/actuator/info")
	viper.SetDefault("eureka.instance.metadata-map.health.path", "/actuator/health")
	viper.SetDefault("eureka.instance.metadata-map.management.context-path", "/actuator")
	viper.SetDefault("eureka.instance.metadata-map.metrics.path", "/actuator/prometheus")
	viper.SetDefault("eureka.renewalIntervalInSecs", 30)
	cfg = eurekaConfig{
		clientServiceUrl:      strings.TrimSuffix(viper.GetString("eureka.client.serviceUrl.defaultZone"), "/"),
		infoPath:              viper.GetString("eureka.instance.metadata-map.info.path"),
		healthPath:            viper.GetString("eureka.instance.metadata-map.health.path"),
		managementContextPath: viper.GetString("eureka.instance.metadata-map.management.context-path"),
		metricsPath:           viper.GetString("eureka.instance.metadata-map.metrics.path"),
		renewalIntervalInSecs: viper.GetInt("eureka.renewalIntervalInSecs"),
	}
}
