package handlers

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/config"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type ActuatorHandler struct {
	*logrus.Entry
	refresh chan<- bool
}

func NewActuator(refresh chan<- bool) *ActuatorHandler {
	log := logging.Instance().WithField("pkg", "handlers")
	return &ActuatorHandler{
		log.WithField("type", "ActuatorHandler"),
		refresh,
	}
}

func (ah *ActuatorHandler) Base(w http.ResponseWriter, r *http.Request) {
	resp := actuatorResponse{
		Links: actuatorLinks{
			Self: actuatorLink{
				Href:      "",
				Templated: false,
			},
			Health: actuatorLink{
				Href:      "",
				Templated: false,
			},
			Info: actuatorLink{
				Href:      "",
				Templated: false,
			},
		},
	}
	err := json.NewEncoder(w).Encode(resp)
	if err != nil {
		ah.WithError(err).Error("encoding actuator response")
	}
}

func (ah *ActuatorHandler) Info(w http.ResponseWriter, r *http.Request) {
	err := utils.ToJSON(config.Debug(), w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (ah *ActuatorHandler) Health(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, "{\"status\":\"UP\"}")
}

func (ah *ActuatorHandler) Refresh(writer http.ResponseWriter, request *http.Request) {
	config.LoadCloud()

	//wait for refresh event main handler
	ah.refresh <- true

	ah.Info(writer, request)
}

type actuatorLink struct {
	Href      string `json:"href"`
	Templated bool   `json:"templated"`
}

type actuatorLinks struct {
	Self   actuatorLink `json:"self"`
	Health actuatorLink `json:"health"`
	Info   actuatorLink `json:"info"`
}

type actuatorResponse struct {
	Links actuatorLinks `json:"_links"`
}
