package logging

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"fmt"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
	"strings"
	"sync"
)

var once sync.Once
var logger *logrus.Logger

// Instance returns the logrus instance (it's recommended to make this a global instance called `log`)
func Instance() *logrus.Logger {
	once.Do(lazyInit)
	return logger
}

func lazyInit() {
	appflags.Parse()

	//it's recommended to make this a global instance called `log`
	logger = logrus.New()
	//if appflags.Localhost.Active() {
	logger.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:          true,
		TimestampFormat:        "2006-01-02 15:04:05.000",
		DisableLevelTruncation: false,
		PadLevelText:           true,
	})
	//} else {
	//	logger.SetFormatter(&logrus.JSONFormatter{
	//		TimestampFormat: "2006-01-02 15:04:05.000",
	//	})
	//}

	//logger.SetReportCaller(true) //do not use, affects performance
	logger.SetLevel(logrus.TraceLevel)
	if !appflags.Localhost.Active() && *appflags.LogFile != "" {
		host, err := utils.ResolveHostFromHostsFile()
		if err != nil {
			host, err = utils.ResolveIPFromHostsFile()
			if err != nil {
				logger.Fatal("could not resolve hostname/IP from /etc/hosts")
			}
		}
		file := fmt.Sprintf(*appflags.LogFile, host)
		logger.Debug("output file: ", file)
		logger.SetOutput(&lumberjack.Logger{
			Filename: file,
			//MaxSize:  100, // megabytes
			//MaxBackups: 30,  //the maximum number of old log files to retain.  The default is to retain all old log files
			MaxAge:   60,   //days
			Compress: true, // disabled by default
		})
	}
}

func SetLevel(lvl string) {
	switch strings.ToLower(lvl) {
	case "debug":
		logger.SetLevel(logrus.DebugLevel)
	case "info":
		logger.SetLevel(logrus.InfoLevel)
	case "warn":
		logger.SetLevel(logrus.WarnLevel)
	case "error":
		logger.SetLevel(logrus.ErrorLevel)
	default:
		logger.SetLevel(logrus.TraceLevel)
	}
}
