package appflags

import (
	"flag"
	"sync"
	"time"
)

var once sync.Once

//application flags
var (
	profile    = AppProfileFlag("profile", Localhost, "environment profile, one of the following:\n\"default\"\n\"localhost\"\n\"stage\"\n\"prelive\"\n\"production\"")
	ConfigPath = flag.String("configPath", "", "configuration file path, non empty")
	Memprofile = flag.String("memprofile", "", "write memory profile to `file`")
	LogFile    = flag.String("logFile", "", "log file path")
	LogLevel   = flag.String("logLevel", "", "log level")

	StopTimeoutSec = flag.Duration("stopTimeoutSeconds", 120*time.Second, "timeout for gracefully shutting down the server")

	ReadTimeoutSec  = flag.Duration("serverReadTimeoutSeconds", 60*time.Second, "max time to read request from the client")
	WriteTimeoutSec = flag.Duration("serverWriteTimeoutSeconds", 60*time.Second, "max time to write response to the client")
	IdleTimeoutSec  = flag.Duration("serverIdleTimeoutSeconds", 20*time.Second, "max time for connections using TCP Keep-Alive")

	Certs = flag.String("certs", "", "comma separated list of certificate files")
)

func Parse() {
	once.Do(func() {
		flag.Parse()
	})
}
