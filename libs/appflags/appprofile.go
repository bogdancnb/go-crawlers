package appflags

import (
	"flag"
	"fmt"
	"strings"
)

type AppProfile string

const (
	Default    AppProfile = "default"
	Localhost  AppProfile = "localhost"
	Stage      AppProfile = "stage"
	Prelive    AppProfile = "prelive"
	Production AppProfile = "production"
)

var validValue = map[string]AppProfile{
	"default":    Default,
	"localhost":  Localhost,
	"stage":      Stage,
	"prelive":    Prelive,
	"production": Production,
}

func (e AppProfile) String() string {
	return string(e)
}

func (e AppProfile) Active() bool {
	Parse()
	return e == *profile
}

type appProfileFlag struct {
	AppProfile
}

func (e *appProfileFlag) Set(s string) error {
	v, ok := validValue[strings.ToLower(s)]
	if !ok {
		return fmt.Errorf("not a valid value: %q", s)
	}
	e.AppProfile = v
	return nil
}

func AppProfileFlag(name string, value AppProfile, usage string) *AppProfile {
	f := &appProfileFlag{value}
	flag.CommandLine.Var(f, name, usage)
	return &f.AppProfile
}

func Profile() string {
	Parse()
	return profile.String()
}
