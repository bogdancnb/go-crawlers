package utils

import (
	"fmt"
	"io/ioutil"
	"net"
	"strconv"
	"strings"
)

// ResolveIPFromHostsFile reads the final IP address of the /etc/hosts file. Works for docker, typically at least...
func ResolveIPFromHostsFile() (string, error) {
	parts, err := etcHostsLine()
	if err != nil {
		return "", etcHostErr(err)
	}
	if len(parts) == 0 || parts[0] == "" {
		return "", fmt.Errorf("parsing /etc/hosts: no ip")
	}
	return parts[0], nil
}

// ResolveHostFromHostsFile reads the hostname from the /etc/hosts file. Works for docker, typically at least...
func ResolveHostFromHostsFile() (string, error) {
	parts, err := etcHostsLine()
	if err != nil {
		return "", etcHostErr(err)
	}
	if len(parts) < 2 || parts[1] == "" {
		return "", fmt.Errorf("parsing /etc/hosts: no hostname")
	}
	return parts[1], nil
}

func etcHostErr(err error) error {
	return fmt.Errorf("parsing /etc/hosts: " + err.Error())
}

func etcHostsLine() ([]string, error) {
	data, err := ioutil.ReadFile("/etc/hosts")
	if err != nil {
		return nil, fmt.Errorf("Problem reading /etc/hosts: " + err.Error())
	}

	lines := strings.Split(string(data), "\n")

	// Get last line
	line := lines[len(lines)-1]

	if len(line) < 2 {
		line = lines[len(lines)-2]
	}

	parts := strings.Split(line, "\t")
	return parts, nil
}

// GetIP returns the first non-loopback IP address
func GetIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "127.0.0.1", nil
}

func Port(address string) (int, error) {
	parts := strings.Split(address, ":")
	portPart := parts[len(parts)-1]
	port, err := strconv.Atoi(portPart)
	return port, err
}
