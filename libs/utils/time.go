package utils

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

var Formats = map[string]string{
	`dd.MM.yyyy`:          "02.01.2006",
	`dd.MM.yyyy mm:HH:ss`: "02.01.2006 15:04:05",
	`MM/dd/yyyy`:          "01/02/2006",
	`dd/MM/yyyy`:          "02/01/2006",
	`yy/MM`:               "06/01",
	`yyyy-MM-ddmm:HH:ss`:  "2006-01-0215:04:05",
	`yyyy-MM-ddTmm:HH:ss`: "2006-01-0215:04:05",
	`yyyy-MM-dd`:          "2006-01-02",
}

func Format(formatString string, timeString string) (*time.Time, error) {
	f, ok := Formats[formatString]
	if !ok {
		return nil, errors.New(`format not registered: ` + formatString)
	}
	t, err := time.Parse(f, timeString)
	if err == nil {
		return nil, errors.New(fmt.Sprintf(`parse error for string=%s with format=%s`, timeString, f))
	}
	return &t, nil
}

var ShortMonthsRO = map[string]string{
	"ianuarie":   "ian",
	"februarie":  "feb",
	"martie":     "mar",
	"aprilie":    "apr",
	"mai":        "mai",
	"iunie":      "iun",
	"iulie":      "iul",
	"august":     "aug",
	"septembrie": "sep",
	"octombrie":  "oct",
	"noiembrie":  "noi",
	"decembrie":  "dec",
}

var MonthsNrRO = map[string]string{
	"ian": "01",
	"feb": "02",
	"mar": "03",
	"apr": "04",
	"mai": "05",
	"iun": "06",
	"iul": "07",
	"aug": "08",
	"sep": "09",
	"oct": "10",
	"noi": "11",
	"dec": "12",
}

func Millis(t time.Time) int64 {
	return t.UnixNano() / int64(time.Millisecond)
}

func MillisString(t time.Time) string {
	timestamp := Millis(t)
	return fmt.Sprintf("%d", timestamp)
}

func FromMillisString(millis string) (*time.Time, error) {
	i, err := strconv.ParseInt(millis, 10, 64)
	if err != nil {
		return nil, err
	}
	unix := time.Unix(i/1000, 0)
	return &unix, nil
}

func FromMillis(millis int64) *time.Time {
	unix := time.Unix(millis/1000, 0)
	return &unix
}

func FirstOfMonth(t *time.Time) time.Time {
	currentYear, currentMonth, _ := t.Date()
	currentLocation := t.Location()
	firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
	return firstOfMonth
}

func LastOfMonth(t *time.Time) time.Time {
	firstOfMonth := FirstOfMonth(t)
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
	return lastOfMonth
}

func SleepMillis(min, max int) {
	rand.Seed(time.Now().Unix())
	r := rand.Float64()
	time.Sleep(time.Duration(float64(min)+r*float64(max-min)) * time.Millisecond)
}
