package utils

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
	"unicode/utf8"
)

var spaces = regexp.MustCompile(`\s+`)
var nonASCII = regexp.MustCompile("[[:^ascii:]]")

func RemoveNonASCII(s string) string {
	return nonASCII.ReplaceAllLiteralString(s, "")
}

func StringVal(p *string) string {
	if p == nil {
		return ""
	}
	return *p
}

func RemoveSpaces(selected string) string {
	selected = strings.TrimSpace(selected)
	return spaces.ReplaceAllString(selected, "")
}

//RemoveAdjacentUnicodeSpaces squashes each run of adjacent unicode spaces into a single ASCII space
func RemoveAdjacentUnicodeSpaces(slice []byte) []byte {
	first := true
	i := 0
	for i < len(slice) {
		r, size := utf8.DecodeRune(slice[i:])
		if unicode.IsSpace(r) {
			if first {
				first = false
				i += size
			} else {
				copy(slice[i:], slice[i+size:])
				slice = slice[:len(slice)-size]
			}
		} else {
			first = true
			i += size
		}
	}
	return slice[:i]
}

//RemoveUnicodeSpaces removes all runes for which unicode.IsSpace returns true
func RemoveUnicodeSpaces(slice []byte) string {
	out := make([]rune, len(slice))
	out = out[:0]
	runes := []rune(string(slice))
	for _, r := range runes {
		if !unicode.IsSpace(r) {
			out = append(out, r)
		}
	}
	return string(out)
}

// MatchRegex returns the match in matches[0][1]
func MatchRegex(text string, reg *regexp.Regexp) ([][]string, error) {
	matches := reg.FindAllStringSubmatch(text, -1)
	if len(matches) == 0 || len(matches[0]) < 2 {
		return nil, fmt.Errorf("could not match " + reg.String() + " from text: " + text)
	}
	return matches, nil
}

func MatchRegexGroups(text string, r *regexp.Regexp) map[string]string {
	result := make(map[string]string)
	match := r.FindStringSubmatch(text)
	for i, name := range r.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}
	return result
}
