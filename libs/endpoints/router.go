package endpoints

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/handlers"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
)

// NewRouter returns a Gorilla request router and dispatcher (https://github.com/gorilla/mux)
func NewRouter(routeGroupes []*RouteGroup, refresh chan<- bool) *mux.Router {
	log := logging.Instance().WithField("pkg", "endpoints")
	router := mux.NewRouter()

	//requestTracer
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			log.WithField("urlPath", r.URL.Path).Trace()
			//start := time.Now()
			//defer func() {
			//	log.WithField("urlPath", r.URL.Path).Trace("done in ", time.Since(start))
			//}()
			next.ServeHTTP(w, r)
		})
	})
	router.Use(handlers.JSONResponse)

	addPrefixMappings(router, actuator(refresh))
	for _, routeGroup := range routeGroupes {
		addPrefixMappings(router, routeGroup)
	}

	router.NotFoundHandler = http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		//log.Errorf("404: %s  %s", request.Method, request.URL.Path)
		http.Error(writer, "not found", 404)
	})

	err := router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		pathTemplate, err := route.GetPathTemplate()
		if err == nil {
			log.Trace("ROUTE:", pathTemplate)
		}
		pathRegexp, err := route.GetPathRegexp()
		if err == nil {
			log.Trace("Path regexp:", pathRegexp)
		}
		queriesTemplates, err := route.GetQueriesTemplates()
		if err == nil {
			log.Trace("Queries templates:", strings.Join(queriesTemplates, ","))
		}
		queriesRegexps, err := route.GetQueriesRegexp()
		if err == nil {
			log.Trace("Queries regexps:", strings.Join(queriesRegexps, ","))
		}
		methods, err := route.GetMethods()
		if err == nil {
			log.Trace("Methods:", strings.Join(methods, ","))
		}
		log.Trace("Handler: ", route.GetHandler())
		log.Traceln()
		return nil
	})
	if err != nil {
		log.Trace(err)
	}
	return router
}

func addPrefixMappings(router *mux.Router, routeGroup *RouteGroup) {
	subRouter := router.PathPrefix(routeGroup.Prefix).Subrouter()
	for _, appRoute := range routeGroup.List {
		subRouter.
			Path(appRoute.Pattern).
			Methods(appRoute.Method).
			HandlerFunc(appRoute.HandlerFunc)
	}
	if routeGroup.Validator != nil {
		subRouter.Use(routeGroup.Validator)
	}
}
