package endpoints

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/handlers"
	"net/http"
)

type Route struct {
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type RouteGroup struct {
	Prefix    string
	Validator func(handler http.Handler) http.Handler
	List      []*Route
}

var routeGroup RouteGroup

func actuator(refresh chan<- bool) *RouteGroup {
	actuatorHandler := handlers.NewActuator(refresh)
	routeGroup = RouteGroup{
		Prefix:    "/actuator",
		Validator: nil,
		List: []*Route{
			{
				Method:      http.MethodGet,
				Pattern:     "/",
				HandlerFunc: actuatorHandler.Base,
			},
			{
				Method:      http.MethodGet,
				Pattern:     "/info",
				HandlerFunc: actuatorHandler.Info,
			},
			{
				Method:      http.MethodGet,
				Pattern:     "/health",
				HandlerFunc: actuatorHandler.Health,
			},
			{
				Method:      http.MethodPost,
				Pattern:     "/refresh",
				HandlerFunc: actuatorHandler.Refresh,
			},
		},
	}
	return &routeGroup
}
