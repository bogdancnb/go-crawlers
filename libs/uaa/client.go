package uaa

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

type bearerTokenKey struct {
	oauthUrl     string
	grantType    string
	username     string
	password     string
	clientId     string
	clientSecret string
}

type bearerToken struct {
	http.Header
	createdAt time.Time
}

const tokenTimeout = 60 * time.Second

var once sync.Once
var anonKey bearerTokenKey
var secretKey bearerTokenKey

var tokensMutex sync.Mutex
var tokens = make(map[bearerTokenKey]*bearerToken)

var noAuthHeadersErr = errors.New("no auth headers received")

func Authenticate() (http.Header, error) {
	log := logging.Instance().WithField("pkg", "uaa")
	Init()

	token, err := token(log, anonKey)
	if err != nil {
		return nil, err
	}
	return token.Header, nil
}

func AuthenticateSecret() (http.Header, error) {
	log := logging.Instance().WithField("pkg", "uaa")
	Init()

	token, err := token(log, secretKey)
	if err != nil {
		return nil, err
	}
	return token.Header, nil
}

func Init() {
	once.Do(initAuthInfo)
}

func initAuthInfo() {
	viper.SetDefault("security.oauth2.client.accessTokenUri", "http://localhost:8181/uaa/oauth/token")
	viper.SetDefault("security.oauth2.client.grantType", "pago")
	viper.SetDefault("security.oauth2.client.username", "anonymous")
	anonKey = bearerTokenKey{
		oauthUrl:     viper.GetString("security.oauth2.client.accessTokenUri"),
		grantType:    viper.GetString("security.oauth2.client.grantType"),
		username:     viper.GetString("security.oauth2.client.username"),
		password:     "",
		clientId:     "pago-mobile-app",
		clientSecret: "pago-mobile-app-secret",
	}

	viper.SetDefault("secrets.accessTokenUri", "http://localhost:8181/uaa/oauth/token")
	viper.SetDefault("secrets.secretServiceUri", "http://localhost:9111/secret")
	secretKey = bearerTokenKey{
		oauthUrl:     viper.GetString("secrets.accessTokenUri"),
		grantType:    viper.GetString("security.oauth2.client.grantType"),
		username:     viper.GetString("secrets.user"),
		password:     viper.GetString("secrets.pass"),
		clientId:     "pago-mobile-app",
		clientSecret: "pago-mobile-app-secret",
	}
}

func token(log *logrus.Entry, key bearerTokenKey) (*bearerToken, error) {
	token, ok := tokens[key]
	if !ok || time.Since(token.createdAt) > tokenTimeout {
		tokensMutex.Lock()
		defer tokensMutex.Unlock()

		delete(tokens, key)
		headers := authenticate(log, key)
		if headers == nil {
			log.Error(noAuthHeadersErr)
			return nil, noAuthHeadersErr
		}
		tokens[key] = &bearerToken{headers, time.Now()}
		log.Trace(tokens[key])
		return tokens[key], nil
	}
	log.Trace("found cached token")
	return token, nil
}

func authenticate(log *logrus.Entry, key bearerTokenKey) http.Header {
	log.Trace("calling token endpoint")
	authBody := authBody(key)
	authHeaders := authHeaders(key)
	log.Trace("headers=", authHeaders, " body=", authBody)

	data := url.Values{}
	for k, v := range authBody {
		data.Set(k, v)
	}
	req, err := http.NewRequest(http.MethodPost, key.oauthUrl, strings.NewReader(data.Encode()))
	if err != nil {
		log.Errorf("could not create auth request: %v", err)
	}
	req.Header = authHeaders
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Errorf("could not make auth request: %v", err)
		return nil
	}
	defer res.Body.Close()
	var resBody map[string]string
	json.NewDecoder(res.Body).Decode(&resBody)

	return http.Header{
		"Authorization":   []string{"Bearer " + resBody["access_token"]},
		utils.ContentType: []string{"application/json"},
	}
}

func authHeaders(key bearerTokenKey) http.Header {
	base64 := base64.StdEncoding.EncodeToString([]byte(key.clientId + ":" + key.clientSecret))
	return http.Header{
		"Authorization":   []string{"Basic " + base64},
		utils.ContentType: []string{"application/x-www-form-urlencoded"},
	}
}

func authBody(key bearerTokenKey) map[string]string {
	body := map[string]string{
		"grant_type": key.grantType,
		"username":   key.username,
	}
	if key.password != "" {
		body["password"] = key.password
	}
	return body
}
