package uaa

import (
	"testing"
)

func TestAuthenticate(t *testing.T) {
	header, err := Authenticate()
	t.Log(header, err)
}

func TestAuthenticateSecret(t *testing.T) {
	header, err := AuthenticateSecret()
	t.Log(header, err)
}
