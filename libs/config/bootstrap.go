//config uses the Viper library to read properties (https://github.com/spf13/viper)
//
// Viper uses the following precedence order. Each item takes precedence over the item below it:
//
//    explicit call to Set
//    flag
//    env
//    config
//    key/value store
//    default
package config

import (
	. "bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

const interpolatePattern = `(?U).*(\${(.+)}).*`

var (
	applicationName string
	regex           = regexp.MustCompile(interpolatePattern)
)

func ApplicationName() string { return applicationName }

func Init() {
	Parse()
	logger := logging.Instance().WithField("pkg", "config")
	logging.SetLevel(*LogLevel)
	loadConfigFiles(logger)
	applicationName = viper.GetString("application.name")
	extrapolateConfigs()
	setDefaults()

	LoadCloud()
}

func setDefaults() {
	viper.SetDefault("certs.basepath", "/usr/local/app/")
}

func loadConfigFiles(log *logrus.Entry) {
	if *ConfigPath == "" {
		log.Fatal("path not set for configuration file")
	}
	log.Info("Starting application with profile ", Profile())
	viper.SetConfigType("yml")
	viper.AddConfigPath(*ConfigPath)

	//parse default.yml
	viper.SetConfigName(Default.String())
	err := viper.ReadInConfig()
	if err != nil {
		log.WithError(err).Fatal("could not read config file")
	}

	//parse profile.yml
	f, err := os.Open(filepath.Join(*ConfigPath, Profile()+".yml"))
	if err != nil {
		log.Warn("could not open config file for active profile ", Profile())
	}
	defer f.Close()
	err = viper.MergeConfig(f)
	if err != nil {
		log.WithError(err).Fatalf("could not parse config file %q", f.Name())
	}
}

func extrapolateConfigs() {
	for _, key := range viper.AllKeys() {
		val := viper.GetString(key)
		if val != "" {
			res, err := extrapolated(val)
			if err != nil {
				log.Fatalf("could not extrapolate key=%q, val=%q: %v", key, val, err)
			}
			if res != val {
				viper.Set(key, res)
			}
		}
	}
}

func Debug() map[string]interface{} {
	return viper.AllSettings()
}
