package config

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"bitbucket.org/bogdancnb/go-crawlers/libs/utils"
	"encoding/base64"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
)

const cloudConfig = "cloudConfig"

type springCloudConfigClient struct {
	Username string
	Password string
	Uri      string
}

type springCloudConfigResponse struct {
	Name            string           //`json:"name"`
	Profiles        []string         //`json:"profiles"`
	Label           string           //`json:"label"`
	Version         string           //`json:"version"`
	State           string           //`json:"state"`
	PropertySources []PropertySource //`json:"propertySources"`
}

type PropertySource struct {
	Name   string                 //`json:"name"`
	Source map[string]interface{} //`json:"source"`
}

var client *springCloudConfigClient

func LoadCloud() {
	logger := logging.Instance().WithField("pkg", "config")
	logger.Debug("fetching cloud config")
	if client == nil {
		buildCloudConfigClient(logger)
	}
	parseCloud(logger)
	extrapolateConfigs()
	_ = utils.ToJSON(Debug(), logger.Writer())
}

func parseCloud(log *logrus.Entry) {
	cloudRes := cloudRequest(log, client)
	//log.Trace("cloud config: ", cloudRes)

	defProps := fmt.Sprintf("%s-default", ApplicationName())
	profileProps := fmt.Sprintf("%s-%s", ApplicationName(), appflags.Profile())
	properties := make(map[string]map[string]interface{})

	parseProperties(log, cloudRes, defProps, profileProps, properties)
	//log.Traceln("parsed config: ")
	//viper.Debug()
}

func parseProperties(log *logrus.Entry, cloudRes *springCloudConfigResponse, defProps string, profileProps string, properties map[string]map[string]interface{}) {
	for _, p := range cloudRes.PropertySources {
		if p.Name == defProps || p.Name == profileProps {
			properties[p.Name] = p.Source
		}
	}
	if len(properties) == 0 {
		log.Fatalf("no properties found for profiles default or %s", appflags.Profile())
	}
	parsePropertySource(log, defProps, properties[defProps])
	parsePropertySource(log, profileProps, properties[profileProps])
}

func buildCloudConfigClient(log *logrus.Entry) {
	err := viper.Sub(cloudConfig).Unmarshal(&client)
	if err != nil {
		log.WithError(err).Error("parsing spring cloud config settings")
	}
}

func parsePropertySource(log *logrus.Entry, name string, propertySource map[string]interface{}) {
	err := viper.MergeConfigMap(propertySource)
	if err != nil {
		log.WithError(err).Fatalf("parsing properties for %q", name)
	}
}

func cloudRequest(log *logrus.Entry, client *springCloudConfigClient) *springCloudConfigResponse {
	auth := base64.StdEncoding.EncodeToString([]byte(client.Username + ":" + client.Password))
	uri := fmt.Sprintf("%s/%s/%s", client.Uri, ApplicationName(), appflags.Profile())
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		log.WithError(err).Fatal("could not create cloud config request")
	}
	req.Header.Add("Authorization", "Basic "+auth)
	req.Header.Add(utils.ContentType, "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.WithError(err).Fatal("cloud config request")
	}
	defer res.Body.Close()

	cloudRes := new(springCloudConfigResponse)
	err = utils.FromJSON(cloudRes, res.Body)
	if err != nil {
		log.WithError(err).Fatal("could not deserialize cloud config response")
	}
	return cloudRes
}
