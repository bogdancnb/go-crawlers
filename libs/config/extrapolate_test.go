package config

import (
	"github.com/spf13/viper"
	"strings"
	"testing"
)

var json = `
{
  "application.name": "pago-enel-crawler",
  "crawler.errorsleepmillis": "20000",
  "crawler.getproxy.url": "${pago.internalRouter}/proxy-rotating-service/{crawler}/get-proxy",
  "crawler.getwork.errorsleepmillis": "5000",
  "crawler.getwork.size": "20",
  "crawler.getwork.url": "${pago.internalRouter}/data-provider/{crawler}/get-work/?size={size}",
  "crawler.queuesize": "5",
  "crawler.senderror.url": "${pago.internalRouter}/data-provider/send-error",
  "crawler.sendpdfs.url": "${pago.internalRouter}/pago-invoice/send-pdfs",
  "crawler.sendwork.posurl": "http://stargate/pos_v1_1/account/send-work",
  "crawler.sendwork.url": "${pago.internalRouter}/data-provider/send-work",
  "crawler.threadcount": "1",
  "crawler.timeoutmillis": "60000",
  "crawler.uri": "enel.crawler",
  "crawler.worksleepmillis": "5000",
  "eureka.client.fetchregistry": "true",
  "eureka.client.registerwitheureka": "true",
  "eureka.client.serviceurl.defaultzone": "${pago.eurekaServer}/eureka/",
  "eureka.instance.metadata-map.health.path": "${management.context-path:/actuator}/${endpoints.health.id:health}",
  "eureka.instance.metadata-map.management.context-path": "${management.context-path:/actuator}",
  "eureka.instance.metadata-map.metrics.path": "${management.context-path:/actuator}/${endpoints.prometheus.id:prometheus}",
  "eureka.instance.prefer-ip-address": "false",
  "hystrix.command.default.execution.isolation.thread.timeoutinmilliseconds": "300000",
  "kafka.consumer.brokers": "localhost:9021",
  "kafka.consumer.topic": "crawler_get_work_enel",
  "kafka.producer.brokers": "localhost:9021",
  "management.endpoint.health.show-details": "when_authorized",
  "management.endpoint.prometheus.enabled": "true",
  "management.endpoints.web.exposure.include": "*",
  "management.metrics.export.prometheus.enabled": "true",
  "pago.edgeserver": "http://edge-service:9999",
  "pago.eurekaserver": "http://localhost:8761",
  "pago.internalrouter": "http://localhost:8888",
  "pago.server": "localhost",
  "pago.uaaserver": "http://localhost:8181",
  "ribbon.connecttimeout": "300000",
  "ribbon.maxautoretries": "5",
  "ribbon.maxautoretriesnextserver": "5",
  "ribbon.readtimeout": "300000",
  "security.oauth2.client.accesstokenuri": "${pago.uaaServer}/uaa/oauth/token",
  "security.oauth2.client.clientid": "swagger-client",
  "security.oauth2.client.clientsecret": "swagger-client-secret",
  "security.oauth2.client.userauthorizationuri": "${pago.uaaServer}/uaa/oauth/authorize",
  "security.oauth2.resource.userinfouri": "${pago.uaaServer}/uaa/user",
  "server.port": "0",
  "spring.cloud.config.allowoverride": "true",
  "spring.cloud.config.overridenone": "true",
  "spring.sleuth.sampler.percentage": "1.0",
  "spring.zipkin.baseurl": "http://zipkin:9411",
  "endpoints.health.id": "iamhealhy"
}
`
var _ = func() bool {
	testing.Init()
	return true
}()

func Test_extrapolated(t *testing.T) {
	tests := []struct {
		arg  string
		want string
	}{
		{
			arg:  "${pago.eurekaServer}/eureka/",
			want: "http://localhost:8761/eureka/",
		},
		{
			arg:  "${management.context-path:/actuator}/${endpoints.health.id:health}",
			want: "/actuator/iamhealhy",
		},
	}
	viper.SetConfigType("json")
	err := viper.ReadConfig(strings.NewReader(json))
	if err != nil {
		t.Fatal(err)
	}
	for _, tt := range tests {
		res, err := extrapolated(tt.arg)
		if err != nil {
			t.Fatal(err)
		}
		if res != tt.want {
			t.Errorf("mismatch got=%q, want=%q", res, tt.want)
		}
	}
}
