package config

import (
	"github.com/spf13/viper"
	"testing"
)

var _ = func() bool {
	testing.Init()
	return true
}()

func Test_parseProperties(t *testing.T) {
	type args struct {
		cloudRes     *springCloudConfigResponse
		defProps     string
		profileProps string
		properties   map[string]map[string]interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{
			name: "crawler.threadCount",
			args: args{
				cloudRes: &springCloudConfigResponse{
					Name:     "testResponse",
					Profiles: nil,
					Label:    "",
					Version:  "",
					State:    "",
					PropertySources: []PropertySource{
						{
							Name: "default",
							Source: map[string]interface{}{
								"crawler.threadCount": 10,
							},
						}, {
							Name: "localhost",
							Source: map[string]interface{}{
								"crawler.threadCount": -1,
							},
						},
					},
				},
				defProps:     "default",
				profileProps: "localhost",
				properties:   make(map[string]map[string]interface{}),
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		parseProperties(nil, tt.args.cloudRes, tt.args.defProps, tt.args.profileProps, tt.args.properties)
		got := viper.GetInt(tt.name)
		if got != tt.want {
			t.Error("failed")
		}
	}
}
