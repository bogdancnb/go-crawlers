package config

import (
	"fmt"
	"github.com/spf13/viper"
	"strings"
)

//	extrapolated replaces any occurrence of `${key}` inside the config value, using the value of `key` config, if set;
//	if no occurrence is found, returns the original value and nil error
//	if any matched `key` is not found in the configuration, it returns an empty string and non nil error
//	also, any other parse error is reported
func extrapolated(val string) (string, error) {
	matches := regex.FindAllStringSubmatch(val, -1)
	if len(matches) == 0 {
		return val, nil
	}

	res := val
	for _, match := range matches {
		if len(match) != 3 {
			return "", fmt.Errorf("property %q has invalid interpolation format", val)
		}
		placeholder := match[1]
		keyParts := strings.Split(match[2], ":")
		key := keyParts[0]
		defVal := ""
		if len(keyParts) > 1 {
			defVal = keyParts[1]
		}
		if !viper.IsSet(key) {
			if defVal == "" {
				return "", fmt.Errorf("property %q is not set", match[2])
			} else {
				res = strings.ReplaceAll(res, placeholder, defVal)
				continue
			}
		}
		keyVal := viper.GetString(key)
		resVal, err := extrapolated(keyVal)
		if err != nil {
			return "", fmt.Errorf("%q extrapolation: %v", keyVal, err)
		}
		res = strings.ReplaceAll(res, placeholder, resVal)
	}
	return res, nil
}
