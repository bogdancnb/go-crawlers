module bitbucket.org/bogdancnb/go-crawlers/libs

go 1.14

require (
	github.com/ArthurHlt/go-eureka-client v1.1.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.4.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
