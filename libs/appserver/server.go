package appserver

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"context"
	gorillactx "github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net"
	"sync"
	"syscall"
	"time"

	"net/http"
	"os"
	"os/signal"
)

type BeforeShutdownCall func()

func Start(router *mux.Router, shutdownCallsToWait []BeforeShutdownCall, netAddress chan<- string, shutdownBroadcast chan<- struct{}) (err error) {
	log := logging.Instance().WithField("pkg", "server")
	signalCtx := trapSignal(log)

	cors := handlers.CORS(handlers.AllowedOrigins([]string{"*"}))
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		log.Fatal("could not create net listener")
	}
	server := http.Server{
		Handler: cors(gorillactx.ClearHandler(router)), // set the default handler
		//ErrorLog:     sl,                // the logger for the server
		//todo set by profile:
		ReadTimeout:  *appflags.ReadTimeoutSec,  // max time to read request from the client
		WriteTimeout: *appflags.WriteTimeoutSec, // max time to write response to the client
		IdleTimeout:  *appflags.IdleTimeoutSec,  // max time for connections using TCP Keep-Alive
	}
	// start the server
	go func() {
		log.WithField("profile", appflags.Profile()).Info("Starting http server", " on ", listener.Addr().String())
		err := server.Serve(listener)
		if err != nil {
			if err != http.ErrServerClosed {
				log.Fatalf("Serve error (!= http.ErrServerClosed): %v", err)
			} else {
				log.Info(err)
			}
		}
	}()
	netAddress <- listener.Addr().String()

	<-signalCtx.Done()
	log.Warn("beginning shutdown")

	waitForCalls(shutdownCallsToWait)

	//broadcast shutdown for cleanup
	log.Warn("broadcasting shutdown")
	close(shutdownBroadcast)

	log.Warnf("gracefully shutdown the server, waiting max %v seconds for current operations to complete", *appflags.StopTimeoutSec)
	shutdownTimer := time.After(*appflags.StopTimeoutSec)
	<-shutdownTimer

	ctx, cancel := context.WithTimeout(context.Background(), *appflags.StopTimeoutSec)
	defer func() {
		cancel()
	}()
	err = server.Shutdown(ctx)
	if err != nil {
		log.WithError(err).Fatal("shutdown failed")
	}
	if err == http.ErrServerClosed {
		err = nil
	}
	return
}

func waitForCalls(shutdownCallsToWait []BeforeShutdownCall) {
	var wg sync.WaitGroup
	for _, call := range shutdownCallsToWait {
		wg.Add(1)
		go func(c BeforeShutdownCall) {
			defer wg.Done()
			c()
		}(call)
	}
	wg.Wait()
}

// trapSignal traps sigterm or interrupt and gracefully shutdown the server
func trapSignal(log *logrus.Entry) context.Context {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch,
		os.Interrupt,
		os.Kill,
		syscall.SIGTERM,
		syscall.SIGINT,
	)
	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		osCall := <-ch
		log.Infof("received OS signal %+v", osCall)
		cancel()
	}()

	return ctx
}
