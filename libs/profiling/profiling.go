package profiling

import (
	"bitbucket.org/bogdancnb/go-crawlers/libs/appflags"
	"bitbucket.org/bogdancnb/go-crawlers/libs/logging"
	"os"
	"runtime"
	"runtime/pprof"
)

func Heap() {
	log := logging.Instance().WithField("pkg", "profiling")
	if *appflags.Memprofile != "" {
		f, err := os.Create(*appflags.Memprofile)
		if err != nil {
			log.Error("could not create memory profile: ", err)
		}

		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Error("could not write memory profile: ", err)
		}

		err = f.Close()
		if err != nil {
			log.Error("could not close memory profile file")
		}
	}
}
