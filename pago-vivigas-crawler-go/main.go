package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/vivigas"
)

func main() {
	crawlerapp.Run(vivigas.Init)
}
