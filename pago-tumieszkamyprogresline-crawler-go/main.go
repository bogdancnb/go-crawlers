package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/tumieszkamyprogresline"
)

func main() {
	crawlerapp.Run(tumieszkamyprogresline.Init)
}
