package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/strategies/aGSMEnergia"
)

func main() {
	crawlerapp.Run(aGSMEnergia.Init)
}
