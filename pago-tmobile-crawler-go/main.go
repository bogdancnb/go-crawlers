package main

import (
	"bitbucket.org/bogdancnb/go-crawlers/libschromedp/strategies/tmobile"
	"bitbucket.org/bogdancnb/go-crawlers/libscrawler/crawlerapp"
)

func main() {
	crawlerapp.Run(tmobile.Init)
}
